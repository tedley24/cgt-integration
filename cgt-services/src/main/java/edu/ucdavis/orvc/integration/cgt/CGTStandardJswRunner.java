package edu.ucdavis.orvc.integration.cgt;

import org.tanukisoftware.wrapper.WrapperManager;

import edu.ucdavis.orvc.support.runtime.service.WrapperServiceRunnerBase;


public class CGTStandardJswRunner extends WrapperServiceRunnerBase {

	public CGTStandardJswRunner() {
		super();
		applicationConfigFiles.add("classpath:spring/cgt-app.xml");
		applicationConfigFiles.add("classpath:spring/cgt-hibernate-data.xml");
		applicationConfigFiles.add("classpath:spring/cgt-service.xml");
	}
	
	
	public static void main( String[] args )
    {
        // Start the application.  If the JVM was launched from the native
        //  Wrapper then the application will wait for the native Wrapper to
        //  call the application's start method.  Otherwise the start method
        //  will be called immediately.
        WrapperManager.start( new CGTStandardJswRunner(), args );
    }
	

}
