package edu.ucdavis.orvc.integration.cgt;

import edu.ucdavis.orvc.support.runtime.service.StandardServiceRunnerBase;

public class CGTStandardRunner extends StandardServiceRunnerBase  {

	
	static {
			APPLICATION_CONFIG_FILES.add("classpath:spring/cgt-app.xml");
			APPLICATION_CONFIG_FILES.add("classpath:spring/cgt-hibernate-data.xml");
			APPLICATION_CONFIG_FILES.add("classpath:spring/cgt-service-discoverable.xml");
	}
	
	
	
}
