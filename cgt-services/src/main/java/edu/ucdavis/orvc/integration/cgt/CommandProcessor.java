package edu.ucdavis.orvc.integration.cgt;

import java.util.List;

import asg.cliche.Command;
import asg.cliche.Shell;
import asg.cliche.ShellDependent;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtEfaTransactionProcessInfo;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtProject;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtTransaction;
import edu.ucdavis.orvc.integration.cgt.api.domain.service.CgtFeedTransactionInfoService;
import edu.ucdavis.orvc.integration.cgt.api.domain.service.CgtProjectInfoService;
import edu.ucdavis.orvc.integration.cgt.service.impl.CgaFeedTransactionInfoServiceImpl;


public class CommandProcessor implements ShellDependent {

	private Shell shell;
	
	protected CgtProjectInfoService cgtProjectInfoService;
	protected CgtFeedTransactionInfoService cgaFeedTransactionInfoService;
	
	public CommandProcessor(Shell shell) {
		super();
		this.shell = shell;
	}

	public CommandProcessor(Shell shell, CgtProjectInfoService cgtProjectInfoService,
			CgaFeedTransactionInfoServiceImpl cgaFeedTransactionInfoService) {
		super();
		this.shell = shell;
		this.cgtProjectInfoService = cgtProjectInfoService;
		this.cgaFeedTransactionInfoService = cgaFeedTransactionInfoService;
	}

	public CommandProcessor() {
		super();
	}

	/**
	 * @param projectNumber
	 * @return
	 * @see edu.ucdavis.orvc.integration.cgt.api.domain.service.CgtProjectInfoService#getEfaProcessInfo(java.lang.String)
	 */
	@Command
	public List<CgtEfaTransactionProcessInfo> getEfaProcessInfo(String projectNumber) {
		return cgtProjectInfoService.getEfaProcessInfo(projectNumber);
	}

	/**
	 * @param projectNumber
	 * @return
	 * @see edu.ucdavis.orvc.integration.cgt.api.domain.service.CgtProjectInfoService#getProject(java.lang.String)
	 */
	@Command
	public CgtProject getProject(String projectNumber) {
		return cgtProjectInfoService.getProject(projectNumber);
	}

	/**
	 * @param projectNumber
	 * @param active
	 * @return
	 * @see edu.ucdavis.orvc.integration.cgt.api.domain.service.CgtProjectInfoService#getProject(java.lang.String, boolean)
	 */
	@Command
	public CgtProject getProject(String projectNumber, boolean active) {
		return cgtProjectInfoService.getProject(projectNumber, active);
	}

	/**
	 * @param maxDaysOld
	 * @return
	 * @see edu.ucdavis.orvc.integration.cgt.service.impl.CgaFeedTransactionInfoServiceImpl#getUnprocessedTransactions(int)
	 */
	@Command
	public List<CgtTransaction> getUnprocessedTransactions(int maxDaysOld) {
		return cgaFeedTransactionInfoService.getUnprocessedTransactions(maxDaysOld);
	}


	
	@Override
	public void cliSetShell(Shell theShell) {
		this.shell = theShell;
	}

	/**
	 * @return the cgtProjectInfoService
	 */
	public CgtProjectInfoService getCgtProjectInfoService() {
		return cgtProjectInfoService;
	}

	/**
	 * @param cgtProjectInfoService the cgtProjectInfoService to set
	 */
	public void setCgtProjectInfoService(CgtProjectInfoService cgtProjectInfoService) {
		this.cgtProjectInfoService = cgtProjectInfoService;
	}

	/**
	 * @return the cgaFeedTransactionInfoService
	 */
	public CgtFeedTransactionInfoService getCgaFeedTransactionInfoService() {
		return cgaFeedTransactionInfoService;
	}

	/**
	 * @param cgaFeedTransactionInfoService the cgaFeedTransactionInfoService to set
	 */
	public void setCgaFeedTransactionInfoService(CgtFeedTransactionInfoService cgaFeedTransactionInfoService) {
		this.cgaFeedTransactionInfoService = cgaFeedTransactionInfoService;
	}
}
