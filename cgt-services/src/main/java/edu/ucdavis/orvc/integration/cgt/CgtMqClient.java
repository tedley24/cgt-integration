package edu.ucdavis.orvc.integration.cgt;

import edu.ucdavis.orvc.support.runtime.service.StandardCommandClientBase;

public class CgtMqClient extends StandardCommandClientBase {

	static {
		APPLICATION_CONFIG_FILES.add("classpath:spring/cgt-mq-client-discoverable-service.xml");
	}
	
	
}
