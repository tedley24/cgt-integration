<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:task="http://www.springframework.org/schema/task"
       xmlns:rabbit="http://www.springframework.org/schema/rabbit"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-3.1.xsd
		http://www.springframework.org/schema/rabbit http://www.springframework.org/schema/rabbit/spring-rabbit.xsd
		http://www.springframework.org/schema/task http://www.springframework.org/schema/task/spring-task-3.1.xsd">
	


	<!-- 
		These beans define data that will be stored in zookeeper.  These essentially help define
		a map between the service interface for our service and the routing key and othwer MQ data
		that clients need use to connect to the service. 
	 -->
	
	<bean id = "cgaFeedTransactionInfoServiceDiscoveryDef" class = "edu.ucdavis.orvc.integration.support.discovery.ORVCServiceSpec">
		<property name="rabbitExchangeName" value = "${client.message.server.exchange}"/>
		<property name="routingKey" value = "${cgt.mqServices.cgaFeedTransactionInfoService}"/>
	</bean>

	<bean id = "cgtProjectInfoServiceDiscoveryDef" class = "edu.ucdavis.orvc.integration.support.discovery.ORVCServiceSpec">
		<property name="rabbitExchangeName" value = "${client.message.server.exchange}"/>
		<property name="routingKey" value = "${cgt.mqServices.cgtProjectInfoService}"/>
	</bean>
	
	<!-- This bean holds all of the connection details a client needs to hook up to the 
		 MQ server we are binding to. The routing information for each service is defined
		 in the serviceSpecs map that maps the services interface to the beans defined above 
		 containing the routing key and exchange name.  The username and password set here
		 is the username and password that a client shoudl use and using these creds guarantees
		 that the client wil be able to connect and route as expected to use these services. 
		 
		 -->
	 
	<bean id="serviceSpecs" class = "edu.ucdavis.orvc.integration.support.discovery.ORVCServiceSpecs">
		<property name = "username" value = "${client.message.server.user}"/>
		<property name = "password" value = "${client.message.server.password}"/>
		<property name = "useSSL" value = "${client.message.server.ssl}"/>
		<property name = "virtualHost" value = "${client.message.server.virtual-host}"/>
		<property name = "suggestedHeartbeat" value = "${client.message.server.requestedHeartbeat}"/>
		
		<property name = "serviceSpecs">
			<map>
				<entry key="edu.ucdavis.orvc.integration.cgt.api.domain.service.CgtFeedTransactionInfoService" 
					value-ref = "cgaFeedTransactionInfoServiceDiscoveryDef"/>
				<entry key="edu.ucdavis.orvc.integration.cgt.api.domain.service.CgtProjectInfoService" 
					value-ref = "cgtProjectInfoServiceDiscoveryDef"/>
			</map>
		</property>
	</bean>
	
	<!-- This bean is the discovery server service.  Servers use this to publish their service details
		 to the zookeeper making it available to their clients.  The serviceName here is the name that our
		 details will be published as and the node will contain details for each mq service we are publishing 
		  -->
	
	<bean id = "cgtDiscoveryService" class = "edu.ucdavis.orvc.integration.support.discovery.impl.DiscoveryServerServiceExec" init-method="init">
		<property name = "serviceName" value = "${cgt.discoverable.serviceName}"/>
		<property name = "zkAddress" value = "${orvc.zk.serverAddress}"/>
		<property name = "zkPort" value = "${orvc.zk.serverPort}"/>
		<property name = "basePath" value = "${orvc.mqServices.zk.basePath}"/>
		<property name = "startServiceDiscoveryOnInit" value = "${cgt.discoverable.startZkDiscoveryOnBeanInit}"/>
		<property name = "serviceAddress" value = "${cgt.discoverable.serviceAddress}"/>
		<property name = "servicePort" value = "${cgt.discoverable.servicePort}"/>
		<property name = "serviceSpecs" ref = "serviceSpecs"/>
	</bean>
	
	<bean id = "cgtDiscoveryClientService" class = "edu.ucdavis.orvc.integration.support.discovery.impl.DiscoveryClientServiceExec" init-method="init">
		<property name = "startServiceDiscoveryOnInit" value = "${cgt.discoverable.startZkDiscoveryOnBeanInit}"/>
		<property name = "cacheServiceProvidersByName" value = "${cgt.discoverable.cacheServiceProvidersByName}"/>
		<property name = "serverServiceExec" ref = "cgtDiscoveryService"/>
	</bean>
	
	<bean id = "sslContextConnectionFactory" 
   		 class = "edu.ucdavis.orvc.core.integration.support.discovery.amqp.connection.UcdRabbitClientDiscoveryFactory" init-method="init" 
   		 depends-on="cgtDiscoveryService,cgtDiscoveryClientService">
		<property name = "discoverClientService" ref = "cgtDiscoveryClientService"/>
		<property name = "serviceName" value = "${cgt.discoverable.serviceName}"/>
	</bean>
	
	<bean id = "rabbitConnectionCustomThreadFactory" class = "org.springframework.scheduling.concurrent.CustomizableThreadFactory">
		<property name = "threadGroup">
			<bean class = "java.lang.ThreadGroup">
				<constructor-arg type = "java.lang.String" value = "cgt-rabbit-connection-group"/>
			</bean>
		</property>
		
		<property name = "threadNamePrefix" value = "cgt-rabbit-connection-factory-"/>
	</bean>
	
	<task:executor id = "rabbitConnectionTaskExec" keep-alive="600" queue-capacity="20" pool-size="5-30"/>
	
	<rabbit:connection-factory id="connectionFactory" connection-factory="sslContextConnectionFactory"
		thread-factory="rabbitConnectionCustomThreadFactory" executor="rabbitConnectionTaskExec"
		cache-mode="CONNECTION" connection-cache-size="10" connection-limit="30" connection-timeout="6000"
		 />
		 
	<rabbit:admin id = "rabbitAdmin"  connection-factory="connectionFactory"  />
	

	<!-- If the queues we want to bind to do not exist, then this will create them. -->
	<rabbit:queue name="${cgtServices.cgaFeedTransactionInfoService.name}" auto-declare="true" declared-by="rabbitAdmin"  />
	<rabbit:queue name="${cgtServices.cgtProjectInfoService.name}" auto-declare="true" declared-by="rabbitAdmin"/>
	
	
	
	<!-- Each of these exports a service that can be bound to a queue.  The -->
	<bean id="cgaFeedTransactionInfoServiceExporter" class="edu.ucdavis.orvc.core.integration.support.discovery.amqp.connection.UcdDiscoveryAmqpInvokerServiceExporter" >
		<property name="serviceInterface" value="edu.ucdavis.orvc.integration.cgt.api.domain.service.CgtFeedTransactionInfoService" />
		<property name="service" ref="cgtFeedTransactionInfoService" />
		<property name="discoverClientService" ref = "cgtDiscoveryClientService"/>
		
		<property name="amqpTemplate">
			<bean class = "edu.ucdavis.orvc.core.integration.support.discovery.amqp.connection.UcdDiscoveryRabbitTemplate" init-method="init">
				<property name = "connectionFactory" ref = "connectionFactory" />
				<property name = "discoverClientService" ref = "cgtDiscoveryClientService"/>
			</bean>
		</property>
	</bean>
	
	<bean id="cgtProjectInfoServiceExporter" class="edu.ucdavis.orvc.core.integration.support.discovery.amqp.connection.UcdDiscoveryAmqpInvokerServiceExporter" >
		<property name="serviceInterface" value="edu.ucdavis.orvc.integration.cgt.api.domain.service.CgtProjectInfoService" />
		<property name="service" ref="cgtProjectInfoService" />
		<property name="discoverClientService" ref = "cgtDiscoveryClientService"/>
		
		<property name="amqpTemplate">
			<bean class = "edu.ucdavis.orvc.core.integration.support.discovery.amqp.connection.UcdDiscoveryRabbitTemplate" init-method="init">
				<property name = "connectionFactory" ref = "connectionFactory" />
				<property name = "discoverClientService" ref = "cgtDiscoveryClientService"/>
			</bean>
		</property>
	</bean>

	<!-- binds the queues to our routing keys (will do nothing if the binding already exists) -->	
	<rabbit:topic-exchange name="${cgt.message.server.exchange}" durable="true" auto-delete="false" declared-by="rabbitAdmin" id="primaryTopicExchange" >
	   	<rabbit:bindings>
			<rabbit:binding queue="${cgtServices.cgaFeedTransactionInfoService.name}" pattern="${cgtServices.cgaFeedTransactionInfoService.routingKey}"/>
       		<rabbit:binding queue="${cgtServices.cgtProjectInfoService.name}" pattern="${cgtServices.cgtProjectInfoService.routingKey}"/>
	   	</rabbit:bindings>
	</rabbit:topic-exchange>
	<!-- End creation of queues and queue bindings. -->
	
	<!--  These next two beans define a task executor and listener container. The container 
		  is what processes messages and delivers them to the appropriate spring services based 
		  on the configuration.  The TaskExec is optional, but allows us to name the threads that 
		  the listener-container is creating. -->
	<bean id = "cgtTaskExec" class = "org.springframework.core.task.SimpleAsyncTaskExecutor">
		<property name = "threadGroupName" value = "${cgt.taskExec.threadGroupName}"/>
		<property name = "threadNamePrefix" value = "${cgt.taskExec.threadNamePrefix}"/>
		<property name = "concurrencyLimit" value = "${cgt.taskExec.concurrencyLimit}"/>
		<property name = "daemon" value = "true"/>
	</bean>
		
	<rabbit:listener-container connection-factory="connectionFactory" auto-startup="true" task-executor="cgtTaskExec"  
		max-concurrency="${cgt.rabbit.listener-container.max-concurrency}" 
		concurrency="${cgt.rabbit.listener-container.concurrency}"
		min-start-interval="${cgt.rabbit.listener-container.min-start-interval}"
		prefetch="${cgt.rabbit.listener-container.prefetch}"> 
    	<rabbit:listener ref="cgaFeedTransactionInfoServiceExporter" queue-names="${cgtServices.cgaFeedTransactionInfoService.name}"   />
    	<rabbit:listener ref="cgtProjectInfoServiceExporter" queue-names="${cgtServices.cgtProjectInfoService.name}"   />
   </rabbit:listener-container>
	
	
	<beans profile="enable-jmx">
		<bean id="mbeanServer" class="org.springframework.jmx.support.MBeanServerFactoryBean">
		</bean>
	
		<bean id="managementService" class="net.sf.ehcache.management.ManagementService" 
         		init-method="init"
         		destroy-method="dispose">
      			<constructor-arg ref="cacheManager"/>
      			<constructor-arg ref="mbeanServer"/>
      			<constructor-arg index="2" value="true"/>
      			<constructor-arg index="3" value="true"/>
      			<constructor-arg index="4" value="true"/>
      			<constructor-arg index="5" value="true"/>
		</bean>
	
	
		<bean id="exporter" class="org.springframework.jmx.export.MBeanExporter">
    		<property name="beans">
      			<map>
        			<entry key="bean:name=jsonRabbitTaskExecutor" value-ref="jsonTaskExec"/>
        			<entry key="bean:name=sslContextConnectionFactory" value-ref="sslContextConnectionFactory"/>
        			<entry key="bean:name=rabbitAdmin" value-ref="rabbitAdmin"/>
        			<entry key="bean:name=${cgtServices.cgtProjectInfoService.name}" 
        				value-ref="${cgtServices.cgtProjectInfoService.name}"/>
					<entry key="bean:name=${cgtServices.cgaFeedTransactionInfoService.name}" 
        				value-ref="${cgtServices.cgaFeedTransactionInfoService.name}"/>        			
        			<entry key="bean:name=topicExchange" value-ref="topicExchange"/>
       				<entry key="bean:name=cgaFeedTransactionInfoServiceListener" value-ref="cgaFeedTransactionInfoServiceListener"/>
        			<entry key="bean:name=cgtProjectInfoJsonServiceListener" value-ref="cgtProjectInfoServiceListener"/>
        			
       			</map>
    		</property>
    		<property name="server" ref="mbeanServer"/>
  		</bean>
		
		<beans profile="enable-json-services">
			
			<bean id="jsonJmxExporter" class="org.springframework.jmx.export.MBeanExporter">
    			<property name="beans">
      				<map>
        				<entry key="bean:name=rabbitTaskExecutor" value-ref="taskExec"/>
        				<entry key="bean:name=cgaFeedTransactionInfoService" value-ref="cgaFeedTransactionInfoService"/>
        				<entry key="bean:name=cgtProjectInfoJsonService" value-ref="cgtProjectInfoService"/>
        				<entry key="bean:name=${cgtServices.cgtProjectInfoService.json.name}" 
        				value-ref="${cgtServices.cgtProjectInfoService.json.name}"/>
						<entry key="bean:name=${cgtServices.cgaFeedTransactionInfoService.json.name}" 
        				value-ref="${cgtServices.cgaFeedTransactionInfoService.json.name}"/>        			
        				<entry key="bean:name=jsonTopicExchange" value-ref="jsonTopicExchange"/>
       					<entry key="bean:name=cgaFeedTransactionInfoServiceJsonListener" value-ref="cgaFeedTransactionInfoServiceJsonListener"/>
        				<entry key="bean:name=cgtProjectInfoJsonServiceJsonListener" value-ref="cgtProjectInfoServiceJsonListener"/>
        			
       				</map>
    			</property>
    			<property name="server" ref="mbeanServer"/>
  			</bean>
		
		</beans>
	
	</beans>

	 
	

	
</beans>
