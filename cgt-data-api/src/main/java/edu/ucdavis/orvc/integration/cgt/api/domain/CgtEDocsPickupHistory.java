package edu.ucdavis.orvc.integration.cgt.api.domain;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtEDocsPickupHistoryDTO;

public interface CgtEDocsPickupHistory extends CgtBaseInterface<CgtEDocsPickupHistoryDTO> { 

	CgtEDocsPickupHistoryId getId();

	void setId(CgtEDocsPickupHistoryId id);

	int getPicPickupCount();

	void setPicPickupCount(int picPickupCount);

	LocalDateTime getPicDateCreated();

	void setPicDateCreated(LocalDateTime picDateCreated);

	LocalDateTime getPicDateUpdated();

	void setPicDateUpdated(LocalDateTime picDateUpdated);

}