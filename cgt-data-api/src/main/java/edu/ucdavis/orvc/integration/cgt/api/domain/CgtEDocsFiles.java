package edu.ucdavis.orvc.integration.cgt.api.domain;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtEDocsFilesDTO;

public interface CgtEDocsFiles extends CgtBaseInterface<CgtEDocsFilesDTO> { 

	int getFilFileKey();

	void setFilFileKey(int filFileKey);

	String getFilOriginalFileName();

	void setFilOriginalFileName(String filOriginalFileName);

	String getFilRelativePath();

	void setFilRelativePath(String filRelativePath);

	String getFilFileName();

	void setFilFileName(String filFileName);

	String getFilFileExtension();

	void setFilFileExtension(String filFileExtension);

	String getFilExtension();

	void setFilExtension(String filExtension);

	String getFilMimeType();

	void setFilMimeType(String filMimeType);

	int getFilFileSize();

	void setFilFileSize(int filFileSize);

	String getFilFilePath();

	void setFilFilePath(String filFilePath);

	String getFilDescription();

	void setFilDescription(String filDescription);

	String getFilContentAsText();

	void setFilContentAsText(String filContentAsText);

	boolean isFilIsActive();

	void setFilIsActive(boolean filIsActive);

	LocalDateTime getFilDateCreated();

	void setFilDateCreated(LocalDateTime filDateCreated);

	LocalDateTime getFilDateUpdated();

	void setFilDateUpdated(LocalDateTime filDateUpdated);

	String getFilUpdatedBy();

	void setFilUpdatedBy(String filUpdatedBy);

}