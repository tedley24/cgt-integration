package edu.ucdavis.orvc.integration.cgt.api.domain.dto;

import java.math.BigDecimal;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtSubContracts;



/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/


public class CgtSubContractsDTO implements CgtSubContracts {

	private static final long serialVersionUID = 3204318843654865902L;

	private int sucSubContractKey;
	private String sucProjectKey;
	private String sucSubContractorKey;
	private String sucLegacyContractorName;
	private String sucAgreementNumber;
	private String sucPiLastName;
	private String sucPiFirstName;
	private String sucPiPhone;
	private String sucPiFax;
	private String sucPiEmail;
	private Integer sucSubContractorType;
	private String sucDuns;
	private LocalDate sucSubcontractStart;
	private LocalDate sucSubcontractEnd;
	private LocalDate sucDateSign;
	private LocalDate sucDateRenewal;
	private LocalDate sucDateMail;
	private BigDecimal sucAmount;
	private String sucNotes;
	private String sucConditions;
	private Boolean sucIsDelegated;
	private boolean sucIsActive;
	private String sucUpdatedBy;
	private LocalDateTime sucDateCreated;
	private LocalDateTime sucDateUpdated;

	public CgtSubContractsDTO(final CgtSubContracts fromObj) {



	}

	public CgtSubContractsDTO() {
	}

	public CgtSubContractsDTO(final int sucSubContractKey, final String sucProjectKey, final String sucSubContractorKey,
			final String sucAgreementNumber, final boolean sucIsActive, final String sucUpdatedBy, final LocalDateTime sucDateCreated,
			final LocalDateTime sucDateUpdated) {
		this.sucSubContractKey = sucSubContractKey;
		this.sucProjectKey = sucProjectKey;
		this.sucSubContractorKey = sucSubContractorKey;
		this.sucAgreementNumber = sucAgreementNumber;
		this.sucIsActive = sucIsActive;
		this.sucUpdatedBy = sucUpdatedBy;
		this.sucDateCreated = sucDateCreated;
		this.sucDateUpdated = sucDateUpdated;
	}

	public CgtSubContractsDTO(final int sucSubContractKey, final String sucProjectKey, final String sucSubContractorKey,
			final String sucLegacyContractorName, final String sucAgreementNumber, final String sucPiLastName, final String sucPiFirstName,
			final String sucPiPhone, final String sucPiFax, final String sucPiEmail, final Integer sucSubContractorType, final String sucDuns,
			final LocalDate sucSubcontractStart, final LocalDate sucSubcontractEnd, final LocalDate sucDateSign, final LocalDate sucDateRenewal, final LocalDate sucDateMail,
			final BigDecimal sucAmount, final String sucNotes, final String sucConditions, final Boolean sucIsDelegated, final boolean sucIsActive,
			final String sucUpdatedBy, final LocalDateTime sucDateCreated, final LocalDateTime sucDateUpdated) {
		this.sucSubContractKey = sucSubContractKey;
		this.sucProjectKey = sucProjectKey;
		this.sucSubContractorKey = sucSubContractorKey;
		this.sucLegacyContractorName = sucLegacyContractorName;
		this.sucAgreementNumber = sucAgreementNumber;
		this.sucPiLastName = sucPiLastName;
		this.sucPiFirstName = sucPiFirstName;
		this.sucPiPhone = sucPiPhone;
		this.sucPiFax = sucPiFax;
		this.sucPiEmail = sucPiEmail;
		this.sucSubContractorType = sucSubContractorType;
		this.sucDuns = sucDuns;
		this.sucSubcontractStart = sucSubcontractStart;
		this.sucSubcontractEnd = sucSubcontractEnd;
		this.sucDateSign = sucDateSign;
		this.sucDateRenewal = sucDateRenewal;
		this.sucDateMail = sucDateMail;
		this.sucAmount = sucAmount;
		this.sucNotes = sucNotes;
		this.sucConditions = sucConditions;
		this.sucIsDelegated = sucIsDelegated;
		this.sucIsActive = sucIsActive;
		this.sucUpdatedBy = sucUpdatedBy;
		this.sucDateCreated = sucDateCreated;
		this.sucDateUpdated = sucDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContracts#getSucSubContractKey()
	 */
	@Override
	
	
	public int getSucSubContractKey() {
		return this.sucSubContractKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContracts#setSucSubContractKey(int)
	 */
	@Override
	public void setSucSubContractKey(final int sucSubContractKey) {
		this.sucSubContractKey = sucSubContractKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContracts#getSucProjectKey()
	 */
	@Override
	
	public String getSucProjectKey() {
		return this.sucProjectKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContracts#setSucProjectKey(java.lang.String)
	 */
	@Override
	public void setSucProjectKey(final String sucProjectKey) {
		this.sucProjectKey = sucProjectKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContracts#getSucSubContractorKey()
	 */
	@Override
	
	public String getSucSubContractorKey() {
		return this.sucSubContractorKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContracts#setSucSubContractorKey(java.lang.String)
	 */
	@Override
	public void setSucSubContractorKey(final String sucSubContractorKey) {
		this.sucSubContractorKey = sucSubContractorKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContracts#getSucLegacyContractorName()
	 */
	@Override
	
	public String getSucLegacyContractorName() {
		return this.sucLegacyContractorName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContracts#setSucLegacyContractorName(java.lang.String)
	 */
	@Override
	public void setSucLegacyContractorName(final String sucLegacyContractorName) {
		this.sucLegacyContractorName = sucLegacyContractorName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContracts#getSucAgreementNumber()
	 */
	@Override
	
	public String getSucAgreementNumber() {
		return this.sucAgreementNumber;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContracts#setSucAgreementNumber(java.lang.String)
	 */
	@Override
	public void setSucAgreementNumber(final String sucAgreementNumber) {
		this.sucAgreementNumber = sucAgreementNumber;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContracts#getSucPiLastName()
	 */
	@Override
	
	public String getSucPiLastName() {
		return this.sucPiLastName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContracts#setSucPiLastName(java.lang.String)
	 */
	@Override
	public void setSucPiLastName(final String sucPiLastName) {
		this.sucPiLastName = sucPiLastName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContracts#getSucPiFirstName()
	 */
	@Override
	
	public String getSucPiFirstName() {
		return this.sucPiFirstName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContracts#setSucPiFirstName(java.lang.String)
	 */
	@Override
	public void setSucPiFirstName(final String sucPiFirstName) {
		this.sucPiFirstName = sucPiFirstName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContracts#getSucPiPhone()
	 */
	@Override
	
	public String getSucPiPhone() {
		return this.sucPiPhone;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContracts#setSucPiPhone(java.lang.String)
	 */
	@Override
	public void setSucPiPhone(final String sucPiPhone) {
		this.sucPiPhone = sucPiPhone;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContracts#getSucPiFax()
	 */
	@Override
	
	public String getSucPiFax() {
		return this.sucPiFax;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContracts#setSucPiFax(java.lang.String)
	 */
	@Override
	public void setSucPiFax(final String sucPiFax) {
		this.sucPiFax = sucPiFax;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContracts#getSucPiEmail()
	 */
	@Override
	
	public String getSucPiEmail() {
		return this.sucPiEmail;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContracts#setSucPiEmail(java.lang.String)
	 */
	@Override
	public void setSucPiEmail(final String sucPiEmail) {
		this.sucPiEmail = sucPiEmail;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContracts#getSucSubContractorType()
	 */
	@Override
	
	public Integer getSucSubContractorType() {
		return this.sucSubContractorType;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContracts#setSucSubContractorType(java.lang.Integer)
	 */
	@Override
	public void setSucSubContractorType(final Integer sucSubContractorType) {
		this.sucSubContractorType = sucSubContractorType;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContracts#getSucDuns()
	 */
	@Override
	
	public String getSucDuns() {
		return this.sucDuns;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContracts#setSucDuns(java.lang.String)
	 */
	@Override
	public void setSucDuns(final String sucDuns) {
		this.sucDuns = sucDuns;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContracts#getSucSubcontractStart()
	 */
	@Override
	
	
	public LocalDate getSucSubcontractStart() {
		return this.sucSubcontractStart;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContracts#setSucSubcontractStart(org.joda.time.LocalDate)
	 */
	@Override
	public void setSucSubcontractStart(final LocalDate sucSubcontractStart) {
		this.sucSubcontractStart = sucSubcontractStart;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContracts#getSucSubcontractEnd()
	 */
	@Override
	
	
	public LocalDate getSucSubcontractEnd() {
		return this.sucSubcontractEnd;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContracts#setSucSubcontractEnd(org.joda.time.LocalDate)
	 */
	@Override
	public void setSucSubcontractEnd(final LocalDate sucSubcontractEnd) {
		this.sucSubcontractEnd = sucSubcontractEnd;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContracts#getSucDateSign()
	 */
	@Override
	
	
	public LocalDate getSucDateSign() {
		return this.sucDateSign;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContracts#setSucDateSign(org.joda.time.LocalDate)
	 */
	@Override
	public void setSucDateSign(final LocalDate sucDateSign) {
		this.sucDateSign = sucDateSign;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContracts#getSucDateRenewal()
	 */
	@Override
	
	
	public LocalDate getSucDateRenewal() {
		return this.sucDateRenewal;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContracts#setSucDateRenewal(org.joda.time.LocalDate)
	 */
	@Override
	public void setSucDateRenewal(final LocalDate sucDateRenewal) {
		this.sucDateRenewal = sucDateRenewal;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContracts#getSucDateMail()
	 */
	@Override
	
	
	public LocalDate getSucDateMail() {
		return this.sucDateMail;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContracts#setSucDateMail(org.joda.time.LocalDate)
	 */
	@Override
	public void setSucDateMail(final LocalDate sucDateMail) {
		this.sucDateMail = sucDateMail;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContracts#getSucAmount()
	 */
	@Override
	
	public BigDecimal getSucAmount() {
		return this.sucAmount;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContracts#setSucAmount(java.math.BigDecimal)
	 */
	@Override
	public void setSucAmount(final BigDecimal sucAmount) {
		this.sucAmount = sucAmount;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContracts#getSucNotes()
	 */
	@Override
	
	public String getSucNotes() {
		return this.sucNotes;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContracts#setSucNotes(java.lang.String)
	 */
	@Override
	public void setSucNotes(final String sucNotes) {
		this.sucNotes = sucNotes;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContracts#getSucConditions()
	 */
	@Override
	
	public String getSucConditions() {
		return this.sucConditions;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContracts#setSucConditions(java.lang.String)
	 */
	@Override
	public void setSucConditions(final String sucConditions) {
		this.sucConditions = sucConditions;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContracts#getSucIsDelegated()
	 */
	@Override
	
	public Boolean getSucIsDelegated() {
		return this.sucIsDelegated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContracts#setSucIsDelegated(java.lang.Boolean)
	 */
	@Override
	public void setSucIsDelegated(final Boolean sucIsDelegated) {
		this.sucIsDelegated = sucIsDelegated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContracts#isSucIsActive()
	 */
	@Override
	
	public boolean isSucIsActive() {
		return this.sucIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContracts#setSucIsActive(boolean)
	 */
	@Override
	public void setSucIsActive(final boolean sucIsActive) {
		this.sucIsActive = sucIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContracts#getSucUpdatedBy()
	 */
	@Override
	
	public String getSucUpdatedBy() {
		return this.sucUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContracts#setSucUpdatedBy(java.lang.String)
	 */
	@Override
	public void setSucUpdatedBy(final String sucUpdatedBy) {
		this.sucUpdatedBy = sucUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContracts#getSucDateCreated()
	 */
	@Override
	
	
	public LocalDateTime getSucDateCreated() {
		return this.sucDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContracts#setSucDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setSucDateCreated(final LocalDateTime sucDateCreated) {
		this.sucDateCreated = sucDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContracts#getSucDateUpdated()
	 */
	@Override
	
	
	public LocalDateTime getSucDateUpdated() {
		return this.sucDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContracts#setSucDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setSucDateUpdated(final LocalDateTime sucDateUpdated) {
		this.sucDateUpdated = sucDateUpdated;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((sucAgreementNumber == null) ? 0 : sucAgreementNumber.hashCode());
		result = prime * result + ((sucAmount == null) ? 0 : sucAmount.hashCode());
		result = prime * result + ((sucConditions == null) ? 0 : sucConditions.hashCode());
		result = prime * result + ((sucDateCreated == null) ? 0 : sucDateCreated.hashCode());
		result = prime * result + ((sucDateMail == null) ? 0 : sucDateMail.hashCode());
		result = prime * result + ((sucDateRenewal == null) ? 0 : sucDateRenewal.hashCode());
		result = prime * result + ((sucDateSign == null) ? 0 : sucDateSign.hashCode());
		result = prime * result + ((sucDateUpdated == null) ? 0 : sucDateUpdated.hashCode());
		result = prime * result + ((sucDuns == null) ? 0 : sucDuns.hashCode());
		result = prime * result + (sucIsActive ? 1231 : 1237);
		result = prime * result + ((sucIsDelegated == null) ? 0 : sucIsDelegated.hashCode());
		result = prime * result + ((sucLegacyContractorName == null) ? 0 : sucLegacyContractorName.hashCode());
		result = prime * result + ((sucNotes == null) ? 0 : sucNotes.hashCode());
		result = prime * result + ((sucPiEmail == null) ? 0 : sucPiEmail.hashCode());
		result = prime * result + ((sucPiFax == null) ? 0 : sucPiFax.hashCode());
		result = prime * result + ((sucPiFirstName == null) ? 0 : sucPiFirstName.hashCode());
		result = prime * result + ((sucPiLastName == null) ? 0 : sucPiLastName.hashCode());
		result = prime * result + ((sucPiPhone == null) ? 0 : sucPiPhone.hashCode());
		result = prime * result + ((sucProjectKey == null) ? 0 : sucProjectKey.hashCode());
		result = prime * result + sucSubContractKey;
		result = prime * result + ((sucSubContractorKey == null) ? 0 : sucSubContractorKey.hashCode());
		result = prime * result + ((sucSubContractorType == null) ? 0 : sucSubContractorType.hashCode());
		result = prime * result + ((sucSubcontractEnd == null) ? 0 : sucSubcontractEnd.hashCode());
		result = prime * result + ((sucSubcontractStart == null) ? 0 : sucSubcontractStart.hashCode());
		result = prime * result + ((sucUpdatedBy == null) ? 0 : sucUpdatedBy.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof CgtSubContractsDTO))
			return false;
		final CgtSubContractsDTO other = (CgtSubContractsDTO) obj;
		if (sucAgreementNumber == null) {
			if (other.sucAgreementNumber != null)
				return false;
		} else if (!sucAgreementNumber.equals(other.sucAgreementNumber))
			return false;
		if (sucAmount == null) {
			if (other.sucAmount != null)
				return false;
		} else if (!sucAmount.equals(other.sucAmount))
			return false;
		if (sucConditions == null) {
			if (other.sucConditions != null)
				return false;
		} else if (!sucConditions.equals(other.sucConditions))
			return false;
		if (sucDateCreated == null) {
			if (other.sucDateCreated != null)
				return false;
		} else if (!sucDateCreated.equals(other.sucDateCreated))
			return false;
		if (sucDateMail == null) {
			if (other.sucDateMail != null)
				return false;
		} else if (!sucDateMail.equals(other.sucDateMail))
			return false;
		if (sucDateRenewal == null) {
			if (other.sucDateRenewal != null)
				return false;
		} else if (!sucDateRenewal.equals(other.sucDateRenewal))
			return false;
		if (sucDateSign == null) {
			if (other.sucDateSign != null)
				return false;
		} else if (!sucDateSign.equals(other.sucDateSign))
			return false;
		if (sucDateUpdated == null) {
			if (other.sucDateUpdated != null)
				return false;
		} else if (!sucDateUpdated.equals(other.sucDateUpdated))
			return false;
		if (sucDuns == null) {
			if (other.sucDuns != null)
				return false;
		} else if (!sucDuns.equals(other.sucDuns))
			return false;
		if (sucIsActive != other.sucIsActive)
			return false;
		if (sucIsDelegated == null) {
			if (other.sucIsDelegated != null)
				return false;
		} else if (!sucIsDelegated.equals(other.sucIsDelegated))
			return false;
		if (sucLegacyContractorName == null) {
			if (other.sucLegacyContractorName != null)
				return false;
		} else if (!sucLegacyContractorName.equals(other.sucLegacyContractorName))
			return false;
		if (sucNotes == null) {
			if (other.sucNotes != null)
				return false;
		} else if (!sucNotes.equals(other.sucNotes))
			return false;
		if (sucPiEmail == null) {
			if (other.sucPiEmail != null)
				return false;
		} else if (!sucPiEmail.equals(other.sucPiEmail))
			return false;
		if (sucPiFax == null) {
			if (other.sucPiFax != null)
				return false;
		} else if (!sucPiFax.equals(other.sucPiFax))
			return false;
		if (sucPiFirstName == null) {
			if (other.sucPiFirstName != null)
				return false;
		} else if (!sucPiFirstName.equals(other.sucPiFirstName))
			return false;
		if (sucPiLastName == null) {
			if (other.sucPiLastName != null)
				return false;
		} else if (!sucPiLastName.equals(other.sucPiLastName))
			return false;
		if (sucPiPhone == null) {
			if (other.sucPiPhone != null)
				return false;
		} else if (!sucPiPhone.equals(other.sucPiPhone))
			return false;
		if (sucProjectKey == null) {
			if (other.sucProjectKey != null)
				return false;
		} else if (!sucProjectKey.equals(other.sucProjectKey))
			return false;
		if (sucSubContractKey != other.sucSubContractKey)
			return false;
		if (sucSubContractorKey == null) {
			if (other.sucSubContractorKey != null)
				return false;
		} else if (!sucSubContractorKey.equals(other.sucSubContractorKey))
			return false;
		if (sucSubContractorType == null) {
			if (other.sucSubContractorType != null)
				return false;
		} else if (!sucSubContractorType.equals(other.sucSubContractorType))
			return false;
		if (sucSubcontractEnd == null) {
			if (other.sucSubcontractEnd != null)
				return false;
		} else if (!sucSubcontractEnd.equals(other.sucSubcontractEnd))
			return false;
		if (sucSubcontractStart == null) {
			if (other.sucSubcontractStart != null)
				return false;
		} else if (!sucSubcontractStart.equals(other.sucSubcontractStart))
			return false;
		if (sucUpdatedBy == null) {
			if (other.sucUpdatedBy != null)
				return false;
		} else if (!sucUpdatedBy.equals(other.sucUpdatedBy))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"CgtSubContracts [sucSubContractKey=%s, sucProjectKey=%s, sucSubContractorKey=%s, sucLegacyContractorName=%s, sucAgreementNumber=%s, sucPiLastName=%s, sucPiFirstName=%s, sucPiPhone=%s, sucPiFax=%s, sucPiEmail=%s, sucSubContractorType=%s, sucDuns=%s, sucSubcontractStart=%s, sucSubcontractEnd=%s, sucDateSign=%s, sucDateRenewal=%s, sucDateMail=%s, sucAmount=%s, sucNotes=%s, sucConditions=%s, sucIsDelegated=%s, sucIsActive=%s, sucUpdatedBy=%s, sucDateCreated=%s, sucDateUpdated=%s]",
				sucSubContractKey, sucProjectKey, sucSubContractorKey, sucLegacyContractorName, sucAgreementNumber,
				sucPiLastName, sucPiFirstName, sucPiPhone, sucPiFax, sucPiEmail, sucSubContractorType, sucDuns,
				sucSubcontractStart, sucSubcontractEnd, sucDateSign, sucDateRenewal, sucDateMail, sucAmount, sucNotes,
				sucConditions, sucIsDelegated, sucIsActive, sucUpdatedBy, sucDateCreated, sucDateUpdated);
	}

	public CgtSubContractsDTO toDTO() {
		return new CgtSubContractsDTO(this);
	}
}
