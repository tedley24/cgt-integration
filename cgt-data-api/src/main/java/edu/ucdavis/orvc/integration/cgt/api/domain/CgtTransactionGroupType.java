package edu.ucdavis.orvc.integration.cgt.api.domain;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtTransactionGroupTypeDTO;

public interface CgtTransactionGroupType  extends CgtBaseInterface<CgtTransactionGroupTypeDTO> {

	int getTgtTransactionGroupTypeKey();

	void setTgtTransactionGroupTypeKey(int tgtTransactionGroupTypeKey);

	int getTgtTransactionTypeKey();

	void setTgtTransactionTypeKey(int tgtTransactionTypeKey);

	String getTgtName();

	void setTgtName(String tgtName);

	String getTgtDisplayText();

	void setTgtDisplayText(String tgtDisplayText);

	boolean isTgtRequireSequenceNumber();

	void setTgtRequireSequenceNumber(boolean tgtRequireSequenceNumber);

}