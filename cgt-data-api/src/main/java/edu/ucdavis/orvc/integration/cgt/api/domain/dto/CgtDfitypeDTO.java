package edu.ucdavis.orvc.integration.cgt.api.domain.dto;


import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtDfitype;

/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/


public class CgtDfitypeDTO implements CgtDfitype {

	private static final long serialVersionUID = 7089320604167246511L;
	private int dftDfitypeKey;
	private String dftName;
	private String dftShortName;
	private Integer dftSortOrder;
	private boolean dftIsActive;
	private String dftUpdatedBy;
	private LocalDateTime dftDateCreated;
	private LocalDateTime dftDateUpdated;

	public CgtDfitypeDTO(final CgtDfitype fromObj) {



	}

	public CgtDfitypeDTO() {
	}

	public CgtDfitypeDTO(final int dftDfitypeKey, final String dftName, final boolean dftIsActive, final String dftUpdatedBy, final LocalDateTime dftDateCreated,
			final LocalDateTime dftDateUpdated) {
		this.dftDfitypeKey = dftDfitypeKey;
		this.dftName = dftName;
		this.dftIsActive = dftIsActive;
		this.dftUpdatedBy = dftUpdatedBy;
		this.dftDateCreated = dftDateCreated;
		this.dftDateUpdated = dftDateUpdated;
	}

	public CgtDfitypeDTO(final int dftDfitypeKey, final String dftName, final String dftShortName, final Integer dftSortOrder, final boolean dftIsActive,
			final String dftUpdatedBy, final LocalDateTime dftDateCreated, final LocalDateTime dftDateUpdated) {
		this.dftDfitypeKey = dftDfitypeKey;
		this.dftName = dftName;
		this.dftShortName = dftShortName;
		this.dftSortOrder = dftSortOrder;
		this.dftIsActive = dftIsActive;
		this.dftUpdatedBy = dftUpdatedBy;
		this.dftDateCreated = dftDateCreated;
		this.dftDateUpdated = dftDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDfitype#getDftDfitypeKey()
	 */
	@Override
	

	
	public int getDftDfitypeKey() {
		return this.dftDfitypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDfitype#setDftDfitypeKey(int)
	 */
	@Override
	public void setDftDfitypeKey(final int dftDfitypeKey) {
		this.dftDfitypeKey = dftDfitypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDfitype#getDftName()
	 */
	@Override
	
	public String getDftName() {
		return this.dftName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDfitype#setDftName(java.lang.String)
	 */
	@Override
	public void setDftName(final String dftName) {
		this.dftName = dftName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDfitype#getDftShortName()
	 */
	@Override
	
	public String getDftShortName() {
		return this.dftShortName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDfitype#setDftShortName(java.lang.String)
	 */
	@Override
	public void setDftShortName(final String dftShortName) {
		this.dftShortName = dftShortName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDfitype#getDftSortOrder()
	 */
	@Override
	
	public Integer getDftSortOrder() {
		return this.dftSortOrder;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDfitype#setDftSortOrder(java.lang.Integer)
	 */
	@Override
	public void setDftSortOrder(final Integer dftSortOrder) {
		this.dftSortOrder = dftSortOrder;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDfitype#isDftIsActive()
	 */
	@Override
	
	public boolean isDftIsActive() {
		return this.dftIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDfitype#setDftIsActive(boolean)
	 */
	@Override
	public void setDftIsActive(final boolean dftIsActive) {
		this.dftIsActive = dftIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDfitype#getDftUpdatedBy()
	 */
	@Override
	
	public String getDftUpdatedBy() {
		return this.dftUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDfitype#setDftUpdatedBy(java.lang.String)
	 */
	@Override
	public void setDftUpdatedBy(final String dftUpdatedBy) {
		this.dftUpdatedBy = dftUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDfitype#getDftDateCreated()
	 */
	@Override
	
	
	public LocalDateTime getDftDateCreated() {
		return this.dftDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDfitype#setDftDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setDftDateCreated(final LocalDateTime dftDateCreated) {
		this.dftDateCreated = dftDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDfitype#getDftDateUpdated()
	 */
	@Override
	
	
	public LocalDateTime getDftDateUpdated() {
		return this.dftDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDfitype#setDftDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setDftDateUpdated(final LocalDateTime dftDateUpdated) {
		this.dftDateUpdated = dftDateUpdated;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((dftDateCreated == null) ? 0 : dftDateCreated.hashCode());
		result = prime * result + ((dftDateUpdated == null) ? 0 : dftDateUpdated.hashCode());
		result = prime * result + dftDfitypeKey;
		result = prime * result + (dftIsActive ? 1231 : 1237);
		result = prime * result + ((dftName == null) ? 0 : dftName.hashCode());
		result = prime * result + ((dftShortName == null) ? 0 : dftShortName.hashCode());
		result = prime * result + ((dftSortOrder == null) ? 0 : dftSortOrder.hashCode());
		result = prime * result + ((dftUpdatedBy == null) ? 0 : dftUpdatedBy.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof CgtDfitypeDTO)) {
			return false;
		}
		CgtDfitypeDTO other = (CgtDfitypeDTO) obj;
		if (dftDateCreated == null) {
			if (other.dftDateCreated != null) {
				return false;
			}
		} else if (!dftDateCreated.equals(other.dftDateCreated)) {
			return false;
		}
		if (dftDateUpdated == null) {
			if (other.dftDateUpdated != null) {
				return false;
			}
		} else if (!dftDateUpdated.equals(other.dftDateUpdated)) {
			return false;
		}
		if (dftDfitypeKey != other.dftDfitypeKey) {
			return false;
		}
		if (dftIsActive != other.dftIsActive) {
			return false;
		}
		if (dftName == null) {
			if (other.dftName != null) {
				return false;
			}
		} else if (!dftName.equals(other.dftName)) {
			return false;
		}
		if (dftShortName == null) {
			if (other.dftShortName != null) {
				return false;
			}
		} else if (!dftShortName.equals(other.dftShortName)) {
			return false;
		}
		if (dftSortOrder == null) {
			if (other.dftSortOrder != null) {
				return false;
			}
		} else if (!dftSortOrder.equals(other.dftSortOrder)) {
			return false;
		}
		if (dftUpdatedBy == null) {
			if (other.dftUpdatedBy != null) {
				return false;
			}
		} else if (!dftUpdatedBy.equals(other.dftUpdatedBy)) {
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"CgtDfitypeImpl [dftDfitypeKey=%s, dftName=%s, dftShortName=%s, dftSortOrder=%s, dftIsActive=%s, dftUpdatedBy=%s, dftDateCreated=%s, dftDateUpdated=%s]",
				dftDfitypeKey, dftName, dftShortName, dftSortOrder, dftIsActive, dftUpdatedBy, dftDateCreated,
				dftDateUpdated);
	}

	public CgtDfitypeDTO toDTO() {
		return new CgtDfitypeDTO(this);
	}
}
