package edu.ucdavis.orvc.integration.cgt.api.domain.dto;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtEfaTransactionProcessInfo;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtTransaction;

public class CgtEfaTransactionProcessInfoDTO implements CgtEfaTransactionProcessInfo {

	private static final long serialVersionUID = 1L;

	private int ptrTransactionKey;
	private Integer ptrParentTransactionKey;
	private String ptrProjectKey;
	
	private String efadocumentNumber;
	private String efafailedReason;
	private LocalDateTime efadateProcessed;
	
	public CgtEfaTransactionProcessInfoDTO() {
		super();
	}
	
	public CgtEfaTransactionProcessInfoDTO(int ptrTransactionKey, Integer ptrParentTransactionKey, String ptrProjectKey,
			String efadocumentNumber, String efafailedReason, LocalDateTime efadateProcessed) {
		super();
		this.ptrTransactionKey = ptrTransactionKey;
		this.ptrParentTransactionKey = ptrParentTransactionKey;
		this.ptrProjectKey = ptrProjectKey;
		this.efadocumentNumber = efadocumentNumber;
		this.efafailedReason = efafailedReason;
		this.efadateProcessed = efadateProcessed;
	}

	public CgtEfaTransactionProcessInfoDTO(CgtTransaction transaction) {
		super();
		this.ptrTransactionKey = transaction.getPtrTransactionKey();
		this.ptrParentTransactionKey = transaction.getPtrParentTransactionKey();
		this.ptrProjectKey = transaction.getPtrProjectKey();
		this.efadocumentNumber = transaction.getPtrEfadocumentNumber();
		this.efafailedReason = transaction.getPtrEfafailedReason();
		this.efadateProcessed = transaction.getPtrEfadateProcessed();
	}

	public CgtEfaTransactionProcessInfoDTO(CgtEfaTransactionProcessInfo fromObj) {
		this.ptrTransactionKey = fromObj.getPtrTransactionKey();
		this.ptrParentTransactionKey = fromObj.getPtrParentTransactionKey();
		this.ptrProjectKey = fromObj.getPtrProjectKey();
		this.efadocumentNumber = fromObj.getEfadocumentNumber();
		this.efafailedReason = fromObj.getEfafailedReason();
		this.efadateProcessed = fromObj.getEfadateProcessed();
	}

	/**
	 * @return the efadocumentNumber
	 */
	/**
	 * @return
	 */
	@Override
	public String getEfadocumentNumber() {
		return efadocumentNumber;
	}
	/**
	 * @param efadocumentNumber the efadocumentNumber to set
	 */
	@Override
	public void setEfadocumentNumber(String efadocumentNumber) {
		this.efadocumentNumber = efadocumentNumber;
	}
	/**
	 * @return the efafailedReason
	 */
	@Override
	public String getEfafailedReason() {
		return efafailedReason;
	}
	/**
	 * @param efafailedReason the efafailedReason to set
	 */
	@Override
	public void setEfafailedReason(String efafailedReason) {
		this.efafailedReason = efafailedReason;
	}
	/**
	 * @return the efadateProcessed
	 */
	@Override
	public LocalDateTime getEfadateProcessed() {
		return efadateProcessed;
	}
	/**
	 * @param efadateProcessed the efadateProcessed to set
	 */
	@Override
	public void setEfadateProcessed(LocalDateTime efadateProcessed) {
		this.efadateProcessed = efadateProcessed;
	}
	/**
	 * @return the ptrTransactionKey
	 */
	@Override
	public int getPtrTransactionKey() {
		return ptrTransactionKey;
	}
	/**
	 * @param ptrTransactionKey the ptrTransactionKey to set
	 */
	@Override
	public void setPtrTransactionKey(int ptrTransactionKey) {
		this.ptrTransactionKey = ptrTransactionKey;
	}
	/**
	 * @return the ptrParentTransactionKey
	 */
	@Override
	public Integer getPtrParentTransactionKey() {
		return ptrParentTransactionKey;
	}
	/**
	 * @param ptrParentTransactionKey the ptrParentTransactionKey to set
	 */
	@Override
	public void setPtrParentTransactionKey(Integer ptrParentTransactionKey) {
		this.ptrParentTransactionKey = ptrParentTransactionKey;
	}
	/**
	 * @return the ptrProjectKey
	 */
	@Override
	public String getPtrProjectKey() {
		return ptrProjectKey;
	}
	/**
	 * @param ptrProjectKey the ptrProjectKey to set
	 */
	@Override
	public void setPtrProjectKey(String ptrProjectKey) {
		this.ptrProjectKey = ptrProjectKey;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((efadateProcessed == null) ? 0 : efadateProcessed.hashCode());
		result = prime * result + ((efadocumentNumber == null) ? 0 : efadocumentNumber.hashCode());
		result = prime * result + ((efafailedReason == null) ? 0 : efafailedReason.hashCode());
		result = prime * result + ((ptrParentTransactionKey == null) ? 0 : ptrParentTransactionKey.hashCode());
		result = prime * result + ((ptrProjectKey == null) ? 0 : ptrProjectKey.hashCode());
		result = prime * result + ptrTransactionKey;
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof CgtEfaTransactionProcessInfoDTO)) {
			return false;
		}
		CgtEfaTransactionProcessInfoDTO other = (CgtEfaTransactionProcessInfoDTO) obj;
		if (efadateProcessed == null) {
			if (other.efadateProcessed != null) {
				return false;
			}
		} else if (!efadateProcessed.equals(other.efadateProcessed)) {
			return false;
		}
		if (efadocumentNumber == null) {
			if (other.efadocumentNumber != null) {
				return false;
			}
		} else if (!efadocumentNumber.equals(other.efadocumentNumber)) {
			return false;
		}
		if (efafailedReason == null) {
			if (other.efafailedReason != null) {
				return false;
			}
		} else if (!efafailedReason.equals(other.efafailedReason)) {
			return false;
		}
		if (ptrParentTransactionKey == null) {
			if (other.ptrParentTransactionKey != null) {
				return false;
			}
		} else if (!ptrParentTransactionKey.equals(other.ptrParentTransactionKey)) {
			return false;
		}
		if (ptrProjectKey == null) {
			if (other.ptrProjectKey != null) {
				return false;
			}
		} else if (!ptrProjectKey.equals(other.ptrProjectKey)) {
			return false;
		}
		if (ptrTransactionKey != other.ptrTransactionKey) {
			return false;
		}
		return true;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"CgtEfaTransactionProcessInfoDTO [ptrTransactionKey=%s, ptrParentTransactionKey=%s, ptrProjectKey=%s, efadocumentNumber=%s, efafailedReason=%s, efadateProcessed=%s]",
				ptrTransactionKey, ptrParentTransactionKey, ptrProjectKey, efadocumentNumber, efafailedReason,
				efadateProcessed);
	}

	@Override
	public CgtEfaTransactionProcessInfoDTO toDTO() {
		return new CgtEfaTransactionProcessInfoDTO(this);
	}

	
	
	
}
