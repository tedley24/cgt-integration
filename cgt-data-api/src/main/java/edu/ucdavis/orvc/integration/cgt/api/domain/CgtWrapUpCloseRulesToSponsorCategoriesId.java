package edu.ucdavis.orvc.integration.cgt.api.domain;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtWrapUpCloseRulesToSponsorCategoriesIdDTO;

public interface CgtWrapUpCloseRulesToSponsorCategoriesId extends CgtBaseInterface<CgtWrapUpCloseRulesToSponsorCategoriesIdDTO> { 

	int getW2cCloseRuleKey();

	void setW2cCloseRuleKey(int w2cCloseRuleKey);

	String getW2cSponsorCategoryKey();

	void setW2cSponsorCategoryKey(String w2cSponsorCategoryKey);

}