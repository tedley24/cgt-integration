package edu.ucdavis.orvc.integration.cgt.api.domain;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtEDocsLinkDTO;

public interface CgtEDocsLink extends CgtBaseInterface<CgtEDocsLinkDTO> { 

	int getEdlEDocsLinkKey();

	void setEdlEDocsLinkKey(int edlEDocsLinkKey);

	int getEdlFileKey();

	void setEdlFileKey(int edlFileKey);

	String getEdlProjectKey();

	void setEdlProjectKey(String edlProjectKey);

	Integer getEdlTransactionKey();

	void setEdlTransactionKey(Integer edlTransactionKey);

	int getEdlEDocsTypeKey();

	void setEdlEDocsTypeKey(int edlEDocsTypeKey);

	int getEdlVersion();

	void setEdlVersion(int edlVersion);

	boolean isEdlIsActive();

	void setEdlIsActive(boolean edlIsActive);

	LocalDateTime getEdlDateCreated();

	void setEdlDateCreated(LocalDateTime edlDateCreated);

	LocalDateTime getEdlDateUpdated();

	void setEdlDateUpdated(LocalDateTime edlDateUpdated);

	String getEdlUpdatedBy();

	void setEdlUpdatedBy(String edlUpdatedBy);

}