package edu.ucdavis.orvc.integration.cgt.api.domain.service;

import java.util.List;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtEfaTransactionProcessInfo;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtProject;

public interface CgtProjectInfoService {

	List<CgtEfaTransactionProcessInfo> getEfaProcessInfo(String projectNumber);

	CgtProject getProject(String projectNumber);

	CgtProject getProject(String projectNumber, boolean active);

}