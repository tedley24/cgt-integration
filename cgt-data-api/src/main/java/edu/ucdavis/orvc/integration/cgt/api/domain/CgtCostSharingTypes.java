package edu.ucdavis.orvc.integration.cgt.api.domain;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtCostSharingTypesDTO;

public interface CgtCostSharingTypes extends CgtBaseInterface<CgtCostSharingTypesDTO> { 

	int getCstCostSharingTypeKey();

	void setCstCostSharingTypeKey(int cstCostSharingTypeKey);

	String getCstName();

	void setCstName(String cstName);

	String getCstShortName();

	void setCstShortName(String cstShortName);

	Integer getCstSortOrder();

	void setCstSortOrder(Integer cstSortOrder);

	boolean isCstIsActive();

	void setCstIsActive(boolean cstIsActive);

	String getCstUpdatedBy();

	void setCstUpdatedBy(String cstUpdatedBy);

	LocalDateTime getCstDateCreated();

	void setCstDateCreated(LocalDateTime cstDateCreated);

	LocalDateTime getCstDateUpdated();

	void setCstDateUpdated(LocalDateTime cstDateUpdated);

}