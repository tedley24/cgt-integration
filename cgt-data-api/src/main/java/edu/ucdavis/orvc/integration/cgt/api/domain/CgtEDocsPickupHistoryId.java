package edu.ucdavis.orvc.integration.cgt.api.domain;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtEDocsPickupHistoryIdDTO;

public interface CgtEDocsPickupHistoryId extends CgtBaseInterface<CgtEDocsPickupHistoryIdDTO> { 

	String getPicEDocKey();

	void setPicEDocKey(String picEDocKey);

	int getPicFileKey();

	void setPicFileKey(int picFileKey);

	String getPicIpAddress();

	void setPicIpAddress(String picIpAddress);

}