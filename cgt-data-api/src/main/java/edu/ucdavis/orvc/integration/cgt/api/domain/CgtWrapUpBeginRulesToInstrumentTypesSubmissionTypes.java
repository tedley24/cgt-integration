package edu.ucdavis.orvc.integration.cgt.api.domain;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtWrapUpBeginRulesToInstrumentTypesSubmissionTypesDTO;

public interface CgtWrapUpBeginRulesToInstrumentTypesSubmissionTypes extends CgtBaseInterface<CgtWrapUpBeginRulesToInstrumentTypesSubmissionTypesDTO> { 

	CgtWrapUpBeginRulesToInstrumentTypesSubmissionTypesId getId();

	void setId(CgtWrapUpBeginRulesToInstrumentTypesSubmissionTypesId id);

	Integer getW2iTransactionGroupTypeKey();

	void setW2iTransactionGroupTypeKey(Integer w2iTransactionGroupTypeKey);

	Integer getW2iNotTransactionGroupTypeKey();

	void setW2iNotTransactionGroupTypeKey(Integer w2iNotTransactionGroupTypeKey);

}