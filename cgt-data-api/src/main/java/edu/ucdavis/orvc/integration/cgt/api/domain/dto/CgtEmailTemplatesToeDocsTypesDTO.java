package edu.ucdavis.orvc.integration.cgt.api.domain.dto;


import edu.ucdavis.orvc.integration.cgt.api.domain.CgtEmailTemplatesToeDocsTypes;

/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/


public class CgtEmailTemplatesToeDocsTypesDTO implements CgtEmailTemplatesToeDocsTypes {

	private static final long serialVersionUID = -1235526281387128432L;
	private int e2dId;
	private Integer e2dEmailTemplateKey;
	private int e2dEDocsTypeKey;
	private boolean e2dIsRequired;
	private Integer e2dParentTransactionTypeKey;
	private Integer e2dParentTransactionGroupTypeKey;

	public CgtEmailTemplatesToeDocsTypesDTO(final CgtEmailTemplatesToeDocsTypes fromObj) {



	}

	public CgtEmailTemplatesToeDocsTypesDTO() {
	}

	public CgtEmailTemplatesToeDocsTypesDTO(final int e2dId, final int e2dEDocsTypeKey, final boolean e2dIsRequired) {
		this.e2dId = e2dId;
		this.e2dEDocsTypeKey = e2dEDocsTypeKey;
		this.e2dIsRequired = e2dIsRequired;
	}

	public CgtEmailTemplatesToeDocsTypesDTO(final int e2dId, final Integer e2dEmailTemplateKey, final int e2dEDocsTypeKey,
			final boolean e2dIsRequired, final Integer e2dParentTransactionTypeKey, final Integer e2dParentTransactionGroupTypeKey) {
		this.e2dId = e2dId;
		this.e2dEmailTemplateKey = e2dEmailTemplateKey;
		this.e2dEDocsTypeKey = e2dEDocsTypeKey;
		this.e2dIsRequired = e2dIsRequired;
		this.e2dParentTransactionTypeKey = e2dParentTransactionTypeKey;
		this.e2dParentTransactionGroupTypeKey = e2dParentTransactionGroupTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEmailTemplatesToeDocsTypes#getE2dId()
	 */
	@Override
	

	
	public int getE2dId() {
		return this.e2dId;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEmailTemplatesToeDocsTypes#setE2dId(int)
	 */
	@Override
	public void setE2dId(final int e2dId) {
		this.e2dId = e2dId;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEmailTemplatesToeDocsTypes#getE2dEmailTemplateKey()
	 */
	@Override
	
	public Integer getE2dEmailTemplateKey() {
		return this.e2dEmailTemplateKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEmailTemplatesToeDocsTypes#setE2dEmailTemplateKey(java.lang.Integer)
	 */
	@Override
	public void setE2dEmailTemplateKey(final Integer e2dEmailTemplateKey) {
		this.e2dEmailTemplateKey = e2dEmailTemplateKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEmailTemplatesToeDocsTypes#getE2dEDocsTypeKey()
	 */
	@Override
	
	public int getE2dEDocsTypeKey() {
		return this.e2dEDocsTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEmailTemplatesToeDocsTypes#setE2dEDocsTypeKey(int)
	 */
	@Override
	public void setE2dEDocsTypeKey(final int e2dEDocsTypeKey) {
		this.e2dEDocsTypeKey = e2dEDocsTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEmailTemplatesToeDocsTypes#isE2dIsRequired()
	 */
	@Override
	
	public boolean isE2dIsRequired() {
		return this.e2dIsRequired;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEmailTemplatesToeDocsTypes#setE2dIsRequired(boolean)
	 */
	@Override
	public void setE2dIsRequired(final boolean e2dIsRequired) {
		this.e2dIsRequired = e2dIsRequired;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEmailTemplatesToeDocsTypes#getE2dParentTransactionTypeKey()
	 */
	@Override
	
	public Integer getE2dParentTransactionTypeKey() {
		return this.e2dParentTransactionTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEmailTemplatesToeDocsTypes#setE2dParentTransactionTypeKey(java.lang.Integer)
	 */
	@Override
	public void setE2dParentTransactionTypeKey(final Integer e2dParentTransactionTypeKey) {
		this.e2dParentTransactionTypeKey = e2dParentTransactionTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEmailTemplatesToeDocsTypes#getE2dParentTransactionGroupTypeKey()
	 */
	@Override
	
	public Integer getE2dParentTransactionGroupTypeKey() {
		return this.e2dParentTransactionGroupTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEmailTemplatesToeDocsTypes#setE2dParentTransactionGroupTypeKey(java.lang.Integer)
	 */
	@Override
	public void setE2dParentTransactionGroupTypeKey(final Integer e2dParentTransactionGroupTypeKey) {
		this.e2dParentTransactionGroupTypeKey = e2dParentTransactionGroupTypeKey;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + e2dEDocsTypeKey;
		result = prime * result + ((e2dEmailTemplateKey == null) ? 0 : e2dEmailTemplateKey.hashCode());
		result = prime * result + e2dId;
		result = prime * result + (e2dIsRequired ? 1231 : 1237);
		result = prime * result
				+ ((e2dParentTransactionGroupTypeKey == null) ? 0 : e2dParentTransactionGroupTypeKey.hashCode());
		result = prime * result + ((e2dParentTransactionTypeKey == null) ? 0 : e2dParentTransactionTypeKey.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof CgtEmailTemplatesToeDocsTypesDTO)) {
			return false;
		}
		CgtEmailTemplatesToeDocsTypesDTO other = (CgtEmailTemplatesToeDocsTypesDTO) obj;
		if (e2dEDocsTypeKey != other.e2dEDocsTypeKey) {
			return false;
		}
		if (e2dEmailTemplateKey == null) {
			if (other.e2dEmailTemplateKey != null) {
				return false;
			}
		} else if (!e2dEmailTemplateKey.equals(other.e2dEmailTemplateKey)) {
			return false;
		}
		if (e2dId != other.e2dId) {
			return false;
		}
		if (e2dIsRequired != other.e2dIsRequired) {
			return false;
		}
		if (e2dParentTransactionGroupTypeKey == null) {
			if (other.e2dParentTransactionGroupTypeKey != null) {
				return false;
			}
		} else if (!e2dParentTransactionGroupTypeKey.equals(other.e2dParentTransactionGroupTypeKey)) {
			return false;
		}
		if (e2dParentTransactionTypeKey == null) {
			if (other.e2dParentTransactionTypeKey != null) {
				return false;
			}
		} else if (!e2dParentTransactionTypeKey.equals(other.e2dParentTransactionTypeKey)) {
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"CgtEmailTemplatesToeDocsTypesImpl [e2dId=%s, e2dEmailTemplateKey=%s, e2dEDocsTypeKey=%s, e2dIsRequired=%s, e2dParentTransactionTypeKey=%s, e2dParentTransactionGroupTypeKey=%s]",
				e2dId, e2dEmailTemplateKey, e2dEDocsTypeKey, e2dIsRequired, e2dParentTransactionTypeKey,
				e2dParentTransactionGroupTypeKey);
	}

	public CgtEmailTemplatesToeDocsTypesDTO toDTO() {
		return new CgtEmailTemplatesToeDocsTypesDTO(this);
	}
}
