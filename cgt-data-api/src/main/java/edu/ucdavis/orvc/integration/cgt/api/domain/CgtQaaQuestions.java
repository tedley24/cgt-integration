package edu.ucdavis.orvc.integration.cgt.api.domain;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtQaaQuestionsDTO;

public interface CgtQaaQuestions extends CgtBaseInterface<CgtQaaQuestionsDTO> { 

	int getQueQuestionKey();

	void setQueQuestionKey(int queQuestionKey);

	String getQueDataSourceKey();

	void setQueDataSourceKey(String queDataSourceKey);

	int getQueSectionKey();

	void setQueSectionKey(int queSectionKey);

	Integer getQueSortOrder();

	void setQueSortOrder(Integer queSortOrder);

	int getQueQuestionType();

	void setQueQuestionType(int queQuestionType);

	String getQueQuestion();

	void setQueQuestion(String queQuestion);

	String getQueAnswers();

	void setQueAnswers(String queAnswers);

	String getQueDefaultAnswer();

	void setQueDefaultAnswer(String queDefaultAnswer);

	boolean isQueIsAdditionalAnswer();

	void setQueIsAdditionalAnswer(boolean queIsAdditionalAnswer);

	boolean isQueIsActive();

	void setQueIsActive(boolean queIsActive);

	LocalDateTime getQueDateCreated();

	void setQueDateCreated(LocalDateTime queDateCreated);

	LocalDateTime getQueDateUpdated();

	void setQueDateUpdated(LocalDateTime queDateUpdated);

	String getQueUpdatedBy();

	void setQueUpdatedBy(String queUpdatedBy);

}