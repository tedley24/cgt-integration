package edu.ucdavis.orvc.integration.cgt.api.domain.dto;


import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtSubContractors;



/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/


public class CgtSubContractorsDTO implements CgtSubContractors {

	private static final long serialVersionUID = -1208683208000537449L;
	private String scoSubContractorKey;
	private String scoName;
	private Boolean scoIsActive;
	private String scoUpdatedBy;
	private LocalDateTime scoDateUpdated;
	private LocalDateTime scoDateCreated;

	public CgtSubContractorsDTO(final CgtSubContractors fromObj) {



	}

	public CgtSubContractorsDTO() {
	}

	public CgtSubContractorsDTO(final String scoSubContractorKey, final String scoName) {
		this.scoSubContractorKey = scoSubContractorKey;
		this.scoName = scoName;
	}

	public CgtSubContractorsDTO(final String scoSubContractorKey, final String scoName, final Boolean scoIsActive, final String scoUpdatedBy,
			final LocalDateTime scoDateUpdated, final LocalDateTime scoDateCreated) {
		this.scoSubContractorKey = scoSubContractorKey;
		this.scoName = scoName;
		this.scoIsActive = scoIsActive;
		this.scoUpdatedBy = scoUpdatedBy;
		this.scoDateUpdated = scoDateUpdated;
		this.scoDateCreated = scoDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContractors#getScoSubContractorKey()
	 */
	@Override
	

	
	public String getScoSubContractorKey() {
		return this.scoSubContractorKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContractors#setScoSubContractorKey(java.lang.String)
	 */
	@Override
	public void setScoSubContractorKey(final String scoSubContractorKey) {
		this.scoSubContractorKey = scoSubContractorKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContractors#getScoName()
	 */
	@Override
	
	public String getScoName() {
		return this.scoName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContractors#setScoName(java.lang.String)
	 */
	@Override
	public void setScoName(final String scoName) {
		this.scoName = scoName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContractors#getScoIsActive()
	 */
	@Override
	
	public Boolean getScoIsActive() {
		return this.scoIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContractors#setScoIsActive(java.lang.Boolean)
	 */
	@Override
	public void setScoIsActive(final Boolean scoIsActive) {
		this.scoIsActive = scoIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContractors#getScoUpdatedBy()
	 */
	@Override
	
	public String getScoUpdatedBy() {
		return this.scoUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContractors#setScoUpdatedBy(java.lang.String)
	 */
	@Override
	public void setScoUpdatedBy(final String scoUpdatedBy) {
		this.scoUpdatedBy = scoUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContractors#getScoDateUpdated()
	 */
	@Override
	
	
	public LocalDateTime getScoDateUpdated() {
		return this.scoDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContractors#setScoDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setScoDateUpdated(final LocalDateTime scoDateUpdated) {
		this.scoDateUpdated = scoDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContractors#getScoDateCreated()
	 */
	@Override
	
	
	public LocalDateTime getScoDateCreated() {
		return this.scoDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContractors#setScoDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setScoDateCreated(final LocalDateTime scoDateCreated) {
		this.scoDateCreated = scoDateCreated;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((scoDateCreated == null) ? 0 : scoDateCreated.hashCode());
		result = prime * result + ((scoDateUpdated == null) ? 0 : scoDateUpdated.hashCode());
		result = prime * result + ((scoIsActive == null) ? 0 : scoIsActive.hashCode());
		result = prime * result + ((scoName == null) ? 0 : scoName.hashCode());
		result = prime * result + ((scoSubContractorKey == null) ? 0 : scoSubContractorKey.hashCode());
		result = prime * result + ((scoUpdatedBy == null) ? 0 : scoUpdatedBy.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof CgtSubContractorsDTO)) {
			return false;
		}
		CgtSubContractorsDTO other = (CgtSubContractorsDTO) obj;
		if (scoDateCreated == null) {
			if (other.scoDateCreated != null) {
				return false;
			}
		} else if (!scoDateCreated.equals(other.scoDateCreated)) {
			return false;
		}
		if (scoDateUpdated == null) {
			if (other.scoDateUpdated != null) {
				return false;
			}
		} else if (!scoDateUpdated.equals(other.scoDateUpdated)) {
			return false;
		}
		if (scoIsActive == null) {
			if (other.scoIsActive != null) {
				return false;
			}
		} else if (!scoIsActive.equals(other.scoIsActive)) {
			return false;
		}
		if (scoName == null) {
			if (other.scoName != null) {
				return false;
			}
		} else if (!scoName.equals(other.scoName)) {
			return false;
		}
		if (scoSubContractorKey == null) {
			if (other.scoSubContractorKey != null) {
				return false;
			}
		} else if (!scoSubContractorKey.equals(other.scoSubContractorKey)) {
			return false;
		}
		if (scoUpdatedBy == null) {
			if (other.scoUpdatedBy != null) {
				return false;
			}
		} else if (!scoUpdatedBy.equals(other.scoUpdatedBy)) {
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"CgtSubContractorsImpl [scoSubContractorKey=%s, scoName=%s, scoIsActive=%s, scoUpdatedBy=%s, scoDateUpdated=%s, scoDateCreated=%s]",
				scoSubContractorKey, scoName, scoIsActive, scoUpdatedBy, scoDateUpdated, scoDateCreated);
	}

	public CgtSubContractorsDTO toDTO() {
		return new CgtSubContractorsDTO(this);
	}
}
