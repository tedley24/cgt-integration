package edu.ucdavis.orvc.integration.cgt.api.domain.dto;


import edu.ucdavis.orvc.integration.cgt.api.domain.CgtTransactionGroupTypeMisspellings;

/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/


public class CgtTransactionGroupTypeMisspellingsDTO implements CgtTransactionGroupTypeMisspellings {

	private static final long serialVersionUID = 5253269419712189332L;
	private int id;
	private int transactionTypeKey;
	private String misspelling;
	private int newTransactionGroupTypeKey;
	private String newTransactionGroupName;
	private Integer newTransactionGroupSequenceNumber;
	private String newTransactionGroupSubContractorKey;
	private boolean needsReview;

	public CgtTransactionGroupTypeMisspellingsDTO(final CgtTransactionGroupTypeMisspellings fromObj) {



	}

	public CgtTransactionGroupTypeMisspellingsDTO() {
	}

	public CgtTransactionGroupTypeMisspellingsDTO(final int id, final int transactionTypeKey, final String misspelling,
			final int newTransactionGroupTypeKey, final boolean needsReview) {
		this.id = id;
		this.transactionTypeKey = transactionTypeKey;
		this.misspelling = misspelling;
		this.newTransactionGroupTypeKey = newTransactionGroupTypeKey;
		this.needsReview = needsReview;
	}

	public CgtTransactionGroupTypeMisspellingsDTO(final int id, final int transactionTypeKey, final String misspelling,
			final int newTransactionGroupTypeKey, final String newTransactionGroupName, final Integer newTransactionGroupSequenceNumber,
			final String newTransactionGroupSubContractorKey, final boolean needsReview) {
		this.id = id;
		this.transactionTypeKey = transactionTypeKey;
		this.misspelling = misspelling;
		this.newTransactionGroupTypeKey = newTransactionGroupTypeKey;
		this.newTransactionGroupName = newTransactionGroupName;
		this.newTransactionGroupSequenceNumber = newTransactionGroupSequenceNumber;
		this.newTransactionGroupSubContractorKey = newTransactionGroupSubContractorKey;
		this.needsReview = needsReview;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionGroupTypeMisspellings#getId()
	 */
	@Override
	

	
	public int getId() {
		return this.id;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionGroupTypeMisspellings#setId(int)
	 */
	@Override
	public void setId(final int id) {
		this.id = id;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionGroupTypeMisspellings#getTransactionTypeKey()
	 */
	@Override
	
	public int getTransactionTypeKey() {
		return this.transactionTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionGroupTypeMisspellings#setTransactionTypeKey(int)
	 */
	@Override
	public void setTransactionTypeKey(final int transactionTypeKey) {
		this.transactionTypeKey = transactionTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionGroupTypeMisspellings#getMisspelling()
	 */
	@Override
	
	public String getMisspelling() {
		return this.misspelling;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionGroupTypeMisspellings#setMisspelling(java.lang.String)
	 */
	@Override
	public void setMisspelling(final String misspelling) {
		this.misspelling = misspelling;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionGroupTypeMisspellings#getNewTransactionGroupTypeKey()
	 */
	@Override
	
	public int getNewTransactionGroupTypeKey() {
		return this.newTransactionGroupTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionGroupTypeMisspellings#setNewTransactionGroupTypeKey(int)
	 */
	@Override
	public void setNewTransactionGroupTypeKey(final int newTransactionGroupTypeKey) {
		this.newTransactionGroupTypeKey = newTransactionGroupTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionGroupTypeMisspellings#getNewTransactionGroupName()
	 */
	@Override
	
	public String getNewTransactionGroupName() {
		return this.newTransactionGroupName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionGroupTypeMisspellings#setNewTransactionGroupName(java.lang.String)
	 */
	@Override
	public void setNewTransactionGroupName(final String newTransactionGroupName) {
		this.newTransactionGroupName = newTransactionGroupName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionGroupTypeMisspellings#getNewTransactionGroupSequenceNumber()
	 */
	@Override
	
	public Integer getNewTransactionGroupSequenceNumber() {
		return this.newTransactionGroupSequenceNumber;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionGroupTypeMisspellings#setNewTransactionGroupSequenceNumber(java.lang.Integer)
	 */
	@Override
	public void setNewTransactionGroupSequenceNumber(final Integer newTransactionGroupSequenceNumber) {
		this.newTransactionGroupSequenceNumber = newTransactionGroupSequenceNumber;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionGroupTypeMisspellings#getNewTransactionGroupSubContractorKey()
	 */
	@Override
	
	public String getNewTransactionGroupSubContractorKey() {
		return this.newTransactionGroupSubContractorKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionGroupTypeMisspellings#setNewTransactionGroupSubContractorKey(java.lang.String)
	 */
	@Override
	public void setNewTransactionGroupSubContractorKey(final String newTransactionGroupSubContractorKey) {
		this.newTransactionGroupSubContractorKey = newTransactionGroupSubContractorKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionGroupTypeMisspellings#isNeedsReview()
	 */
	@Override
	
	public boolean isNeedsReview() {
		return this.needsReview;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionGroupTypeMisspellings#setNeedsReview(boolean)
	 */
	@Override
	public void setNeedsReview(final boolean needsReview) {
		this.needsReview = needsReview;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + id;
		result = prime * result + ((misspelling == null) ? 0 : misspelling.hashCode());
		result = prime * result + (needsReview ? 1231 : 1237);
		result = prime * result + ((newTransactionGroupName == null) ? 0 : newTransactionGroupName.hashCode());
		result = prime * result
				+ ((newTransactionGroupSequenceNumber == null) ? 0 : newTransactionGroupSequenceNumber.hashCode());
		result = prime * result
				+ ((newTransactionGroupSubContractorKey == null) ? 0 : newTransactionGroupSubContractorKey.hashCode());
		result = prime * result + newTransactionGroupTypeKey;
		result = prime * result + transactionTypeKey;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof CgtTransactionGroupTypeMisspellingsDTO))
			return false;
		final CgtTransactionGroupTypeMisspellingsDTO other = (CgtTransactionGroupTypeMisspellingsDTO) obj;
		if (id != other.id)
			return false;
		if (misspelling == null) {
			if (other.misspelling != null)
				return false;
		} else if (!misspelling.equals(other.misspelling))
			return false;
		if (needsReview != other.needsReview)
			return false;
		if (newTransactionGroupName == null) {
			if (other.newTransactionGroupName != null)
				return false;
		} else if (!newTransactionGroupName.equals(other.newTransactionGroupName))
			return false;
		if (newTransactionGroupSequenceNumber == null) {
			if (other.newTransactionGroupSequenceNumber != null)
				return false;
		} else if (!newTransactionGroupSequenceNumber.equals(other.newTransactionGroupSequenceNumber))
			return false;
		if (newTransactionGroupSubContractorKey == null) {
			if (other.newTransactionGroupSubContractorKey != null)
				return false;
		} else if (!newTransactionGroupSubContractorKey.equals(other.newTransactionGroupSubContractorKey))
			return false;
		if (newTransactionGroupTypeKey != other.newTransactionGroupTypeKey)
			return false;
		if (transactionTypeKey != other.transactionTypeKey)
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"CgtTransactionGroupTypeMisspellings [id=%s, transactionTypeKey=%s, misspelling=%s, newTransactionGroupTypeKey=%s, newTransactionGroupName=%s, newTransactionGroupSequenceNumber=%s, newTransactionGroupSubContractorKey=%s, needsReview=%s]",
				id, transactionTypeKey, misspelling, newTransactionGroupTypeKey, newTransactionGroupName,
				newTransactionGroupSequenceNumber, newTransactionGroupSubContractorKey, needsReview);
	}

	public CgtTransactionGroupTypeMisspellingsDTO toDTO() {
		return new CgtTransactionGroupTypeMisspellingsDTO(this);
	}
}
