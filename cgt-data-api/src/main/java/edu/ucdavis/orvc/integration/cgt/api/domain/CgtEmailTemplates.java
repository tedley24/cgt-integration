package edu.ucdavis.orvc.integration.cgt.api.domain;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtEmailTemplatesDTO;

public interface CgtEmailTemplates extends CgtBaseInterface<CgtEmailTemplatesDTO> { 

	int getEmtEmailTemplateKey();

	void setEmtEmailTemplateKey(int emtEmailTemplateKey);

	int getEmtTransactionSubmissionTypeKey();

	void setEmtTransactionSubmissionTypeKey(int emtTransactionSubmissionTypeKey);

	int getEmtTransactionTypeKey();

	void setEmtTransactionTypeKey(int emtTransactionTypeKey);

	String getEmtName();

	void setEmtName(String emtName);

	String getEmtTo();

	void setEmtTo(String emtTo);

	String getEmtFrom();

	void setEmtFrom(String emtFrom);

	String getEmtCc();

	void setEmtCc(String emtCc);

	String getEmtOptionalCc();

	void setEmtOptionalCc(String emtOptionalCc);

	String getEmtBcc();

	void setEmtBcc(String emtBcc);

	String getEmtSubject();

	void setEmtSubject(String emtSubject);

	String getEmtBody();

	void setEmtBody(String emtBody);

	boolean isEmtAllowAttachments();

	void setEmtAllowAttachments(boolean emtAllowAttachments);

	boolean isEmtIsActive();

	void setEmtIsActive(boolean emtIsActive);

	String getEmtUpdatedBy();

	void setEmtUpdatedBy(String emtUpdatedBy);

	LocalDateTime getEmtDateUpdated();

	void setEmtDateUpdated(LocalDateTime emtDateUpdated);

	LocalDateTime getEmtDateCreated();

	void setEmtDateCreated(LocalDateTime emtDateCreated);

}