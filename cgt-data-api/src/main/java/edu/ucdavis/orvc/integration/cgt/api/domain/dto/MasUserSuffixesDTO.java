package edu.ucdavis.orvc.integration.cgt.api.domain.dto;


import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.MasUserSuffixes;



/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/


public class MasUserSuffixesDTO implements MasUserSuffixes {

	private static final long serialVersionUID = 7722207683871415924L;
	private int ussUsersuffixKey;
	private String ussName;
	private Boolean ussIsactive;
	private LocalDateTime ussDatecreated;
	private LocalDateTime ussDateupdated;
	private String ussUpdatedby;

	public MasUserSuffixesDTO(final MasUserSuffixes fromObj) {



	}

	public MasUserSuffixesDTO() {
	}

	public MasUserSuffixesDTO(final int ussUsersuffixKey) {
		this.ussUsersuffixKey = ussUsersuffixKey;
	}

	public MasUserSuffixesDTO(final int ussUsersuffixKey, final String ussName, final Boolean ussIsactive, final LocalDateTime ussDatecreated,
			final LocalDateTime ussDateupdated, final String ussUpdatedby) {
		this.ussUsersuffixKey = ussUsersuffixKey;
		this.ussName = ussName;
		this.ussIsactive = ussIsactive;
		this.ussDatecreated = ussDatecreated;
		this.ussDateupdated = ussDateupdated;
		this.ussUpdatedby = ussUpdatedby;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUserSuffixes#getUssUsersuffixKey()
	 */
	@Override
	

	
	public int getUssUsersuffixKey() {
		return this.ussUsersuffixKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUserSuffixes#setUssUsersuffixKey(int)
	 */
	@Override
	public void setUssUsersuffixKey(final int ussUsersuffixKey) {
		this.ussUsersuffixKey = ussUsersuffixKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUserSuffixes#getUssName()
	 */
	@Override
	
	public String getUssName() {
		return this.ussName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUserSuffixes#setUssName(java.lang.String)
	 */
	@Override
	public void setUssName(final String ussName) {
		this.ussName = ussName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUserSuffixes#getUssIsactive()
	 */
	@Override
	
	public Boolean getUssIsactive() {
		return this.ussIsactive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUserSuffixes#setUssIsactive(java.lang.Boolean)
	 */
	@Override
	public void setUssIsactive(final Boolean ussIsactive) {
		this.ussIsactive = ussIsactive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUserSuffixes#getUssDatecreated()
	 */
	@Override
	
	
	public LocalDateTime getUssDatecreated() {
		return this.ussDatecreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUserSuffixes#setUssDatecreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setUssDatecreated(final LocalDateTime ussDatecreated) {
		this.ussDatecreated = ussDatecreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUserSuffixes#getUssDateupdated()
	 */
	@Override
	
	
	public LocalDateTime getUssDateupdated() {
		return this.ussDateupdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUserSuffixes#setUssDateupdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setUssDateupdated(final LocalDateTime ussDateupdated) {
		this.ussDateupdated = ussDateupdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUserSuffixes#getUssUpdatedby()
	 */
	@Override
	
	public String getUssUpdatedby() {
		return this.ussUpdatedby;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUserSuffixes#setUssUpdatedby(java.lang.String)
	 */
	@Override
	public void setUssUpdatedby(final String ussUpdatedby) {
		this.ussUpdatedby = ussUpdatedby;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((ussDatecreated == null) ? 0 : ussDatecreated.hashCode());
		result = prime * result + ((ussDateupdated == null) ? 0 : ussDateupdated.hashCode());
		result = prime * result + ((ussIsactive == null) ? 0 : ussIsactive.hashCode());
		result = prime * result + ((ussName == null) ? 0 : ussName.hashCode());
		result = prime * result + ((ussUpdatedby == null) ? 0 : ussUpdatedby.hashCode());
		result = prime * result + ussUsersuffixKey;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof MasUserSuffixesDTO))
			return false;
		final MasUserSuffixesDTO other = (MasUserSuffixesDTO) obj;
		if (ussDatecreated == null) {
			if (other.ussDatecreated != null)
				return false;
		} else if (!ussDatecreated.equals(other.ussDatecreated))
			return false;
		if (ussDateupdated == null) {
			if (other.ussDateupdated != null)
				return false;
		} else if (!ussDateupdated.equals(other.ussDateupdated))
			return false;
		if (ussIsactive == null) {
			if (other.ussIsactive != null)
				return false;
		} else if (!ussIsactive.equals(other.ussIsactive))
			return false;
		if (ussName == null) {
			if (other.ussName != null)
				return false;
		} else if (!ussName.equals(other.ussName))
			return false;
		if (ussUpdatedby == null) {
			if (other.ussUpdatedby != null)
				return false;
		} else if (!ussUpdatedby.equals(other.ussUpdatedby))
			return false;
		if (ussUsersuffixKey != other.ussUsersuffixKey)
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"MasUserSuffixes [ussUsersuffixKey=%s, ussName=%s, ussIsactive=%s, ussDatecreated=%s, ussDateupdated=%s, ussUpdatedby=%s]",
				ussUsersuffixKey, ussName, ussIsactive, ussDatecreated, ussDateupdated, ussUpdatedby);
	}

	public MasUserSuffixesDTO toDTO() {
		return new MasUserSuffixesDTO(this);
	}
}
