package edu.ucdavis.orvc.integration.cgt.api.domain.dto;


import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtEDocsPickupHistory;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtEDocsPickupHistoryId;



/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/


public class CgtEDocsPickupHistoryDTO implements CgtEDocsPickupHistory {

	private static final long serialVersionUID = 2387450257702263059L;
	private CgtEDocsPickupHistoryIdDTO id;
	private int picPickupCount;
	private LocalDateTime picDateCreated;
	private LocalDateTime picDateUpdated;

	public CgtEDocsPickupHistoryDTO(final CgtEDocsPickupHistory fromObj) {



	}

	public CgtEDocsPickupHistoryDTO() {
	}

	public CgtEDocsPickupHistoryDTO(final CgtEDocsPickupHistoryIdDTO id, final int picPickupCount, final LocalDateTime picDateCreated,
			final LocalDateTime picDateUpdated) {
		this.id = id;
		this.picPickupCount = picPickupCount;
		this.picDateCreated = picDateCreated;
		this.picDateUpdated = picDateUpdated;
	}

	public CgtEDocsPickupHistoryIdDTO getIdImpl() {
		return this.id;
	}

	public void setIdImpl(final CgtEDocsPickupHistoryIdDTO id) {
		this.id = id;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsPickupHistory#getPicPickupCount()
	 */
	@Override
	
	public int getPicPickupCount() {
		return this.picPickupCount;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsPickupHistory#setPicPickupCount(int)
	 */
	@Override
	public void setPicPickupCount(final int picPickupCount) {
		this.picPickupCount = picPickupCount;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsPickupHistory#getPicDateCreated()
	 */
	@Override
	
	
	public LocalDateTime getPicDateCreated() {
		return this.picDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsPickupHistory#setPicDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setPicDateCreated(final LocalDateTime picDateCreated) {
		this.picDateCreated = picDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsPickupHistory#getPicDateUpdated()
	 */
	@Override
	
	
	public LocalDateTime getPicDateUpdated() {
		return this.picDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsPickupHistory#setPicDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setPicDateUpdated(final LocalDateTime picDateUpdated) {
		this.picDateUpdated = picDateUpdated;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((picDateCreated == null) ? 0 : picDateCreated.hashCode());
		result = prime * result + ((picDateUpdated == null) ? 0 : picDateUpdated.hashCode());
		result = prime * result + picPickupCount;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof CgtEDocsPickupHistoryDTO)) {
			return false;
		}
		CgtEDocsPickupHistoryDTO other = (CgtEDocsPickupHistoryDTO) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (picDateCreated == null) {
			if (other.picDateCreated != null) {
				return false;
			}
		} else if (!picDateCreated.equals(other.picDateCreated)) {
			return false;
		}
		if (picDateUpdated == null) {
			if (other.picDateUpdated != null) {
				return false;
			}
		} else if (!picDateUpdated.equals(other.picDateUpdated)) {
			return false;
		}
		if (picPickupCount != other.picPickupCount) {
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"CgtEDocsPickupHistoryImpl [id=%s, picPickupCount=%s, picDateCreated=%s, picDateUpdated=%s]", id,
				picPickupCount, picDateCreated, picDateUpdated);
	}

	@Override
	public void setId(CgtEDocsPickupHistoryId id) {
		this.id = (CgtEDocsPickupHistoryIdDTO) id;
	}

	@Override
	
	public CgtEDocsPickupHistoryId getId() {
		return id;
	}

	public CgtEDocsPickupHistoryDTO toDTO() {
		return new CgtEDocsPickupHistoryDTO(this);
	}
}
