package edu.ucdavis.orvc.integration.cgt.api.domain;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtEDocsWordsDTO;

public interface CgtEDocsWords extends CgtBaseInterface<CgtEDocsWordsDTO> { 

	int getWrdId();

	void setWrdId(int wrdId);

	int getWrdFileKey();

	void setWrdFileKey(int wrdFileKey);

	String getWrdWord();

	void setWrdWord(String wrdWord);

}