package edu.ucdavis.orvc.integration.cgt.api.domain;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtDeliverableFormsDTO;

public interface CgtDeliverableForms extends CgtBaseInterface<CgtDeliverableFormsDTO> {

	int getDefDeliverableFormKey();

	void setDefDeliverableFormKey(int defDeliverableFormKey);

	String getDefName();

	void setDefName(String defName);

	String getDefShortName();

	void setDefShortName(String defShortName);

	Integer getDefSortOrder();

	void setDefSortOrder(Integer defSortOrder);

	Boolean getDefIsActive();

	void setDefIsActive(Boolean defIsActive);

	String getDefUpdatedBy();

	void setDefUpdatedBy(String defUpdatedBy);

	LocalDateTime getDefDateUpdated();

	void setDefDateUpdated(LocalDateTime defDateUpdated);

	LocalDateTime getDefDateCreated();

	void setDefDateCreated(LocalDateTime defDateCreated);

}