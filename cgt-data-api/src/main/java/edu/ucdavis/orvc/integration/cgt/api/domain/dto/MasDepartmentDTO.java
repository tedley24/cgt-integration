package edu.ucdavis.orvc.integration.cgt.api.domain.dto;


import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.MasDepartment;



/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/


public class MasDepartmentDTO implements MasDepartment {

	private static final long serialVersionUID = 2018962174695459936L;
	private String depDepartmentKey;
	private String depBoucCode;
	private String depName;
	private Boolean depIsHide;
	private Character depUsedIndicator;
	private boolean depPrimaryIndicator;
	private String depDataSourceKey;
	private String depNotes;
	private boolean depIsActive;
	private LocalDateTime depDateCreated;
	private LocalDateTime depDateUpdated;
	private String depUpdatedBy;

	public MasDepartmentDTO(final MasDepartment fromObj) {



	}

	public MasDepartmentDTO() {
	}

	public MasDepartmentDTO(final String depDepartmentKey, final String depName, final boolean depPrimaryIndicator, final String depDataSourceKey,
			final boolean depIsActive, final LocalDateTime depDateCreated, final LocalDateTime depDateUpdated, final String depUpdatedBy) {
		this.depDepartmentKey = depDepartmentKey;
		this.depName = depName;
		this.depPrimaryIndicator = depPrimaryIndicator;
		this.depDataSourceKey = depDataSourceKey;
		this.depIsActive = depIsActive;
		this.depDateCreated = depDateCreated;
		this.depDateUpdated = depDateUpdated;
		this.depUpdatedBy = depUpdatedBy;
	}

	public MasDepartmentDTO(final String depDepartmentKey, final String depBoucCode, final String depName, final Boolean depIsHide,
			final Character depUsedIndicator, final boolean depPrimaryIndicator, final String depDataSourceKey, final String depNotes,
			final boolean depIsActive, final LocalDateTime depDateCreated, final LocalDateTime depDateUpdated, final String depUpdatedBy) {
		this.depDepartmentKey = depDepartmentKey;
		this.depBoucCode = depBoucCode;
		this.depName = depName;
		this.depIsHide = depIsHide;
		this.depUsedIndicator = depUsedIndicator;
		this.depPrimaryIndicator = depPrimaryIndicator;
		this.depDataSourceKey = depDataSourceKey;
		this.depNotes = depNotes;
		this.depIsActive = depIsActive;
		this.depDateCreated = depDateCreated;
		this.depDateUpdated = depDateUpdated;
		this.depUpdatedBy = depUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasDepartment#getDepDepartmentKey()
	 */
	@Override
	

	
	public String getDepDepartmentKey() {
		return this.depDepartmentKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasDepartment#setDepDepartmentKey(java.lang.String)
	 */
	@Override
	public void setDepDepartmentKey(final String depDepartmentKey) {
		this.depDepartmentKey = depDepartmentKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasDepartment#getDepBoucCode()
	 */
	@Override
	
	public String getDepBoucCode() {
		return this.depBoucCode;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasDepartment#setDepBoucCode(java.lang.String)
	 */
	@Override
	public void setDepBoucCode(final String depBoucCode) {
		this.depBoucCode = depBoucCode;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasDepartment#getDepName()
	 */
	@Override
	
	public String getDepName() {
		return this.depName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasDepartment#setDepName(java.lang.String)
	 */
	@Override
	public void setDepName(final String depName) {
		this.depName = depName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasDepartment#getDepIsHide()
	 */
	@Override
	
	public Boolean getDepIsHide() {
		return this.depIsHide;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasDepartment#setDepIsHide(java.lang.Boolean)
	 */
	@Override
	public void setDepIsHide(final Boolean depIsHide) {
		this.depIsHide = depIsHide;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasDepartment#getDepUsedIndicator()
	 */
	@Override
	
	public Character getDepUsedIndicator() {
		return this.depUsedIndicator;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasDepartment#setDepUsedIndicator(java.lang.Character)
	 */
	@Override
	public void setDepUsedIndicator(final Character depUsedIndicator) {
		this.depUsedIndicator = depUsedIndicator;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasDepartment#isDepPrimaryIndicator()
	 */
	@Override
	
	public boolean isDepPrimaryIndicator() {
		return this.depPrimaryIndicator;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasDepartment#setDepPrimaryIndicator(boolean)
	 */
	@Override
	public void setDepPrimaryIndicator(final boolean depPrimaryIndicator) {
		this.depPrimaryIndicator = depPrimaryIndicator;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasDepartment#getDepDataSourceKey()
	 */
	@Override
	
	public String getDepDataSourceKey() {
		return this.depDataSourceKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasDepartment#setDepDataSourceKey(java.lang.String)
	 */
	@Override
	public void setDepDataSourceKey(final String depDataSourceKey) {
		this.depDataSourceKey = depDataSourceKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasDepartment#getDepNotes()
	 */
	@Override
	
	public String getDepNotes() {
		return this.depNotes;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasDepartment#setDepNotes(java.lang.String)
	 */
	@Override
	public void setDepNotes(final String depNotes) {
		this.depNotes = depNotes;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasDepartment#isDepIsActive()
	 */
	@Override
	
	public boolean isDepIsActive() {
		return this.depIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasDepartment#setDepIsActive(boolean)
	 */
	@Override
	public void setDepIsActive(final boolean depIsActive) {
		this.depIsActive = depIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasDepartment#getDepDateCreated()
	 */
	@Override
	
	
	public LocalDateTime getDepDateCreated() {
		return this.depDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasDepartment#setDepDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setDepDateCreated(final LocalDateTime depDateCreated) {
		this.depDateCreated = depDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasDepartment#getDepDateUpdated()
	 */
	@Override
	
	
	public LocalDateTime getDepDateUpdated() {
		return this.depDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasDepartment#setDepDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setDepDateUpdated(final LocalDateTime depDateUpdated) {
		this.depDateUpdated = depDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasDepartment#getDepUpdatedBy()
	 */
	@Override
	
	public String getDepUpdatedBy() {
		return this.depUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasDepartment#setDepUpdatedBy(java.lang.String)
	 */
	@Override
	public void setDepUpdatedBy(final String depUpdatedBy) {
		this.depUpdatedBy = depUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((depBoucCode == null) ? 0 : depBoucCode.hashCode());
		result = prime * result + ((depDataSourceKey == null) ? 0 : depDataSourceKey.hashCode());
		result = prime * result + ((depDateCreated == null) ? 0 : depDateCreated.hashCode());
		result = prime * result + ((depDateUpdated == null) ? 0 : depDateUpdated.hashCode());
		result = prime * result + ((depDepartmentKey == null) ? 0 : depDepartmentKey.hashCode());
		result = prime * result + (depIsActive ? 1231 : 1237);
		result = prime * result + ((depIsHide == null) ? 0 : depIsHide.hashCode());
		result = prime * result + ((depName == null) ? 0 : depName.hashCode());
		result = prime * result + ((depNotes == null) ? 0 : depNotes.hashCode());
		result = prime * result + (depPrimaryIndicator ? 1231 : 1237);
		result = prime * result + ((depUpdatedBy == null) ? 0 : depUpdatedBy.hashCode());
		result = prime * result + ((depUsedIndicator == null) ? 0 : depUsedIndicator.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof MasDepartmentDTO))
			return false;
		final MasDepartmentDTO other = (MasDepartmentDTO) obj;
		if (depBoucCode == null) {
			if (other.depBoucCode != null)
				return false;
		} else if (!depBoucCode.equals(other.depBoucCode))
			return false;
		if (depDataSourceKey == null) {
			if (other.depDataSourceKey != null)
				return false;
		} else if (!depDataSourceKey.equals(other.depDataSourceKey))
			return false;
		if (depDateCreated == null) {
			if (other.depDateCreated != null)
				return false;
		} else if (!depDateCreated.equals(other.depDateCreated))
			return false;
		if (depDateUpdated == null) {
			if (other.depDateUpdated != null)
				return false;
		} else if (!depDateUpdated.equals(other.depDateUpdated))
			return false;
		if (depDepartmentKey == null) {
			if (other.depDepartmentKey != null)
				return false;
		} else if (!depDepartmentKey.equals(other.depDepartmentKey))
			return false;
		if (depIsActive != other.depIsActive)
			return false;
		if (depIsHide == null) {
			if (other.depIsHide != null)
				return false;
		} else if (!depIsHide.equals(other.depIsHide))
			return false;
		if (depName == null) {
			if (other.depName != null)
				return false;
		} else if (!depName.equals(other.depName))
			return false;
		if (depNotes == null) {
			if (other.depNotes != null)
				return false;
		} else if (!depNotes.equals(other.depNotes))
			return false;
		if (depPrimaryIndicator != other.depPrimaryIndicator)
			return false;
		if (depUpdatedBy == null) {
			if (other.depUpdatedBy != null)
				return false;
		} else if (!depUpdatedBy.equals(other.depUpdatedBy))
			return false;
		if (depUsedIndicator == null) {
			if (other.depUsedIndicator != null)
				return false;
		} else if (!depUsedIndicator.equals(other.depUsedIndicator))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"MasDepartments [depDepartmentKey=%s, depBoucCode=%s, depName=%s, depIsHide=%s, depUsedIndicator=%s, depPrimaryIndicator=%s, depDataSourceKey=%s, depNotes=%s, depIsActive=%s, depDateCreated=%s, depDateUpdated=%s, depUpdatedBy=%s]",
				depDepartmentKey, depBoucCode, depName, depIsHide, depUsedIndicator, depPrimaryIndicator,
				depDataSourceKey, depNotes, depIsActive, depDateCreated, depDateUpdated, depUpdatedBy);
	}

	public MasDepartmentDTO toDTO() {
		return new MasDepartmentDTO(this);
	}
}
