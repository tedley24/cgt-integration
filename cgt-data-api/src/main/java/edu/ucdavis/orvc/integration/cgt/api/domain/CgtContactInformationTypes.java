package edu.ucdavis.orvc.integration.cgt.api.domain;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtContactInformationTypesDTO;

public interface CgtContactInformationTypes extends CgtBaseInterface<CgtContactInformationTypesDTO> { 

	int getCntContactInfoTypeKey();

	void setCntContactInfoTypeKey(int cntContactInfoTypeKey);

	String getCntName();

	void setCntName(String cntName);

}