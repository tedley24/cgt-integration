package edu.ucdavis.orvc.integration.cgt.api.domain;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtWrapUpCloseRulesDTO;

public interface CgtWrapUpCloseRules extends CgtBaseInterface<CgtWrapUpCloseRulesDTO> { 

	int getWucCloseRuleKey();

	void setWucCloseRuleKey(int wucCloseRuleKey);

	int getWucBeginRuleKey();

	void setWucBeginRuleKey(int wucBeginRuleKey);

	Integer getWucResultSubmissionTypeKey();

	void setWucResultSubmissionTypeKey(Integer wucResultSubmissionTypeKey);

	boolean isWucIsActive();

	void setWucIsActive(boolean wucIsActive);

	LocalDateTime getWucDateCreated();

	void setWucDateCreated(LocalDateTime wucDateCreated);

	LocalDateTime getWucDateUpdated();

	void setWucDateUpdated(LocalDateTime wucDateUpdated);

	String getWucUpdatedBy();

	void setWucUpdatedBy(String wucUpdatedBy);

}