package edu.ucdavis.orvc.integration.cgt.api.domain;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.MasUserSuffixesDTO;

public interface MasUserSuffixes extends CgtBaseInterface<MasUserSuffixesDTO> { 

	int getUssUsersuffixKey();

	void setUssUsersuffixKey(int ussUsersuffixKey);

	String getUssName();

	void setUssName(String ussName);

	Boolean getUssIsactive();

	void setUssIsactive(Boolean ussIsactive);

	LocalDateTime getUssDatecreated();

	void setUssDatecreated(LocalDateTime ussDatecreated);

	LocalDateTime getUssDateupdated();

	void setUssDateupdated(LocalDateTime ussDateupdated);

	String getUssUpdatedby();

	void setUssUpdatedby(String ussUpdatedby);

}