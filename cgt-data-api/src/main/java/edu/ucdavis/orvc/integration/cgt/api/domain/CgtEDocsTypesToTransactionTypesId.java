package edu.ucdavis.orvc.integration.cgt.api.domain;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtEDocsTypesToTransactionTypesIdDTO;

public interface CgtEDocsTypesToTransactionTypesId extends CgtBaseInterface<CgtEDocsTypesToTransactionTypesIdDTO> { 

	int getE2tEDocsTypeKey();

	void setE2tEDocsTypeKey(int e2tEDocsTypeKey);

	int getE2tTransactionSubmissionTypeKey();

	void setE2tTransactionSubmissionTypeKey(int e2tTransactionSubmissionTypeKey);

}