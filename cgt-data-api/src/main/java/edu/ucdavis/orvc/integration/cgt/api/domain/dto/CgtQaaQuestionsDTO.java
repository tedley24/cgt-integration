package edu.ucdavis.orvc.integration.cgt.api.domain.dto;


import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtQaaQuestions;



/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/


public class CgtQaaQuestionsDTO implements CgtQaaQuestions {

	private static final long serialVersionUID = -8287517306978366245L;
	private int queQuestionKey;
	private String queDataSourceKey;
	private int queSectionKey;
	private Integer queSortOrder;
	private int queQuestionType;
	private String queQuestion;
	private String queAnswers;
	private String queDefaultAnswer;
	private boolean queIsAdditionalAnswer;
	private boolean queIsActive;
	private LocalDateTime queDateCreated;
	private LocalDateTime queDateUpdated;
	private String queUpdatedBy;

	public CgtQaaQuestionsDTO(final CgtQaaQuestions fromObj) {



	}

	public CgtQaaQuestionsDTO() {
	}

	public CgtQaaQuestionsDTO(final int queQuestionKey, final int queSectionKey, final int queQuestionType, final String queQuestion,
			final String queAnswers, final String queDefaultAnswer, final boolean queIsAdditionalAnswer, final boolean queIsActive,
			final LocalDateTime queDateCreated, final LocalDateTime queDateUpdated, final String queUpdatedBy) {
		this.queQuestionKey = queQuestionKey;
		this.queSectionKey = queSectionKey;
		this.queQuestionType = queQuestionType;
		this.queQuestion = queQuestion;
		this.queAnswers = queAnswers;
		this.queDefaultAnswer = queDefaultAnswer;
		this.queIsAdditionalAnswer = queIsAdditionalAnswer;
		this.queIsActive = queIsActive;
		this.queDateCreated = queDateCreated;
		this.queDateUpdated = queDateUpdated;
		this.queUpdatedBy = queUpdatedBy;
	}

	public CgtQaaQuestionsDTO(final int queQuestionKey, final String queDataSourceKey, final int queSectionKey, final Integer queSortOrder,
			final int queQuestionType, final String queQuestion, final String queAnswers, final String queDefaultAnswer,
			final boolean queIsAdditionalAnswer, final boolean queIsActive, final LocalDateTime queDateCreated, final LocalDateTime queDateUpdated,
			final String queUpdatedBy) {
		this.queQuestionKey = queQuestionKey;
		this.queDataSourceKey = queDataSourceKey;
		this.queSectionKey = queSectionKey;
		this.queSortOrder = queSortOrder;
		this.queQuestionType = queQuestionType;
		this.queQuestion = queQuestion;
		this.queAnswers = queAnswers;
		this.queDefaultAnswer = queDefaultAnswer;
		this.queIsAdditionalAnswer = queIsAdditionalAnswer;
		this.queIsActive = queIsActive;
		this.queDateCreated = queDateCreated;
		this.queDateUpdated = queDateUpdated;
		this.queUpdatedBy = queUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaQuestions#getQueQuestionKey()
	 */
	@Override
	

	
	public int getQueQuestionKey() {
		return this.queQuestionKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaQuestions#setQueQuestionKey(int)
	 */
	@Override
	public void setQueQuestionKey(final int queQuestionKey) {
		this.queQuestionKey = queQuestionKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaQuestions#getQueDataSourceKey()
	 */
	@Override
	
	public String getQueDataSourceKey() {
		return this.queDataSourceKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaQuestions#setQueDataSourceKey(java.lang.String)
	 */
	@Override
	public void setQueDataSourceKey(final String queDataSourceKey) {
		this.queDataSourceKey = queDataSourceKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaQuestions#getQueSectionKey()
	 */
	@Override
	
	public int getQueSectionKey() {
		return this.queSectionKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaQuestions#setQueSectionKey(int)
	 */
	@Override
	public void setQueSectionKey(final int queSectionKey) {
		this.queSectionKey = queSectionKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaQuestions#getQueSortOrder()
	 */
	@Override
	
	public Integer getQueSortOrder() {
		return this.queSortOrder;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaQuestions#setQueSortOrder(java.lang.Integer)
	 */
	@Override
	public void setQueSortOrder(final Integer queSortOrder) {
		this.queSortOrder = queSortOrder;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaQuestions#getQueQuestionType()
	 */
	@Override
	
	public int getQueQuestionType() {
		return this.queQuestionType;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaQuestions#setQueQuestionType(int)
	 */
	@Override
	public void setQueQuestionType(final int queQuestionType) {
		this.queQuestionType = queQuestionType;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaQuestions#getQueQuestion()
	 */
	@Override
	
	public String getQueQuestion() {
		return this.queQuestion;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaQuestions#setQueQuestion(java.lang.String)
	 */
	@Override
	public void setQueQuestion(final String queQuestion) {
		this.queQuestion = queQuestion;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaQuestions#getQueAnswers()
	 */
	@Override
	
	public String getQueAnswers() {
		return this.queAnswers;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaQuestions#setQueAnswers(java.lang.String)
	 */
	@Override
	public void setQueAnswers(final String queAnswers) {
		this.queAnswers = queAnswers;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaQuestions#getQueDefaultAnswer()
	 */
	@Override
	
	public String getQueDefaultAnswer() {
		return this.queDefaultAnswer;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaQuestions#setQueDefaultAnswer(java.lang.String)
	 */
	@Override
	public void setQueDefaultAnswer(final String queDefaultAnswer) {
		this.queDefaultAnswer = queDefaultAnswer;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaQuestions#isQueIsAdditionalAnswer()
	 */
	@Override
	
	public boolean isQueIsAdditionalAnswer() {
		return this.queIsAdditionalAnswer;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaQuestions#setQueIsAdditionalAnswer(boolean)
	 */
	@Override
	public void setQueIsAdditionalAnswer(final boolean queIsAdditionalAnswer) {
		this.queIsAdditionalAnswer = queIsAdditionalAnswer;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaQuestions#isQueIsActive()
	 */
	@Override
	
	public boolean isQueIsActive() {
		return this.queIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaQuestions#setQueIsActive(boolean)
	 */
	@Override
	public void setQueIsActive(final boolean queIsActive) {
		this.queIsActive = queIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaQuestions#getQueDateCreated()
	 */
	@Override
	
	
	public LocalDateTime getQueDateCreated() {
		return this.queDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaQuestions#setQueDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setQueDateCreated(final LocalDateTime queDateCreated) {
		this.queDateCreated = queDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaQuestions#getQueDateUpdated()
	 */
	@Override
	
	
	public LocalDateTime getQueDateUpdated() {
		return this.queDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaQuestions#setQueDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setQueDateUpdated(final LocalDateTime queDateUpdated) {
		this.queDateUpdated = queDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaQuestions#getQueUpdatedBy()
	 */
	@Override
	
	public String getQueUpdatedBy() {
		return this.queUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaQuestions#setQueUpdatedBy(java.lang.String)
	 */
	@Override
	public void setQueUpdatedBy(final String queUpdatedBy) {
		this.queUpdatedBy = queUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"CgtQaaQuestionsImpl [queQuestionKey=%s, queDataSourceKey=%s, queSectionKey=%s, queSortOrder=%s, queQuestionType=%s, queQuestion=%s, queAnswers=%s, queDefaultAnswer=%s, queIsAdditionalAnswer=%s, queIsActive=%s, queDateCreated=%s, queDateUpdated=%s, queUpdatedBy=%s]",
				queQuestionKey, queDataSourceKey, queSectionKey, queSortOrder, queQuestionType, queQuestion, queAnswers,
				queDefaultAnswer, queIsAdditionalAnswer, queIsActive, queDateCreated, queDateUpdated, queUpdatedBy);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((queAnswers == null) ? 0 : queAnswers.hashCode());
		result = prime * result + ((queDataSourceKey == null) ? 0 : queDataSourceKey.hashCode());
		result = prime * result + ((queDateCreated == null) ? 0 : queDateCreated.hashCode());
		result = prime * result + ((queDateUpdated == null) ? 0 : queDateUpdated.hashCode());
		result = prime * result + ((queDefaultAnswer == null) ? 0 : queDefaultAnswer.hashCode());
		result = prime * result + (queIsActive ? 1231 : 1237);
		result = prime * result + (queIsAdditionalAnswer ? 1231 : 1237);
		result = prime * result + ((queQuestion == null) ? 0 : queQuestion.hashCode());
		result = prime * result + queQuestionKey;
		result = prime * result + queQuestionType;
		result = prime * result + queSectionKey;
		result = prime * result + ((queSortOrder == null) ? 0 : queSortOrder.hashCode());
		result = prime * result + ((queUpdatedBy == null) ? 0 : queUpdatedBy.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof CgtQaaQuestionsDTO)) {
			return false;
		}
		CgtQaaQuestionsDTO other = (CgtQaaQuestionsDTO) obj;
		if (queAnswers == null) {
			if (other.queAnswers != null) {
				return false;
			}
		} else if (!queAnswers.equals(other.queAnswers)) {
			return false;
		}
		if (queDataSourceKey == null) {
			if (other.queDataSourceKey != null) {
				return false;
			}
		} else if (!queDataSourceKey.equals(other.queDataSourceKey)) {
			return false;
		}
		if (queDateCreated == null) {
			if (other.queDateCreated != null) {
				return false;
			}
		} else if (!queDateCreated.equals(other.queDateCreated)) {
			return false;
		}
		if (queDateUpdated == null) {
			if (other.queDateUpdated != null) {
				return false;
			}
		} else if (!queDateUpdated.equals(other.queDateUpdated)) {
			return false;
		}
		if (queDefaultAnswer == null) {
			if (other.queDefaultAnswer != null) {
				return false;
			}
		} else if (!queDefaultAnswer.equals(other.queDefaultAnswer)) {
			return false;
		}
		if (queIsActive != other.queIsActive) {
			return false;
		}
		if (queIsAdditionalAnswer != other.queIsAdditionalAnswer) {
			return false;
		}
		if (queQuestion == null) {
			if (other.queQuestion != null) {
				return false;
			}
		} else if (!queQuestion.equals(other.queQuestion)) {
			return false;
		}
		if (queQuestionKey != other.queQuestionKey) {
			return false;
		}
		if (queQuestionType != other.queQuestionType) {
			return false;
		}
		if (queSectionKey != other.queSectionKey) {
			return false;
		}
		if (queSortOrder == null) {
			if (other.queSortOrder != null) {
				return false;
			}
		} else if (!queSortOrder.equals(other.queSortOrder)) {
			return false;
		}
		if (queUpdatedBy == null) {
			if (other.queUpdatedBy != null) {
				return false;
			}
		} else if (!queUpdatedBy.equals(other.queUpdatedBy)) {
			return false;
		}
		return true;
	}

	public CgtQaaQuestionsDTO toDTO() {
		return new CgtQaaQuestionsDTO(this);
	}
}
