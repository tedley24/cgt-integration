package edu.ucdavis.orvc.integration.cgt.api.domain.dto;


import edu.ucdavis.orvc.integration.cgt.api.domain.CgtContactInformation;

/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/


public class CgtContactInformationDTO implements CgtContactInformation {

	private static final long serialVersionUID = -7903657811559246858L;

	private int contactInfoKey;
	private String projectKey;
	private int contactInfoTypeKey;
	private String firstName;
	private String lastName;
	private String email;
	private String phone;
	private String fax;
	private String street1;
	private String street2;
	private String city;
	private String state;
	private String postalCode;
	private boolean isActive;

	public CgtContactInformationDTO() {
		super();
	}

	public CgtContactInformationDTO(final int contactInfoKey, final String projectKey, final int contactInfoTypeKey, final boolean isActive) {
		super();
		this.contactInfoKey = contactInfoKey;
		this.projectKey = projectKey;
		this.contactInfoTypeKey = contactInfoTypeKey;
		this.isActive = isActive;
	}

	public CgtContactInformationDTO(final int contactInfoKey, final String projectKey, final int contactInfoTypeKey, final String firstName,
			final String lastName, final String email, final String phone, final String fax, final String street1, final String street2, final String city,
			final String state, final String postalCode, final boolean isActive) {
		super();
		this.contactInfoKey = contactInfoKey;
		this.projectKey = projectKey;
		this.contactInfoTypeKey = contactInfoTypeKey;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.phone = phone;
		this.fax = fax;
		this.street1 = street1;
		this.street2 = street2;
		this.city = city;
		this.state = state;
		this.postalCode = postalCode;
		this.isActive = isActive;
	}


	public CgtContactInformationDTO(final CgtContactInformation fromObj) {
		super();
		this.contactInfoKey = fromObj.getContactInfoKey();
		this.projectKey = fromObj.getProjectKey();
		this.contactInfoTypeKey = fromObj.getContactInfoTypeKey();
		this.firstName = fromObj.getFirstName();
		this.lastName = fromObj.getLastName();
		this.email = fromObj.getEmail();
		this.phone = fromObj.getPhone();
		this.fax = fromObj.getFax();
		this.street1 = fromObj.getStreet1();
		this.street2 = fromObj.getStreet2();
		this.city = fromObj.getCity();
		this.state = fromObj.getState();
		this.postalCode = fromObj.getPostalCode();
		this.isActive = fromObj.isIsActive();
	}
	
	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtContactInformation#getContactInfoKey()
	 */
	@Override
	public int getContactInfoKey() {
		return this.contactInfoKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtContactInformation#setContactInfoKey(int)
	 */
	@Override
	public void setContactInfoKey(final int contactInfoKey) {
		this.contactInfoKey = contactInfoKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtContactInformation#getProjectKey()
	 */
	@Override
	
	public String getProjectKey() {
		return this.projectKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtContactInformation#setProjectKey(java.lang.String)
	 */
	@Override
	public void setProjectKey(final String projectKey) {
		this.projectKey = projectKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtContactInformation#getContactInfoTypeKey()
	 */
	@Override
	
	public int getContactInfoTypeKey() {
		return this.contactInfoTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtContactInformation#setContactInfoTypeKey(int)
	 */
	@Override
	public void setContactInfoTypeKey(final int contactInfoTypeKey) {
		this.contactInfoTypeKey = contactInfoTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtContactInformation#getFirstName()
	 */
	@Override
	
	public String getFirstName() {
		return this.firstName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtContactInformation#setFirstName(java.lang.String)
	 */
	@Override
	public void setFirstName(final String firstName) {
		this.firstName = firstName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtContactInformation#getLastName()
	 */
	@Override
	
	public String getLastName() {
		return this.lastName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtContactInformation#setLastName(java.lang.String)
	 */
	@Override
	public void setLastName(final String lastName) {
		this.lastName = lastName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtContactInformation#getEmail()
	 */
	@Override
	
	public String getEmail() {
		return this.email;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtContactInformation#setEmail(java.lang.String)
	 */
	@Override
	public void setEmail(final String email) {
		this.email = email;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtContactInformation#getPhone()
	 */
	@Override
	
	public String getPhone() {
		return this.phone;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtContactInformation#setPhone(java.lang.String)
	 */
	@Override
	public void setPhone(final String phone) {
		this.phone = phone;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtContactInformation#getFax()
	 */
	@Override
	
	public String getFax() {
		return this.fax;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtContactInformation#setFax(java.lang.String)
	 */
	@Override
	public void setFax(final String fax) {
		this.fax = fax;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtContactInformation#getStreet1()
	 */
	@Override
	
	public String getStreet1() {
		return this.street1;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtContactInformation#setStreet1(java.lang.String)
	 */
	@Override
	public void setStreet1(final String street1) {
		this.street1 = street1;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtContactInformation#getStreet2()
	 */
	@Override
	
	public String getStreet2() {
		return this.street2;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtContactInformation#setStreet2(java.lang.String)
	 */
	@Override
	public void setStreet2(final String street2) {
		this.street2 = street2;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtContactInformation#getCity()
	 */
	@Override
	
	public String getCity() {
		return this.city;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtContactInformation#setCity(java.lang.String)
	 */
	@Override
	public void setCity(final String city) {
		this.city = city;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtContactInformation#getState()
	 */
	@Override
	
	public String getState() {
		return this.state;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtContactInformation#setState(java.lang.String)
	 */
	@Override
	public void setState(final String state) {
		this.state = state;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtContactInformation#getPostalCode()
	 */
	@Override
	
	public String getPostalCode() {
		return this.postalCode;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtContactInformation#setPostalCode(java.lang.String)
	 */
	@Override
	public void setPostalCode(final String postalCode) {
		this.postalCode = postalCode;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtContactInformation#isIsActive()
	 */
	@Override
	
	public boolean isIsActive() {
		return this.isActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtContactInformation#setIsActive(boolean)
	 */
	@Override
	public void setIsActive(final boolean isActive) {
		this.isActive = isActive;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + contactInfoKey;
		result = prime * result + contactInfoTypeKey;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((fax == null) ? 0 : fax.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + (isActive ? 1231 : 1237);
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((phone == null) ? 0 : phone.hashCode());
		result = prime * result + ((postalCode == null) ? 0 : postalCode.hashCode());
		result = prime * result + ((projectKey == null) ? 0 : projectKey.hashCode());
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		result = prime * result + ((street1 == null) ? 0 : street1.hashCode());
		result = prime * result + ((street2 == null) ? 0 : street2.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof CgtContactInformationDTO))
			return false;
		final CgtContactInformationDTO other = (CgtContactInformationDTO) obj;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (contactInfoKey != other.contactInfoKey)
			return false;
		if (contactInfoTypeKey != other.contactInfoTypeKey)
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (fax == null) {
			if (other.fax != null)
				return false;
		} else if (!fax.equals(other.fax))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (isActive != other.isActive)
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (phone == null) {
			if (other.phone != null)
				return false;
		} else if (!phone.equals(other.phone))
			return false;
		if (postalCode == null) {
			if (other.postalCode != null)
				return false;
		} else if (!postalCode.equals(other.postalCode))
			return false;
		if (projectKey == null) {
			if (other.projectKey != null)
				return false;
		} else if (!projectKey.equals(other.projectKey))
			return false;
		if (state == null) {
			if (other.state != null)
				return false;
		} else if (!state.equals(other.state))
			return false;
		if (street1 == null) {
			if (other.street1 != null)
				return false;
		} else if (!street1.equals(other.street1))
			return false;
		if (street2 == null) {
			if (other.street2 != null)
				return false;
		} else if (!street2.equals(other.street2))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"CgtContactInformation [contactInfoKey=%s, projectKey=%s, contactInfoTypeKey=%s, firstName=%s, lastName=%s, email=%s, phone=%s, fax=%s, street1=%s, street2=%s, city=%s, state=%s, postalCode=%s, isActive=%s]",
				contactInfoKey, projectKey, contactInfoTypeKey, firstName, lastName, email, phone, fax, street1,
				street2, city, state, postalCode, isActive);
	}

	public CgtContactInformationDTO toDTO() {
		return new CgtContactInformationDTO(this);
	}
}
