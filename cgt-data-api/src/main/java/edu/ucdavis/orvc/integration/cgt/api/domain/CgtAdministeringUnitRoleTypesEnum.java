package edu.ucdavis.orvc.integration.cgt.api.domain;

public enum CgtAdministeringUnitRoleTypesEnum {

	PRIMARY (1,"Primary"),
	ALTERNATE (2,"Alternate");
	
	public int typeKey;
	public String shortName;
	
	private CgtAdministeringUnitRoleTypesEnum(int typeKey,String shortName) {
		this.typeKey = typeKey;
		this.shortName = shortName;
	}

	

}
