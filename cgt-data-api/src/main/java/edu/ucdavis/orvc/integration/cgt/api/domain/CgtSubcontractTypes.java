package edu.ucdavis.orvc.integration.cgt.api.domain;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtSubcontractTypesDTO;

public interface CgtSubcontractTypes extends CgtBaseInterface<CgtSubcontractTypesDTO> { 

	int getSctSubcontractTypeKey();

	void setSctSubcontractTypeKey(int sctSubcontractTypeKey);

	String getSctName();

	void setSctName(String sctName);

	String getSctShortName();

	void setSctShortName(String sctShortName);

	Integer getSctSortOrder();

	void setSctSortOrder(Integer sctSortOrder);

	boolean isSctIsActive();

	void setSctIsActive(boolean sctIsActive);

	String getSctUpdatedBy();

	void setSctUpdatedBy(String sctUpdatedBy);

	LocalDateTime getSctDateCreated();

	void setSctDateCreated(LocalDateTime sctDateCreated);

	LocalDateTime getSctDateUpdated();

	void setSctDateUpdated(LocalDateTime sctDateUpdated);

}