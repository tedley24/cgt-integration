package edu.ucdavis.orvc.integration.cgt.api.domain.dto;


import edu.ucdavis.orvc.integration.cgt.api.domain.CgtWrapUpCloseRulesToSponsorCategoriesId;

/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/

public class CgtWrapUpCloseRulesToSponsorCategoriesIdDTO implements CgtWrapUpCloseRulesToSponsorCategoriesId {

	private static final long serialVersionUID = -5214974680030553649L;
	private int w2cCloseRuleKey;
	private String w2cSponsorCategoryKey;

	public CgtWrapUpCloseRulesToSponsorCategoriesIdDTO(final CgtWrapUpCloseRulesToSponsorCategoriesId fromObj) {



	}

	public CgtWrapUpCloseRulesToSponsorCategoriesIdDTO() {
	}

	public CgtWrapUpCloseRulesToSponsorCategoriesIdDTO(final int w2cCloseRuleKey, final String w2cSponsorCategoryKey) {
		this.w2cCloseRuleKey = w2cCloseRuleKey;
		this.w2cSponsorCategoryKey = w2cSponsorCategoryKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpCloseRulesToSponsorCategoriesId#getW2cCloseRuleKey()
	 */
	@Override
	
	public int getW2cCloseRuleKey() {
		return this.w2cCloseRuleKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpCloseRulesToSponsorCategoriesId#setW2cCloseRuleKey(int)
	 */
	@Override
	public void setW2cCloseRuleKey(final int w2cCloseRuleKey) {
		this.w2cCloseRuleKey = w2cCloseRuleKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpCloseRulesToSponsorCategoriesId#getW2cSponsorCategoryKey()
	 */
	@Override
	
	public String getW2cSponsorCategoryKey() {
		return this.w2cSponsorCategoryKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpCloseRulesToSponsorCategoriesId#setW2cSponsorCategoryKey(java.lang.String)
	 */
	@Override
	public void setW2cSponsorCategoryKey(final String w2cSponsorCategoryKey) {
		this.w2cSponsorCategoryKey = w2cSponsorCategoryKey;
	}

	@Override
	public boolean equals(final Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof CgtWrapUpCloseRulesToSponsorCategoriesIdDTO))
			return false;
		final CgtWrapUpCloseRulesToSponsorCategoriesId castOther = (CgtWrapUpCloseRulesToSponsorCategoriesId) other;

		return (this.getW2cCloseRuleKey() == castOther.getW2cCloseRuleKey())
				&& ((this.getW2cSponsorCategoryKey() == castOther.getW2cSponsorCategoryKey())
						|| (this.getW2cSponsorCategoryKey() != null && castOther.getW2cSponsorCategoryKey() != null
						&& this.getW2cSponsorCategoryKey().equals(castOther.getW2cSponsorCategoryKey())));
	}

	@Override
	public int hashCode() {
		int result = 17;

		result = 37 * result + this.getW2cCloseRuleKey();
		result = 37 * result + (getW2cSponsorCategoryKey() == null ? 0 : this.getW2cSponsorCategoryKey().hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format("CgtWrapUpCloseRulesToSponsorCategoriesId [w2cCloseRuleKey=%s, w2cSponsorCategoryKey=%s]",
				w2cCloseRuleKey, w2cSponsorCategoryKey);
	}

	public CgtWrapUpCloseRulesToSponsorCategoriesIdDTO toDTO() {
		return new CgtWrapUpCloseRulesToSponsorCategoriesIdDTO(this);
	}
}
