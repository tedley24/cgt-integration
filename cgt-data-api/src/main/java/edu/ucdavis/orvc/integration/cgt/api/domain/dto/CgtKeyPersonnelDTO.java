package edu.ucdavis.orvc.integration.cgt.api.domain.dto;


import java.math.BigDecimal;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtKeyPersonnel;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtKeyPersonnelRole;
import edu.ucdavis.orvc.integration.cgt.api.domain.MasUser;



/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/


public class CgtKeyPersonnelDTO implements CgtKeyPersonnel {

	private static final long serialVersionUID = -1135553836657043476L;
	private int kepKeyPersonnelKey;
	private String kepProjectKey;
	private String kepUserKey;
	private int kepKeyPersonnelRoleKey;
	private BigDecimal kepEffortCharged;
	private BigDecimal kepEffortCostShared;
	private BigDecimal kepEffortCommitted;
	private Boolean kepIsCommitmentLetter;
	private Boolean kepIsSameUnit;
	private boolean kepIsActive;
	private String kepUpdatedBy;
	private LocalDateTime kepDateCreated;
	private LocalDateTime kepDateUpdated;
	
	private CgtKeyPersonnelRoleDTO personnelRole;
	private MasUserDTO user;

	public CgtKeyPersonnelDTO(final CgtKeyPersonnel fromObj) {



	}

	public CgtKeyPersonnelDTO() {
	}

	public CgtKeyPersonnelDTO(final int kepKeyPersonnelKey, final String kepProjectKey, final String kepUserKey, final int kepKeyPersonnelRoleKey,
			final boolean kepIsActive, final String kepUpdatedBy, final LocalDateTime kepDateCreated, final LocalDateTime kepDateUpdated) {
		this.kepKeyPersonnelKey = kepKeyPersonnelKey;
		this.kepProjectKey = kepProjectKey;
		this.kepUserKey = kepUserKey;
		this.kepKeyPersonnelRoleKey = kepKeyPersonnelRoleKey;
		this.kepIsActive = kepIsActive;
		this.kepUpdatedBy = kepUpdatedBy;
		this.kepDateCreated = kepDateCreated;
		this.kepDateUpdated = kepDateUpdated;
	}

	public CgtKeyPersonnelDTO(final int kepKeyPersonnelKey, final String kepProjectKey, final String kepUserKey, final int kepKeyPersonnelRoleKey,
			final BigDecimal kepEffortCharged, final BigDecimal kepEffortCostShared, final BigDecimal kepEffortCommitted, final Boolean kepIsCommitmentLetter,
			final Boolean kepIsSameUnit, final boolean kepIsActive, final String kepUpdatedBy, final LocalDateTime kepDateCreated, final LocalDateTime kepDateUpdated) {
		this.kepKeyPersonnelKey = kepKeyPersonnelKey;
		this.kepProjectKey = kepProjectKey;
		this.kepUserKey = kepUserKey;
		this.kepKeyPersonnelRoleKey = kepKeyPersonnelRoleKey;
		this.kepEffortCharged = kepEffortCharged;
		this.kepEffortCostShared = kepEffortCostShared;
		this.kepEffortCommitted = kepEffortCommitted;
		this.kepIsCommitmentLetter = kepIsCommitmentLetter;
		this.kepIsSameUnit = kepIsSameUnit;
		this.kepIsActive = kepIsActive;
		this.kepUpdatedBy = kepUpdatedBy;
		this.kepDateCreated = kepDateCreated;
		this.kepDateUpdated = kepDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtKeyPersonnel#getKepKeyPersonnelKey()
	 */
	@Override
	

	
	public int getKepKeyPersonnelKey() {
		return this.kepKeyPersonnelKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtKeyPersonnel#setKepKeyPersonnelKey(int)
	 */
	@Override
	public void setKepKeyPersonnelKey(final int kepKeyPersonnelKey) {
		this.kepKeyPersonnelKey = kepKeyPersonnelKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtKeyPersonnel#getKepProjectKey()
	 */
	@Override
	
	public String getKepProjectKey() {
		return this.kepProjectKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtKeyPersonnel#setKepProjectKey(java.lang.String)
	 */
	@Override
	public void setKepProjectKey(final String kepProjectKey) {
		this.kepProjectKey = kepProjectKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtKeyPersonnel#getKepUserKey()
	 */
	@Override
	
	public String getKepUserKey() {
		return this.kepUserKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtKeyPersonnel#setKepUserKey(java.lang.String)
	 */
	@Override
	public void setKepUserKey(final String kepUserKey) {
		this.kepUserKey = kepUserKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtKeyPersonnel#getKepKeyPersonnelRoleKey()
	 */
	@Override
	
	public int getKepKeyPersonnelRoleKey() {
		return this.kepKeyPersonnelRoleKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtKeyPersonnel#setKepKeyPersonnelRoleKey(int)
	 */
	@Override
	public void setKepKeyPersonnelRoleKey(final int kepKeyPersonnelRoleKey) {
		this.kepKeyPersonnelRoleKey = kepKeyPersonnelRoleKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtKeyPersonnel#getKepEffortCharged()
	 */
	@Override
	
	public BigDecimal getKepEffortCharged() {
		return this.kepEffortCharged;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtKeyPersonnel#setKepEffortCharged(java.math.BigDecimal)
	 */
	@Override
	public void setKepEffortCharged(final BigDecimal kepEffortCharged) {
		this.kepEffortCharged = kepEffortCharged;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtKeyPersonnel#getKepEffortCostShared()
	 */
	@Override
	
	public BigDecimal getKepEffortCostShared() {
		return this.kepEffortCostShared;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtKeyPersonnel#setKepEffortCostShared(java.math.BigDecimal)
	 */
	@Override
	public void setKepEffortCostShared(final BigDecimal kepEffortCostShared) {
		this.kepEffortCostShared = kepEffortCostShared;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtKeyPersonnel#getKepEffortCommitted()
	 */
	@Override
	
	public BigDecimal getKepEffortCommitted() {
		return this.kepEffortCommitted;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtKeyPersonnel#setKepEffortCommitted(java.math.BigDecimal)
	 */
	@Override
	public void setKepEffortCommitted(final BigDecimal kepEffortCommitted) {
		this.kepEffortCommitted = kepEffortCommitted;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtKeyPersonnel#getKepIsCommitmentLetter()
	 */
	@Override
	
	public Boolean getKepIsCommitmentLetter() {
		return this.kepIsCommitmentLetter;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtKeyPersonnel#setKepIsCommitmentLetter(java.lang.Boolean)
	 */
	@Override
	public void setKepIsCommitmentLetter(final Boolean kepIsCommitmentLetter) {
		this.kepIsCommitmentLetter = kepIsCommitmentLetter;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtKeyPersonnel#getKepIsSameUnit()
	 */
	@Override
	
	public Boolean getKepIsSameUnit() {
		return this.kepIsSameUnit;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtKeyPersonnel#setKepIsSameUnit(java.lang.Boolean)
	 */
	@Override
	public void setKepIsSameUnit(final Boolean kepIsSameUnit) {
		this.kepIsSameUnit = kepIsSameUnit;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtKeyPersonnel#isKepIsActive()
	 */
	@Override
	
	public boolean isKepIsActive() {
		return this.kepIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtKeyPersonnel#setKepIsActive(boolean)
	 */
	@Override
	public void setKepIsActive(final boolean kepIsActive) {
		this.kepIsActive = kepIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtKeyPersonnel#getKepUpdatedBy()
	 */
	@Override
	
	public String getKepUpdatedBy() {
		return this.kepUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtKeyPersonnel#setKepUpdatedBy(java.lang.String)
	 */
	@Override
	public void setKepUpdatedBy(final String kepUpdatedBy) {
		this.kepUpdatedBy = kepUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtKeyPersonnel#getKepDateCreated()
	 */
	@Override
	
	
	public LocalDateTime getKepDateCreated() {
		return this.kepDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtKeyPersonnel#setKepDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setKepDateCreated(final LocalDateTime kepDateCreated) {
		this.kepDateCreated = kepDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtKeyPersonnel#getKepDateUpdated()
	 */
	@Override
	
	
	public LocalDateTime getKepDateUpdated() {
		return this.kepDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtKeyPersonnel#setKepDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setKepDateUpdated(final LocalDateTime kepDateUpdated) {
		this.kepDateUpdated = kepDateUpdated;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((kepDateCreated == null) ? 0 : kepDateCreated.hashCode());
		result = prime * result + ((kepDateUpdated == null) ? 0 : kepDateUpdated.hashCode());
		result = prime * result + ((kepEffortCharged == null) ? 0 : kepEffortCharged.hashCode());
		result = prime * result + ((kepEffortCommitted == null) ? 0 : kepEffortCommitted.hashCode());
		result = prime * result + ((kepEffortCostShared == null) ? 0 : kepEffortCostShared.hashCode());
		result = prime * result + (kepIsActive ? 1231 : 1237);
		result = prime * result + ((kepIsCommitmentLetter == null) ? 0 : kepIsCommitmentLetter.hashCode());
		result = prime * result + ((kepIsSameUnit == null) ? 0 : kepIsSameUnit.hashCode());
		result = prime * result + kepKeyPersonnelKey;
		result = prime * result + kepKeyPersonnelRoleKey;
		result = prime * result + ((kepProjectKey == null) ? 0 : kepProjectKey.hashCode());
		result = prime * result + ((kepUpdatedBy == null) ? 0 : kepUpdatedBy.hashCode());
		result = prime * result + ((kepUserKey == null) ? 0 : kepUserKey.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof CgtKeyPersonnelDTO)) {
			return false;
		}
		CgtKeyPersonnelDTO other = (CgtKeyPersonnelDTO) obj;
		if (kepDateCreated == null) {
			if (other.kepDateCreated != null) {
				return false;
			}
		} else if (!kepDateCreated.equals(other.kepDateCreated)) {
			return false;
		}
		if (kepDateUpdated == null) {
			if (other.kepDateUpdated != null) {
				return false;
			}
		} else if (!kepDateUpdated.equals(other.kepDateUpdated)) {
			return false;
		}
		if (kepEffortCharged == null) {
			if (other.kepEffortCharged != null) {
				return false;
			}
		} else if (!kepEffortCharged.equals(other.kepEffortCharged)) {
			return false;
		}
		if (kepEffortCommitted == null) {
			if (other.kepEffortCommitted != null) {
				return false;
			}
		} else if (!kepEffortCommitted.equals(other.kepEffortCommitted)) {
			return false;
		}
		if (kepEffortCostShared == null) {
			if (other.kepEffortCostShared != null) {
				return false;
			}
		} else if (!kepEffortCostShared.equals(other.kepEffortCostShared)) {
			return false;
		}
		if (kepIsActive != other.kepIsActive) {
			return false;
		}
		if (kepIsCommitmentLetter == null) {
			if (other.kepIsCommitmentLetter != null) {
				return false;
			}
		} else if (!kepIsCommitmentLetter.equals(other.kepIsCommitmentLetter)) {
			return false;
		}
		if (kepIsSameUnit == null) {
			if (other.kepIsSameUnit != null) {
				return false;
			}
		} else if (!kepIsSameUnit.equals(other.kepIsSameUnit)) {
			return false;
		}
		if (kepKeyPersonnelKey != other.kepKeyPersonnelKey) {
			return false;
		}
		if (kepKeyPersonnelRoleKey != other.kepKeyPersonnelRoleKey) {
			return false;
		}
		if (kepProjectKey == null) {
			if (other.kepProjectKey != null) {
				return false;
			}
		} else if (!kepProjectKey.equals(other.kepProjectKey)) {
			return false;
		}
		if (kepUpdatedBy == null) {
			if (other.kepUpdatedBy != null) {
				return false;
			}
		} else if (!kepUpdatedBy.equals(other.kepUpdatedBy)) {
			return false;
		}
		if (kepUserKey == null) {
			if (other.kepUserKey != null) {
				return false;
			}
		} else if (!kepUserKey.equals(other.kepUserKey)) {
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"CgtKeyPersonnel [kepKeyPersonnelKey=%s, kepProjectKey=%s, kepUserKey=%s, kepKeyPersonnelRoleKey=%s, kepEffortCharged=%s, kepEffortCostShared=%s, kepEffortCommitted=%s, kepIsCommitmentLetter=%s, kepIsSameUnit=%s, kepIsActive=%s, kepUpdatedBy=%s, kepDateCreated=%s, kepDateUpdated=%s]",
				kepKeyPersonnelKey, kepProjectKey, kepUserKey, kepKeyPersonnelRoleKey, kepEffortCharged,
				kepEffortCostShared, kepEffortCommitted, kepIsCommitmentLetter, kepIsSameUnit, kepIsActive,
				kepUpdatedBy, kepDateCreated, kepDateUpdated);
	}


	
	
	public CgtKeyPersonnelRoleDTO getPersonnelRoleImpl() {
		return personnelRole;
	}

	public void setPersonnelRoleImpl(CgtKeyPersonnelRoleDTO personnelRole) {
		this.personnelRole = personnelRole;
	}

	
	
	public MasUserDTO getUserImpl() {
		return user;
	}

	public void setUserImpl(MasUserDTO user) {
		this.user = user;
	}

	
	
	@Override
	
	public CgtKeyPersonnelRole getPersonnelRole() {
		return this.personnelRole;
	}

	@Override
	public void setPersonnelRole(CgtKeyPersonnelRole personnelRole) {
		this.personnelRole = (CgtKeyPersonnelRoleDTO) personnelRole;
	}

	@Override
	
	public MasUser getUser() {
		return this.user;
	}

	@Override
	public void setUser(MasUser user) {
		this.user = (MasUserDTO) user;
	}
	
	public CgtKeyPersonnelDTO toDTO() {
		return new CgtKeyPersonnelDTO(this);
	}
}
