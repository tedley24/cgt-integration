package edu.ucdavis.orvc.integration.cgt.api.domain;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtEDocsFileInsertTypesDTO;

public interface CgtEDocsFileInsertTypes extends CgtBaseInterface<CgtEDocsFileInsertTypesDTO> { 

	int getFinFileInsertTypeKey();

	void setFinFileInsertTypeKey(int finFileInsertTypeKey);

	Integer getFinParentKey();

	void setFinParentKey(Integer finParentKey);

	String getFinName();

	void setFinName(String finName);

	String getFinDescription();

	void setFinDescription(String finDescription);

	String getFinColor();

	void setFinColor(String finColor);

	boolean isFinIsCoverSheet();

	void setFinIsCoverSheet(boolean finIsCoverSheet);

	int getFinSortOrder();

	void setFinSortOrder(int finSortOrder);

	boolean isFinIsActive();

	void setFinIsActive(boolean finIsActive);

}