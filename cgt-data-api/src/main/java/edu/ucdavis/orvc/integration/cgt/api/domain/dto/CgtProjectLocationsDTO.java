package edu.ucdavis.orvc.integration.cgt.api.domain.dto;


import java.math.BigDecimal;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtProjectLocations;



/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/


public class CgtProjectLocationsDTO implements CgtProjectLocations {

	private static final long serialVersionUID = 10668408084475521L;
	
	private int ploProjectLocationKey;
	private String ploProjectKey;
	private String ploName;
	private BigDecimal ploUcowned;
	private BigDecimal ploUcleased;
	private BigDecimal ploOtherUcownedLeased;
	private Boolean ploIsNewLeased;
	private boolean ploIsActive;
	private String ploUpdatedBy;
	private LocalDateTime ploDateCreated;
	private LocalDateTime ploDateUpdated;

	public CgtProjectLocationsDTO(final CgtProjectLocations fromObj) {



	}

	public CgtProjectLocationsDTO() {
	}

	public CgtProjectLocationsDTO(final int ploProjectLocationKey, final String ploProjectKey, final boolean ploIsActive,
			final String ploUpdatedBy, final LocalDateTime ploDateCreated, final LocalDateTime ploDateUpdated) {
		this.ploProjectLocationKey = ploProjectLocationKey;
		this.ploProjectKey = ploProjectKey;
		this.ploIsActive = ploIsActive;
		this.ploUpdatedBy = ploUpdatedBy;
		this.ploDateCreated = ploDateCreated;
		this.ploDateUpdated = ploDateUpdated;
	}

	public CgtProjectLocationsDTO(final int ploProjectLocationKey, final String ploProjectKey, final String ploName, final BigDecimal ploUcowned,
			final BigDecimal ploUcleased, final BigDecimal ploOtherUcownedLeased, final Boolean ploIsNewLeased, final boolean ploIsActive,
			final String ploUpdatedBy, final LocalDateTime ploDateCreated, final LocalDateTime ploDateUpdated) {
		this.ploProjectLocationKey = ploProjectLocationKey;
		this.ploProjectKey = ploProjectKey;
		this.ploName = ploName;
		this.ploUcowned = ploUcowned;
		this.ploUcleased = ploUcleased;
		this.ploOtherUcownedLeased = ploOtherUcownedLeased;
		this.ploIsNewLeased = ploIsNewLeased;
		this.ploIsActive = ploIsActive;
		this.ploUpdatedBy = ploUpdatedBy;
		this.ploDateCreated = ploDateCreated;
		this.ploDateUpdated = ploDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectLocations#getPloProjectLocationKey()
	 */
	@Override
	

	
	public int getPloProjectLocationKey() {
		return this.ploProjectLocationKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectLocations#setPloProjectLocationKey(int)
	 */
	@Override
	public void setPloProjectLocationKey(final int ploProjectLocationKey) {
		this.ploProjectLocationKey = ploProjectLocationKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectLocations#getPloProjectKey()
	 */
	@Override
	
	public String getPloProjectKey() {
		return this.ploProjectKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectLocations#setPloProjectKey(java.lang.String)
	 */
	@Override
	public void setPloProjectKey(final String ploProjectKey) {
		this.ploProjectKey = ploProjectKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectLocations#getPloName()
	 */
	@Override
	
	public String getPloName() {
		return this.ploName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectLocations#setPloName(java.lang.String)
	 */
	@Override
	public void setPloName(final String ploName) {
		this.ploName = ploName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectLocations#getPloUcowned()
	 */
	@Override
	
	public BigDecimal getPloUcowned() {
		return this.ploUcowned;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectLocations#setPloUcowned(java.math.BigDecimal)
	 */
	@Override
	public void setPloUcowned(final BigDecimal ploUcowned) {
		this.ploUcowned = ploUcowned;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectLocations#getPloUcleased()
	 */
	@Override
	
	public BigDecimal getPloUcleased() {
		return this.ploUcleased;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectLocations#setPloUcleased(java.math.BigDecimal)
	 */
	@Override
	public void setPloUcleased(final BigDecimal ploUcleased) {
		this.ploUcleased = ploUcleased;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectLocations#getPloOtherUcownedLeased()
	 */
	@Override
	
	public BigDecimal getPloOtherUcownedLeased() {
		return this.ploOtherUcownedLeased;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectLocations#setPloOtherUcownedLeased(java.math.BigDecimal)
	 */
	@Override
	public void setPloOtherUcownedLeased(final BigDecimal ploOtherUcownedLeased) {
		this.ploOtherUcownedLeased = ploOtherUcownedLeased;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectLocations#getPloIsNewLeased()
	 */
	@Override
	
	public Boolean getPloIsNewLeased() {
		return this.ploIsNewLeased;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectLocations#setPloIsNewLeased(java.lang.Boolean)
	 */
	@Override
	public void setPloIsNewLeased(final Boolean ploIsNewLeased) {
		this.ploIsNewLeased = ploIsNewLeased;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectLocations#isPloIsActive()
	 */
	@Override
	
	public boolean isPloIsActive() {
		return this.ploIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectLocations#setPloIsActive(boolean)
	 */
	@Override
	public void setPloIsActive(final boolean ploIsActive) {
		this.ploIsActive = ploIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectLocations#getPloUpdatedBy()
	 */
	@Override
	
	public String getPloUpdatedBy() {
		return this.ploUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectLocations#setPloUpdatedBy(java.lang.String)
	 */
	@Override
	public void setPloUpdatedBy(final String ploUpdatedBy) {
		this.ploUpdatedBy = ploUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectLocations#getPloDateCreated()
	 */
	@Override
	
	
	public LocalDateTime getPloDateCreated() {
		return this.ploDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectLocations#setPloDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setPloDateCreated(final LocalDateTime ploDateCreated) {
		this.ploDateCreated = ploDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectLocations#getPloDateUpdated()
	 */
	@Override
	
	
	public LocalDateTime getPloDateUpdated() {
		return this.ploDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectLocations#setPloDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setPloDateUpdated(final LocalDateTime ploDateUpdated) {
		this.ploDateUpdated = ploDateUpdated;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((ploDateCreated == null) ? 0 : ploDateCreated.hashCode());
		result = prime * result + ((ploDateUpdated == null) ? 0 : ploDateUpdated.hashCode());
		result = prime * result + (ploIsActive ? 1231 : 1237);
		result = prime * result + ((ploIsNewLeased == null) ? 0 : ploIsNewLeased.hashCode());
		result = prime * result + ((ploName == null) ? 0 : ploName.hashCode());
		result = prime * result + ((ploOtherUcownedLeased == null) ? 0 : ploOtherUcownedLeased.hashCode());
		result = prime * result + ((ploProjectKey == null) ? 0 : ploProjectKey.hashCode());
		result = prime * result + ploProjectLocationKey;
		result = prime * result + ((ploUcleased == null) ? 0 : ploUcleased.hashCode());
		result = prime * result + ((ploUcowned == null) ? 0 : ploUcowned.hashCode());
		result = prime * result + ((ploUpdatedBy == null) ? 0 : ploUpdatedBy.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof CgtProjectLocationsDTO)) {
			return false;
		}
		CgtProjectLocationsDTO other = (CgtProjectLocationsDTO) obj;
		if (ploDateCreated == null) {
			if (other.ploDateCreated != null) {
				return false;
			}
		} else if (!ploDateCreated.equals(other.ploDateCreated)) {
			return false;
		}
		if (ploDateUpdated == null) {
			if (other.ploDateUpdated != null) {
				return false;
			}
		} else if (!ploDateUpdated.equals(other.ploDateUpdated)) {
			return false;
		}
		if (ploIsActive != other.ploIsActive) {
			return false;
		}
		if (ploIsNewLeased == null) {
			if (other.ploIsNewLeased != null) {
				return false;
			}
		} else if (!ploIsNewLeased.equals(other.ploIsNewLeased)) {
			return false;
		}
		if (ploName == null) {
			if (other.ploName != null) {
				return false;
			}
		} else if (!ploName.equals(other.ploName)) {
			return false;
		}
		if (ploOtherUcownedLeased == null) {
			if (other.ploOtherUcownedLeased != null) {
				return false;
			}
		} else if (!ploOtherUcownedLeased.equals(other.ploOtherUcownedLeased)) {
			return false;
		}
		if (ploProjectKey == null) {
			if (other.ploProjectKey != null) {
				return false;
			}
		} else if (!ploProjectKey.equals(other.ploProjectKey)) {
			return false;
		}
		if (ploProjectLocationKey != other.ploProjectLocationKey) {
			return false;
		}
		if (ploUcleased == null) {
			if (other.ploUcleased != null) {
				return false;
			}
		} else if (!ploUcleased.equals(other.ploUcleased)) {
			return false;
		}
		if (ploUcowned == null) {
			if (other.ploUcowned != null) {
				return false;
			}
		} else if (!ploUcowned.equals(other.ploUcowned)) {
			return false;
		}
		if (ploUpdatedBy == null) {
			if (other.ploUpdatedBy != null) {
				return false;
			}
		} else if (!ploUpdatedBy.equals(other.ploUpdatedBy)) {
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"CgtProjectLocationsImpl [ploProjectLocationKey=%s, ploProjectKey=%s, ploName=%s, ploUcowned=%s, ploUcleased=%s, ploOtherUcownedLeased=%s, ploIsNewLeased=%s, ploIsActive=%s, ploUpdatedBy=%s, ploDateCreated=%s, ploDateUpdated=%s]",
				ploProjectLocationKey, ploProjectKey, ploName, ploUcowned, ploUcleased, ploOtherUcownedLeased,
				ploIsNewLeased, ploIsActive, ploUpdatedBy, ploDateCreated, ploDateUpdated);
	}

	public CgtProjectLocationsDTO toDTO() {
		return new CgtProjectLocationsDTO(this);
	}
}
