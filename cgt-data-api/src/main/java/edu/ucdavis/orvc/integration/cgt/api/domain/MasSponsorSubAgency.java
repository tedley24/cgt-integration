package edu.ucdavis.orvc.integration.cgt.api.domain;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.MasSponsorSubAgencyDTO;

public interface MasSponsorSubAgency extends CgtBaseInterface<MasSponsorSubAgencyDTO> { 

	String getSuaCode();

	void setSuaCode(String suaCode);

	String getSuaName();

	void setSuaName(String suaName);

	String getSuaShortName();

	void setSuaShortName(String suaShortName);

	Boolean getSuaIsActive();

	void setSuaIsActive(Boolean suaIsActive);

	LocalDateTime getSuaDateCreated();

	void setSuaDateCreated(LocalDateTime suaDateCreated);

	LocalDateTime getSuaDateUpdated();

	void setSuaDateUpdated(LocalDateTime suaDateUpdated);

	String getSuaUpdatedBy();

	void setSuaUpdatedBy(String suaUpdatedBy);

}