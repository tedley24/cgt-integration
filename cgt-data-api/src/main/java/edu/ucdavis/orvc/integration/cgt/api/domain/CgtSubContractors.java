package edu.ucdavis.orvc.integration.cgt.api.domain;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtSubContractorsDTO;

public interface CgtSubContractors extends CgtBaseInterface<CgtSubContractorsDTO> { 

	String getScoSubContractorKey();

	void setScoSubContractorKey(String scoSubContractorKey);

	String getScoName();

	void setScoName(String scoName);

	Boolean getScoIsActive();

	void setScoIsActive(Boolean scoIsActive);

	String getScoUpdatedBy();

	void setScoUpdatedBy(String scoUpdatedBy);

	LocalDateTime getScoDateUpdated();

	void setScoDateUpdated(LocalDateTime scoDateUpdated);

	LocalDateTime getScoDateCreated();

	void setScoDateCreated(LocalDateTime scoDateCreated);

}