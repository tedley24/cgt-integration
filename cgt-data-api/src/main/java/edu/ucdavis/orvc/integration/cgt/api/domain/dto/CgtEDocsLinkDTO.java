package edu.ucdavis.orvc.integration.cgt.api.domain.dto;


import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtEDocsLink;



/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/


public class CgtEDocsLinkDTO implements CgtEDocsLink {

	private static final long serialVersionUID = -5347894845514805589L;
	private int edlEDocsLinkKey;
	private int edlFileKey;
	private String edlProjectKey;
	private Integer edlTransactionKey;
	private int edlEDocsTypeKey;
	private int edlVersion;
	private boolean edlIsActive;
	private LocalDateTime edlDateCreated;
	private LocalDateTime edlDateUpdated;
	private String edlUpdatedBy;

	public CgtEDocsLinkDTO(final CgtEDocsLink fromObj) {



	}

	public CgtEDocsLinkDTO() {
	}

	public CgtEDocsLinkDTO(final int edlEDocsLinkKey, final int edlFileKey, final String edlProjectKey, final int edlEDocsTypeKey, final int edlVersion,
			final boolean edlIsActive, final LocalDateTime edlDateCreated, final LocalDateTime edlDateUpdated, final String edlUpdatedBy) {
		this.edlEDocsLinkKey = edlEDocsLinkKey;
		this.edlFileKey = edlFileKey;
		this.edlProjectKey = edlProjectKey;
		this.edlEDocsTypeKey = edlEDocsTypeKey;
		this.edlVersion = edlVersion;
		this.edlIsActive = edlIsActive;
		this.edlDateCreated = edlDateCreated;
		this.edlDateUpdated = edlDateUpdated;
		this.edlUpdatedBy = edlUpdatedBy;
	}

	public CgtEDocsLinkDTO(final int edlEDocsLinkKey, final int edlFileKey, final String edlProjectKey, final Integer edlTransactionKey,
			final int edlEDocsTypeKey, final int edlVersion, final boolean edlIsActive, final LocalDateTime edlDateCreated, final LocalDateTime edlDateUpdated,
			final String edlUpdatedBy) {
		this.edlEDocsLinkKey = edlEDocsLinkKey;
		this.edlFileKey = edlFileKey;
		this.edlProjectKey = edlProjectKey;
		this.edlTransactionKey = edlTransactionKey;
		this.edlEDocsTypeKey = edlEDocsTypeKey;
		this.edlVersion = edlVersion;
		this.edlIsActive = edlIsActive;
		this.edlDateCreated = edlDateCreated;
		this.edlDateUpdated = edlDateUpdated;
		this.edlUpdatedBy = edlUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsLink#getEdlEDocsLinkKey()
	 */
	@Override
	

	
	public int getEdlEDocsLinkKey() {
		return this.edlEDocsLinkKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsLink#setEdlEDocsLinkKey(int)
	 */
	@Override
	public void setEdlEDocsLinkKey(final int edlEDocsLinkKey) {
		this.edlEDocsLinkKey = edlEDocsLinkKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsLink#getEdlFileKey()
	 */
	@Override
	
	public int getEdlFileKey() {
		return this.edlFileKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsLink#setEdlFileKey(int)
	 */
	@Override
	public void setEdlFileKey(final int edlFileKey) {
		this.edlFileKey = edlFileKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsLink#getEdlProjectKey()
	 */
	@Override
	
	public String getEdlProjectKey() {
		return this.edlProjectKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsLink#setEdlProjectKey(java.lang.String)
	 */
	@Override
	public void setEdlProjectKey(final String edlProjectKey) {
		this.edlProjectKey = edlProjectKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsLink#getEdlTransactionKey()
	 */
	@Override
	
	public Integer getEdlTransactionKey() {
		return this.edlTransactionKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsLink#setEdlTransactionKey(java.lang.Integer)
	 */
	@Override
	public void setEdlTransactionKey(final Integer edlTransactionKey) {
		this.edlTransactionKey = edlTransactionKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsLink#getEdlEDocsTypeKey()
	 */
	@Override
	
	public int getEdlEDocsTypeKey() {
		return this.edlEDocsTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsLink#setEdlEDocsTypeKey(int)
	 */
	@Override
	public void setEdlEDocsTypeKey(final int edlEDocsTypeKey) {
		this.edlEDocsTypeKey = edlEDocsTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsLink#getEdlVersion()
	 */
	@Override
	
	public int getEdlVersion() {
		return this.edlVersion;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsLink#setEdlVersion(int)
	 */
	@Override
	public void setEdlVersion(final int edlVersion) {
		this.edlVersion = edlVersion;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsLink#isEdlIsActive()
	 */
	@Override
	
	public boolean isEdlIsActive() {
		return this.edlIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsLink#setEdlIsActive(boolean)
	 */
	@Override
	public void setEdlIsActive(final boolean edlIsActive) {
		this.edlIsActive = edlIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsLink#getEdlDateCreated()
	 */
	@Override
	
	
	public LocalDateTime getEdlDateCreated() {
		return this.edlDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsLink#setEdlDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setEdlDateCreated(final LocalDateTime edlDateCreated) {
		this.edlDateCreated = edlDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsLink#getEdlDateUpdated()
	 */
	@Override
	
	
	public LocalDateTime getEdlDateUpdated() {
		return this.edlDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsLink#setEdlDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setEdlDateUpdated(final LocalDateTime edlDateUpdated) {
		this.edlDateUpdated = edlDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsLink#getEdlUpdatedBy()
	 */
	@Override
	
	public String getEdlUpdatedBy() {
		return this.edlUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsLink#setEdlUpdatedBy(java.lang.String)
	 */
	@Override
	public void setEdlUpdatedBy(final String edlUpdatedBy) {
		this.edlUpdatedBy = edlUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((edlDateCreated == null) ? 0 : edlDateCreated.hashCode());
		result = prime * result + ((edlDateUpdated == null) ? 0 : edlDateUpdated.hashCode());
		result = prime * result + edlEDocsLinkKey;
		result = prime * result + edlEDocsTypeKey;
		result = prime * result + edlFileKey;
		result = prime * result + (edlIsActive ? 1231 : 1237);
		result = prime * result + ((edlProjectKey == null) ? 0 : edlProjectKey.hashCode());
		result = prime * result + ((edlTransactionKey == null) ? 0 : edlTransactionKey.hashCode());
		result = prime * result + ((edlUpdatedBy == null) ? 0 : edlUpdatedBy.hashCode());
		result = prime * result + edlVersion;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof CgtEDocsLinkDTO)) {
			return false;
		}
		CgtEDocsLinkDTO other = (CgtEDocsLinkDTO) obj;
		if (edlDateCreated == null) {
			if (other.edlDateCreated != null) {
				return false;
			}
		} else if (!edlDateCreated.equals(other.edlDateCreated)) {
			return false;
		}
		if (edlDateUpdated == null) {
			if (other.edlDateUpdated != null) {
				return false;
			}
		} else if (!edlDateUpdated.equals(other.edlDateUpdated)) {
			return false;
		}
		if (edlEDocsLinkKey != other.edlEDocsLinkKey) {
			return false;
		}
		if (edlEDocsTypeKey != other.edlEDocsTypeKey) {
			return false;
		}
		if (edlFileKey != other.edlFileKey) {
			return false;
		}
		if (edlIsActive != other.edlIsActive) {
			return false;
		}
		if (edlProjectKey == null) {
			if (other.edlProjectKey != null) {
				return false;
			}
		} else if (!edlProjectKey.equals(other.edlProjectKey)) {
			return false;
		}
		if (edlTransactionKey == null) {
			if (other.edlTransactionKey != null) {
				return false;
			}
		} else if (!edlTransactionKey.equals(other.edlTransactionKey)) {
			return false;
		}
		if (edlUpdatedBy == null) {
			if (other.edlUpdatedBy != null) {
				return false;
			}
		} else if (!edlUpdatedBy.equals(other.edlUpdatedBy)) {
			return false;
		}
		if (edlVersion != other.edlVersion) {
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"CgtEDocsLinkImpl [edlEDocsLinkKey=%s, edlFileKey=%s, edlProjectKey=%s, edlTransactionKey=%s, edlEDocsTypeKey=%s, edlVersion=%s, edlIsActive=%s, edlDateCreated=%s, edlDateUpdated=%s, edlUpdatedBy=%s]",
				edlEDocsLinkKey, edlFileKey, edlProjectKey, edlTransactionKey, edlEDocsTypeKey, edlVersion, edlIsActive,
				edlDateCreated, edlDateUpdated, edlUpdatedBy);
	}

	public CgtEDocsLinkDTO toDTO() {
		return new CgtEDocsLinkDTO(this);
	}
}
