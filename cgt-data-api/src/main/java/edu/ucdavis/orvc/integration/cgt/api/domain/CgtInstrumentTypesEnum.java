package edu.ucdavis.orvc.integration.cgt.api.domain;

/*
 * 
 * 	1	Grant						Grant
	2	Contract					Contract
	3	Cooperative Agreement		Coop Agreement
	4	Gift						Gift
	5	Pre-Proposal				Pre Pro
	6	Non-Monetary Agreements 	No Money
 * 
 * 
 */

public enum CgtInstrumentTypesEnum {

	GRANT (1,	"Grant",	"Grant")
   ,CONTRACT (2, "Contract", "Contract")
   ,COOP_AGREEMENT(3,"Cooperative Agreement", "Cooperative Agreement")
   ,GIFT(4,"Gift", "Gift")
   ,PRE_PROPOSAL(5,"Pre-Proposal","Pre Pro")
   ,NON_MONETARY(6,"Non-Monetary Agreements","No Money");
	
	public int typeKey;
	public String shortName;
	public String name;
	
	private CgtInstrumentTypesEnum(int typeKey,String name, String shortName) {
		this.typeKey = typeKey;
		this.name = name;
		this.shortName = shortName;
	}

}
