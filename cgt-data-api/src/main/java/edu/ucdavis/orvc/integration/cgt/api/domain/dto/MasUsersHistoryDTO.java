package edu.ucdavis.orvc.integration.cgt.api.domain.dto;


import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.MasUsersHistory;



/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/


public class MasUsersHistoryDTO implements MasUsersHistory {

	private static final long serialVersionUID = 8560179378349880015L;

	private int id;
	private String useUserKey;
	private String useUcdlogin;
	private String useEmployeeId;
	private String useFirstName;
	private String useMiddleName;
	private String useLastName;
	private String useMothraId;
	private String useUserTitleKey;
	private String useDepartmentKey;
	private String usePhone;
	private String useFax;
	private String useEmail;
	private boolean useIsActive;
	private LocalDateTime useStartDate;
	private LocalDateTime useEndDate;
	private String useUpdatedBy;

	public MasUsersHistoryDTO(final MasUsersHistory fromObj) {



	}

	public MasUsersHistoryDTO() {
	}

	public MasUsersHistoryDTO(final int id, final String useUserKey, final String useFirstName, final String useLastName, final boolean useIsActive,
			final LocalDateTime useStartDate, final String useUpdatedBy) {
		this.id = id;
		this.useUserKey = useUserKey;
		this.useFirstName = useFirstName;
		this.useLastName = useLastName;
		this.useIsActive = useIsActive;
		this.useStartDate = useStartDate;
		this.useUpdatedBy = useUpdatedBy;
	}

	public MasUsersHistoryDTO(final int id, final String useUserKey, final String useUcdlogin, final String useEmployeeId, final String useFirstName,
			final String useMiddleName, final String useLastName, final String useMothraId, final String useUserTitleKey,
			final String useDepartmentKey, final String usePhone, final String useFax, final String useEmail, final boolean useIsActive,
			final LocalDateTime useStartDate, final LocalDateTime useEndDate, final String useUpdatedBy) {
		this.id = id;
		this.useUserKey = useUserKey;
		this.useUcdlogin = useUcdlogin;
		this.useEmployeeId = useEmployeeId;
		this.useFirstName = useFirstName;
		this.useMiddleName = useMiddleName;
		this.useLastName = useLastName;
		this.useMothraId = useMothraId;
		this.useUserTitleKey = useUserTitleKey;
		this.useDepartmentKey = useDepartmentKey;
		this.usePhone = usePhone;
		this.useFax = useFax;
		this.useEmail = useEmail;
		this.useIsActive = useIsActive;
		this.useStartDate = useStartDate;
		this.useEndDate = useEndDate;
		this.useUpdatedBy = useUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUsersHistory#getId()
	 */
	@Override
	

	
	public int getId() {
		return this.id;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUsersHistory#setId(int)
	 */
	@Override
	public void setId(final int id) {
		this.id = id;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUsersHistory#getUseUserKey()
	 */
	@Override
	
	public String getUseUserKey() {
		return this.useUserKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUsersHistory#setUseUserKey(java.lang.String)
	 */
	@Override
	public void setUseUserKey(final String useUserKey) {
		this.useUserKey = useUserKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUsersHistory#getUseUcdlogin()
	 */
	@Override
	
	public String getUseUcdlogin() {
		return this.useUcdlogin;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUsersHistory#setUseUcdlogin(java.lang.String)
	 */
	@Override
	public void setUseUcdlogin(final String useUcdlogin) {
		this.useUcdlogin = useUcdlogin;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUsersHistory#getUseEmployeeId()
	 */
	@Override
	
	public String getUseEmployeeId() {
		return this.useEmployeeId;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUsersHistory#setUseEmployeeId(java.lang.String)
	 */
	@Override
	public void setUseEmployeeId(final String useEmployeeId) {
		this.useEmployeeId = useEmployeeId;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUsersHistory#getUseFirstName()
	 */
	@Override
	
	public String getUseFirstName() {
		return this.useFirstName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUsersHistory#setUseFirstName(java.lang.String)
	 */
	@Override
	public void setUseFirstName(final String useFirstName) {
		this.useFirstName = useFirstName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUsersHistory#getUseMiddleName()
	 */
	@Override
	
	public String getUseMiddleName() {
		return this.useMiddleName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUsersHistory#setUseMiddleName(java.lang.String)
	 */
	@Override
	public void setUseMiddleName(final String useMiddleName) {
		this.useMiddleName = useMiddleName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUsersHistory#getUseLastName()
	 */
	@Override
	
	public String getUseLastName() {
		return this.useLastName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUsersHistory#setUseLastName(java.lang.String)
	 */
	@Override
	public void setUseLastName(final String useLastName) {
		this.useLastName = useLastName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUsersHistory#getUseMothraId()
	 */
	@Override
	
	public String getUseMothraId() {
		return this.useMothraId;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUsersHistory#setUseMothraId(java.lang.String)
	 */
	@Override
	public void setUseMothraId(final String useMothraId) {
		this.useMothraId = useMothraId;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUsersHistory#getUseUserTitleKey()
	 */
	@Override
	
	public String getUseUserTitleKey() {
		return this.useUserTitleKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUsersHistory#setUseUserTitleKey(java.lang.String)
	 */
	@Override
	public void setUseUserTitleKey(final String useUserTitleKey) {
		this.useUserTitleKey = useUserTitleKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUsersHistory#getUseDepartmentKey()
	 */
	@Override
	
	public String getUseDepartmentKey() {
		return this.useDepartmentKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUsersHistory#setUseDepartmentKey(java.lang.String)
	 */
	@Override
	public void setUseDepartmentKey(final String useDepartmentKey) {
		this.useDepartmentKey = useDepartmentKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUsersHistory#getUsePhone()
	 */
	@Override
	
	public String getUsePhone() {
		return this.usePhone;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUsersHistory#setUsePhone(java.lang.String)
	 */
	@Override
	public void setUsePhone(final String usePhone) {
		this.usePhone = usePhone;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUsersHistory#getUseFax()
	 */
	@Override
	
	public String getUseFax() {
		return this.useFax;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUsersHistory#setUseFax(java.lang.String)
	 */
	@Override
	public void setUseFax(final String useFax) {
		this.useFax = useFax;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUsersHistory#getUseEmail()
	 */
	@Override
	
	public String getUseEmail() {
		return this.useEmail;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUsersHistory#setUseEmail(java.lang.String)
	 */
	@Override
	public void setUseEmail(final String useEmail) {
		this.useEmail = useEmail;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUsersHistory#isUseIsActive()
	 */
	@Override
	
	public boolean isUseIsActive() {
		return this.useIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUsersHistory#setUseIsActive(boolean)
	 */
	@Override
	public void setUseIsActive(final boolean useIsActive) {
		this.useIsActive = useIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUsersHistory#getUseStartDate()
	 */
	@Override
	
	
	public LocalDateTime getUseStartDate() {
		return this.useStartDate;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUsersHistory#setUseStartDate(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setUseStartDate(final LocalDateTime useStartDate) {
		this.useStartDate = useStartDate;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUsersHistory#getUseEndDate()
	 */
	@Override
	
	
	public LocalDateTime getUseEndDate() {
		return this.useEndDate;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUsersHistory#setUseEndDate(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setUseEndDate(final LocalDateTime useEndDate) {
		this.useEndDate = useEndDate;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUsersHistory#getUseUpdatedBy()
	 */
	@Override
	
	public String getUseUpdatedBy() {
		return this.useUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUsersHistory#setUseUpdatedBy(java.lang.String)
	 */
	@Override
	public void setUseUpdatedBy(final String useUpdatedBy) {
		this.useUpdatedBy = useUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + id;
		result = prime * result + ((useDepartmentKey == null) ? 0 : useDepartmentKey.hashCode());
		result = prime * result + ((useEmail == null) ? 0 : useEmail.hashCode());
		result = prime * result + ((useEmployeeId == null) ? 0 : useEmployeeId.hashCode());
		result = prime * result + ((useEndDate == null) ? 0 : useEndDate.hashCode());
		result = prime * result + ((useFax == null) ? 0 : useFax.hashCode());
		result = prime * result + ((useFirstName == null) ? 0 : useFirstName.hashCode());
		result = prime * result + (useIsActive ? 1231 : 1237);
		result = prime * result + ((useLastName == null) ? 0 : useLastName.hashCode());
		result = prime * result + ((useMiddleName == null) ? 0 : useMiddleName.hashCode());
		result = prime * result + ((useMothraId == null) ? 0 : useMothraId.hashCode());
		result = prime * result + ((usePhone == null) ? 0 : usePhone.hashCode());
		result = prime * result + ((useStartDate == null) ? 0 : useStartDate.hashCode());
		result = prime * result + ((useUcdlogin == null) ? 0 : useUcdlogin.hashCode());
		result = prime * result + ((useUpdatedBy == null) ? 0 : useUpdatedBy.hashCode());
		result = prime * result + ((useUserKey == null) ? 0 : useUserKey.hashCode());
		result = prime * result + ((useUserTitleKey == null) ? 0 : useUserTitleKey.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof MasUsersHistoryDTO))
			return false;
		final MasUsersHistoryDTO other = (MasUsersHistoryDTO) obj;
		if (id != other.id)
			return false;
		if (useDepartmentKey == null) {
			if (other.useDepartmentKey != null)
				return false;
		} else if (!useDepartmentKey.equals(other.useDepartmentKey))
			return false;
		if (useEmail == null) {
			if (other.useEmail != null)
				return false;
		} else if (!useEmail.equals(other.useEmail))
			return false;
		if (useEmployeeId == null) {
			if (other.useEmployeeId != null)
				return false;
		} else if (!useEmployeeId.equals(other.useEmployeeId))
			return false;
		if (useEndDate == null) {
			if (other.useEndDate != null)
				return false;
		} else if (!useEndDate.equals(other.useEndDate))
			return false;
		if (useFax == null) {
			if (other.useFax != null)
				return false;
		} else if (!useFax.equals(other.useFax))
			return false;
		if (useFirstName == null) {
			if (other.useFirstName != null)
				return false;
		} else if (!useFirstName.equals(other.useFirstName))
			return false;
		if (useIsActive != other.useIsActive)
			return false;
		if (useLastName == null) {
			if (other.useLastName != null)
				return false;
		} else if (!useLastName.equals(other.useLastName))
			return false;
		if (useMiddleName == null) {
			if (other.useMiddleName != null)
				return false;
		} else if (!useMiddleName.equals(other.useMiddleName))
			return false;
		if (useMothraId == null) {
			if (other.useMothraId != null)
				return false;
		} else if (!useMothraId.equals(other.useMothraId))
			return false;
		if (usePhone == null) {
			if (other.usePhone != null)
				return false;
		} else if (!usePhone.equals(other.usePhone))
			return false;
		if (useStartDate == null) {
			if (other.useStartDate != null)
				return false;
		} else if (!useStartDate.equals(other.useStartDate))
			return false;
		if (useUcdlogin == null) {
			if (other.useUcdlogin != null)
				return false;
		} else if (!useUcdlogin.equals(other.useUcdlogin))
			return false;
		if (useUpdatedBy == null) {
			if (other.useUpdatedBy != null)
				return false;
		} else if (!useUpdatedBy.equals(other.useUpdatedBy))
			return false;
		if (useUserKey == null) {
			if (other.useUserKey != null)
				return false;
		} else if (!useUserKey.equals(other.useUserKey))
			return false;
		if (useUserTitleKey == null) {
			if (other.useUserTitleKey != null)
				return false;
		} else if (!useUserTitleKey.equals(other.useUserTitleKey))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"MasUsersHistory [id=%s, useUserKey=%s, useUcdlogin=%s, useEmployeeId=%s, useFirstName=%s, useMiddleName=%s, useLastName=%s, useMothraId=%s, useUserTitleKey=%s, useDepartmentKey=%s, usePhone=%s, useFax=%s, useEmail=%s, useIsActive=%s, useStartDate=%s, useEndDate=%s, useUpdatedBy=%s]",
				id, useUserKey, useUcdlogin, useEmployeeId, useFirstName, useMiddleName, useLastName, useMothraId,
				useUserTitleKey, useDepartmentKey, usePhone, useFax, useEmail, useIsActive, useStartDate, useEndDate,
				useUpdatedBy);
	}

	public MasUsersHistoryDTO toDTO() {
		return new MasUsersHistoryDTO(this);
	}
}
