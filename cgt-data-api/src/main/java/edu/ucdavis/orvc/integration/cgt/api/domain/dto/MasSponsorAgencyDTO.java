package edu.ucdavis.orvc.integration.cgt.api.domain.dto;


import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.MasSponsorAgency;



/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/


public class MasSponsorAgencyDTO implements MasSponsorAgency {

	private static final long serialVersionUID = -5731159688423120697L;
	private String spaSponsorAgencyKey;
	private String spaLongName;
	private String spaShortName;
	private Boolean spaIsActive;
	private LocalDateTime spaDateCreated;
	private LocalDateTime spaDateUpdated;
	private String spaUpdatedBy;

	public MasSponsorAgencyDTO(final MasSponsorAgency fromObj) {



	}

	public MasSponsorAgencyDTO() {
	}

	public MasSponsorAgencyDTO(final String spaSponsorAgencyKey, final String spaLongName, final String spaShortName) {
		this.spaSponsorAgencyKey = spaSponsorAgencyKey;
		this.spaLongName = spaLongName;
		this.spaShortName = spaShortName;
	}

	public MasSponsorAgencyDTO(final String spaSponsorAgencyKey, final String spaLongName, final String spaShortName, final Boolean spaIsActive,
			final LocalDateTime spaDateCreated, final LocalDateTime spaDateUpdated, final String spaUpdatedBy) {
		this.spaSponsorAgencyKey = spaSponsorAgencyKey;
		this.spaLongName = spaLongName;
		this.spaShortName = spaShortName;
		this.spaIsActive = spaIsActive;
		this.spaDateCreated = spaDateCreated;
		this.spaDateUpdated = spaDateUpdated;
		this.spaUpdatedBy = spaUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorAgency#getSpaSponsorAgencyKey()
	 */
	@Override
	

	
	public String getSpaSponsorAgencyKey() {
		return this.spaSponsorAgencyKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorAgency#setSpaSponsorAgencyKey(java.lang.String)
	 */
	@Override
	public void setSpaSponsorAgencyKey(final String spaSponsorAgencyKey) {
		this.spaSponsorAgencyKey = spaSponsorAgencyKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorAgency#getSpaLongName()
	 */
	@Override
	
	public String getSpaLongName() {
		return this.spaLongName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorAgency#setSpaLongName(java.lang.String)
	 */
	@Override
	public void setSpaLongName(final String spaLongName) {
		this.spaLongName = spaLongName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorAgency#getSpaShortName()
	 */
	@Override
	
	public String getSpaShortName() {
		return this.spaShortName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorAgency#setSpaShortName(java.lang.String)
	 */
	@Override
	public void setSpaShortName(final String spaShortName) {
		this.spaShortName = spaShortName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorAgency#getSpaIsActive()
	 */
	@Override
	
	public Boolean getSpaIsActive() {
		return this.spaIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorAgency#setSpaIsActive(java.lang.Boolean)
	 */
	@Override
	public void setSpaIsActive(final Boolean spaIsActive) {
		this.spaIsActive = spaIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorAgency#getSpaDateCreated()
	 */
	@Override
	
	
	public LocalDateTime getSpaDateCreated() {
		return this.spaDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorAgency#setSpaDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setSpaDateCreated(final LocalDateTime spaDateCreated) {
		this.spaDateCreated = spaDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorAgency#getSpaDateUpdated()
	 */
	@Override
	
	
	public LocalDateTime getSpaDateUpdated() {
		return this.spaDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorAgency#setSpaDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setSpaDateUpdated(final LocalDateTime spaDateUpdated) {
		this.spaDateUpdated = spaDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorAgency#getSpaUpdatedBy()
	 */
	@Override
	
	public String getSpaUpdatedBy() {
		return this.spaUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorAgency#setSpaUpdatedBy(java.lang.String)
	 */
	@Override
	public void setSpaUpdatedBy(final String spaUpdatedBy) {
		this.spaUpdatedBy = spaUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((spaDateCreated == null) ? 0 : spaDateCreated.hashCode());
		result = prime * result + ((spaDateUpdated == null) ? 0 : spaDateUpdated.hashCode());
		result = prime * result + ((spaIsActive == null) ? 0 : spaIsActive.hashCode());
		result = prime * result + ((spaLongName == null) ? 0 : spaLongName.hashCode());
		result = prime * result + ((spaShortName == null) ? 0 : spaShortName.hashCode());
		result = prime * result + ((spaSponsorAgencyKey == null) ? 0 : spaSponsorAgencyKey.hashCode());
		result = prime * result + ((spaUpdatedBy == null) ? 0 : spaUpdatedBy.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof MasSponsorAgencyDTO))
			return false;
		final MasSponsorAgencyDTO other = (MasSponsorAgencyDTO) obj;
		if (spaDateCreated == null) {
			if (other.spaDateCreated != null)
				return false;
		} else if (!spaDateCreated.equals(other.spaDateCreated))
			return false;
		if (spaDateUpdated == null) {
			if (other.spaDateUpdated != null)
				return false;
		} else if (!spaDateUpdated.equals(other.spaDateUpdated))
			return false;
		if (spaIsActive == null) {
			if (other.spaIsActive != null)
				return false;
		} else if (!spaIsActive.equals(other.spaIsActive))
			return false;
		if (spaLongName == null) {
			if (other.spaLongName != null)
				return false;
		} else if (!spaLongName.equals(other.spaLongName))
			return false;
		if (spaShortName == null) {
			if (other.spaShortName != null)
				return false;
		} else if (!spaShortName.equals(other.spaShortName))
			return false;
		if (spaSponsorAgencyKey == null) {
			if (other.spaSponsorAgencyKey != null)
				return false;
		} else if (!spaSponsorAgencyKey.equals(other.spaSponsorAgencyKey))
			return false;
		if (spaUpdatedBy == null) {
			if (other.spaUpdatedBy != null)
				return false;
		} else if (!spaUpdatedBy.equals(other.spaUpdatedBy))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"MasSponsorAgencys [spaSponsorAgencyKey=%s, spaLongName=%s, spaShortName=%s, spaIsActive=%s, spaDateCreated=%s, spaDateUpdated=%s, spaUpdatedBy=%s]",
				spaSponsorAgencyKey, spaLongName, spaShortName, spaIsActive, spaDateCreated, spaDateUpdated,
				spaUpdatedBy);
	}

	public MasSponsorAgencyDTO toDTO() {
		return new MasSponsorAgencyDTO(this);
	}
}
