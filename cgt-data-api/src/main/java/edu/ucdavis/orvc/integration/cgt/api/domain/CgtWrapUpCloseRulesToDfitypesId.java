package edu.ucdavis.orvc.integration.cgt.api.domain;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtWrapUpCloseRulesToDfitypesIdDTO;

public interface CgtWrapUpCloseRulesToDfitypesId extends CgtBaseInterface<CgtWrapUpCloseRulesToDfitypesIdDTO> { 

	int getW2dCloseRuleKey();

	void setW2dCloseRuleKey(int w2dCloseRuleKey);

	int getW2dDfiTypeKey();

	void setW2dDfiTypeKey(int w2dDfiTypeKey);

}