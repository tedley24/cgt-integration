package edu.ucdavis.orvc.integration.cgt.api.domain;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtAdministeringUnitDTO;

public interface CgtAdministeringUnit extends CgtBaseInterface<CgtAdministeringUnitDTO> {

	int getAduAdministeringUnitKey();

	void setAduAdministeringUnitKey(int aduAdministeringUnitKey);

	String getAduDepartmentKey();

	void setAduDepartmentKey(String aduDepartmentKey);

	Integer getAduUnitRoleKey();

	void setAduUnitRoleKey(Integer aduUnitRoleKey);

	String getAduProjectKey();

	void setAduProjectKey(String aduProjectKey);

	String getAduOpaccount();

	void setAduOpaccount(String aduOpaccount);

	boolean isAduIsActive();

	void setAduIsActive(boolean aduIsActive);

	String getAduUpdatedBy();

	void setAduUpdatedBy(String aduUpdatedBy);

	LocalDateTime getAduDateUpdated();

	void setAduDateUpdated(LocalDateTime aduDateUpdated);

	LocalDateTime getAduDateCreated();

	void setAduDateCreated(LocalDateTime aduDateCreated);

	/**
	 * @return the department
	 */

	MasDepartment getDepartment();

	/**
	 * @param department the department to set
	 */
	void setDepartment(MasDepartment department);

	/**
	 * @return the administeringUnitRole
	 */
	CgtAdministeringUnitRole getAdministeringUnitRole();

	/**
	 * @param administeringUnitRole the administeringUnitRole to set
	 */
	void setAdministeringUnitRole(CgtAdministeringUnitRole administeringUnitRole);

	/**
	 * @return the project
	 */
	CgtProject getProject();

	/**
	 * @param project the project to set
	 */
	void setProject(CgtProject project);

}