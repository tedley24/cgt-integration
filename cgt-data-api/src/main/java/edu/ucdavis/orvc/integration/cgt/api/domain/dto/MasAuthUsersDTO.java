package edu.ucdavis.orvc.integration.cgt.api.domain.dto;


import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.MasAuthUsers;



/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/


public class MasAuthUsersDTO implements MasAuthUsers {

	private static final long serialVersionUID = -5288208246601083443L;
	private int auuAuthUserKey;
	private String auuUserName;
	private String auuIpaddress;
	private String auuDataSourceKey;
	private LocalDateTime auuDateCreated;
	private LocalDateTime auuDateUpdated;
	private String auuUpdatedBy;

	public MasAuthUsersDTO(final MasAuthUsers fromObj) {



	}

	public MasAuthUsersDTO() {
	}

	public MasAuthUsersDTO(final int auuAuthUserKey, final String auuUserName, final String auuIpaddress, final String auuDataSourceKey,
			final LocalDateTime auuDateCreated, final LocalDateTime auuDateUpdated, final String auuUpdatedBy) {
		this.auuAuthUserKey = auuAuthUserKey;
		this.auuUserName = auuUserName;
		this.auuIpaddress = auuIpaddress;
		this.auuDataSourceKey = auuDataSourceKey;
		this.auuDateCreated = auuDateCreated;
		this.auuDateUpdated = auuDateUpdated;
		this.auuUpdatedBy = auuUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasAuthUsers#getAuuAuthUserKey()
	 */
	@Override
	

	
	public int getAuuAuthUserKey() {
		return this.auuAuthUserKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasAuthUsers#setAuuAuthUserKey(int)
	 */
	@Override
	public void setAuuAuthUserKey(final int auuAuthUserKey) {
		this.auuAuthUserKey = auuAuthUserKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasAuthUsers#getAuuUserName()
	 */
	@Override
	
	public String getAuuUserName() {
		return this.auuUserName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasAuthUsers#setAuuUserName(java.lang.String)
	 */
	@Override
	public void setAuuUserName(final String auuUserName) {
		this.auuUserName = auuUserName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasAuthUsers#getAuuIpaddress()
	 */
	@Override
	
	public String getAuuIpaddress() {
		return this.auuIpaddress;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasAuthUsers#setAuuIpaddress(java.lang.String)
	 */
	@Override
	public void setAuuIpaddress(final String auuIpaddress) {
		this.auuIpaddress = auuIpaddress;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasAuthUsers#getAuuDataSourceKey()
	 */
	@Override
	
	public String getAuuDataSourceKey() {
		return this.auuDataSourceKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasAuthUsers#setAuuDataSourceKey(java.lang.String)
	 */
	@Override
	public void setAuuDataSourceKey(final String auuDataSourceKey) {
		this.auuDataSourceKey = auuDataSourceKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasAuthUsers#getAuuDateCreated()
	 */
	@Override
	
	
	public LocalDateTime getAuuDateCreated() {
		return this.auuDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasAuthUsers#setAuuDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setAuuDateCreated(final LocalDateTime auuDateCreated) {
		this.auuDateCreated = auuDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasAuthUsers#getAuuDateUpdated()
	 */
	@Override
	
	
	public LocalDateTime getAuuDateUpdated() {
		return this.auuDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasAuthUsers#setAuuDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setAuuDateUpdated(final LocalDateTime auuDateUpdated) {
		this.auuDateUpdated = auuDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasAuthUsers#getAuuUpdatedBy()
	 */
	@Override
	
	public String getAuuUpdatedBy() {
		return this.auuUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasAuthUsers#setAuuUpdatedBy(java.lang.String)
	 */
	@Override
	public void setAuuUpdatedBy(final String auuUpdatedBy) {
		this.auuUpdatedBy = auuUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + auuAuthUserKey;
		result = prime * result + ((auuDataSourceKey == null) ? 0 : auuDataSourceKey.hashCode());
		result = prime * result + ((auuDateCreated == null) ? 0 : auuDateCreated.hashCode());
		result = prime * result + ((auuDateUpdated == null) ? 0 : auuDateUpdated.hashCode());
		result = prime * result + ((auuIpaddress == null) ? 0 : auuIpaddress.hashCode());
		result = prime * result + ((auuUpdatedBy == null) ? 0 : auuUpdatedBy.hashCode());
		result = prime * result + ((auuUserName == null) ? 0 : auuUserName.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof MasAuthUsersDTO))
			return false;
		final MasAuthUsersDTO other = (MasAuthUsersDTO) obj;
		if (auuAuthUserKey != other.auuAuthUserKey)
			return false;
		if (auuDataSourceKey == null) {
			if (other.auuDataSourceKey != null)
				return false;
		} else if (!auuDataSourceKey.equals(other.auuDataSourceKey))
			return false;
		if (auuDateCreated == null) {
			if (other.auuDateCreated != null)
				return false;
		} else if (!auuDateCreated.equals(other.auuDateCreated))
			return false;
		if (auuDateUpdated == null) {
			if (other.auuDateUpdated != null)
				return false;
		} else if (!auuDateUpdated.equals(other.auuDateUpdated))
			return false;
		if (auuIpaddress == null) {
			if (other.auuIpaddress != null)
				return false;
		} else if (!auuIpaddress.equals(other.auuIpaddress))
			return false;
		if (auuUpdatedBy == null) {
			if (other.auuUpdatedBy != null)
				return false;
		} else if (!auuUpdatedBy.equals(other.auuUpdatedBy))
			return false;
		if (auuUserName == null) {
			if (other.auuUserName != null)
				return false;
		} else if (!auuUserName.equals(other.auuUserName))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"MasAuthUsers [auuAuthUserKey=%s, auuUserName=%s, auuIpaddress=%s, auuDataSourceKey=%s, auuDateCreated=%s, auuDateUpdated=%s, auuUpdatedBy=%s]",
				auuAuthUserKey, auuUserName, auuIpaddress, auuDataSourceKey, auuDateCreated, auuDateUpdated,
				auuUpdatedBy);
	}

	public MasAuthUsersDTO toDTO() {
		return new MasAuthUsersDTO(this);
	}
}
