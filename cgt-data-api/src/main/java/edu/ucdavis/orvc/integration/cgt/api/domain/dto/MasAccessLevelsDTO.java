package edu.ucdavis.orvc.integration.cgt.api.domain.dto;


import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.MasAccessLevels;



/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/


public class MasAccessLevelsDTO implements MasAccessLevels {

	private static final long serialVersionUID = -7177581840087709248L;
	private int aclAccessLevelKey;
	private String aclDataSourceKey;
	private String aclUserKey;
	private LocalDateTime aclDateCreated;
	private LocalDateTime aclDateUpdated;
	private String aclUdatedBy;

	public MasAccessLevelsDTO(final MasAccessLevels fromObj) {



	}

	public MasAccessLevelsDTO() {
	}

	public MasAccessLevelsDTO(final int aclAccessLevelKey, final String aclDataSourceKey, final String aclUserKey, final LocalDateTime aclDateCreated,
			final LocalDateTime aclDateUpdated, final String aclUdatedBy) {
		this.aclAccessLevelKey = aclAccessLevelKey;
		this.aclDataSourceKey = aclDataSourceKey;
		this.aclUserKey = aclUserKey;
		this.aclDateCreated = aclDateCreated;
		this.aclDateUpdated = aclDateUpdated;
		this.aclUdatedBy = aclUdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasAccessLevels#getAclAccessLevelKey()
	 */
	@Override
	

	
	public int getAclAccessLevelKey() {
		return this.aclAccessLevelKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasAccessLevels#setAclAccessLevelKey(int)
	 */
	@Override
	public void setAclAccessLevelKey(final int aclAccessLevelKey) {
		this.aclAccessLevelKey = aclAccessLevelKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasAccessLevels#getAclDataSourceKey()
	 */
	@Override
	
	public String getAclDataSourceKey() {
		return this.aclDataSourceKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasAccessLevels#setAclDataSourceKey(java.lang.String)
	 */
	@Override
	public void setAclDataSourceKey(final String aclDataSourceKey) {
		this.aclDataSourceKey = aclDataSourceKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasAccessLevels#getAclUserKey()
	 */
	@Override
	
	public String getAclUserKey() {
		return this.aclUserKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasAccessLevels#setAclUserKey(java.lang.String)
	 */
	@Override
	public void setAclUserKey(final String aclUserKey) {
		this.aclUserKey = aclUserKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasAccessLevels#getAclDateCreated()
	 */
	@Override
	
	
	public LocalDateTime getAclDateCreated() {
		return this.aclDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasAccessLevels#setAclDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setAclDateCreated(final LocalDateTime aclDateCreated) {
		this.aclDateCreated = aclDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasAccessLevels#getAclDateUpdated()
	 */
	@Override
	
	
	public LocalDateTime getAclDateUpdated() {
		return this.aclDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasAccessLevels#setAclDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setAclDateUpdated(final LocalDateTime aclDateUpdated) {
		this.aclDateUpdated = aclDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasAccessLevels#getAclUdatedBy()
	 */
	@Override
	
	public String getAclUdatedBy() {
		return this.aclUdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasAccessLevels#setAclUdatedBy(java.lang.String)
	 */
	@Override
	public void setAclUdatedBy(final String aclUdatedBy) {
		this.aclUdatedBy = aclUdatedBy;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + aclAccessLevelKey;
		result = prime * result + ((aclDataSourceKey == null) ? 0 : aclDataSourceKey.hashCode());
		result = prime * result + ((aclDateCreated == null) ? 0 : aclDateCreated.hashCode());
		result = prime * result + ((aclDateUpdated == null) ? 0 : aclDateUpdated.hashCode());
		result = prime * result + ((aclUdatedBy == null) ? 0 : aclUdatedBy.hashCode());
		result = prime * result + ((aclUserKey == null) ? 0 : aclUserKey.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof MasAccessLevelsDTO))
			return false;
		final MasAccessLevelsDTO other = (MasAccessLevelsDTO) obj;
		if (aclAccessLevelKey != other.aclAccessLevelKey)
			return false;
		if (aclDataSourceKey == null) {
			if (other.aclDataSourceKey != null)
				return false;
		} else if (!aclDataSourceKey.equals(other.aclDataSourceKey))
			return false;
		if (aclDateCreated == null) {
			if (other.aclDateCreated != null)
				return false;
		} else if (!aclDateCreated.equals(other.aclDateCreated))
			return false;
		if (aclDateUpdated == null) {
			if (other.aclDateUpdated != null)
				return false;
		} else if (!aclDateUpdated.equals(other.aclDateUpdated))
			return false;
		if (aclUdatedBy == null) {
			if (other.aclUdatedBy != null)
				return false;
		} else if (!aclUdatedBy.equals(other.aclUdatedBy))
			return false;
		if (aclUserKey == null) {
			if (other.aclUserKey != null)
				return false;
		} else if (!aclUserKey.equals(other.aclUserKey))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"MasAccessLevels [aclAccessLevelKey=%s, aclDataSourceKey=%s, aclUserKey=%s, aclDateCreated=%s, aclDateUpdated=%s, aclUdatedBy=%s]",
				aclAccessLevelKey, aclDataSourceKey, aclUserKey, aclDateCreated, aclDateUpdated, aclUdatedBy);
	}

	public MasAccessLevelsDTO toDTO() {
		return new MasAccessLevelsDTO(this);
	}
}
