package edu.ucdavis.orvc.integration.cgt.api.domain;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtSponsorDTO;

public interface CgtSponsor extends CgtBaseInterface<CgtSponsorDTO> { 

	int getPspProjectSponsorKey();

	void setPspProjectSponsorKey(int pspProjectSponsorKey);

	String getPspSponsorKey();

	void setPspSponsorKey(String pspSponsorKey);

	String getPspProjectKey();

	void setPspProjectKey(String pspProjectKey);

	Integer getPspSponsorRoleKey();

	void setPspSponsorRoleKey(Integer pspSponsorRoleKey);

	String getPspCfda();

	void setPspCfda(String pspCfda);

	String getPspOfficeCode();

	void setPspOfficeCode(String pspOfficeCode);

	String getPspMisSponsorName();

	void setPspMisSponsorName(String pspMisSponsorName);

	String getPspProgramName();

	void setPspProgramName(String pspProgramName);

	String getZPspRootAwardNumber();

	void setZPspRootAwardNumber(String ZPspRootAwardNumber);

	String getPspAddress();

	void setPspAddress(String pspAddress);

	String getPspContactName();

	void setPspContactName(String pspContactName);

	String getPspPhone();

	void setPspPhone(String pspPhone);

	String getPspFax();

	void setPspFax(String pspFax);

	String getPspEmail();

	void setPspEmail(String pspEmail);

	boolean isPspIsActive();

	void setPspIsActive(boolean pspIsActive);

	String getPspUpdatedBy();

	void setPspUpdatedBy(String pspUpdatedBy);

	LocalDateTime getPspDateCreated();

	void setPspDateCreated(LocalDateTime pspDateCreated);

	LocalDateTime getPspDateUpdated();

	void setPspDateUpdated(LocalDateTime pspDateUpdated);

	/**
	 * @return the project
	 */
	CgtProject getProject();

	/**
	 * @param project the project to set
	 */
	void setProject(CgtProject project);

	/**
	 * @return the sponsor
	 */
	MasSponsor getMasSponsor();

	/**
	 * @param sponsor the sponsor to set
	 */
	void setMasSponsor(MasSponsor sponsor);

	/**
	 * @return the sponsorRoleType
	 */
	CgtSponsorRoleType getSponsorRoleType();

	/**
	 * @param sponsorRoleType the sponsorRoleType to set
	 */
	void setSponsorRoleType(CgtSponsorRoleType sponsorRoleType);

}