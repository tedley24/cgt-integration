package edu.ucdavis.orvc.integration.cgt.api.domain;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtStopWordsDTO;

public interface CgtStopWords extends CgtBaseInterface<CgtStopWordsDTO> { 

	String getWord();

	void setWord(String word);

}