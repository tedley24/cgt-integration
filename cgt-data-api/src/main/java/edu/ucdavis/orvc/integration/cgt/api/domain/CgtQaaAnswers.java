package edu.ucdavis.orvc.integration.cgt.api.domain;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtQaaAnswersDTO;

public interface CgtQaaAnswers extends CgtBaseInterface<CgtQaaAnswersDTO> { 

	int getAnsAnswerKey();

	void setAnsAnswerKey(int ansAnswerKey);

	int getAnsQuestionKey();

	void setAnsQuestionKey(int ansQuestionKey);

	String getAnsRecordKey();

	void setAnsRecordKey(String ansRecordKey);

	String getAnsAnswer();

	void setAnsAnswer(String ansAnswer);

	String getAnsAdditionalAnswer();

	void setAnsAdditionalAnswer(String ansAdditionalAnswer);

	boolean isAnsIsActive();

	void setAnsIsActive(boolean ansIsActive);

	LocalDateTime getAnsDateCreated();

	void setAnsDateCreated(LocalDateTime ansDateCreated);

	LocalDateTime getAnsDateUpdated();

	void setAnsDateUpdated(LocalDateTime ansDateUpdated);

	String getAnsUpdatedBy();

	void setAnsUpdatedBy(String ansUpdatedBy);

}