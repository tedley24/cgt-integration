package edu.ucdavis.orvc.integration.cgt.api.domain;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.MasSponsorCategoryDTO;

public interface MasSponsorCategory extends CgtBaseInterface<MasSponsorCategoryDTO> { 

	String getSpcSponsorCategoryKey();

	void setSpcSponsorCategoryKey(String spcSponsorCategoryKey);

	String getSpcName();

	void setSpcName(String spcName);

	boolean isSpcIsActive();

	void setSpcIsActive(boolean spcIsActive);

	LocalDateTime getSpcDateCreated();

	void setSpcDateCreated(LocalDateTime spcDateCreated);

	LocalDateTime getSpcDateUpdated();

	void setSpcDateUpdated(LocalDateTime spcDateUpdated);

	String getSpcUpdatedBy();

	void setSpcUpdatedBy(String spcUpdatedBy);

}