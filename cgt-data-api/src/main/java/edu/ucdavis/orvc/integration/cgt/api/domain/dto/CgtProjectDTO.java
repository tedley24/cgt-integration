package edu.ucdavis.orvc.integration.cgt.api.domain.dto;


import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtAdministeringUnit;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtAdministeringUnitRoleTypesEnum;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtInstrumentType;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtKeyPersonnel;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtProject;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtProjectType;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtSponsor;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtSponsorRoleTypesEnum;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtTeam;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtTransaction;



/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/


public class CgtProjectDTO implements CgtProject {

	private static final long serialVersionUID = -5405613783702533995L;

	private String proProjectKey;
	private Integer proProjectTypeKey;
	private String proParentProjectKey;
	private LocalDateTime proSponsorSubmissionDate;
	private LocalDateTime proProcessingDueDate;
	private Integer proProjectReceivedTypeKey;
	private String proReceivedFrom;
	private LocalDateTime proDateTimeReceived;
	private String proTitle;
	private int proResponsibleUnitKey;
	private Integer proTeamKey;
	private LocalDateTime proDateSigned;
	private LocalDate proAwardStart;
	private LocalDate proAwardEnd;
	private LocalDate proProposedStart;
	private LocalDate proProposedEnd;
	private String proProjectLeadUserKey;
	private String proAwardLeadUserKey;
	private Integer proInstrumentTypeKey;
	private String proCloseoutbox;
	private LocalDate proCloseoutDate;
	private Integer proCostSharingTypeKey;
	private Integer proDfitypeKey;
	private String proRootAwardNumber;
	private String proOrderNumber;
	private String proFundingAgencyCode;
	private String proAwardingAgencyCode;
	private String proTas;
	private String proPrimeDuns;
	private String proPrimeAwardNumber;
	private Integer proPrimeInstrumentType;
	private Boolean proIsDelegatedtoUcd;
	private Boolean proIsProjectAccepted;
	private Boolean proIsActive;
	private String proUpdatedBy;
	private LocalDateTime proDateCreated;
	private LocalDateTime proDateUpdated;

	
	
	//Collections
	private List<CgtTransactionDTO> transactions;
	private CgtProjectTypeDTO projectType;
	private CgtTeamDTO team;
	private List<CgtAdministeringUnitDTO> administeringUnits;
	private List<CgtSponsorDTO> projectSponsor;
	private CgtInstrumentTypeDTO instrumentType;
	private CgtInstrumentTypeDTO primeInstrumentType;
	private List<CgtKeyPersonnelDTO> keyPersonnel;
	
	
	public CgtProjectDTO() {
	}

	public CgtProjectDTO(final String proProjectKey, final String proTitle, final int proResponsibleUnitKey, final boolean proIsActive,
			final String proUpdatedBy, final LocalDateTime proDateCreated, final LocalDateTime proDateUpdated) {
		this.proProjectKey = proProjectKey;
		this.proTitle = proTitle;
		this.proResponsibleUnitKey = proResponsibleUnitKey;
		this.proIsActive = proIsActive;
		this.proUpdatedBy = proUpdatedBy;
		this.proDateCreated = proDateCreated;
		this.proDateUpdated = proDateUpdated;
	}

	
	
	public CgtProjectDTO(final String proProjectKey, final Integer proProjectTypeKey, final String proParentProjectKey,
			final LocalDateTime proSponsorSubmissionDate, final LocalDateTime proProcessingDueDate, final Integer proProjectReceivedTypeKey,
			final String proReceivedFrom, final LocalDateTime proDateTimeReceived, final String proTitle, final int proResponsibleUnitKey,
			final Integer proTeamKey, final LocalDateTime proDateSigned, final LocalDate proAwardStart, final LocalDate proAwardEnd, final LocalDate proProposedStart,
			final LocalDate proProposedEnd, final String proProjectLeadUserKey, final String proAwardLeadUserKey, final Integer proInstrumentTypeKey,
			final String proCloseoutbox, final LocalDate proCloseoutDate, final Integer proCostSharingTypeKey, final Integer proDfitypeKey,
			final String proRootAwardNumber, final String proOrderNumber, final String proFundingAgencyCode, final String proAwardingAgencyCode,
			final String proTas, final String proPrimeDuns, final String proPrimeAwardNumber, final Integer proPrimeInstrumentType,
			final Boolean proIsDelegatedtoUcd, final Boolean proIsProjectAccepted, final Boolean proIsActive, final String proUpdatedBy,
			final LocalDateTime proDateCreated, final LocalDateTime proDateUpdated) {
		this.proProjectKey = proProjectKey;
		this.proProjectTypeKey = proProjectTypeKey;
		this.proParentProjectKey = proParentProjectKey;
		this.proSponsorSubmissionDate = proSponsorSubmissionDate;
		this.proProcessingDueDate = proProcessingDueDate;
		this.proProjectReceivedTypeKey = proProjectReceivedTypeKey;
		this.proReceivedFrom = proReceivedFrom;
		this.proDateTimeReceived = proDateTimeReceived;
		this.proTitle = proTitle;
		this.proResponsibleUnitKey = proResponsibleUnitKey;
		this.proTeamKey = proTeamKey;
		this.proDateSigned = proDateSigned;
		this.proAwardStart = proAwardStart;
		this.proAwardEnd = proAwardEnd;
		this.proProposedStart = proProposedStart;
		this.proProposedEnd = proProposedEnd;
		this.proProjectLeadUserKey = proProjectLeadUserKey;
		this.proAwardLeadUserKey = proAwardLeadUserKey;
		this.proInstrumentTypeKey = proInstrumentTypeKey;
		this.proCloseoutbox = proCloseoutbox;
		this.proCloseoutDate = proCloseoutDate;
		this.proCostSharingTypeKey = proCostSharingTypeKey;
		this.proDfitypeKey = proDfitypeKey;
		this.proRootAwardNumber = proRootAwardNumber;
		this.proOrderNumber = proOrderNumber;
		this.proFundingAgencyCode = proFundingAgencyCode;
		this.proAwardingAgencyCode = proAwardingAgencyCode;
		this.proTas = proTas;
		this.proPrimeDuns = proPrimeDuns;
		this.proPrimeAwardNumber = proPrimeAwardNumber;
		this.proPrimeInstrumentType = proPrimeInstrumentType;
		this.proIsDelegatedtoUcd = proIsDelegatedtoUcd;
		this.proIsProjectAccepted = proIsProjectAccepted;
		this.proIsActive = proIsActive;
		this.proUpdatedBy = proUpdatedBy;
		this.proDateCreated = proDateCreated;
		this.proDateUpdated = proDateUpdated;
	}

	public CgtProjectDTO(CgtProject project) {
		this.proProjectKey = project.getProProjectKey();
		this.proProjectTypeKey = project.getProProjectTypeKey();
		this.proParentProjectKey = project.getProParentProjectKey();
		this.proSponsorSubmissionDate = project.getProSponsorSubmissionDate();
		this.proProcessingDueDate = project.getProProcessingDueDate();
		this.proProjectReceivedTypeKey = project.getProProjectReceivedTypeKey();
		this.proReceivedFrom = project.getProReceivedFrom();
		this.proDateTimeReceived = project.getProDateTimeReceived();
		this.proTitle = project.getProTitle();
		this.proResponsibleUnitKey = project.getProResponsibleUnitKey();
		this.proTeamKey = project.getProTeamKey();
		this.proDateSigned = project.getProDateSigned();
		this.proAwardStart = project.getProAwardStart();
		this.proAwardEnd = project.getProAwardEnd();
		this.proProposedStart = project.getProProposedStart();
		this.proProposedEnd = project.getProProposedEnd();
		this.proProjectLeadUserKey = project.getProProjectLeadUserKey();
		this.proAwardLeadUserKey = project.getProAwardLeadUserKey();
		this.proInstrumentTypeKey = project.getProInstrumentTypeKey();
		this.proCloseoutbox = project.getProCloseoutbox();
		this.proCloseoutDate = project.getProCloseoutDate();
		this.proCostSharingTypeKey = project.getProCostSharingTypeKey();
		this.proDfitypeKey = project.getProDfitypeKey();
		this.proRootAwardNumber = project.getProRootAwardNumber();
		this.proOrderNumber = project.getProOrderNumber();
		this.proFundingAgencyCode = project.getProFundingAgencyCode();
		this.proAwardingAgencyCode = project.getProAwardingAgencyCode();
		this.proTas = project.getProTas();
		this.proPrimeDuns = project.getProPrimeDuns();
		this.proPrimeAwardNumber = project.getProPrimeAwardNumber();
		this.proPrimeInstrumentType = project.getProPrimeInstrumentType();
		this.proIsDelegatedtoUcd = project.getProIsDelegatedtoUcd();
		this.proIsProjectAccepted = project.getProIsProjectAccepted();
		this.proIsActive = project.isProIsActive();
		this.proUpdatedBy = project.getProUpdatedBy();
		this.proDateCreated = project.getProDateCreated();
		this.proDateUpdated = project.getProDateUpdated();
	}

	
	
	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#getProProjectKey()
	 */
	@Override
	public String getProProjectKey() {
		return this.proProjectKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#setProProjectKey(java.lang.String)
	 */
	@Override
	public void setProProjectKey(final String proProjectKey) {
		this.proProjectKey = proProjectKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#getProProjectTypeKey()
	 */
	@Override
	
	public Integer getProProjectTypeKey() {
		return this.proProjectTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#setProProjectTypeKey(java.lang.Integer)
	 */
	@Override
	public void setProProjectTypeKey(final Integer proProjectTypeKey) {
		this.proProjectTypeKey = proProjectTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#getProParentProjectKey()
	 */
	@Override
	
	public String getProParentProjectKey() {
		return this.proParentProjectKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#setProParentProjectKey(java.lang.String)
	 */
	@Override
	public void setProParentProjectKey(final String proParentProjectKey) {
		this.proParentProjectKey = proParentProjectKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#getProSponsorSubmissionDate()
	 */
	@Override
	
	
	public LocalDateTime getProSponsorSubmissionDate() {
		return this.proSponsorSubmissionDate;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#setProSponsorSubmissionDate(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setProSponsorSubmissionDate(final LocalDateTime proSponsorSubmissionDate) {
		this.proSponsorSubmissionDate = proSponsorSubmissionDate;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#getProProcessingDueDate()
	 */
	@Override
	
	
	public LocalDateTime getProProcessingDueDate() {
		return this.proProcessingDueDate;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#setProProcessingDueDate(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setProProcessingDueDate(final LocalDateTime proProcessingDueDate) {
		this.proProcessingDueDate = proProcessingDueDate;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#getProProjectReceivedTypeKey()
	 */
	@Override
	
	public Integer getProProjectReceivedTypeKey() {
		return this.proProjectReceivedTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#setProProjectReceivedTypeKey(java.lang.Integer)
	 */
	@Override
	public void setProProjectReceivedTypeKey(final Integer proProjectReceivedTypeKey) {
		this.proProjectReceivedTypeKey = proProjectReceivedTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#getProReceivedFrom()
	 */
	@Override
	
	public String getProReceivedFrom() {
		return this.proReceivedFrom;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#setProReceivedFrom(java.lang.String)
	 */
	@Override
	public void setProReceivedFrom(final String proReceivedFrom) {
		this.proReceivedFrom = proReceivedFrom;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#getProDateTimeReceived()
	 */
	@Override
	
	
	public LocalDateTime getProDateTimeReceived() {
		return this.proDateTimeReceived;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#setProDateTimeReceived(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setProDateTimeReceived(final LocalDateTime proDateTimeReceived) {
		this.proDateTimeReceived = proDateTimeReceived;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#getProTitle()
	 */
	@Override
	
	public String getProTitle() {
		return this.proTitle;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#setProTitle(java.lang.String)
	 */
	@Override
	public void setProTitle(final String proTitle) {
		this.proTitle = proTitle;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#getProResponsibleUnitKey()
	 */
	@Override
	
	public int getProResponsibleUnitKey() {
		return this.proResponsibleUnitKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#setProResponsibleUnitKey(int)
	 */
	@Override
	public void setProResponsibleUnitKey(final int proResponsibleUnitKey) {
		this.proResponsibleUnitKey = proResponsibleUnitKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#getProTeamKey()
	 */
	@Override
	
	public Integer getProTeamKey() {
		return this.proTeamKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#setProTeamKey(java.lang.Integer)
	 */
	@Override
	public void setProTeamKey(final Integer proTeamKey) {
		this.proTeamKey = proTeamKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#getProDateSigned()
	 */
	@Override
	
	
	public LocalDateTime getProDateSigned() {
		return this.proDateSigned;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#setProDateSigned(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setProDateSigned(final LocalDateTime proDateSigned) {
		this.proDateSigned = proDateSigned;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#getProAwardStart()
	 */
	@Override
	
	
	public LocalDate getProAwardStart() {
		return this.proAwardStart;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#setProAwardStart(org.joda.time.LocalDate)
	 */
	@Override
	public void setProAwardStart(final LocalDate proAwardStart) {
		this.proAwardStart = proAwardStart;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#getProAwardEnd()
	 */
	@Override
	
	
	public LocalDate getProAwardEnd() {
		return this.proAwardEnd;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#setProAwardEnd(org.joda.time.LocalDate)
	 */
	@Override
	public void setProAwardEnd(final LocalDate proAwardEnd) {
		this.proAwardEnd = proAwardEnd;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#getProProposedStart()
	 */
	@Override
	
	
	public LocalDate getProProposedStart() {
		return this.proProposedStart;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#setProProposedStart(org.joda.time.LocalDate)
	 */
	@Override
	public void setProProposedStart(final LocalDate proProposedStart) {
		this.proProposedStart = proProposedStart;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#getProProposedEnd()
	 */
	@Override
	
	
	public LocalDate getProProposedEnd() {
		return this.proProposedEnd;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#setProProposedEnd(org.joda.time.LocalDate)
	 */
	@Override
	public void setProProposedEnd(final LocalDate proProposedEnd) {
		this.proProposedEnd = proProposedEnd;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#getProProjectLeadUserKey()
	 */
	@Override
	
	public String getProProjectLeadUserKey() {
		return this.proProjectLeadUserKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#setProProjectLeadUserKey(java.lang.String)
	 */
	@Override
	public void setProProjectLeadUserKey(final String proProjectLeadUserKey) {
		this.proProjectLeadUserKey = proProjectLeadUserKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#getProAwardLeadUserKey()
	 */
	@Override
	
	public String getProAwardLeadUserKey() {
		return this.proAwardLeadUserKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#setProAwardLeadUserKey(java.lang.String)
	 */
	@Override
	public void setProAwardLeadUserKey(final String proAwardLeadUserKey) {
		this.proAwardLeadUserKey = proAwardLeadUserKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#getProInstrumentTypeKey()
	 */
	@Override
	
	public Integer getProInstrumentTypeKey() {
		return this.proInstrumentTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#setProInstrumentTypeKey(java.lang.Integer)
	 */
	@Override
	public void setProInstrumentTypeKey(final Integer proInstrumentTypeKey) {
		this.proInstrumentTypeKey = proInstrumentTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#getProCloseoutbox()
	 */
	@Override
	
	public String getProCloseoutbox() {
		return this.proCloseoutbox;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#setProCloseoutbox(java.lang.String)
	 */
	@Override
	public void setProCloseoutbox(final String proCloseoutbox) {
		this.proCloseoutbox = proCloseoutbox;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#getProCloseoutDate()
	 */
	@Override
	
	
	public LocalDate getProCloseoutDate() {
		return this.proCloseoutDate;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#setProCloseoutDate(org.joda.time.LocalDate)
	 */
	@Override
	public void setProCloseoutDate(final LocalDate proCloseoutDate) {
		this.proCloseoutDate = proCloseoutDate;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#getProCostSharingTypeKey()
	 */
	@Override
	
	public Integer getProCostSharingTypeKey() {
		return this.proCostSharingTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#setProCostSharingTypeKey(java.lang.Integer)
	 */
	@Override
	public void setProCostSharingTypeKey(final Integer proCostSharingTypeKey) {
		this.proCostSharingTypeKey = proCostSharingTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#getProDfitypeKey()
	 */
	@Override
	
	public Integer getProDfitypeKey() {
		return this.proDfitypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#setProDfitypeKey(java.lang.Integer)
	 */
	@Override
	public void setProDfitypeKey(final Integer proDfitypeKey) {
		this.proDfitypeKey = proDfitypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#getProRootAwardNumber()
	 */
	@Override
	
	public String getProRootAwardNumber() {
		return this.proRootAwardNumber;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#setProRootAwardNumber(java.lang.String)
	 */
	@Override
	public void setProRootAwardNumber(final String proRootAwardNumber) {
		this.proRootAwardNumber = proRootAwardNumber;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#getProOrderNumber()
	 */
	@Override
	
	public String getProOrderNumber() {
		return this.proOrderNumber;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#setProOrderNumber(java.lang.String)
	 */
	@Override
	public void setProOrderNumber(final String proOrderNumber) {
		this.proOrderNumber = proOrderNumber;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#getProFundingAgencyCode()
	 */
	@Override
	
	public String getProFundingAgencyCode() {
		return this.proFundingAgencyCode;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#setProFundingAgencyCode(java.lang.String)
	 */
	@Override
	public void setProFundingAgencyCode(final String proFundingAgencyCode) {
		this.proFundingAgencyCode = proFundingAgencyCode;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#getProAwardingAgencyCode()
	 */
	@Override
	
	public String getProAwardingAgencyCode() {
		return this.proAwardingAgencyCode;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#setProAwardingAgencyCode(java.lang.String)
	 */
	@Override
	public void setProAwardingAgencyCode(final String proAwardingAgencyCode) {
		this.proAwardingAgencyCode = proAwardingAgencyCode;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#getProTas()
	 */
	@Override
	
	public String getProTas() {
		return this.proTas;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#setProTas(java.lang.String)
	 */
	@Override
	public void setProTas(final String proTas) {
		this.proTas = proTas;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#getProPrimeDuns()
	 */
	@Override
	
	public String getProPrimeDuns() {
		return this.proPrimeDuns;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#setProPrimeDuns(java.lang.String)
	 */
	@Override
	public void setProPrimeDuns(final String proPrimeDuns) {
		this.proPrimeDuns = proPrimeDuns;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#getProPrimeAwardNumber()
	 */
	@Override
	
	public String getProPrimeAwardNumber() {
		return this.proPrimeAwardNumber;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#setProPrimeAwardNumber(java.lang.String)
	 */
	@Override
	public void setProPrimeAwardNumber(final String proPrimeAwardNumber) {
		this.proPrimeAwardNumber = proPrimeAwardNumber;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#getProPrimeInstrumentType()
	 */
	@Override
	
	public Integer getProPrimeInstrumentType() {
		return this.proPrimeInstrumentType;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#setProPrimeInstrumentType(java.lang.Integer)
	 */
	@Override
	public void setProPrimeInstrumentType(final Integer proPrimeInstrumentType) {
		this.proPrimeInstrumentType = proPrimeInstrumentType;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#getProIsDelegatedtoUcd()
	 */
	@Override
	
	public Boolean getProIsDelegatedtoUcd() {
		return this.proIsDelegatedtoUcd;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#setProIsDelegatedtoUcd(java.lang.Boolean)
	 */
	@Override
	public void setProIsDelegatedtoUcd(final Boolean proIsDelegatedtoUcd) {
		this.proIsDelegatedtoUcd = proIsDelegatedtoUcd;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#getProIsProjectAccepted()
	 */
	@Override
	
	public Boolean getProIsProjectAccepted() {
		return this.proIsProjectAccepted;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#setProIsProjectAccepted(java.lang.Boolean)
	 */
	@Override
	public void setProIsProjectAccepted(final Boolean proIsProjectAccepted) {
		this.proIsProjectAccepted = proIsProjectAccepted;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#isProIsActive()
	 */
	@Override
	
	public boolean isProIsActive() {
		return this.proIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#setProIsActive(java.lang.Boolean)
	 */
	@Override
	public void setProIsActive(final Boolean proIsActive) {
		this.proIsActive = proIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#getProUpdatedBy()
	 */
	@Override
	
	public String getProUpdatedBy() {
		return this.proUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#setProUpdatedBy(java.lang.String)
	 */
	@Override
	public void setProUpdatedBy(final String proUpdatedBy) {
		this.proUpdatedBy = proUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#getProDateCreated()
	 */
	@Override
	
	
	public LocalDateTime getProDateCreated() {
		return this.proDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#setProDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setProDateCreated(final LocalDateTime proDateCreated) {
		this.proDateCreated = proDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#getProDateUpdated()
	 */
	@Override
	
	
	public LocalDateTime getProDateUpdated() {
		return this.proDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#setProDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setProDateUpdated(final LocalDateTime proDateUpdated) {
		this.proDateUpdated = proDateUpdated;
	}


	
    
	public CgtProjectTypeDTO getProjectTypeImpl() {
		return projectType;
	}

	public void setProjectTypeImpl(CgtProjectTypeDTO projectType) {
		this.projectType = projectType;
	}

	
    
	public CgtTeamDTO getTeamImpl() {
		return team;
	}

	public void setTeamImpl(CgtTeamDTO team) {
		this.team = team;
	}
	
	
	public List<CgtTransactionDTO> getTransactionImpls() {
		return transactions;
	}
	
	public void setTransactionImpls(List<CgtTransactionDTO> transactions) {
		this.transactions = transactions;
	}

	
	
	//@OrderBy("administeringUnitRole.aurSortOrder")
	public List<CgtAdministeringUnitDTO> getAdministeringUnitImpls() {
		return administeringUnits;
	}

	public void setAdministeringUnitImpls(List<CgtAdministeringUnitDTO> administeringUnits) {
		this.administeringUnits = administeringUnits;
	}



	
	public List<CgtSponsorDTO> getProjectSponsorImpls() {
		return projectSponsor;
	}	

	public void setProjectSponsorImpls(List<CgtSponsorDTO> projectSponsor) {
		this.projectSponsor = projectSponsor;
	}

	
    
	public CgtInstrumentTypeDTO getInstrumentTypeImpl() {
		return instrumentType;
	}

	public void setInstrumentTypeImpl(CgtInstrumentTypeDTO instrumentType) {
		this.instrumentType = instrumentType;
	}

	
    
	public CgtInstrumentTypeDTO getPrimeInstrumentTypeImpl() {
		return primeInstrumentType;
	}

	public void setPrimeInstrumentTypeImpl(CgtInstrumentTypeDTO primeInstrumentType) {
		this.primeInstrumentType = primeInstrumentType;
	}

	
	
	public List<CgtKeyPersonnelDTO> getKeyPersonnelImpl() {
		return keyPersonnel;
	}

	public void setKeyPersonnelImpl(List<CgtKeyPersonnelDTO> keyPersonnel) {
		this.keyPersonnel = keyPersonnel;
	}
	

	//required to implement interface.
	
	
	
	@Override
	public CgtProjectType getProjectType() {
		return projectType;
	}

	@Override
	public void setProjectType(CgtProjectType projectType) {
		this.projectType = (CgtProjectTypeDTO)projectType;
	}
	
	@Override
	
	public CgtTeam getTeam() {
		return this.team;
	}

	@Override
	public void setTeam(CgtTeam team) {
		this.team = (CgtTeamDTO) team;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	
	public List<CgtTransaction> getTransactions() {
		List a = transactions;
		return a;
	}

	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void setTransactions(List<CgtTransaction> transactions) {
		List a = transactions;
		this.transactions = a;
	}
	
	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	
	public List<CgtAdministeringUnit> getAdministeringUnits() {
		return (List)this.administeringUnits;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void setAdministeringUnits(List<CgtAdministeringUnit> administeringUnits) {
		this.administeringUnits = (List)administeringUnits;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	
	public List<CgtSponsor> getProjectSponsors() {
		return (List)projectSponsor;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void setProjectSponsors(List<CgtSponsor> projectSponsor) {
		this.projectSponsor = (List)projectSponsor;
	}

	@Override
	
	public CgtInstrumentType getInstrumentType() {
		return instrumentType;
	}

	@Override
	public void setInstrumentType(CgtInstrumentType instrumentType) {
		this.instrumentType = (CgtInstrumentTypeDTO) instrumentType;
	}

	@Override
	
	public CgtInstrumentType getPrimeInstrumentType() {
		return primeInstrumentType;
	}

	@Override
	public void setPrimeInstrumentType(CgtInstrumentType primeInstrumentType) {
		this.primeInstrumentType = (CgtInstrumentTypeDTO) primeInstrumentType;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	
	public List<CgtKeyPersonnel> getKeyPersonnel() {
		return (List)keyPersonnel;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void setKeyPersonnel(List<CgtKeyPersonnel> keyPersonnel) {
		this.keyPersonnel = (List)keyPersonnel;
	}

	//calculated members
		/* (non-Javadoc)
		 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#getPrimaryProjectSponsor()
		 */
		@Override
		
		public CgtSponsor getPrimaryProjectSponsor() {
			return getProjectSponsorByType(CgtSponsorRoleTypesEnum.PRIMARY);
		}

		/* (non-Javadoc)
		 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#getOriginatingProjectSponsor()
		 */
		@Override
		
		public CgtSponsor getOriginatingProjectSponsor() {
			return getProjectSponsorByType(CgtSponsorRoleTypesEnum.ORIGINATING);
		}

		/* (non-Javadoc)
		 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#getProjectSponsorByType(edu.ucdavis.orvc.integration.cgt.api.domain.CgtSponsorRoleTypesEnum)
		 */
		@Override
		
		public CgtSponsor getProjectSponsorByType(CgtSponsorRoleTypesEnum type) {
			CgtSponsor result = null;
			for (CgtSponsor sponsor : getProjectSponsors()) {
				if ((StringUtils.equals(sponsor.getSponsorRoleType().getSrtShortName(),type.shortName))
						&& sponsor.isPspIsActive()) {
					result = sponsor;
					break;
				}
			}
			return result;
		}
		
		/* (non-Javadoc)
		 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#getAdminsteringUnitByType(edu.ucdavis.orvc.integration.cgt.api.domain.CgtAdministeringUnitRoleTypesEnum)
		 */
		@Override
		
		public CgtAdministeringUnit getAdminsteringUnitByType(CgtAdministeringUnitRoleTypesEnum type) {
			CgtAdministeringUnit result = null;
			for (CgtAdministeringUnit adminUnit:getAdministeringUnits()) {
				if ((adminUnit.getAdministeringUnitRole().getAurUnitRoleKey() == type.typeKey)
						&& adminUnit.isAduIsActive()) {
					result = adminUnit;
					break;
				}
			}
			return result;
		}
		
		/* (non-Javadoc)
		 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#getPrimaryAdminsteringUnit()
		 */
		@Override
		
		public CgtAdministeringUnit getPrimaryAdminsteringUnit() {
			return getAdminsteringUnitByType(CgtAdministeringUnitRoleTypesEnum.PRIMARY);
		}
			
		/* (non-Javadoc)
		 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProject#getAlternateAdminsteringUnit()
		 */
		@Override
		
		public CgtAdministeringUnit getAlternateAdminsteringUnit() {
			return getAdminsteringUnitByType(CgtAdministeringUnitRoleTypesEnum.ALTERNATE);
		}
	
	

		/* (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return String.format(
					"CgtProjects [proProjectKey=%s, proProjectTypeKey=%s, proParentProjectKey=%s, proSponsorSubmissionDate=%s, proProcessingDueDate=%s, proProjectReceivedTypeKey=%s, proReceivedFrom=%s, proDateTimeReceived=%s, proTitle=%s, proResponsibleUnitKey=%s, proTeamKey=%s, proDateSigned=%s, proAwardStart=%s, proAwardEnd=%s, proProposedStart=%s, proProposedEnd=%s, proProjectLeadUserKey=%s, proAwardLeadUserKey=%s, proInstrumentTypeKey=%s, proCloseoutbox=%s, proCloseoutDate=%s, proCostSharingTypeKey=%s, proDfitypeKey=%s, proRootAwardNumber=%s, proOrderNumber=%s, proFundingAgencyCode=%s, proAwardingAgencyCode=%s, proTas=%s, proPrimeDuns=%s, proPrimeAwardNumber=%s, proPrimeInstrumentType=%s, proIsDelegatedtoUcd=%s, proIsProjectAccepted=%s, proIsActive=%s, proUpdatedBy=%s, proDateCreated=%s, proDateUpdated=%s]",
					proProjectKey, proProjectTypeKey, proParentProjectKey, proSponsorSubmissionDate, proProcessingDueDate,
					proProjectReceivedTypeKey, proReceivedFrom, proDateTimeReceived, proTitle, proResponsibleUnitKey,
					proTeamKey, proDateSigned, proAwardStart, proAwardEnd, proProposedStart, proProposedEnd,
					proProjectLeadUserKey, proAwardLeadUserKey, proInstrumentTypeKey, proCloseoutbox, proCloseoutDate,
					proCostSharingTypeKey, proDfitypeKey, proRootAwardNumber, proOrderNumber, proFundingAgencyCode,
					proAwardingAgencyCode, proTas, proPrimeDuns, proPrimeAwardNumber, proPrimeInstrumentType,
					proIsDelegatedtoUcd, proIsProjectAccepted, proIsActive, proUpdatedBy, proDateCreated, proDateUpdated);
		}

		/* (non-Javadoc)
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = super.hashCode();
			result = prime * result + ((proAwardEnd == null) ? 0 : proAwardEnd.hashCode());
			result = prime * result + ((proAwardLeadUserKey == null) ? 0 : proAwardLeadUserKey.hashCode());
			result = prime * result + ((proAwardStart == null) ? 0 : proAwardStart.hashCode());
			result = prime * result + ((proAwardingAgencyCode == null) ? 0 : proAwardingAgencyCode.hashCode());
			result = prime * result + ((proCloseoutDate == null) ? 0 : proCloseoutDate.hashCode());
			result = prime * result + ((proCloseoutbox == null) ? 0 : proCloseoutbox.hashCode());
			result = prime * result + ((proCostSharingTypeKey == null) ? 0 : proCostSharingTypeKey.hashCode());
			result = prime * result + ((proDateCreated == null) ? 0 : proDateCreated.hashCode());
			result = prime * result + ((proDateSigned == null) ? 0 : proDateSigned.hashCode());
			result = prime * result + ((proDateTimeReceived == null) ? 0 : proDateTimeReceived.hashCode());
			result = prime * result + ((proDateUpdated == null) ? 0 : proDateUpdated.hashCode());
			result = prime * result + ((proDfitypeKey == null) ? 0 : proDfitypeKey.hashCode());
			result = prime * result + ((proFundingAgencyCode == null) ? 0 : proFundingAgencyCode.hashCode());
			result = prime * result + ((proInstrumentTypeKey == null) ? 0 : proInstrumentTypeKey.hashCode());
			result = prime * result + ((proIsActive == null) ? 0 : proIsActive.hashCode());
			result = prime * result + ((proIsDelegatedtoUcd == null) ? 0 : proIsDelegatedtoUcd.hashCode());
			result = prime * result + ((proIsProjectAccepted == null) ? 0 : proIsProjectAccepted.hashCode());
			result = prime * result + ((proOrderNumber == null) ? 0 : proOrderNumber.hashCode());
			result = prime * result + ((proParentProjectKey == null) ? 0 : proParentProjectKey.hashCode());
			result = prime * result + ((proPrimeAwardNumber == null) ? 0 : proPrimeAwardNumber.hashCode());
			result = prime * result + ((proPrimeDuns == null) ? 0 : proPrimeDuns.hashCode());
			result = prime * result + ((proPrimeInstrumentType == null) ? 0 : proPrimeInstrumentType.hashCode());
			result = prime * result + ((proProcessingDueDate == null) ? 0 : proProcessingDueDate.hashCode());
			result = prime * result + ((proProjectKey == null) ? 0 : proProjectKey.hashCode());
			result = prime * result + ((proProjectLeadUserKey == null) ? 0 : proProjectLeadUserKey.hashCode());
			result = prime * result + ((proProjectReceivedTypeKey == null) ? 0 : proProjectReceivedTypeKey.hashCode());
			result = prime * result + ((proProjectTypeKey == null) ? 0 : proProjectTypeKey.hashCode());
			result = prime * result + ((proProposedEnd == null) ? 0 : proProposedEnd.hashCode());
			result = prime * result + ((proProposedStart == null) ? 0 : proProposedStart.hashCode());
			result = prime * result + ((proReceivedFrom == null) ? 0 : proReceivedFrom.hashCode());
			result = prime * result + proResponsibleUnitKey;
			result = prime * result + ((proRootAwardNumber == null) ? 0 : proRootAwardNumber.hashCode());
			result = prime * result + ((proSponsorSubmissionDate == null) ? 0 : proSponsorSubmissionDate.hashCode());
			result = prime * result + ((proTas == null) ? 0 : proTas.hashCode());
			result = prime * result + ((proTeamKey == null) ? 0 : proTeamKey.hashCode());
			result = prime * result + ((proTitle == null) ? 0 : proTitle.hashCode());
			result = prime * result + ((proUpdatedBy == null) ? 0 : proUpdatedBy.hashCode());
			return result;
		}

		/* (non-Javadoc)
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(final Object obj) {
			if (this == obj)
				return true;
			if (!super.equals(obj))
				return false;
			if (!(obj instanceof CgtProjectDTO))
				return false;
			final CgtProjectDTO other = (CgtProjectDTO) obj;
			if (proAwardEnd == null) {
				if (other.proAwardEnd != null)
					return false;
			} else if (!proAwardEnd.equals(other.proAwardEnd))
				return false;
			if (proAwardLeadUserKey == null) {
				if (other.proAwardLeadUserKey != null)
					return false;
			} else if (!proAwardLeadUserKey.equals(other.proAwardLeadUserKey))
				return false;
			if (proAwardStart == null) {
				if (other.proAwardStart != null)
					return false;
			} else if (!proAwardStart.equals(other.proAwardStart))
				return false;
			if (proAwardingAgencyCode == null) {
				if (other.proAwardingAgencyCode != null)
					return false;
			} else if (!proAwardingAgencyCode.equals(other.proAwardingAgencyCode))
				return false;
			if (proCloseoutDate == null) {
				if (other.proCloseoutDate != null)
					return false;
			} else if (!proCloseoutDate.equals(other.proCloseoutDate))
				return false;
			if (proCloseoutbox == null) {
				if (other.proCloseoutbox != null)
					return false;
			} else if (!proCloseoutbox.equals(other.proCloseoutbox))
				return false;
			if (proCostSharingTypeKey == null) {
				if (other.proCostSharingTypeKey != null)
					return false;
			} else if (!proCostSharingTypeKey.equals(other.proCostSharingTypeKey))
				return false;
			if (proDateCreated == null) {
				if (other.proDateCreated != null)
					return false;
			} else if (!proDateCreated.equals(other.proDateCreated))
				return false;
			if (proDateSigned == null) {
				if (other.proDateSigned != null)
					return false;
			} else if (!proDateSigned.equals(other.proDateSigned))
				return false;
			if (proDateTimeReceived == null) {
				if (other.proDateTimeReceived != null)
					return false;
			} else if (!proDateTimeReceived.equals(other.proDateTimeReceived))
				return false;
			if (proDateUpdated == null) {
				if (other.proDateUpdated != null)
					return false;
			} else if (!proDateUpdated.equals(other.proDateUpdated))
				return false;
			if (proDfitypeKey == null) {
				if (other.proDfitypeKey != null)
					return false;
			} else if (!proDfitypeKey.equals(other.proDfitypeKey))
				return false;
			if (proFundingAgencyCode == null) {
				if (other.proFundingAgencyCode != null)
					return false;
			} else if (!proFundingAgencyCode.equals(other.proFundingAgencyCode))
				return false;
			if (proInstrumentTypeKey == null) {
				if (other.proInstrumentTypeKey != null)
					return false;
			} else if (!proInstrumentTypeKey.equals(other.proInstrumentTypeKey))
				return false;
			if (proIsActive == null) {
				if (other.proIsActive != null)
					return false;
			} else if (!proIsActive.equals(other.proIsActive))
				return false;
			if (proIsDelegatedtoUcd == null) {
				if (other.proIsDelegatedtoUcd != null)
					return false;
			} else if (!proIsDelegatedtoUcd.equals(other.proIsDelegatedtoUcd))
				return false;
			if (proIsProjectAccepted == null) {
				if (other.proIsProjectAccepted != null)
					return false;
			} else if (!proIsProjectAccepted.equals(other.proIsProjectAccepted))
				return false;
			if (proOrderNumber == null) {
				if (other.proOrderNumber != null)
					return false;
			} else if (!proOrderNumber.equals(other.proOrderNumber))
				return false;
			if (proParentProjectKey == null) {
				if (other.proParentProjectKey != null)
					return false;
			} else if (!proParentProjectKey.equals(other.proParentProjectKey))
				return false;
			if (proPrimeAwardNumber == null) {
				if (other.proPrimeAwardNumber != null)
					return false;
			} else if (!proPrimeAwardNumber.equals(other.proPrimeAwardNumber))
				return false;
			if (proPrimeDuns == null) {
				if (other.proPrimeDuns != null)
					return false;
			} else if (!proPrimeDuns.equals(other.proPrimeDuns))
				return false;
			if (proPrimeInstrumentType == null) {
				if (other.proPrimeInstrumentType != null)
					return false;
			} else if (!proPrimeInstrumentType.equals(other.proPrimeInstrumentType))
				return false;
			if (proProcessingDueDate == null) {
				if (other.proProcessingDueDate != null)
					return false;
			} else if (!proProcessingDueDate.equals(other.proProcessingDueDate))
				return false;
			if (proProjectKey == null) {
				if (other.proProjectKey != null)
					return false;
			} else if (!proProjectKey.equals(other.proProjectKey))
				return false;
			if (proProjectLeadUserKey == null) {
				if (other.proProjectLeadUserKey != null)
					return false;
			} else if (!proProjectLeadUserKey.equals(other.proProjectLeadUserKey))
				return false;
			if (proProjectReceivedTypeKey == null) {
				if (other.proProjectReceivedTypeKey != null)
					return false;
			} else if (!proProjectReceivedTypeKey.equals(other.proProjectReceivedTypeKey))
				return false;
			if (proProjectTypeKey == null) {
				if (other.proProjectTypeKey != null)
					return false;
			} else if (!proProjectTypeKey.equals(other.proProjectTypeKey))
				return false;
			if (proProposedEnd == null) {
				if (other.proProposedEnd != null)
					return false;
			} else if (!proProposedEnd.equals(other.proProposedEnd))
				return false;
			if (proProposedStart == null) {
				if (other.proProposedStart != null)
					return false;
			} else if (!proProposedStart.equals(other.proProposedStart))
				return false;
			if (proReceivedFrom == null) {
				if (other.proReceivedFrom != null)
					return false;
			} else if (!proReceivedFrom.equals(other.proReceivedFrom))
				return false;
			if (proResponsibleUnitKey != other.proResponsibleUnitKey)
				return false;
			if (proRootAwardNumber == null) {
				if (other.proRootAwardNumber != null)
					return false;
			} else if (!proRootAwardNumber.equals(other.proRootAwardNumber))
				return false;
			if (proSponsorSubmissionDate == null) {
				if (other.proSponsorSubmissionDate != null)
					return false;
			} else if (!proSponsorSubmissionDate.equals(other.proSponsorSubmissionDate))
				return false;
			if (proTas == null) {
				if (other.proTas != null)
					return false;
			} else if (!proTas.equals(other.proTas))
				return false;
			if (proTeamKey == null) {
				if (other.proTeamKey != null)
					return false;
			} else if (!proTeamKey.equals(other.proTeamKey))
				return false;
			if (proTitle == null) {
				if (other.proTitle != null)
					return false;
			} else if (!proTitle.equals(other.proTitle))
				return false;
			if (proUpdatedBy == null) {
				if (other.proUpdatedBy != null)
					return false;
			} else if (!proUpdatedBy.equals(other.proUpdatedBy))
				return false;
			return true;
		}
	
	public CgtProjectDTO toDTO() {
		return new CgtProjectDTO(this);
	}
}
