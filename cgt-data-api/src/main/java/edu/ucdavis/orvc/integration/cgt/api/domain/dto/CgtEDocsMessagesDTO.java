package edu.ucdavis.orvc.integration.cgt.api.domain.dto;


import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtEDocsMessages;



/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/


public class CgtEDocsMessagesDTO implements CgtEDocsMessages {

	private static final long serialVersionUID = 1885861935425897118L;
	private String msgEDocKey;
	private String msgFromUserKey;
	private String msgSubject;
	private String msgMessage;
	private LocalDateTime msgExpirationDate;
	private boolean msgIsActive;
	private LocalDateTime msgDateCreated;
	private LocalDateTime msgDateUpdated;
	private String msgUpdatedBy;

	public CgtEDocsMessagesDTO(final CgtEDocsMessages fromObj) {



	}

	public CgtEDocsMessagesDTO() {
	}

	public CgtEDocsMessagesDTO(final String msgEDocKey, final String msgFromUserKey, final String msgSubject, final LocalDateTime msgExpirationDate,
			final boolean msgIsActive, final LocalDateTime msgDateCreated, final LocalDateTime msgDateUpdated, final String msgUpdatedBy) {
		this.msgEDocKey = msgEDocKey;
		this.msgFromUserKey = msgFromUserKey;
		this.msgSubject = msgSubject;
		this.msgExpirationDate = msgExpirationDate;
		this.msgIsActive = msgIsActive;
		this.msgDateCreated = msgDateCreated;
		this.msgDateUpdated = msgDateUpdated;
		this.msgUpdatedBy = msgUpdatedBy;
	}

	public CgtEDocsMessagesDTO(final String msgEDocKey, final String msgFromUserKey, final String msgSubject, final String msgMessage,
			final LocalDateTime msgExpirationDate, final boolean msgIsActive, final LocalDateTime msgDateCreated, final LocalDateTime msgDateUpdated,
			final String msgUpdatedBy) {
		this.msgEDocKey = msgEDocKey;
		this.msgFromUserKey = msgFromUserKey;
		this.msgSubject = msgSubject;
		this.msgMessage = msgMessage;
		this.msgExpirationDate = msgExpirationDate;
		this.msgIsActive = msgIsActive;
		this.msgDateCreated = msgDateCreated;
		this.msgDateUpdated = msgDateUpdated;
		this.msgUpdatedBy = msgUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsMessages#getMsgEDocKey()
	 */
	@Override
	

	
	public String getMsgEDocKey() {
		return this.msgEDocKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsMessages#setMsgEDocKey(java.lang.String)
	 */
	@Override
	public void setMsgEDocKey(final String msgEDocKey) {
		this.msgEDocKey = msgEDocKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsMessages#getMsgFromUserKey()
	 */
	@Override
	
	public String getMsgFromUserKey() {
		return this.msgFromUserKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsMessages#setMsgFromUserKey(java.lang.String)
	 */
	@Override
	public void setMsgFromUserKey(final String msgFromUserKey) {
		this.msgFromUserKey = msgFromUserKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsMessages#getMsgSubject()
	 */
	@Override
	
	public String getMsgSubject() {
		return this.msgSubject;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsMessages#setMsgSubject(java.lang.String)
	 */
	@Override
	public void setMsgSubject(final String msgSubject) {
		this.msgSubject = msgSubject;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsMessages#getMsgMessage()
	 */
	@Override
	
	public String getMsgMessage() {
		return this.msgMessage;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsMessages#setMsgMessage(java.lang.String)
	 */
	@Override
	public void setMsgMessage(final String msgMessage) {
		this.msgMessage = msgMessage;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsMessages#getMsgExpirationDate()
	 */
	@Override
	
	
	public LocalDateTime getMsgExpirationDate() {
		return this.msgExpirationDate;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsMessages#setMsgExpirationDate(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setMsgExpirationDate(final LocalDateTime msgExpirationDate) {
		this.msgExpirationDate = msgExpirationDate;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsMessages#isMsgIsActive()
	 */
	@Override
	
	public boolean isMsgIsActive() {
		return this.msgIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsMessages#setMsgIsActive(boolean)
	 */
	@Override
	public void setMsgIsActive(final boolean msgIsActive) {
		this.msgIsActive = msgIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsMessages#getMsgDateCreated()
	 */
	@Override
	
	
	public LocalDateTime getMsgDateCreated() {
		return this.msgDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsMessages#setMsgDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setMsgDateCreated(final LocalDateTime msgDateCreated) {
		this.msgDateCreated = msgDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsMessages#getMsgDateUpdated()
	 */
	@Override
	
	
	public LocalDateTime getMsgDateUpdated() {
		return this.msgDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsMessages#setMsgDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setMsgDateUpdated(final LocalDateTime msgDateUpdated) {
		this.msgDateUpdated = msgDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsMessages#getMsgUpdatedBy()
	 */
	@Override
	
	public String getMsgUpdatedBy() {
		return this.msgUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsMessages#setMsgUpdatedBy(java.lang.String)
	 */
	@Override
	public void setMsgUpdatedBy(final String msgUpdatedBy) {
		this.msgUpdatedBy = msgUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((msgDateCreated == null) ? 0 : msgDateCreated.hashCode());
		result = prime * result + ((msgDateUpdated == null) ? 0 : msgDateUpdated.hashCode());
		result = prime * result + ((msgEDocKey == null) ? 0 : msgEDocKey.hashCode());
		result = prime * result + ((msgExpirationDate == null) ? 0 : msgExpirationDate.hashCode());
		result = prime * result + ((msgFromUserKey == null) ? 0 : msgFromUserKey.hashCode());
		result = prime * result + (msgIsActive ? 1231 : 1237);
		result = prime * result + ((msgMessage == null) ? 0 : msgMessage.hashCode());
		result = prime * result + ((msgSubject == null) ? 0 : msgSubject.hashCode());
		result = prime * result + ((msgUpdatedBy == null) ? 0 : msgUpdatedBy.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof CgtEDocsMessagesDTO)) {
			return false;
		}
		CgtEDocsMessagesDTO other = (CgtEDocsMessagesDTO) obj;
		if (msgDateCreated == null) {
			if (other.msgDateCreated != null) {
				return false;
			}
		} else if (!msgDateCreated.equals(other.msgDateCreated)) {
			return false;
		}
		if (msgDateUpdated == null) {
			if (other.msgDateUpdated != null) {
				return false;
			}
		} else if (!msgDateUpdated.equals(other.msgDateUpdated)) {
			return false;
		}
		if (msgEDocKey == null) {
			if (other.msgEDocKey != null) {
				return false;
			}
		} else if (!msgEDocKey.equals(other.msgEDocKey)) {
			return false;
		}
		if (msgExpirationDate == null) {
			if (other.msgExpirationDate != null) {
				return false;
			}
		} else if (!msgExpirationDate.equals(other.msgExpirationDate)) {
			return false;
		}
		if (msgFromUserKey == null) {
			if (other.msgFromUserKey != null) {
				return false;
			}
		} else if (!msgFromUserKey.equals(other.msgFromUserKey)) {
			return false;
		}
		if (msgIsActive != other.msgIsActive) {
			return false;
		}
		if (msgMessage == null) {
			if (other.msgMessage != null) {
				return false;
			}
		} else if (!msgMessage.equals(other.msgMessage)) {
			return false;
		}
		if (msgSubject == null) {
			if (other.msgSubject != null) {
				return false;
			}
		} else if (!msgSubject.equals(other.msgSubject)) {
			return false;
		}
		if (msgUpdatedBy == null) {
			if (other.msgUpdatedBy != null) {
				return false;
			}
		} else if (!msgUpdatedBy.equals(other.msgUpdatedBy)) {
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"CgtEDocsMessagesImpl [msgEDocKey=%s, msgFromUserKey=%s, msgSubject=%s, msgMessage=%s, msgExpirationDate=%s, msgIsActive=%s, msgDateCreated=%s, msgDateUpdated=%s, msgUpdatedBy=%s]",
				msgEDocKey, msgFromUserKey, msgSubject, msgMessage, msgExpirationDate, msgIsActive, msgDateCreated,
				msgDateUpdated, msgUpdatedBy);
	}

	public CgtEDocsMessagesDTO toDTO() {
		return new CgtEDocsMessagesDTO(this);
	}
}
