package edu.ucdavis.orvc.integration.cgt.api.domain;

import java.math.BigDecimal;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtSubContractsDTO;

public interface CgtSubContracts extends CgtBaseInterface<CgtSubContractsDTO> { 

	int getSucSubContractKey();

	void setSucSubContractKey(int sucSubContractKey);

	String getSucProjectKey();

	void setSucProjectKey(String sucProjectKey);

	String getSucSubContractorKey();

	void setSucSubContractorKey(String sucSubContractorKey);

	String getSucLegacyContractorName();

	void setSucLegacyContractorName(String sucLegacyContractorName);

	String getSucAgreementNumber();

	void setSucAgreementNumber(String sucAgreementNumber);

	String getSucPiLastName();

	void setSucPiLastName(String sucPiLastName);

	String getSucPiFirstName();

	void setSucPiFirstName(String sucPiFirstName);

	String getSucPiPhone();

	void setSucPiPhone(String sucPiPhone);

	String getSucPiFax();

	void setSucPiFax(String sucPiFax);

	String getSucPiEmail();

	void setSucPiEmail(String sucPiEmail);

	Integer getSucSubContractorType();

	void setSucSubContractorType(Integer sucSubContractorType);

	String getSucDuns();

	void setSucDuns(String sucDuns);

	LocalDate getSucSubcontractStart();

	void setSucSubcontractStart(LocalDate sucSubcontractStart);

	LocalDate getSucSubcontractEnd();

	void setSucSubcontractEnd(LocalDate sucSubcontractEnd);

	LocalDate getSucDateSign();

	void setSucDateSign(LocalDate sucDateSign);

	LocalDate getSucDateRenewal();

	void setSucDateRenewal(LocalDate sucDateRenewal);

	LocalDate getSucDateMail();

	void setSucDateMail(LocalDate sucDateMail);

	BigDecimal getSucAmount();

	void setSucAmount(BigDecimal sucAmount);

	String getSucNotes();

	void setSucNotes(String sucNotes);

	String getSucConditions();

	void setSucConditions(String sucConditions);

	Boolean getSucIsDelegated();

	void setSucIsDelegated(Boolean sucIsDelegated);

	boolean isSucIsActive();

	void setSucIsActive(boolean sucIsActive);

	String getSucUpdatedBy();

	void setSucUpdatedBy(String sucUpdatedBy);

	LocalDateTime getSucDateCreated();

	void setSucDateCreated(LocalDateTime sucDateCreated);

	LocalDateTime getSucDateUpdated();

	void setSucDateUpdated(LocalDateTime sucDateUpdated);

}