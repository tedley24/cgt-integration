package edu.ucdavis.orvc.integration.cgt.api.domain.dto;


import edu.ucdavis.orvc.integration.cgt.api.domain.CgtEDocsTypesToTransactionTypes;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtEDocsTypesToTransactionTypesId;

/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/


public class CgtEDocsTypesToTransactionTypesDTO implements CgtEDocsTypesToTransactionTypes {

	private static final long serialVersionUID = -3417942255173834675L;
	private CgtEDocsTypesToTransactionTypesIdDTO id;

	public CgtEDocsTypesToTransactionTypesDTO(final CgtEDocsTypesToTransactionTypes fromObj) {



	}

	public CgtEDocsTypesToTransactionTypesDTO() {
	}

	public CgtEDocsTypesToTransactionTypesDTO(final CgtEDocsTypesToTransactionTypesIdDTO id) {
		this.id = id;
	}
	
	public CgtEDocsTypesToTransactionTypesId getIdImpl() {
		return this.id;
	}

	public void setIdImpl(final CgtEDocsTypesToTransactionTypesIdDTO id) {
		this.id = id;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof CgtEDocsTypesToTransactionTypesDTO)) {
			return false;
		}
		CgtEDocsTypesToTransactionTypesDTO other = (CgtEDocsTypesToTransactionTypesDTO) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format("CgtEDocsTypesToTransactionTypesImpl [id=%s]", id);
	}

	@Override
	
	public CgtEDocsTypesToTransactionTypesId getId() {
		return id;
	}

	@Override
	public void setId(CgtEDocsTypesToTransactionTypesId id) {
		this.id = (CgtEDocsTypesToTransactionTypesIdDTO) id;
	}

	public CgtEDocsTypesToTransactionTypesDTO toDTO() {
		return new CgtEDocsTypesToTransactionTypesDTO(this);
	}
}
