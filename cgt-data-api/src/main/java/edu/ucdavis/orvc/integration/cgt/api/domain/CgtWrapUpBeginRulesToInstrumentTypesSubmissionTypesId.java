package edu.ucdavis.orvc.integration.cgt.api.domain;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtWrapUpBeginRulesToInstrumentTypesSubmissionTypesIdDTO;

public interface CgtWrapUpBeginRulesToInstrumentTypesSubmissionTypesId extends CgtBaseInterface<CgtWrapUpBeginRulesToInstrumentTypesSubmissionTypesIdDTO> { 

	int getW2iBeginRuleKey();

	void setW2iBeginRuleKey(int w2iBeginRuleKey);

	int getW2iInstrumentTypeKey();

	void setW2iInstrumentTypeKey(int w2iInstrumentTypeKey);

	int getW2iTransactionTypeKey();

	void setW2iTransactionTypeKey(int w2iTransactionTypeKey);

	int getW2iSubmissionTypeKey();

	void setW2iSubmissionTypeKey(int w2iSubmissionTypeKey);

}