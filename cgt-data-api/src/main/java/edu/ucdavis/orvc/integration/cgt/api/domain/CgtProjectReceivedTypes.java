package edu.ucdavis.orvc.integration.cgt.api.domain;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtProjectReceivedTypesDTO;

public interface CgtProjectReceivedTypes extends CgtBaseInterface<CgtProjectReceivedTypesDTO> { 

	int getPrrProjectReceivedTypeKey();

	void setPrrProjectReceivedTypeKey(int prrProjectReceivedTypeKey);

	String getPrrName();

	void setPrrName(String prrName);

	String getPrrShortName();

	void setPrrShortName(String prrShortName);

	Integer getPrrSortOrder();

	void setPrrSortOrder(Integer prrSortOrder);

	Boolean getPrrIsVisible();

	void setPrrIsVisible(Boolean prrIsVisible);

	Boolean getPrrIsActive();

	void setPrrIsActive(Boolean prrIsActive);

	String getPrrUpdatedBy();

	void setPrrUpdatedBy(String prrUpdatedBy);

	LocalDateTime getPrrDateCreated();

	void setPrrDateCreated(LocalDateTime prrDateCreated);

	LocalDateTime getPrrDateUpdated();

	void setPrrDateUpdated(LocalDateTime prrDateUpdated);

}