package edu.ucdavis.orvc.integration.cgt.api.domain.dto;


import edu.ucdavis.orvc.integration.cgt.api.domain.CgtWrapUpCloseRulesToSponsorCategories;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtWrapUpCloseRulesToSponsorCategoriesId;

/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/


public class CgtWrapUpCloseRulesToSponsorCategoriesDTO implements CgtWrapUpCloseRulesToSponsorCategories {

	private static final long serialVersionUID = -3898134266117809625L;
	private CgtWrapUpCloseRulesToSponsorCategoriesIdDTO id;
	private int w2cResultSubmissionTypeKey;

	public CgtWrapUpCloseRulesToSponsorCategoriesDTO(final CgtWrapUpCloseRulesToSponsorCategories fromObj) {



	}

	public CgtWrapUpCloseRulesToSponsorCategoriesDTO() {
	}

	public CgtWrapUpCloseRulesToSponsorCategoriesDTO(final CgtWrapUpCloseRulesToSponsorCategoriesIdDTO id,
			final int w2cResultSubmissionTypeKey) {
		this.id = id;
		this.w2cResultSubmissionTypeKey = w2cResultSubmissionTypeKey;
	}

	public CgtWrapUpCloseRulesToSponsorCategoriesIdDTO getIdImpl() {
		return this.id;
	}

	public void setIdImpl(final CgtWrapUpCloseRulesToSponsorCategoriesIdDTO id) {
		this.id = id;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpCloseRulesToSponsorCategories#getW2cResultSubmissionTypeKey()
	 */
	@Override
	
	public int getW2cResultSubmissionTypeKey() {
		return this.w2cResultSubmissionTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpCloseRulesToSponsorCategories#setW2cResultSubmissionTypeKey(int)
	 */
	@Override
	public void setW2cResultSubmissionTypeKey(final int w2cResultSubmissionTypeKey) {
		this.w2cResultSubmissionTypeKey = w2cResultSubmissionTypeKey;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + w2cResultSubmissionTypeKey;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof CgtWrapUpCloseRulesToSponsorCategoriesDTO))
			return false;
		final CgtWrapUpCloseRulesToSponsorCategoriesDTO other = (CgtWrapUpCloseRulesToSponsorCategoriesDTO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (w2cResultSubmissionTypeKey != other.w2cResultSubmissionTypeKey)
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format("CgtWrapUpCloseRulesToSponsorCategories [id=%s, w2cResultSubmissionTypeKey=%s]", id,
				w2cResultSubmissionTypeKey);
	}

	@Override
	
	public CgtWrapUpCloseRulesToSponsorCategoriesId getId() {
		return id;
	}

	@Override
	public void setId(CgtWrapUpCloseRulesToSponsorCategoriesId id) {
		this.id = (CgtWrapUpCloseRulesToSponsorCategoriesIdDTO) id;
	}

	public CgtWrapUpCloseRulesToSponsorCategoriesDTO toDTO() {
		return new CgtWrapUpCloseRulesToSponsorCategoriesDTO(this);
	}
}
