package edu.ucdavis.orvc.integration.cgt.api.domain;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtResponsibleUnitDTO;

public interface CgtResponsibleUnit extends CgtBaseInterface<CgtResponsibleUnitDTO> { 

	String getReuResponsibleUnitKey();

	void setReuResponsibleUnitKey(String reuResponsibleUnitKey);

	String getReuName();

	void setReuName(String reuName);

	String getReuShortName();

	void setReuShortName(String reuShortName);

	boolean isReuIsActive();

	void setReuIsActive(boolean reuIsActive);

	String getReuUpdatedBy();

	void setReuUpdatedBy(String reuUpdatedBy);

	LocalDateTime getReuDateCreated();

	void setReuDateCreated(LocalDateTime reuDateCreated);

	LocalDateTime getReuDateUpdated();

	void setReuDateUpdated(LocalDateTime reuDateUpdated);

}