package edu.ucdavis.orvc.integration.cgt.api.domain.dto;


import java.math.BigDecimal;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtProject;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtTransaction;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtTransactionGroupType;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtTransactionSubmissionType;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtTransactionType;


/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/


public class CgtTransactionDTO implements CgtTransaction {

	private static final long serialVersionUID = -5536365787765282152L;
	private int ptrTransactionKey;
	private Integer ptrParentTransactionKey;
	private Integer ptrTransactionGroupTypeKey;
	private Integer ptrTransactionGroupSequenceNumber;
	private String ptrTransactionGroupSubContractorKey;
	private String ptrTransactionGroupNameOld;
	private String ptrTransactionGroupName;
	private String ptrProjectKey;
	private int ptrTransactionTypeKey;
	private int ptrSubmissionTypeKey;
	private LocalDateTime ptrTransactionStart;
	private LocalDateTime ptrTransactionEnd;
	private BigDecimal ptrDirectAmount;
	private BigDecimal ptrIndirectAmount;
	private BigDecimal ptrIcr1;
	private Integer ptrBaseRate1key;
	private BigDecimal ptrIcr2;
	private Integer ptrBaseRate2key;
	private Boolean ptrIsIcrwaiver;
	private String ptrWaiverNumber;
	private BigDecimal ptrCostShareAmount;
	private String ptrOpfundNumber;
	private String ptrSponsorAwardNumber;
	private String ptrNotes;
	private String ptrSurveyKey;
	private boolean ptrIsActive;
	private String ptrUpdatedBy;
	private LocalDateTime ptrDateUpdated;
	private LocalDateTime ptrDateCreated;
	private String ptrUniqueId;
	private String ptrEfadocumentNumber;
	private String ptrEfafailedReason;
	private LocalDateTime ptrEfadateProcessed;
	
	private CgtProjectDTO project;
	private CgtTransactionSubmissionTypeDTO transactionSubmissionType;
	private CgtTransactionTypeDTO transactionType;
	private CgtTransactionGroupTypeDTO transactionGroupType;

	public CgtTransactionDTO() {
	}

	public CgtTransactionDTO(final int ptrTransactionKey, final String ptrProjectKey, final int ptrTransactionTypeKey,
			final int ptrSubmissionTypeKey, final boolean ptrIsActive, final String ptrUpdatedBy, final LocalDateTime ptrDateUpdated,
			final LocalDateTime ptrDateCreated) {
		this.ptrTransactionKey = ptrTransactionKey;
		this.ptrProjectKey = ptrProjectKey;
		this.ptrTransactionTypeKey = ptrTransactionTypeKey;
		this.ptrSubmissionTypeKey = ptrSubmissionTypeKey;
		this.ptrIsActive = ptrIsActive;
		this.ptrUpdatedBy = ptrUpdatedBy;
		this.ptrDateUpdated = ptrDateUpdated;
		this.ptrDateCreated = ptrDateCreated;
	}

	public CgtTransactionDTO(final int ptrTransactionKey, final Integer ptrParentTransactionKey, final Integer ptrTransactionGroupTypeKey,
			final Integer ptrTransactionGroupSequenceNumber, final String ptrTransactionGroupSubContractorKey,
			final String ptrTransactionGroupNameOld, final String ptrTransactionGroupName, final String ptrProjectKey,
			final int ptrTransactionTypeKey, final int ptrSubmissionTypeKey, final LocalDateTime ptrTransactionStart, final LocalDateTime ptrTransactionEnd,
			final BigDecimal ptrDirectAmount, final BigDecimal ptrIndirectAmount, final BigDecimal ptrIcr1, final Integer ptrBaseRate1key,
			final BigDecimal ptrIcr2, final Integer ptrBaseRate2key, final Boolean ptrIsIcrwaiver, final String ptrWaiverNumber,
			final BigDecimal ptrCostShareAmount, final String ptrOpfundNumber, final String ptrSponsorAwardNumber, final String ptrNotes,
			final String ptrSurveyKey, final boolean ptrIsActive, final String ptrUpdatedBy, final LocalDateTime ptrDateUpdated, final LocalDateTime ptrDateCreated,
			final String ptrUniqueId, final String ptrEfadocumentNumber, final String ptrEfafailedReason, final LocalDateTime ptrEfadateProcessed
			) {
		this.ptrTransactionKey = ptrTransactionKey;
		this.ptrParentTransactionKey = ptrParentTransactionKey;
		this.ptrTransactionGroupTypeKey = ptrTransactionGroupTypeKey;
		this.ptrTransactionGroupSequenceNumber = ptrTransactionGroupSequenceNumber;
		this.ptrTransactionGroupSubContractorKey = ptrTransactionGroupSubContractorKey;
		this.ptrTransactionGroupNameOld = ptrTransactionGroupNameOld;
		this.ptrTransactionGroupName = ptrTransactionGroupName;
		this.ptrProjectKey = ptrProjectKey;
		this.ptrTransactionTypeKey = ptrTransactionTypeKey;
		this.ptrSubmissionTypeKey = ptrSubmissionTypeKey;
		this.ptrTransactionStart = ptrTransactionStart;
		this.ptrTransactionEnd = ptrTransactionEnd;
		this.ptrDirectAmount = ptrDirectAmount;
		this.ptrIndirectAmount = ptrIndirectAmount;
		this.ptrIcr1 = ptrIcr1;
		this.ptrBaseRate1key = ptrBaseRate1key;
		this.ptrIcr2 = ptrIcr2;
		this.ptrBaseRate2key = ptrBaseRate2key;
		this.ptrIsIcrwaiver = ptrIsIcrwaiver;
		this.ptrWaiverNumber = ptrWaiverNumber;
		this.ptrCostShareAmount = ptrCostShareAmount;
		this.ptrOpfundNumber = ptrOpfundNumber;
		this.ptrSponsorAwardNumber = ptrSponsorAwardNumber;
		this.ptrNotes = ptrNotes;
		this.ptrSurveyKey = ptrSurveyKey;
		this.ptrIsActive = ptrIsActive;
		this.ptrUpdatedBy = ptrUpdatedBy;
		this.ptrDateUpdated = ptrDateUpdated;
		this.ptrDateCreated = ptrDateCreated;
		this.ptrUniqueId = ptrUniqueId;
		this.ptrEfadocumentNumber = ptrEfadocumentNumber;
		this.ptrEfafailedReason = ptrEfafailedReason;
		this.ptrEfadateProcessed = ptrEfadateProcessed;		
	}

	public CgtTransactionDTO(CgtTransaction transaction) {
		this.ptrTransactionKey = transaction.getPtrTransactionKey();
		this.ptrParentTransactionKey = transaction.getPtrParentTransactionKey();
		this.ptrTransactionGroupTypeKey = transaction.getPtrTransactionGroupTypeKey();
		this.ptrTransactionGroupSequenceNumber = transaction.getPtrTransactionGroupSequenceNumber();
		this.ptrTransactionGroupSubContractorKey = transaction.getPtrTransactionGroupSubContractorKey();
		this.ptrTransactionGroupNameOld = transaction.getPtrTransactionGroupNameOld();
		this.ptrTransactionGroupName = transaction.getPtrTransactionGroupName();
		this.ptrProjectKey = transaction.getPtrProjectKey();
		this.ptrTransactionTypeKey = transaction.getPtrTransactionTypeKey();
		this.ptrSubmissionTypeKey = transaction.getPtrSubmissionTypeKey();
		this.ptrTransactionStart = transaction.getPtrTransactionStart();
		this.ptrTransactionEnd = transaction.getPtrTransactionEnd();
		this.ptrDirectAmount = transaction.getPtrDirectAmount();
		this.ptrIndirectAmount = transaction.getPtrIndirectAmount();
		this.ptrIcr1 = transaction.getPtrIcr1();
		this.ptrBaseRate1key = transaction.getPtrBaseRate1key();
		this.ptrIcr2 = transaction.getPtrIcr2();
		this.ptrBaseRate2key = transaction.getPtrBaseRate2key();
		this.ptrIsIcrwaiver = transaction.getPtrIsIcrwaiver();
		this.ptrWaiverNumber = transaction.getPtrWaiverNumber();
		this.ptrCostShareAmount = transaction.getPtrCostShareAmount();
		this.ptrOpfundNumber = transaction.getPtrOpfundNumber();
		this.ptrSponsorAwardNumber = transaction.getPtrSponsorAwardNumber();
		this.ptrNotes = transaction.getPtrNotes();
		this.ptrSurveyKey = transaction.getPtrSurveyKey();
		this.ptrIsActive = transaction.isPtrIsActive();
		this.ptrUpdatedBy = transaction.getPtrUpdatedBy();
		this.ptrDateUpdated = transaction.getPtrDateUpdated();
		this.ptrDateCreated = transaction.getPtrDateCreated();
		this.ptrUniqueId = transaction.getPtrUniqueId();
		this.ptrEfadocumentNumber = transaction.getPtrEfadocumentNumber();
		this.ptrEfafailedReason = transaction.getPtrEfafailedReason();
		this.ptrEfadateProcessed = transaction.getPtrEfadateProcessed();	
//		this.pspProjectKey = transaction.getPspProjectKey();
//		this.pspSponsorKey = transaction.getPspSponsorKey();
//		this.spoName = transaction.getSpoName();
//		this.spcSponsorCategoryKey = transaction.getSpcSponsorCategoryKey();
//		this.spcName =  transaction.getSpcName();
//		this.spoAgencyKey =  transaction.getSpoAgencyKey();
//		this.proAwardStart =  transaction.getProAwardStart();
//		this.proAwardEnd =  transaction.getProAwardEnd();
//		this.kepUseEmployeeID =  transaction.getKepUseEmployeeID();
//		this.kepUstName =  transaction.getKepUstName();
//		this.proTitle =  transaction.getProTitle();
//		this.pspCFDA =  transaction.getPspCFDA();
//		this.spoSponsorKey =  transaction.getSpoSponsorKey();
//		this.aluFirstName =  transaction.getAluFirstName();
//		this.aluLastName =  transaction.getAluLastName();
//		this.aluPhone =  transaction.getAluPhone();
//		this.aluEmail =  transaction.getAluEmail();
//		this.decUseFirstName =  transaction.getDecUseFirstName();
//		this.decUseLastName =  transaction.getDecUseLastName();
//		this.decUsePhone =  transaction.getDecUsePhone();
//		this.decUseEmail =  transaction.getDecUseEmail();
//		this.depDepartmentKey =  transaction.getDepDepartmentKey();
//		this.depName =  transaction.getDepName();
//		this.totalDirectAmount =  transaction.getTotalDirectAmount();
//		this.totalInDirectAmount =  transaction.getTotalInDirectAmount();
//		this.ansAnswer =  transaction.getAnsAnswer();
	}
	
	
	
	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrTransactionKey()
	 */
	@Override
	public int getPtrTransactionKey() {
		return this.ptrTransactionKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrTransactionKey(int)
	 */
	@Override
	public void setPtrTransactionKey(final int ptrTransactionKey) {
		this.ptrTransactionKey = ptrTransactionKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrParentTransactionKey()
	 */
	@Override
	
	public Integer getPtrParentTransactionKey() {
		return this.ptrParentTransactionKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrParentTransactionKey(java.lang.Integer)
	 */
	@Override
	public void setPtrParentTransactionKey(final Integer ptrParentTransactionKey) {
		this.ptrParentTransactionKey = ptrParentTransactionKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrTransactionGroupTypeKey()
	 */
	@Override
	
	public Integer getPtrTransactionGroupTypeKey() {
		return this.ptrTransactionGroupTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrTransactionGroupTypeKey(java.lang.Integer)
	 */
	@Override
	public void setPtrTransactionGroupTypeKey(final Integer ptrTransactionGroupTypeKey) {
		this.ptrTransactionGroupTypeKey = ptrTransactionGroupTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrTransactionGroupSequenceNumber()
	 */
	@Override
	
	public Integer getPtrTransactionGroupSequenceNumber() {
		return this.ptrTransactionGroupSequenceNumber;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrTransactionGroupSequenceNumber(java.lang.Integer)
	 */
	@Override
	public void setPtrTransactionGroupSequenceNumber(final Integer ptrTransactionGroupSequenceNumber) {
		this.ptrTransactionGroupSequenceNumber = ptrTransactionGroupSequenceNumber;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrTransactionGroupSubContractorKey()
	 */
	@Override
	
	public String getPtrTransactionGroupSubContractorKey() {
		return this.ptrTransactionGroupSubContractorKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrTransactionGroupSubContractorKey(java.lang.String)
	 */
	@Override
	public void setPtrTransactionGroupSubContractorKey(final String ptrTransactionGroupSubContractorKey) {
		this.ptrTransactionGroupSubContractorKey = ptrTransactionGroupSubContractorKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrTransactionGroupNameOld()
	 */
	@Override
	
	public String getPtrTransactionGroupNameOld() {
		return this.ptrTransactionGroupNameOld;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrTransactionGroupNameOld(java.lang.String)
	 */
	@Override
	public void setPtrTransactionGroupNameOld(final String ptrTransactionGroupNameOld) {
		this.ptrTransactionGroupNameOld = ptrTransactionGroupNameOld;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrTransactionGroupName()
	 */
	@Override
	
	public String getPtrTransactionGroupName() {
		return this.ptrTransactionGroupName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrTransactionGroupName(java.lang.String)
	 */
	@Override
	public void setPtrTransactionGroupName(final String ptrTransactionGroupName) {
		this.ptrTransactionGroupName = ptrTransactionGroupName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrProjectKey()
	 */
	@Override
	
	public String getPtrProjectKey() {
		return this.ptrProjectKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrProjectKey(java.lang.String)
	 */
	@Override
	public void setPtrProjectKey(final String ptrProjectKey) {
		this.ptrProjectKey = ptrProjectKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrTransactionTypeKey()
	 */
	@Override
	
	public int getPtrTransactionTypeKey() {
		return this.ptrTransactionTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrTransactionTypeKey(int)
	 */
	@Override
	public void setPtrTransactionTypeKey(final int ptrTransactionTypeKey) {
		this.ptrTransactionTypeKey = ptrTransactionTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrSubmissionTypeKey()
	 */
	@Override
	
	public int getPtrSubmissionTypeKey() {
		return this.ptrSubmissionTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrSubmissionTypeKey(int)
	 */
	@Override
	public void setPtrSubmissionTypeKey(final int ptrSubmissionTypeKey) {
		this.ptrSubmissionTypeKey = ptrSubmissionTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrTransactionStart()
	 */
	@Override
	
	
	public LocalDateTime getPtrTransactionStart() {
		return this.ptrTransactionStart;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrTransactionStart(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setPtrTransactionStart(final LocalDateTime ptrTransactionStart) {
		this.ptrTransactionStart = ptrTransactionStart;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrTransactionEnd()
	 */
	@Override
	
	
	public LocalDateTime getPtrTransactionEnd() {
		return this.ptrTransactionEnd;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrTransactionEnd(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setPtrTransactionEnd(final LocalDateTime ptrTransactionEnd) {
		this.ptrTransactionEnd = ptrTransactionEnd;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrDirectAmount()
	 */
	@Override
	
	public BigDecimal getPtrDirectAmount() {
		return this.ptrDirectAmount;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrDirectAmount(java.math.BigDecimal)
	 */
	@Override
	public void setPtrDirectAmount(final BigDecimal ptrDirectAmount) {
		this.ptrDirectAmount = ptrDirectAmount;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrIndirectAmount()
	 */
	@Override
	
	public BigDecimal getPtrIndirectAmount() {
		return this.ptrIndirectAmount;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrIndirectAmount(java.math.BigDecimal)
	 */
	@Override
	public void setPtrIndirectAmount(final BigDecimal ptrIndirectAmount) {
		this.ptrIndirectAmount = ptrIndirectAmount;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrIcr1()
	 */
	@Override
	
	public BigDecimal getPtrIcr1() {
		return this.ptrIcr1;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrIcr1(java.math.BigDecimal)
	 */
	@Override
	public void setPtrIcr1(final BigDecimal ptrIcr1) {
		this.ptrIcr1 = ptrIcr1;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrBaseRate1key()
	 */
	@Override
	
	public Integer getPtrBaseRate1key() {
		return this.ptrBaseRate1key;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrBaseRate1key(java.lang.Integer)
	 */
	@Override
	public void setPtrBaseRate1key(final Integer ptrBaseRate1key) {
		this.ptrBaseRate1key = ptrBaseRate1key;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrIcr2()
	 */
	@Override
	
	public BigDecimal getPtrIcr2() {
		return this.ptrIcr2;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrIcr2(java.math.BigDecimal)
	 */
	@Override
	public void setPtrIcr2(final BigDecimal ptrIcr2) {
		this.ptrIcr2 = ptrIcr2;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrBaseRate2key()
	 */
	@Override
	
	public Integer getPtrBaseRate2key() {
		return this.ptrBaseRate2key;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrBaseRate2key(java.lang.Integer)
	 */
	@Override
	public void setPtrBaseRate2key(final Integer ptrBaseRate2key) {
		this.ptrBaseRate2key = ptrBaseRate2key;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrIsIcrwaiver()
	 */
	@Override
	
	public Boolean getPtrIsIcrwaiver() {
		return this.ptrIsIcrwaiver;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrIsIcrwaiver(java.lang.Boolean)
	 */
	@Override
	public void setPtrIsIcrwaiver(final Boolean ptrIsIcrwaiver) {
		this.ptrIsIcrwaiver = ptrIsIcrwaiver;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrWaiverNumber()
	 */
	@Override
	
	public String getPtrWaiverNumber() {
		return this.ptrWaiverNumber;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrWaiverNumber(java.lang.String)
	 */
	@Override
	public void setPtrWaiverNumber(final String ptrWaiverNumber) {
		this.ptrWaiverNumber = ptrWaiverNumber;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrCostShareAmount()
	 */
	@Override
	
	public BigDecimal getPtrCostShareAmount() {
		return this.ptrCostShareAmount;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrCostShareAmount(java.math.BigDecimal)
	 */
	@Override
	public void setPtrCostShareAmount(final BigDecimal ptrCostShareAmount) {
		this.ptrCostShareAmount = ptrCostShareAmount;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrOpfundNumber()
	 */
	@Override
	
	public String getPtrOpfundNumber() {
		return this.ptrOpfundNumber;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrOpfundNumber(java.lang.String)
	 */
	@Override
	public void setPtrOpfundNumber(final String ptrOpfundNumber) {
		this.ptrOpfundNumber = ptrOpfundNumber;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrSponsorAwardNumber()
	 */
	@Override
	
	public String getPtrSponsorAwardNumber() {
		return this.ptrSponsorAwardNumber;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrSponsorAwardNumber(java.lang.String)
	 */
	@Override
	public void setPtrSponsorAwardNumber(final String ptrSponsorAwardNumber) {
		this.ptrSponsorAwardNumber = ptrSponsorAwardNumber;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrNotes()
	 */
	@Override
	
	public String getPtrNotes() {
		return this.ptrNotes;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrNotes(java.lang.String)
	 */
	@Override
	public void setPtrNotes(final String ptrNotes) {
		this.ptrNotes = ptrNotes;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrSurveyKey()
	 */
	@Override
	
	public String getPtrSurveyKey() {
		return this.ptrSurveyKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrSurveyKey(java.lang.String)
	 */
	@Override
	public void setPtrSurveyKey(final String ptrSurveyKey) {
		this.ptrSurveyKey = ptrSurveyKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#isPtrIsActive()
	 */
	@Override
	
	public boolean isPtrIsActive() {
		return this.ptrIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrIsActive(boolean)
	 */
	@Override
	public void setPtrIsActive(final boolean ptrIsActive) {
		this.ptrIsActive = ptrIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrUpdatedBy()
	 */
	@Override
	
	public String getPtrUpdatedBy() {
		return this.ptrUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrUpdatedBy(java.lang.String)
	 */
	@Override
	public void setPtrUpdatedBy(final String ptrUpdatedBy) {
		this.ptrUpdatedBy = ptrUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrDateUpdated()
	 */
	@Override
	
	
	public LocalDateTime getPtrDateUpdated() {
		return this.ptrDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setPtrDateUpdated(final LocalDateTime ptrDateUpdated) {
		this.ptrDateUpdated = ptrDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrDateCreated()
	 */
	@Override
	
	
	public LocalDateTime getPtrDateCreated() {
		return this.ptrDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setPtrDateCreated(final LocalDateTime ptrDateCreated) {
		this.ptrDateCreated = ptrDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrUniqueId()
	 */
	@Override
	
	public String getPtrUniqueId() {
		return this.ptrUniqueId;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrUniqueId(java.lang.String)
	 */
	@Override
	public void setPtrUniqueId(final String ptrUniqueId) {
		this.ptrUniqueId = ptrUniqueId;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrEfadocumentNumber()
	 */
	@Override
	
	public String getPtrEfadocumentNumber() {
		return this.ptrEfadocumentNumber;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrEfadocumentNumber(java.lang.String)
	 */
	@Override
	public void setPtrEfadocumentNumber(final String ptrEfadocumentNumber) {
		this.ptrEfadocumentNumber = ptrEfadocumentNumber;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrEfafailedReason()
	 */
	@Override
	
	public String getPtrEfafailedReason() {
		return this.ptrEfafailedReason;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrEfafailedReason(java.lang.String)
	 */
	@Override
	public void setPtrEfafailedReason(final String ptrEfafailedReason) {
		this.ptrEfafailedReason = ptrEfafailedReason;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrEfadateProcessed()
	 */
	@Override
	
	
	public LocalDateTime getPtrEfadateProcessed() {
		return this.ptrEfadateProcessed;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrEfadateProcessed(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setPtrEfadateProcessed(final LocalDateTime ptrEfadateProcessed) {
		this.ptrEfadateProcessed = ptrEfadateProcessed;
	}

	
	public CgtProjectDTO getProjectImpl() {
		return project;
	}
	
	public void setProjectImpl(CgtProjectDTO project) {
		this.project = project;
	}
			
	public CgtTransactionSubmissionTypeDTO getTransactionSubmissionTypeImpl() {
		return transactionSubmissionType;
	}

	
	public void setTransactionSubmissionTypeImpl(CgtTransactionSubmissionTypeDTO transactionSubmissionType) {
		this.transactionSubmissionType = transactionSubmissionType;
	}
	
	public CgtTransactionTypeDTO getTransactionTypeImpl() {
		return transactionType;
	}

	public void setTransactionTypeImpl(CgtTransactionTypeDTO transactionType) {
		this.transactionType = transactionType;
	}

	
	
	public CgtTransactionGroupTypeDTO getTransactionGroupTypeImpl() {
		return transactionGroupType;
	}

	public void setTransactionGroupTypeImpl(CgtTransactionGroupTypeDTO transactionGroupType) {
		this.transactionGroupType = transactionGroupType;
	}
	
	@Override
	public CgtProject getProject() {
		return project;
	}

	@Override
	public void setProject(CgtProject project) {
		this.project = (CgtProjectDTO) project;
	}

	@Override
	
	public CgtTransactionSubmissionType getTransactionSubmissionType() {
		return transactionSubmissionType;
	}

	@Override
	public void setTransactionSubmissionType(CgtTransactionSubmissionType transactionSubmissionType) {
		this.transactionSubmissionType = (CgtTransactionSubmissionTypeDTO) transactionSubmissionType;
	}

	@Override
	public CgtTransactionType getTransactionType() {
		return transactionType;
	}

	@Override
	public void setTransactionType(CgtTransactionType transactionType) {
		this.transactionType = (CgtTransactionTypeDTO) transactionType;
	}

	@Override
	
	public CgtTransactionGroupType getTransactionGroupType() {
		return this.transactionGroupType;
	}

	@Override
	public void setTransactionGroupType(CgtTransactionGroupType transactionGroupType) {
		this.transactionGroupType = (CgtTransactionGroupTypeDTO) transactionGroupType;
	}

	public CgtTransactionDTO toDTO() {
		return new CgtTransactionDTO(this);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((project == null) ? 0 : project.hashCode());
		result = prime * result + ((ptrBaseRate1key == null) ? 0 : ptrBaseRate1key.hashCode());
		result = prime * result + ((ptrBaseRate2key == null) ? 0 : ptrBaseRate2key.hashCode());
		result = prime * result + ((ptrCostShareAmount == null) ? 0 : ptrCostShareAmount.hashCode());
		result = prime * result + ((ptrDateCreated == null) ? 0 : ptrDateCreated.hashCode());
		result = prime * result + ((ptrDateUpdated == null) ? 0 : ptrDateUpdated.hashCode());
		result = prime * result + ((ptrDirectAmount == null) ? 0 : ptrDirectAmount.hashCode());
		result = prime * result + ((ptrEfadateProcessed == null) ? 0 : ptrEfadateProcessed.hashCode());
		result = prime * result + ((ptrEfadocumentNumber == null) ? 0 : ptrEfadocumentNumber.hashCode());
		result = prime * result + ((ptrEfafailedReason == null) ? 0 : ptrEfafailedReason.hashCode());
		result = prime * result + ((ptrIcr1 == null) ? 0 : ptrIcr1.hashCode());
		result = prime * result + ((ptrIcr2 == null) ? 0 : ptrIcr2.hashCode());
		result = prime * result + ((ptrIndirectAmount == null) ? 0 : ptrIndirectAmount.hashCode());
		result = prime * result + (ptrIsActive ? 1231 : 1237);
		result = prime * result + ((ptrIsIcrwaiver == null) ? 0 : ptrIsIcrwaiver.hashCode());
		result = prime * result + ((ptrNotes == null) ? 0 : ptrNotes.hashCode());
		result = prime * result + ((ptrOpfundNumber == null) ? 0 : ptrOpfundNumber.hashCode());
		result = prime * result + ((ptrParentTransactionKey == null) ? 0 : ptrParentTransactionKey.hashCode());
		result = prime * result + ((ptrProjectKey == null) ? 0 : ptrProjectKey.hashCode());
		result = prime * result + ((ptrSponsorAwardNumber == null) ? 0 : ptrSponsorAwardNumber.hashCode());
		result = prime * result + ptrSubmissionTypeKey;
		result = prime * result + ((ptrSurveyKey == null) ? 0 : ptrSurveyKey.hashCode());
		result = prime * result + ((ptrTransactionEnd == null) ? 0 : ptrTransactionEnd.hashCode());
		result = prime * result + ((ptrTransactionGroupName == null) ? 0 : ptrTransactionGroupName.hashCode());
		result = prime * result + ((ptrTransactionGroupNameOld == null) ? 0 : ptrTransactionGroupNameOld.hashCode());
		result = prime * result
				+ ((ptrTransactionGroupSequenceNumber == null) ? 0 : ptrTransactionGroupSequenceNumber.hashCode());
		result = prime * result
				+ ((ptrTransactionGroupSubContractorKey == null) ? 0 : ptrTransactionGroupSubContractorKey.hashCode());
		result = prime * result + ((ptrTransactionGroupTypeKey == null) ? 0 : ptrTransactionGroupTypeKey.hashCode());
		result = prime * result + ptrTransactionKey;
		result = prime * result + ((ptrTransactionStart == null) ? 0 : ptrTransactionStart.hashCode());
		result = prime * result + ptrTransactionTypeKey;
		result = prime * result + ((ptrUniqueId == null) ? 0 : ptrUniqueId.hashCode());
		result = prime * result + ((ptrUpdatedBy == null) ? 0 : ptrUpdatedBy.hashCode());
		result = prime * result + ((ptrWaiverNumber == null) ? 0 : ptrWaiverNumber.hashCode());
		result = prime * result + ((transactionGroupType == null) ? 0 : transactionGroupType.hashCode());
		result = prime * result + ((transactionSubmissionType == null) ? 0 : transactionSubmissionType.hashCode());
		result = prime * result + ((transactionType == null) ? 0 : transactionType.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof CgtTransactionDTO)) {
			return false;
		}
		CgtTransactionDTO other = (CgtTransactionDTO) obj;
		if (project == null) {
			if (other.project != null) {
				return false;
			}
		} else if (!project.equals(other.project)) {
			return false;
		}
		if (ptrBaseRate1key == null) {
			if (other.ptrBaseRate1key != null) {
				return false;
			}
		} else if (!ptrBaseRate1key.equals(other.ptrBaseRate1key)) {
			return false;
		}
		if (ptrBaseRate2key == null) {
			if (other.ptrBaseRate2key != null) {
				return false;
			}
		} else if (!ptrBaseRate2key.equals(other.ptrBaseRate2key)) {
			return false;
		}
		if (ptrCostShareAmount == null) {
			if (other.ptrCostShareAmount != null) {
				return false;
			}
		} else if (!ptrCostShareAmount.equals(other.ptrCostShareAmount)) {
			return false;
		}
		if (ptrDateCreated == null) {
			if (other.ptrDateCreated != null) {
				return false;
			}
		} else if (!ptrDateCreated.equals(other.ptrDateCreated)) {
			return false;
		}
		if (ptrDateUpdated == null) {
			if (other.ptrDateUpdated != null) {
				return false;
			}
		} else if (!ptrDateUpdated.equals(other.ptrDateUpdated)) {
			return false;
		}
		if (ptrDirectAmount == null) {
			if (other.ptrDirectAmount != null) {
				return false;
			}
		} else if (!ptrDirectAmount.equals(other.ptrDirectAmount)) {
			return false;
		}
		if (ptrEfadateProcessed == null) {
			if (other.ptrEfadateProcessed != null) {
				return false;
			}
		} else if (!ptrEfadateProcessed.equals(other.ptrEfadateProcessed)) {
			return false;
		}
		if (ptrEfadocumentNumber == null) {
			if (other.ptrEfadocumentNumber != null) {
				return false;
			}
		} else if (!ptrEfadocumentNumber.equals(other.ptrEfadocumentNumber)) {
			return false;
		}
		if (ptrEfafailedReason == null) {
			if (other.ptrEfafailedReason != null) {
				return false;
			}
		} else if (!ptrEfafailedReason.equals(other.ptrEfafailedReason)) {
			return false;
		}
		if (ptrIcr1 == null) {
			if (other.ptrIcr1 != null) {
				return false;
			}
		} else if (!ptrIcr1.equals(other.ptrIcr1)) {
			return false;
		}
		if (ptrIcr2 == null) {
			if (other.ptrIcr2 != null) {
				return false;
			}
		} else if (!ptrIcr2.equals(other.ptrIcr2)) {
			return false;
		}
		if (ptrIndirectAmount == null) {
			if (other.ptrIndirectAmount != null) {
				return false;
			}
		} else if (!ptrIndirectAmount.equals(other.ptrIndirectAmount)) {
			return false;
		}
		if (ptrIsActive != other.ptrIsActive) {
			return false;
		}
		if (ptrIsIcrwaiver == null) {
			if (other.ptrIsIcrwaiver != null) {
				return false;
			}
		} else if (!ptrIsIcrwaiver.equals(other.ptrIsIcrwaiver)) {
			return false;
		}
		if (ptrNotes == null) {
			if (other.ptrNotes != null) {
				return false;
			}
		} else if (!ptrNotes.equals(other.ptrNotes)) {
			return false;
		}
		if (ptrOpfundNumber == null) {
			if (other.ptrOpfundNumber != null) {
				return false;
			}
		} else if (!ptrOpfundNumber.equals(other.ptrOpfundNumber)) {
			return false;
		}
		if (ptrParentTransactionKey == null) {
			if (other.ptrParentTransactionKey != null) {
				return false;
			}
		} else if (!ptrParentTransactionKey.equals(other.ptrParentTransactionKey)) {
			return false;
		}
		if (ptrProjectKey == null) {
			if (other.ptrProjectKey != null) {
				return false;
			}
		} else if (!ptrProjectKey.equals(other.ptrProjectKey)) {
			return false;
		}
		if (ptrSponsorAwardNumber == null) {
			if (other.ptrSponsorAwardNumber != null) {
				return false;
			}
		} else if (!ptrSponsorAwardNumber.equals(other.ptrSponsorAwardNumber)) {
			return false;
		}
		if (ptrSubmissionTypeKey != other.ptrSubmissionTypeKey) {
			return false;
		}
		if (ptrSurveyKey == null) {
			if (other.ptrSurveyKey != null) {
				return false;
			}
		} else if (!ptrSurveyKey.equals(other.ptrSurveyKey)) {
			return false;
		}
		if (ptrTransactionEnd == null) {
			if (other.ptrTransactionEnd != null) {
				return false;
			}
		} else if (!ptrTransactionEnd.equals(other.ptrTransactionEnd)) {
			return false;
		}
		if (ptrTransactionGroupName == null) {
			if (other.ptrTransactionGroupName != null) {
				return false;
			}
		} else if (!ptrTransactionGroupName.equals(other.ptrTransactionGroupName)) {
			return false;
		}
		if (ptrTransactionGroupNameOld == null) {
			if (other.ptrTransactionGroupNameOld != null) {
				return false;
			}
		} else if (!ptrTransactionGroupNameOld.equals(other.ptrTransactionGroupNameOld)) {
			return false;
		}
		if (ptrTransactionGroupSequenceNumber == null) {
			if (other.ptrTransactionGroupSequenceNumber != null) {
				return false;
			}
		} else if (!ptrTransactionGroupSequenceNumber.equals(other.ptrTransactionGroupSequenceNumber)) {
			return false;
		}
		if (ptrTransactionGroupSubContractorKey == null) {
			if (other.ptrTransactionGroupSubContractorKey != null) {
				return false;
			}
		} else if (!ptrTransactionGroupSubContractorKey.equals(other.ptrTransactionGroupSubContractorKey)) {
			return false;
		}
		if (ptrTransactionGroupTypeKey == null) {
			if (other.ptrTransactionGroupTypeKey != null) {
				return false;
			}
		} else if (!ptrTransactionGroupTypeKey.equals(other.ptrTransactionGroupTypeKey)) {
			return false;
		}
		if (ptrTransactionKey != other.ptrTransactionKey) {
			return false;
		}
		if (ptrTransactionStart == null) {
			if (other.ptrTransactionStart != null) {
				return false;
			}
		} else if (!ptrTransactionStart.equals(other.ptrTransactionStart)) {
			return false;
		}
		if (ptrTransactionTypeKey != other.ptrTransactionTypeKey) {
			return false;
		}
		if (ptrUniqueId == null) {
			if (other.ptrUniqueId != null) {
				return false;
			}
		} else if (!ptrUniqueId.equals(other.ptrUniqueId)) {
			return false;
		}
		if (ptrUpdatedBy == null) {
			if (other.ptrUpdatedBy != null) {
				return false;
			}
		} else if (!ptrUpdatedBy.equals(other.ptrUpdatedBy)) {
			return false;
		}
		if (ptrWaiverNumber == null) {
			if (other.ptrWaiverNumber != null) {
				return false;
			}
		} else if (!ptrWaiverNumber.equals(other.ptrWaiverNumber)) {
			return false;
		}
		if (transactionGroupType == null) {
			if (other.transactionGroupType != null) {
				return false;
			}
		} else if (!transactionGroupType.equals(other.transactionGroupType)) {
			return false;
		}
		if (transactionSubmissionType == null) {
			if (other.transactionSubmissionType != null) {
				return false;
			}
		} else if (!transactionSubmissionType.equals(other.transactionSubmissionType)) {
			return false;
		}
		if (transactionType == null) {
			if (other.transactionType != null) {
				return false;
			}
		} else if (!transactionType.equals(other.transactionType)) {
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"CgtTransactionDTO [ptrTransactionKey=%s, ptrParentTransactionKey=%s, ptrTransactionGroupTypeKey=%s, ptrTransactionGroupSequenceNumber=%s, ptrTransactionGroupSubContractorKey=%s, ptrTransactionGroupNameOld=%s, ptrTransactionGroupName=%s, ptrProjectKey=%s, ptrTransactionTypeKey=%s, ptrSubmissionTypeKey=%s, ptrTransactionStart=%s, ptrTransactionEnd=%s, ptrDirectAmount=%s, ptrIndirectAmount=%s, ptrIcr1=%s, ptrBaseRate1key=%s, ptrIcr2=%s, ptrBaseRate2key=%s, ptrIsIcrwaiver=%s, ptrWaiverNumber=%s, ptrCostShareAmount=%s, ptrOpfundNumber=%s, ptrSponsorAwardNumber=%s, ptrNotes=%s, ptrSurveyKey=%s, ptrIsActive=%s, ptrUpdatedBy=%s, ptrDateUpdated=%s, ptrDateCreated=%s, ptrUniqueId=%s, ptrEfadocumentNumber=%s, ptrEfafailedReason=%s, ptrEfadateProcessed=%s, project=%s, transactionSubmissionType=%s, transactionType=%s, transactionGroupType=%s]",
				ptrTransactionKey, ptrParentTransactionKey, ptrTransactionGroupTypeKey,
				ptrTransactionGroupSequenceNumber, ptrTransactionGroupSubContractorKey, ptrTransactionGroupNameOld,
				ptrTransactionGroupName, ptrProjectKey, ptrTransactionTypeKey, ptrSubmissionTypeKey,
				ptrTransactionStart, ptrTransactionEnd, ptrDirectAmount, ptrIndirectAmount, ptrIcr1, ptrBaseRate1key,
				ptrIcr2, ptrBaseRate2key, ptrIsIcrwaiver, ptrWaiverNumber, ptrCostShareAmount, ptrOpfundNumber,
				ptrSponsorAwardNumber, ptrNotes, ptrSurveyKey, ptrIsActive, ptrUpdatedBy, ptrDateUpdated,
				ptrDateCreated, ptrUniqueId, ptrEfadocumentNumber, ptrEfafailedReason, ptrEfadateProcessed, project,
				transactionSubmissionType, transactionType, transactionGroupType);
	}


	
}
