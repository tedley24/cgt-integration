package edu.ucdavis.orvc.integration.cgt.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtQaaSections;
import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtQaaSectionsDTO;



/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/
@Entity
@Table(name = "CGT_QaaSections")
public class CgtQaaSectionsImpl implements CgtQaaSections {

	private static final long serialVersionUID = -1390498703624138597L;
	private int secSectionKey;
	private String secName;
	private String secDescription;
	private boolean secIsActive;
	private LocalDateTime secDateCreated;
	private LocalDateTime secDateUpdated;
	private String secUpdatedBy;

	public CgtQaaSectionsImpl() {
	}

	public CgtQaaSectionsImpl(final int secSectionKey, final String secName, final boolean secIsActive, final LocalDateTime secDateCreated,
			final LocalDateTime secDateUpdated, final String secUpdatedBy) {
		this.secSectionKey = secSectionKey;
		this.secName = secName;
		this.secIsActive = secIsActive;
		this.secDateCreated = secDateCreated;
		this.secDateUpdated = secDateUpdated;
		this.secUpdatedBy = secUpdatedBy;
	}

	public CgtQaaSectionsImpl(final int secSectionKey, final String secName, final String secDescription, final boolean secIsActive,
			final LocalDateTime secDateCreated, final LocalDateTime secDateUpdated, final String secUpdatedBy) {
		this.secSectionKey = secSectionKey;
		this.secName = secName;
		this.secDescription = secDescription;
		this.secIsActive = secIsActive;
		this.secDateCreated = secDateCreated;
		this.secDateUpdated = secDateUpdated;
		this.secUpdatedBy = secUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaSections#getSecSectionKey()
	 */
	@Override
	@Id
	@Column(name = "sec_sectionKey", unique = true, nullable = false)
	public int getSecSectionKey() {
		return this.secSectionKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaSections#setSecSectionKey(int)
	 */
	@Override
	public void setSecSectionKey(final int secSectionKey) {
		this.secSectionKey = secSectionKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaSections#getSecName()
	 */
	@Override
	@Column(name = "sec_name", nullable = false, length = 50)
	public String getSecName() {
		return this.secName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaSections#setSecName(java.lang.String)
	 */
	@Override
	public void setSecName(final String secName) {
		this.secName = secName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaSections#getSecDescription()
	 */
	@Override
	@Column(name = "sec_description", length = 50)
	public String getSecDescription() {
		return this.secDescription;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaSections#setSecDescription(java.lang.String)
	 */
	@Override
	public void setSecDescription(final String secDescription) {
		this.secDescription = secDescription;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaSections#isSecIsActive()
	 */
	@Override
	@Column(name = "sec_isActive", nullable = false)
	public boolean isSecIsActive() {
		return this.secIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaSections#setSecIsActive(boolean)
	 */
	@Override
	public void setSecIsActive(final boolean secIsActive) {
		this.secIsActive = secIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaSections#getSecDateCreated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "sec_dateCreated", nullable = false, length = 23)
	public LocalDateTime getSecDateCreated() {
		return this.secDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaSections#setSecDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setSecDateCreated(final LocalDateTime secDateCreated) {
		this.secDateCreated = secDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaSections#getSecDateUpdated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "sec_dateUpdated", nullable = false, length = 23)
	public LocalDateTime getSecDateUpdated() {
		return this.secDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaSections#setSecDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setSecDateUpdated(final LocalDateTime secDateUpdated) {
		this.secDateUpdated = secDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaSections#getSecUpdatedBy()
	 */
	@Override
	@Column(name = "sec_updatedBy", nullable = false, length = 35, columnDefinition="char")
	public String getSecUpdatedBy() {
		return this.secUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaSections#setSecUpdatedBy(java.lang.String)
	 */
	@Override
	public void setSecUpdatedBy(final String secUpdatedBy) {
		this.secUpdatedBy = secUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((secDateCreated == null) ? 0 : secDateCreated.hashCode());
		result = prime * result + ((secDateUpdated == null) ? 0 : secDateUpdated.hashCode());
		result = prime * result + ((secDescription == null) ? 0 : secDescription.hashCode());
		result = prime * result + (secIsActive ? 1231 : 1237);
		result = prime * result + ((secName == null) ? 0 : secName.hashCode());
		result = prime * result + secSectionKey;
		result = prime * result + ((secUpdatedBy == null) ? 0 : secUpdatedBy.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof CgtQaaSectionsImpl)) {
			return false;
		}
		CgtQaaSectionsImpl other = (CgtQaaSectionsImpl) obj;
		if (secDateCreated == null) {
			if (other.secDateCreated != null) {
				return false;
			}
		} else if (!secDateCreated.equals(other.secDateCreated)) {
			return false;
		}
		if (secDateUpdated == null) {
			if (other.secDateUpdated != null) {
				return false;
			}
		} else if (!secDateUpdated.equals(other.secDateUpdated)) {
			return false;
		}
		if (secDescription == null) {
			if (other.secDescription != null) {
				return false;
			}
		} else if (!secDescription.equals(other.secDescription)) {
			return false;
		}
		if (secIsActive != other.secIsActive) {
			return false;
		}
		if (secName == null) {
			if (other.secName != null) {
				return false;
			}
		} else if (!secName.equals(other.secName)) {
			return false;
		}
		if (secSectionKey != other.secSectionKey) {
			return false;
		}
		if (secUpdatedBy == null) {
			if (other.secUpdatedBy != null) {
				return false;
			}
		} else if (!secUpdatedBy.equals(other.secUpdatedBy)) {
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"CgtQaaSectionsImpl [secSectionKey=%s, secName=%s, secDescription=%s, secIsActive=%s, secDateCreated=%s, secDateUpdated=%s, secUpdatedBy=%s]",
				secSectionKey, secName, secDescription, secIsActive, secDateCreated, secDateUpdated, secUpdatedBy);
	}



	public CgtQaaSectionsDTO toDTO() {
		return new CgtQaaSectionsDTO(this);
	}

}
