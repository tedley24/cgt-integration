package edu.ucdavis.orvc.integration.cgt.data.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.spi.SessionFactoryOptions;
import org.hibernate.criterion.Restrictions;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtEfaTransactionProcessInfo;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtProject;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtTransaction;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtTransactionTypesEnum;
import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtEfaTransactionProcessInfoDTO;
import edu.ucdavis.orvc.integration.cgt.data.dao.CgtProjectDao;
import edu.ucdavis.orvc.integration.cgt.data.dao.CgtTransactionDao;
import edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionImpl;
import edu.ucdavis.orvc.support.service.GeneralServiceBase;
import edu.ucdavis.orvc.support.service.InvalidArgumentException;


@Repository("cgtTransactionDao")
@Transactional
public class CgtTransactionDaoImpl extends GeneralServiceBase implements CgtTransactionDao {

	@Autowired
	@Qualifier("cgtDataSessionFactory")
	private SessionFactory cgtDataSessionFactory;

	@Autowired
	@Qualifier("cgtProjectDao")
	CgtProjectDao projectDao;
	
	
	//private final Logger logger = LoggerFactory.getLogger(CgtTransactionDaoImpl.class);
	
	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.data.impl.CgtTransactionDao#getProjectTransactionsByTypeAndSubmissionType(java.lang.String, int, int)
	 */
	@Override
	@Transactional
	public List<CgtTransactionImpl> getProjectTransactionsByTypeAndSubmissionType(final int transactionKey, final String projectNumber
																			 ,final int transactionTypeKey
																			 ,final int submissionTypeKey) 
	{
		final Builder<CgtTransactionImpl> resultBuilder = new ImmutableList.Builder<CgtTransactionImpl>();
		final List<CgtTransactionImpl> prelimResults = getCurrentSession().createCriteria(CgtTransactionImpl.class)
				.add(Restrictions.eq(FIELD_TRANSACTION_KEY, transactionKey))
				.add(Restrictions.eq(FIELD_PROJECT_NUMBER, projectNumber))
				.add(Restrictions.eq(FIELD_TRANSACTION_TYPE_KEY, transactionTypeKey))
				.add(Restrictions.eq(FIELD_SUBMISSION_TYPE_KEY, submissionTypeKey))
				.add(Restrictions.eq(FIELD_IS_ACTIVE, true))
				.list();
		resultBuilder.addAll(prelimResults);
		List<CgtTransactionImpl> cgtList= resultBuilder.build();
		SessionFactoryOptions factoryOptions = cgtDataSessionFactory.getSessionFactoryOptions();
		return cgtList;
	}
	
	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.data.impl.CgtTransactionDao#getProjectTransactions(java.lang.String, int, int, boolean, org.joda.time.LocalDate)
	 */
	@Override
	public List<CgtTransactionImpl> getProjectTransactions(final String projectNumber
			 ,final int transactionTypeKey
			 ,final int submissionTypeKey
			 ,final boolean active
			 ,final LocalDate createdAfterDt) 
	{

		Criteria criteria = getCurrentSession().createCriteria(CgtTransactionImpl.class)
				.add(Restrictions.eq(FIELD_PROJECT_NUMBER, projectNumber))
				.add(Restrictions.eq(FIELD_TRANSACTION_TYPE_KEY, transactionTypeKey))
				.add(Restrictions.eq(FIELD_SUBMISSION_TYPE_KEY, submissionTypeKey))
				.add(Restrictions.eq(FIELD_IS_ACTIVE, true));				
		
		if (createdAfterDt!=null) {
			criteria.add(Restrictions.ge(FIELD_DATE_CREATED, createdAfterDt));
		}
		
		return criteria.list();
	}
	

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.data.impl.CgtTransactionDao#findFeedEligibleTransactions(java.lang.String)
	 */
	@Override
	public List<CgtTransactionImpl> findFeedEligibleTransactions(final String projectNumber) {
		final CgtProject prj =projectDao.getCgaEligibleProject(projectNumber, true);
		
		if ( prj == null ) {
			throw new InvalidArgumentException(
					String.format("Project {} does not exist or is not active", projectNumber)
					);
		}
	
		final List<CgtTransactionImpl> candidateTxs = getProjectTransactions(
				projectNumber
			    , CGA_TRANSACTION_TYPE_KEY
				, CGA_SUBMISSION_TYPE_KEY
				, true
				, CGA_ELIGIBLE_START_DATE);
	
		return candidateTxs;
	}
	
	public enum EfaFilterEnum {
		PROCESSED, UN_PROCESSED, ALL, NONE;
	}
	
	protected List<CgtTransactionImpl> filterForEfaProcessed(final List<CgtTransactionImpl> inputList, EfaFilterEnum filter) {
		final List<CgtTransactionImpl> results;
		
		if (inputList.size()>0 && filter !=EfaFilterEnum.NONE) {
			Builder<CgtTransactionImpl> resultsBuilder = new ImmutableList.Builder<CgtTransactionImpl>();
			if (filter == EfaFilterEnum.ALL) {
				resultsBuilder.addAll(inputList);
			} else {
				for (CgtTransactionImpl t:inputList) {
					switch(filter) {
						case PROCESSED:if (t.getPtrEfadateProcessed() != null)  resultsBuilder.add(t) ;
								   	   break;
						case UN_PROCESSED: if(t.getPtrEfadateProcessed() == null) resultsBuilder.add(t);
								   		   break;
						default: break;
					}
				}
			}
			results = resultsBuilder.build();
		} else {
			results = Collections.emptyList();
		}
	
		return results;
	}
	
	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.data.impl.CgtTransactionDao#findCgaProcessedTransactions(java.lang.String)
	 */
	@Override
	public List<CgtTransactionImpl> findCgaProcessedTransactions( final String projectNumber ) {
		List<CgtTransactionImpl> candidateTxs = findFeedEligibleTransactions(projectNumber);
		return filterForEfaProcessed(candidateTxs, EfaFilterEnum.PROCESSED);
	}
	
	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.data.impl.CgtTransactionDao#findCgaPendingTransactions(java.lang.String)
	 */
	@Override
	public List<CgtTransactionImpl> findCgaPendingTransactions( final String projectNumber ) {
		List<CgtTransactionImpl> candidateTxs = findFeedEligibleTransactions(projectNumber);
		return filterForEfaProcessed(candidateTxs, EfaFilterEnum.UN_PROCESSED);
	}
	
	protected SessionFactory getDataCgtSessionFactory() { return cgtDataSessionFactory; }
	protected Session getCurrentSession() { return getDataCgtSessionFactory().getCurrentSession(); }

	/**
	 * @param projectDao the projectDao to set
	 */
	public void setProjectDao(CgtProjectDao projectDao) {
		this.projectDao = projectDao;
	}

	@Override
	public List<CgtEfaTransactionProcessInfo> findAllEfaProcessInfo(String projectNumber) {
		final List<CgtEfaTransactionProcessInfo> result =
				new ArrayList<CgtEfaTransactionProcessInfo>();
		final List<CgtTransactionImpl> fedTxs = findCgaProcessedTransactions(projectNumber);
		for (CgtTransactionImpl tx : fedTxs) {
			result.add(new CgtEfaTransactionProcessInfoDTO(tx));
		}
		return result;
	}
	
	@Override
	public List<CgtTransactionImpl> findUnprocessedFeedEligibleTransactions(final int maxDaysOld) {
		final List<CgtTransactionImpl> results;
		LocalDate createdSince = LocalDate.now().minusDays(maxDaysOld);
		
		
		Criteria criteria = getCurrentSession().createCriteria(CgtTransactionImpl.class)
				.add(Restrictions.eq(FIELD_TRANSACTION_TYPE_KEY, CgtTransactionTypesEnum.AWARD.typeKey))
				.add(Restrictions.eq(FIELD_SUBMISSION_TYPE_KEY, 3))
				.add(Restrictions.eq(FIELD_IS_ACTIVE, true))
				.add(Restrictions.ge(FIELD_DATE_CREATED,createdSince));
		
		results = criteria.list();
		return results;
	}

	public void storeCgtTransactionImpl(CgtTransaction transaction) {
		getCurrentSession().saveOrUpdate(transaction);
	}

	@Override
	public List<CgtTransactionImpl> getProjectTransactionsByTypeAndSubmissionType(
			String projectNumber, int transactionTypeKey, int submissionTypeKey) {
		final Builder<CgtTransactionImpl> resultBuilder = new ImmutableList.Builder<CgtTransactionImpl>();
		final List<CgtTransactionImpl> prelimResults = getCurrentSession().createCriteria(CgtTransactionImpl.class)
				.add(Restrictions.eq(FIELD_PROJECT_NUMBER, projectNumber))
				.add(Restrictions.eq(FIELD_TRANSACTION_TYPE_KEY, transactionTypeKey))
				.add(Restrictions.eq(FIELD_SUBMISSION_TYPE_KEY, submissionTypeKey))
				.add(Restrictions.eq(FIELD_IS_ACTIVE, true))
				.list();
		resultBuilder.addAll(prelimResults);
		List<CgtTransactionImpl> cgtList= resultBuilder.build();
		SessionFactoryOptions factoryOptions = cgtDataSessionFactory.getSessionFactoryOptions();
		return cgtList;
	}

	
	
	
}
