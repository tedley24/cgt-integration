package edu.ucdavis.orvc.integration.cgt.domain;


import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtProject;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtTransaction;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtTransactionGroupType;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtTransactionSubmissionType;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtTransactionType;
import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtTransactionDTO;


/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/
@Entity
@Table(name = "CGT_Transactions")
public class CgtTransactionImpl implements CgtTransaction {

	private static final long serialVersionUID = -5536365787765282152L;
	private int ptrTransactionKey;
	private Integer ptrParentTransactionKey;
	private Integer ptrTransactionGroupTypeKey;
	private Integer ptrTransactionGroupSequenceNumber;
	private String ptrTransactionGroupSubContractorKey;
	private String ptrTransactionGroupNameOld;
	private String ptrTransactionGroupName;
	private String ptrProjectKey;
	private int ptrTransactionTypeKey;
	private int ptrSubmissionTypeKey;
	private LocalDateTime ptrTransactionStart;
	private LocalDateTime ptrTransactionEnd;
	private BigDecimal ptrDirectAmount;
	private BigDecimal ptrIndirectAmount;
	private BigDecimal ptrIcr1;
	private Integer ptrBaseRate1key;
	private BigDecimal ptrIcr2;
	private Integer ptrBaseRate2key;
	private Boolean ptrIsIcrwaiver;
	private String ptrWaiverNumber;
	private BigDecimal ptrCostShareAmount;
	private String ptrOpfundNumber;
	private String ptrSponsorAwardNumber;
	private String ptrNotes;
	private String ptrSurveyKey;
	private boolean ptrIsActive;
	private String ptrUpdatedBy;
	private LocalDateTime ptrDateUpdated;
	private LocalDateTime ptrDateCreated;
	private String ptrUniqueId;
	private String ptrEfadocumentNumber;
	private String ptrEfafailedReason;
	private LocalDateTime ptrEfadateProcessed;
	private String pspSponsorKey;
	private String spoName;
	private String spcSponsorCategoryKey;
	private String spcName;
	private String spoAgencyKey;
	private LocalDateTime proAwardStart;
	private LocalDateTime proAwardEnd;
	private String kepUseEmployeeID;
	private String kepUstName;
	private String proTitle;
	private String pspCFDA;
	private String spoSponsorKey;
	private String aluFirstName;
	private String aluLastName;
	private String aluPhone;
	private String aluEmail;
	private String decUseFirstName;
	private String decUseLastName;
	private String decUsePhone;
	private String decUseEmail;
	private String depDepartmentKey;
	private String depName;
	private BigDecimal totalDirectAmount;
	private BigDecimal totalInDirectAmount;
	private String ansAnswer;
	
	private CgtProjectImpl project;
	private CgtTransactionSubmissionTypeImpl transactionSubmissionType;
	private CgtTransactionTypeImpl transactionType;
	private CgtTransactionGroupTypeImpl transactionGroupType;

	public CgtTransactionImpl() {
	}

	public CgtTransactionImpl(final int ptrTransactionKey, final String ptrProjectKey, final int ptrTransactionTypeKey,
			final int ptrSubmissionTypeKey, final boolean ptrIsActive, final String ptrUpdatedBy, final LocalDateTime ptrDateUpdated,
			final LocalDateTime ptrDateCreated) {
		this.ptrTransactionKey = ptrTransactionKey;
		this.ptrProjectKey = ptrProjectKey;
		this.ptrTransactionTypeKey = ptrTransactionTypeKey;
		this.ptrSubmissionTypeKey = ptrSubmissionTypeKey;
		this.ptrIsActive = ptrIsActive;
		this.ptrUpdatedBy = ptrUpdatedBy;
		this.ptrDateUpdated = ptrDateUpdated;
		this.ptrDateCreated = ptrDateCreated;
	}

	public CgtTransactionImpl(final int ptrTransactionKey, final Integer ptrParentTransactionKey, final Integer ptrTransactionGroupTypeKey,
			final Integer ptrTransactionGroupSequenceNumber, final String ptrTransactionGroupSubContractorKey,
			final String ptrTransactionGroupNameOld, final String ptrTransactionGroupName, final String ptrProjectKey,
			final int ptrTransactionTypeKey, final int ptrSubmissionTypeKey, final LocalDateTime ptrTransactionStart, final LocalDateTime ptrTransactionEnd,
			final BigDecimal ptrDirectAmount, final BigDecimal ptrIndirectAmount, final BigDecimal ptrIcr1, final Integer ptrBaseRate1key,
			final BigDecimal ptrIcr2, final Integer ptrBaseRate2key, final Boolean ptrIsIcrwaiver, final String ptrWaiverNumber,
			final BigDecimal ptrCostShareAmount, final String ptrOpfundNumber, final String ptrSponsorAwardNumber, final String ptrNotes,
			final String ptrSurveyKey, final boolean ptrIsActive, final String ptrUpdatedBy, final LocalDateTime ptrDateUpdated, final LocalDateTime ptrDateCreated,
			final String ptrUniqueId, final String ptrEfadocumentNumber, final String ptrEfafailedReason, final LocalDateTime ptrEfadateProcessed,
			final String pspSponsorKey, final String spoName, final String spcSponsorCategoryKey, final String spcName,
			final String spoAgencyKey, final LocalDateTime proAwardStart, final LocalDateTime proAwardEnd, final String kepUseEmployeeID, final String kepUstName,
			final String proTitle, final String pspCFDA, final String spoSponsorKey, final String aluFirstName, final String aluLastName, final String aluPhone, final String aluEmail, 
			final String decUseFirstName, final String decUseLastName, final String decUsePhone, final String decUseEmail, final String depDepartmentKey,
			final String depName, final BigDecimal totalDirectAmount, final BigDecimal totalInDirectAmount, final String ansAnswer) {
		this.ptrTransactionKey = ptrTransactionKey;
		this.ptrParentTransactionKey = ptrParentTransactionKey;
		this.ptrTransactionGroupTypeKey = ptrTransactionGroupTypeKey;
		this.ptrTransactionGroupSequenceNumber = ptrTransactionGroupSequenceNumber;
		this.ptrTransactionGroupSubContractorKey = ptrTransactionGroupSubContractorKey;
		this.ptrTransactionGroupNameOld = ptrTransactionGroupNameOld;
		this.ptrTransactionGroupName = ptrTransactionGroupName;
		this.ptrProjectKey = ptrProjectKey;
		this.ptrTransactionTypeKey = ptrTransactionTypeKey;
		this.ptrSubmissionTypeKey = ptrSubmissionTypeKey;
		this.ptrTransactionStart = ptrTransactionStart;
		this.ptrTransactionEnd = ptrTransactionEnd;
		this.ptrDirectAmount = ptrDirectAmount;
		this.ptrIndirectAmount = ptrIndirectAmount;
		this.ptrIcr1 = ptrIcr1;
		this.ptrBaseRate1key = ptrBaseRate1key;
		this.ptrIcr2 = ptrIcr2;
		this.ptrBaseRate2key = ptrBaseRate2key;
		this.ptrIsIcrwaiver = ptrIsIcrwaiver;
		this.ptrWaiverNumber = ptrWaiverNumber;
		this.ptrCostShareAmount = ptrCostShareAmount;
		this.ptrOpfundNumber = ptrOpfundNumber;
		this.ptrSponsorAwardNumber = ptrSponsorAwardNumber;
		this.ptrNotes = ptrNotes;
		this.ptrSurveyKey = ptrSurveyKey;
		this.ptrIsActive = ptrIsActive;
		this.ptrUpdatedBy = ptrUpdatedBy;
		this.ptrDateUpdated = ptrDateUpdated;
		this.ptrDateCreated = ptrDateCreated;
		this.ptrUniqueId = ptrUniqueId;
		this.ptrEfadocumentNumber = ptrEfadocumentNumber;
		this.ptrEfafailedReason = ptrEfafailedReason;
		this.ptrEfadateProcessed = ptrEfadateProcessed;		
		this.pspSponsorKey = pspSponsorKey;
		this.spoName = spoName;
		this.spcSponsorCategoryKey = spcSponsorCategoryKey;
		this.spcName = spcName;
		this.spoAgencyKey = spoAgencyKey;
		this.proAwardStart = proAwardStart;
		this.proAwardEnd = proAwardEnd;
		this.kepUseEmployeeID = kepUseEmployeeID;
		this.kepUstName = kepUstName;
		this.proTitle = proTitle;
		this.pspCFDA = pspCFDA;
		this.spoSponsorKey = spoSponsorKey;
		this.aluFirstName = aluFirstName;
		this.aluLastName = aluLastName;
		this.aluPhone = aluPhone;
		this.aluEmail = aluEmail;
		this.decUseFirstName = decUseFirstName;
		this.decUseLastName = decUseLastName;
		this.decUsePhone = decUsePhone;
		this.decUseEmail = decUseEmail;
		this.depDepartmentKey = depDepartmentKey;
		this.depName = depName;
		this.totalDirectAmount = totalDirectAmount;
		this.totalInDirectAmount = totalInDirectAmount;
		this.ansAnswer = ansAnswer;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrTransactionKey()
	 */
	@Override
	@Id
	@Column(name = "ptr_transactionKey", unique = true, nullable = false)
	public int getPtrTransactionKey() {
		return this.ptrTransactionKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrTransactionKey(int)
	 */
	@Override
	public void setPtrTransactionKey(final int ptrTransactionKey) {
		this.ptrTransactionKey = ptrTransactionKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrParentTransactionKey()
	 */
	@Override
	@Column(name = "ptr_parentTransactionKey")
	public Integer getPtrParentTransactionKey() {
		return this.ptrParentTransactionKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrParentTransactionKey(java.lang.Integer)
	 */
	@Override
	public void setPtrParentTransactionKey(final Integer ptrParentTransactionKey) {
		this.ptrParentTransactionKey = ptrParentTransactionKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrTransactionGroupTypeKey()
	 */
	@Override
	@Column(name = "ptr_transactionGroupTypeKey")
	public Integer getPtrTransactionGroupTypeKey() {
		return this.ptrTransactionGroupTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrTransactionGroupTypeKey(java.lang.Integer)
	 */
	@Override
	public void setPtrTransactionGroupTypeKey(final Integer ptrTransactionGroupTypeKey) {
		this.ptrTransactionGroupTypeKey = ptrTransactionGroupTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrTransactionGroupSequenceNumber()
	 */
	@Override
	@Column(name = "ptr_transactionGroupSequenceNumber")
	public Integer getPtrTransactionGroupSequenceNumber() {
		return this.ptrTransactionGroupSequenceNumber;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrTransactionGroupSequenceNumber(java.lang.Integer)
	 */
	@Override
	public void setPtrTransactionGroupSequenceNumber(final Integer ptrTransactionGroupSequenceNumber) {
		this.ptrTransactionGroupSequenceNumber = ptrTransactionGroupSequenceNumber;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrTransactionGroupSubContractorKey()
	 */
	@Override
	@Column(name = "ptr_transactionGroupSubContractorKey", length = 10)
	public String getPtrTransactionGroupSubContractorKey() {
		return this.ptrTransactionGroupSubContractorKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrTransactionGroupSubContractorKey(java.lang.String)
	 */
	@Override
	public void setPtrTransactionGroupSubContractorKey(final String ptrTransactionGroupSubContractorKey) {
		this.ptrTransactionGroupSubContractorKey = ptrTransactionGroupSubContractorKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrTransactionGroupNameOld()
	 */
	@Override
	@Column(name = "ptr_transactionGroupName_Old", length = 100)
	public String getPtrTransactionGroupNameOld() {
		return this.ptrTransactionGroupNameOld;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrTransactionGroupNameOld(java.lang.String)
	 */
	@Override
	public void setPtrTransactionGroupNameOld(final String ptrTransactionGroupNameOld) {
		this.ptrTransactionGroupNameOld = ptrTransactionGroupNameOld;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrTransactionGroupName()
	 */
	@Override
	@Column(name = "ptr_transactionGroupName")
	public String getPtrTransactionGroupName() {
		return this.ptrTransactionGroupName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrTransactionGroupName(java.lang.String)
	 */
	@Override
	public void setPtrTransactionGroupName(final String ptrTransactionGroupName) {
		this.ptrTransactionGroupName = ptrTransactionGroupName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrProjectKey()
	 */
	@Override
	@Column(name = "ptr_projectKey", nullable = false, length = 10)
	public String getPtrProjectKey() {
		return this.ptrProjectKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrProjectKey(java.lang.String)
	 */
	@Override
	public void setPtrProjectKey(final String ptrProjectKey) {
		this.ptrProjectKey = ptrProjectKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrTransactionTypeKey()
	 */
	@Override
	@Column(name = "ptr_transactionTypeKey", nullable = false)
	public int getPtrTransactionTypeKey() {
		return this.ptrTransactionTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrTransactionTypeKey(int)
	 */
	@Override
	public void setPtrTransactionTypeKey(final int ptrTransactionTypeKey) {
		this.ptrTransactionTypeKey = ptrTransactionTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrSubmissionTypeKey()
	 */
	@Override
	@Column(name = "ptr_submissionTypeKey", nullable = false)
	public int getPtrSubmissionTypeKey() {
		return this.ptrSubmissionTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrSubmissionTypeKey(int)
	 */
	@Override
	public void setPtrSubmissionTypeKey(final int ptrSubmissionTypeKey) {
		this.ptrSubmissionTypeKey = ptrSubmissionTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrTransactionStart()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "ptr_transactionStart", length = 23)
	public LocalDateTime getPtrTransactionStart() {
		return this.ptrTransactionStart;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrTransactionStart(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setPtrTransactionStart(final LocalDateTime ptrTransactionStart) {
		this.ptrTransactionStart = ptrTransactionStart;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrTransactionEnd()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "ptr_transactionEnd", length = 23)
	public LocalDateTime getPtrTransactionEnd() {
		return this.ptrTransactionEnd;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrTransactionEnd(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setPtrTransactionEnd(final LocalDateTime ptrTransactionEnd) {
		this.ptrTransactionEnd = ptrTransactionEnd;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrDirectAmount()
	 */
	@Override
	@Column(name = "ptr_directAmount", precision = 18, scale = 2,columnDefinition="decimal(18,2)")
	public BigDecimal getPtrDirectAmount() {
		return this.ptrDirectAmount;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrDirectAmount(java.math.BigDecimal)
	 */
	@Override
	public void setPtrDirectAmount(final BigDecimal ptrDirectAmount) {
		this.ptrDirectAmount = ptrDirectAmount;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrIndirectAmount()
	 */
	@Override
	@Column(name = "ptr_indirectAmount", precision = 18,scale=2,columnDefinition="decimal(18,2)")
	public BigDecimal getPtrIndirectAmount() {
		return this.ptrIndirectAmount;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrIndirectAmount(java.math.BigDecimal)
	 */
	@Override
	public void setPtrIndirectAmount(final BigDecimal ptrIndirectAmount) {
		this.ptrIndirectAmount = ptrIndirectAmount;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrIcr1()
	 */
	@Override
	@Column(name = "ptr_ICR1", precision = 18, scale = 1,columnDefinition="decimal(18,1)")
	public BigDecimal getPtrIcr1() {
		return this.ptrIcr1;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrIcr1(java.math.BigDecimal)
	 */
	@Override
	public void setPtrIcr1(final BigDecimal ptrIcr1) {
		this.ptrIcr1 = ptrIcr1;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrBaseRate1key()
	 */
	@Override
	@Column(name = "ptr_baseRate1Key")
	public Integer getPtrBaseRate1key() {
		return this.ptrBaseRate1key;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrBaseRate1key(java.lang.Integer)
	 */
	@Override
	public void setPtrBaseRate1key(final Integer ptrBaseRate1key) {
		this.ptrBaseRate1key = ptrBaseRate1key;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrIcr2()
	 */
	@Override
	@Column(name = "ptr_ICR2", precision = 18, scale = 1,columnDefinition="decimal(18,1)")
	public BigDecimal getPtrIcr2() {
		return this.ptrIcr2;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrIcr2(java.math.BigDecimal)
	 */
	@Override
	public void setPtrIcr2(final BigDecimal ptrIcr2) {
		this.ptrIcr2 = ptrIcr2;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrBaseRate2key()
	 */
	@Override
	@Column(name = "ptr_baseRate2Key")
	public Integer getPtrBaseRate2key() {
		return this.ptrBaseRate2key;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrBaseRate2key(java.lang.Integer)
	 */
	@Override
	public void setPtrBaseRate2key(final Integer ptrBaseRate2key) {
		this.ptrBaseRate2key = ptrBaseRate2key;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrIsIcrwaiver()
	 */
	@Override
	@Column(name = "ptr_isICRWaiver")
	public Boolean getPtrIsIcrwaiver() {
		return this.ptrIsIcrwaiver;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrIsIcrwaiver(java.lang.Boolean)
	 */
	@Override
	public void setPtrIsIcrwaiver(final Boolean ptrIsIcrwaiver) {
		this.ptrIsIcrwaiver = ptrIsIcrwaiver;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrWaiverNumber()
	 */
	@Override
	@Column(name = "ptr_waiverNumber", length = 100)
	public String getPtrWaiverNumber() {
		return this.ptrWaiverNumber;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrWaiverNumber(java.lang.String)
	 */
	@Override
	public void setPtrWaiverNumber(final String ptrWaiverNumber) {
		this.ptrWaiverNumber = ptrWaiverNumber;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrCostShareAmount()
	 */
	@Override
	@Column(name = "ptr_costShareAmount", precision = 18, scale = 2,columnDefinition="decimal(18,2)")
	public BigDecimal getPtrCostShareAmount() {
		return this.ptrCostShareAmount;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrCostShareAmount(java.math.BigDecimal)
	 */
	@Override
	public void setPtrCostShareAmount(final BigDecimal ptrCostShareAmount) {
		this.ptrCostShareAmount = ptrCostShareAmount;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrOpfundNumber()
	 */
	@Override
	@Column(name = "ptr_OPFundNumber", length = 10)
	public String getPtrOpfundNumber() {
		return this.ptrOpfundNumber;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrOpfundNumber(java.lang.String)
	 */
	@Override
	public void setPtrOpfundNumber(final String ptrOpfundNumber) {
		this.ptrOpfundNumber = ptrOpfundNumber;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrSponsorAwardNumber()
	 */
	@Override
	@Column(name = "ptr_sponsorAwardNumber")
	public String getPtrSponsorAwardNumber() {
		return this.ptrSponsorAwardNumber;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrSponsorAwardNumber(java.lang.String)
	 */
	@Override
	public void setPtrSponsorAwardNumber(final String ptrSponsorAwardNumber) {
		this.ptrSponsorAwardNumber = ptrSponsorAwardNumber;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrNotes()
	 */
	@Override
	@Column(name = "ptr_notes", length = 8000)
	public String getPtrNotes() {
		return this.ptrNotes;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrNotes(java.lang.String)
	 */
	@Override
	public void setPtrNotes(final String ptrNotes) {
		this.ptrNotes = ptrNotes;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrSurveyKey()
	 */
	@Override
	@Column(name = "ptr_surveyKey", length = 35, columnDefinition="char")
	public String getPtrSurveyKey() {
		return this.ptrSurveyKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrSurveyKey(java.lang.String)
	 */
	@Override
	public void setPtrSurveyKey(final String ptrSurveyKey) {
		this.ptrSurveyKey = ptrSurveyKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#isPtrIsActive()
	 */
	@Override
	@Column(name = "ptr_isActive", nullable = false)
	public boolean isPtrIsActive() {
		return this.ptrIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrIsActive(boolean)
	 */
	@Override
	public void setPtrIsActive(final boolean ptrIsActive) {
		this.ptrIsActive = ptrIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrUpdatedBy()
	 */
	@Override
	@Column(name = "ptr_updatedBy", nullable = false, length = 35, columnDefinition="char")
	public String getPtrUpdatedBy() {
		return this.ptrUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrUpdatedBy(java.lang.String)
	 */
	@Override
	public void setPtrUpdatedBy(final String ptrUpdatedBy) {
		this.ptrUpdatedBy = ptrUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrDateUpdated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "ptr_dateUpdated", nullable = false, length = 23)
	public LocalDateTime getPtrDateUpdated() {
		return this.ptrDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setPtrDateUpdated(final LocalDateTime ptrDateUpdated) {
		this.ptrDateUpdated = ptrDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrDateCreated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "ptr_dateCreated", nullable = false, length = 23)
	public LocalDateTime getPtrDateCreated() {
		return this.ptrDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setPtrDateCreated(final LocalDateTime ptrDateCreated) {
		this.ptrDateCreated = ptrDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrUniqueId()
	 */
	@Override
	@Column(name = "ptr_uniqueID", length = 25)
	public String getPtrUniqueId() {
		return this.ptrUniqueId;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrUniqueId(java.lang.String)
	 */
	@Override
	public void setPtrUniqueId(final String ptrUniqueId) {
		this.ptrUniqueId = ptrUniqueId;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrEfadocumentNumber()
	 */
	@Override
	@Column(name = "ptr_EFADocumentNumber", length = 50)
	public String getPtrEfadocumentNumber() {
		return this.ptrEfadocumentNumber;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrEfadocumentNumber(java.lang.String)
	 */
	@Override
	public void setPtrEfadocumentNumber(final String ptrEfadocumentNumber) {
		this.ptrEfadocumentNumber = ptrEfadocumentNumber;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrEfafailedReason()
	 */
	@Override
	@Column(name = "ptr_EFAFailedReason", length = 512)
	public String getPtrEfafailedReason() {
		return this.ptrEfafailedReason;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrEfafailedReason(java.lang.String)
	 */
	@Override
	public void setPtrEfafailedReason(final String ptrEfafailedReason) {
		this.ptrEfafailedReason = ptrEfafailedReason;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#getPtrEfadateProcessed()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "ptr_EFADateProcessed", length = 23)
	public LocalDateTime getPtrEfadateProcessed() {
		return this.ptrEfadateProcessed;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransaction#setPtrEfadateProcessed(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setPtrEfadateProcessed(final LocalDateTime ptrEfadateProcessed) {
		this.ptrEfadateProcessed = ptrEfadateProcessed;
	}
		
	@ManyToOne
	@JoinColumn(name="ptr_projectkey", insertable=false, updatable=false)
	public CgtProjectImpl getProjectImpl() {
		return project;
	}
	
	public void setProjectImpl(CgtProjectImpl project) {
		this.project = project;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumns({
			@JoinColumn(referencedColumnName="tst_transactionTypeKey", name="ptr_transactiontypekey", insertable=false,updatable=false)
		   ,@JoinColumn(referencedColumnName="tst_transactionSubmissionTypeKey", name="ptr_submissiontypekey",insertable=false,updatable=false)
	})
	public CgtTransactionSubmissionTypeImpl getTransactionSubmissionTypeImpl() {
		return transactionSubmissionType;
	}

	
	public void setTransactionSubmissionTypeImpl(CgtTransactionSubmissionTypeImpl transactionSubmissionType) {
		this.transactionSubmissionType = transactionSubmissionType;
	}
	

	@ManyToOne
	@JoinColumn(name="ptr_transactiontypekey",insertable=false,updatable=false)
	public CgtTransactionTypeImpl getTransactionTypeImpl() {
		return transactionType;
	}

	public void setTransactionTypeImpl(CgtTransactionTypeImpl transactionType) {
		this.transactionType = transactionType;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ptr_transactiongrouptypekey",insertable=false,updatable=false)
	public CgtTransactionGroupTypeImpl getTransactionGroupTypeImpl() {
		return transactionGroupType;
	}

	public void setTransactionGroupTypeImpl(CgtTransactionGroupTypeImpl transactionGroupType) {
		this.transactionGroupType = transactionGroupType;
	}
	
	@Override
	@Transient
	public CgtProject getProject() {
		return project;
	}

	@Override
	public void setProject(CgtProject project) {
		this.project = (CgtProjectImpl) project;
	}

	@Override
	@Transient
	public CgtTransactionSubmissionType getTransactionSubmissionType() {
		return transactionSubmissionType;
	}

	@Override
	public void setTransactionSubmissionType(CgtTransactionSubmissionType transactionSubmissionType) {
		this.transactionSubmissionType = (CgtTransactionSubmissionTypeImpl) transactionSubmissionType;
	}

	@Override
	@Transient
	public CgtTransactionType getTransactionType() {
		return transactionType;
	}

	@Override
	public void setTransactionType(CgtTransactionType transactionType) {
		this.transactionType = (CgtTransactionTypeImpl) transactionType;
	}

	@Override
	@Transient
	public CgtTransactionGroupType getTransactionGroupType() {
		return this.transactionGroupType;
	}

	@Override
	public void setTransactionGroupType(CgtTransactionGroupType transactionGroupType) {
		this.transactionGroupType = (CgtTransactionGroupTypeImpl) transactionGroupType;
	}

	public CgtTransactionDTO toDTO() {
		return new CgtTransactionDTO(this);
	}

}
