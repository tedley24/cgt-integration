package edu.ucdavis.orvc.integration.cgt.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtProjectNumbers;
import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtProjectNumbersDTO;



/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/
@Entity
@Table(name = "CGT_ProjectNumbers")
public class CgtProjectNumbersImpl implements CgtProjectNumbers {

	private static final long serialVersionUID = -4100046727749223230L;
	private int prnProjectNumber;
	private boolean prnIsActive;
	private String prnUpdatedBy;
	private LocalDateTime prnDateCreated;
	private LocalDateTime prnDateUpdated;

	public CgtProjectNumbersImpl() {
	}

	public CgtProjectNumbersImpl(final int prnProjectNumber, final boolean prnIsActive, final String prnUpdatedBy, final LocalDateTime prnDateCreated,
			final LocalDateTime prnDateUpdated) {
		this.prnProjectNumber = prnProjectNumber;
		this.prnIsActive = prnIsActive;
		this.prnUpdatedBy = prnUpdatedBy;
		this.prnDateCreated = prnDateCreated;
		this.prnDateUpdated = prnDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectNumbers#getPrnProjectNumber()
	 */
	@Override
	@Id
	@Column(name = "prn_projectNumber", unique = true, nullable = false)
	public int getPrnProjectNumber() {
		return this.prnProjectNumber;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectNumbers#setPrnProjectNumber(int)
	 */
	@Override
	public void setPrnProjectNumber(final int prnProjectNumber) {
		this.prnProjectNumber = prnProjectNumber;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectNumbers#isPrnIsActive()
	 */
	@Override
	@Column(name = "prn_isActive", nullable = false)
	public boolean isPrnIsActive() {
		return this.prnIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectNumbers#setPrnIsActive(boolean)
	 */
	@Override
	public void setPrnIsActive(final boolean prnIsActive) {
		this.prnIsActive = prnIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectNumbers#getPrnUpdatedBy()
	 */
	@Override
	@Column(name = "prn_updatedBy", nullable = false, length = 35, columnDefinition="char")
	public String getPrnUpdatedBy() {
		return this.prnUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectNumbers#setPrnUpdatedBy(java.lang.String)
	 */
	@Override
	public void setPrnUpdatedBy(final String prnUpdatedBy) {
		this.prnUpdatedBy = prnUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectNumbers#getPrnDateCreated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "prn_dateCreated", nullable = false, length = 23)
	public LocalDateTime getPrnDateCreated() {
		return this.prnDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectNumbers#setPrnDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setPrnDateCreated(final LocalDateTime prnDateCreated) {
		this.prnDateCreated = prnDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectNumbers#getPrnDateUpdated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "prn_dateUpdated", nullable = false, length = 23)
	public LocalDateTime getPrnDateUpdated() {
		return this.prnDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectNumbers#setPrnDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setPrnDateUpdated(final LocalDateTime prnDateUpdated) {
		this.prnDateUpdated = prnDateUpdated;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((prnDateCreated == null) ? 0 : prnDateCreated.hashCode());
		result = prime * result + ((prnDateUpdated == null) ? 0 : prnDateUpdated.hashCode());
		result = prime * result + (prnIsActive ? 1231 : 1237);
		result = prime * result + prnProjectNumber;
		result = prime * result + ((prnUpdatedBy == null) ? 0 : prnUpdatedBy.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof CgtProjectNumbersImpl)) {
			return false;
		}
		CgtProjectNumbersImpl other = (CgtProjectNumbersImpl) obj;
		if (prnDateCreated == null) {
			if (other.prnDateCreated != null) {
				return false;
			}
		} else if (!prnDateCreated.equals(other.prnDateCreated)) {
			return false;
		}
		if (prnDateUpdated == null) {
			if (other.prnDateUpdated != null) {
				return false;
			}
		} else if (!prnDateUpdated.equals(other.prnDateUpdated)) {
			return false;
		}
		if (prnIsActive != other.prnIsActive) {
			return false;
		}
		if (prnProjectNumber != other.prnProjectNumber) {
			return false;
		}
		if (prnUpdatedBy == null) {
			if (other.prnUpdatedBy != null) {
				return false;
			}
		} else if (!prnUpdatedBy.equals(other.prnUpdatedBy)) {
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"CgtProjectNumbersImpl [prnProjectNumber=%s, prnIsActive=%s, prnUpdatedBy=%s, prnDateCreated=%s, prnDateUpdated=%s]",
				prnProjectNumber, prnIsActive, prnUpdatedBy, prnDateCreated, prnDateUpdated);
	}



	public CgtProjectNumbersDTO toDTO() {
		return new CgtProjectNumbersDTO(this);
	}

}
