package edu.ucdavis.orvc.integration.cgt.domain;


import javax.persistence.Column;
import javax.persistence.Embeddable;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtEDocsTypesToTransactionTypesId;
import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtEDocsTypesToTransactionTypesIdDTO;

/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/
@Embeddable
public class CgtEDocsTypesToTransactionTypesIdImpl implements CgtEDocsTypesToTransactionTypesId {

	private static final long serialVersionUID = 6196837584403950720L;
	private int e2tEDocsTypeKey;
	private int e2tTransactionSubmissionTypeKey;

	public CgtEDocsTypesToTransactionTypesIdImpl() {
	}

	public CgtEDocsTypesToTransactionTypesIdImpl(final int e2tEDocsTypeKey, final int e2tTransactionSubmissionTypeKey) {
		this.e2tEDocsTypeKey = e2tEDocsTypeKey;
		this.e2tTransactionSubmissionTypeKey = e2tTransactionSubmissionTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsTypesToTransactionTypesId#getE2tEDocsTypeKey()
	 */
	@Override
	@Column(name = "e2t_eDocsTypeKey", nullable = false)
	public int getE2tEDocsTypeKey() {
		return this.e2tEDocsTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsTypesToTransactionTypesId#setE2tEDocsTypeKey(int)
	 */
	@Override
	public void setE2tEDocsTypeKey(final int e2tEDocsTypeKey) {
		this.e2tEDocsTypeKey = e2tEDocsTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsTypesToTransactionTypesId#getE2tTransactionSubmissionTypeKey()
	 */
	@Override
	@Column(name = "e2t_transactionSubmissionTypeKey", nullable = false)
	public int getE2tTransactionSubmissionTypeKey() {
		return this.e2tTransactionSubmissionTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsTypesToTransactionTypesId#setE2tTransactionSubmissionTypeKey(int)
	 */
	@Override
	public void setE2tTransactionSubmissionTypeKey(final int e2tTransactionSubmissionTypeKey) {
		this.e2tTransactionSubmissionTypeKey = e2tTransactionSubmissionTypeKey;
	}

	@Override
	public boolean equals(final Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof CgtEDocsTypesToTransactionTypesIdImpl))
			return false;
		final CgtEDocsTypesToTransactionTypesId castOther = (CgtEDocsTypesToTransactionTypesId) other;

		return (this.getE2tEDocsTypeKey() == castOther.getE2tEDocsTypeKey())
				&& (this.getE2tTransactionSubmissionTypeKey() == castOther.getE2tTransactionSubmissionTypeKey());
	}

	@Override
	public int hashCode() {
		int result = 17;

		result = 37 * result + this.getE2tEDocsTypeKey();
		result = 37 * result + this.getE2tTransactionSubmissionTypeKey();
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"CgtEDocsTypesToTransactionTypesIdImpl [e2tEDocsTypeKey=%s, e2tTransactionSubmissionTypeKey=%s]",
				e2tEDocsTypeKey, e2tTransactionSubmissionTypeKey);
	}

	public CgtEDocsTypesToTransactionTypesIdDTO toDTO() {
		return new CgtEDocsTypesToTransactionTypesIdDTO(this);
	}
}
