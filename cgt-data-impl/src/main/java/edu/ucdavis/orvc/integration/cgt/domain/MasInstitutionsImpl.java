package edu.ucdavis.orvc.integration.cgt.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.MasInstitutions;
import edu.ucdavis.orvc.integration.cgt.api.domain.dto.MasInstitutionsDTO;



/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/
@Entity
@Table(name = "MAS_Institutions")
public class MasInstitutionsImpl implements MasInstitutions {

	private static final long serialVersionUID = 5560941981001087204L;
	private String insInstitutionKey;
	private String insName;
	private String insShortName;
	private String insStateKey;
	private String insZipCode;
	private String insCity;
	private boolean insIsActive;
	private String insDataSourceKey;
	private LocalDateTime insDateCreated;
	private LocalDateTime insDateUpdated;
	private String insUpdatedBy;
	private Boolean insIsopss;

	public MasInstitutionsImpl() {
	}

	public MasInstitutionsImpl(final String insInstitutionKey, final String insName, final boolean insIsActive, final String insDataSourceKey,
			final LocalDateTime insDateCreated, final LocalDateTime insDateUpdated, final String insUpdatedBy) {
		this.insInstitutionKey = insInstitutionKey;
		this.insName = insName;
		this.insIsActive = insIsActive;
		this.insDataSourceKey = insDataSourceKey;
		this.insDateCreated = insDateCreated;
		this.insDateUpdated = insDateUpdated;
		this.insUpdatedBy = insUpdatedBy;
	}

	public MasInstitutionsImpl(final String insInstitutionKey, final String insName, final String insShortName, final String insStateKey,
			final String insZipCode, final String insCity, final boolean insIsActive, final String insDataSourceKey, final LocalDateTime insDateCreated,
			final LocalDateTime insDateUpdated, final String insUpdatedBy, final Boolean insIsopss) {
		this.insInstitutionKey = insInstitutionKey;
		this.insName = insName;
		this.insShortName = insShortName;
		this.insStateKey = insStateKey;
		this.insZipCode = insZipCode;
		this.insCity = insCity;
		this.insIsActive = insIsActive;
		this.insDataSourceKey = insDataSourceKey;
		this.insDateCreated = insDateCreated;
		this.insDateUpdated = insDateUpdated;
		this.insUpdatedBy = insUpdatedBy;
		this.insIsopss = insIsopss;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasInstitutions#getInsInstitutionKey()
	 */
	@Override
	@Id
	@Column(name = "ins_institutionKey", unique = true, nullable = false, length = 10,columnDefinition="char")
	public String getInsInstitutionKey() {
		return this.insInstitutionKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasInstitutions#setInsInstitutionKey(java.lang.String)
	 */
	@Override
	public void setInsInstitutionKey(final String insInstitutionKey) {
		this.insInstitutionKey = insInstitutionKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasInstitutions#getInsName()
	 */
	@Override
	@Column(name = "ins_name", nullable = false)
	public String getInsName() {
		return this.insName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasInstitutions#setInsName(java.lang.String)
	 */
	@Override
	public void setInsName(final String insName) {
		this.insName = insName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasInstitutions#getInsShortName()
	 */
	@Override
	@Column(name = "ins_shortName", length = 50)
	public String getInsShortName() {
		return this.insShortName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasInstitutions#setInsShortName(java.lang.String)
	 */
	@Override
	public void setInsShortName(final String insShortName) {
		this.insShortName = insShortName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasInstitutions#getInsStateKey()
	 */
	@Override
	@Column(name = "ins_stateKey", length = 2,columnDefinition="char")
	public String getInsStateKey() {
		return this.insStateKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasInstitutions#setInsStateKey(java.lang.String)
	 */
	@Override
	public void setInsStateKey(final String insStateKey) {
		this.insStateKey = insStateKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasInstitutions#getInsZipCode()
	 */
	@Override
	@Column(name = "ins_zipCode", length = 50)
	public String getInsZipCode() {
		return this.insZipCode;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasInstitutions#setInsZipCode(java.lang.String)
	 */
	@Override
	public void setInsZipCode(final String insZipCode) {
		this.insZipCode = insZipCode;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasInstitutions#getInsCity()
	 */
	@Override
	@Column(name = "ins_city", length = 150)
	public String getInsCity() {
		return this.insCity;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasInstitutions#setInsCity(java.lang.String)
	 */
	@Override
	public void setInsCity(final String insCity) {
		this.insCity = insCity;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasInstitutions#isInsIsActive()
	 */
	@Override
	@Column(name = "ins_isActive", nullable = false)
	public boolean isInsIsActive() {
		return this.insIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasInstitutions#setInsIsActive(boolean)
	 */
	@Override
	public void setInsIsActive(final boolean insIsActive) {
		this.insIsActive = insIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasInstitutions#getInsDataSourceKey()
	 */
	@Override
	@Column(name = "ins_dataSourceKey", nullable = false, length = 3,columnDefinition="char")
	public String getInsDataSourceKey() {
		return this.insDataSourceKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasInstitutions#setInsDataSourceKey(java.lang.String)
	 */
	@Override
	public void setInsDataSourceKey(final String insDataSourceKey) {
		this.insDataSourceKey = insDataSourceKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasInstitutions#getInsDateCreated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "ins_dateCreated", nullable = false, length = 23)
	public LocalDateTime getInsDateCreated() {
		return this.insDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasInstitutions#setInsDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setInsDateCreated(final LocalDateTime insDateCreated) {
		this.insDateCreated = insDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasInstitutions#getInsDateUpdated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "ins_dateUpdated", nullable = false, length = 23)
	public LocalDateTime getInsDateUpdated() {
		return this.insDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasInstitutions#setInsDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setInsDateUpdated(final LocalDateTime insDateUpdated) {
		this.insDateUpdated = insDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasInstitutions#getInsUpdatedBy()
	 */
	@Override
	@Column(name = "ins_updatedBy", nullable = false, length = 35, columnDefinition="char")
	public String getInsUpdatedBy() {
		return this.insUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasInstitutions#setInsUpdatedBy(java.lang.String)
	 */
	@Override
	public void setInsUpdatedBy(final String insUpdatedBy) {
		this.insUpdatedBy = insUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasInstitutions#getInsIsopss()
	 */
	@Override
	@Column(name = "ins_isopss")
	public Boolean getInsIsopss() {
		return this.insIsopss;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasInstitutions#setInsIsopss(java.lang.Boolean)
	 */
	@Override
	public void setInsIsopss(final Boolean insIsopss) {
		this.insIsopss = insIsopss;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((insCity == null) ? 0 : insCity.hashCode());
		result = prime * result + ((insDataSourceKey == null) ? 0 : insDataSourceKey.hashCode());
		result = prime * result + ((insDateCreated == null) ? 0 : insDateCreated.hashCode());
		result = prime * result + ((insDateUpdated == null) ? 0 : insDateUpdated.hashCode());
		result = prime * result + ((insInstitutionKey == null) ? 0 : insInstitutionKey.hashCode());
		result = prime * result + (insIsActive ? 1231 : 1237);
		result = prime * result + ((insIsopss == null) ? 0 : insIsopss.hashCode());
		result = prime * result + ((insName == null) ? 0 : insName.hashCode());
		result = prime * result + ((insShortName == null) ? 0 : insShortName.hashCode());
		result = prime * result + ((insStateKey == null) ? 0 : insStateKey.hashCode());
		result = prime * result + ((insUpdatedBy == null) ? 0 : insUpdatedBy.hashCode());
		result = prime * result + ((insZipCode == null) ? 0 : insZipCode.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof MasInstitutionsImpl))
			return false;
		final MasInstitutionsImpl other = (MasInstitutionsImpl) obj;
		if (insCity == null) {
			if (other.insCity != null)
				return false;
		} else if (!insCity.equals(other.insCity))
			return false;
		if (insDataSourceKey == null) {
			if (other.insDataSourceKey != null)
				return false;
		} else if (!insDataSourceKey.equals(other.insDataSourceKey))
			return false;
		if (insDateCreated == null) {
			if (other.insDateCreated != null)
				return false;
		} else if (!insDateCreated.equals(other.insDateCreated))
			return false;
		if (insDateUpdated == null) {
			if (other.insDateUpdated != null)
				return false;
		} else if (!insDateUpdated.equals(other.insDateUpdated))
			return false;
		if (insInstitutionKey == null) {
			if (other.insInstitutionKey != null)
				return false;
		} else if (!insInstitutionKey.equals(other.insInstitutionKey))
			return false;
		if (insIsActive != other.insIsActive)
			return false;
		if (insIsopss == null) {
			if (other.insIsopss != null)
				return false;
		} else if (!insIsopss.equals(other.insIsopss))
			return false;
		if (insName == null) {
			if (other.insName != null)
				return false;
		} else if (!insName.equals(other.insName))
			return false;
		if (insShortName == null) {
			if (other.insShortName != null)
				return false;
		} else if (!insShortName.equals(other.insShortName))
			return false;
		if (insStateKey == null) {
			if (other.insStateKey != null)
				return false;
		} else if (!insStateKey.equals(other.insStateKey))
			return false;
		if (insUpdatedBy == null) {
			if (other.insUpdatedBy != null)
				return false;
		} else if (!insUpdatedBy.equals(other.insUpdatedBy))
			return false;
		if (insZipCode == null) {
			if (other.insZipCode != null)
				return false;
		} else if (!insZipCode.equals(other.insZipCode))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"MasInstitutions [insInstitutionKey=%s, insName=%s, insShortName=%s, insStateKey=%s, insZipCode=%s, insCity=%s, insIsActive=%s, insDataSourceKey=%s, insDateCreated=%s, insDateUpdated=%s, insUpdatedBy=%s, insIsopss=%s]",
				insInstitutionKey, insName, insShortName, insStateKey, insZipCode, insCity, insIsActive,
				insDataSourceKey, insDateCreated, insDateUpdated, insUpdatedBy, insIsopss);
	}



	public MasInstitutionsDTO toDTO() {
		return new MasInstitutionsDTO(this);
	}

}
