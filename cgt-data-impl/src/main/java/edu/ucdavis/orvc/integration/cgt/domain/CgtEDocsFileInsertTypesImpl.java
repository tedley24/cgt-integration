package edu.ucdavis.orvc.integration.cgt.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtEDocsFileInsertTypes;
import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtEDocsFileInsertTypesDTO;

/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/
@Entity
@Table(name = "CGT_eDocsFileInsertTypes")
public class CgtEDocsFileInsertTypesImpl implements CgtEDocsFileInsertTypes {

	private static final long serialVersionUID = 422008782088224370L;
	private int finFileInsertTypeKey;
	private Integer finParentKey;
	private String finName;
	private String finDescription;
	private String finColor;
	private boolean finIsCoverSheet;
	private int finSortOrder;
	private boolean finIsActive;

	public CgtEDocsFileInsertTypesImpl() {
	}

	public CgtEDocsFileInsertTypesImpl(final int finFileInsertTypeKey, final String finName, final String finColor, final boolean finIsCoverSheet,
			final int finSortOrder, final boolean finIsActive) {
		this.finFileInsertTypeKey = finFileInsertTypeKey;
		this.finName = finName;
		this.finColor = finColor;
		this.finIsCoverSheet = finIsCoverSheet;
		this.finSortOrder = finSortOrder;
		this.finIsActive = finIsActive;
	}

	public CgtEDocsFileInsertTypesImpl(final int finFileInsertTypeKey, final Integer finParentKey, final String finName,
			final String finDescription, final String finColor, final boolean finIsCoverSheet, final int finSortOrder, final boolean finIsActive) {
		this.finFileInsertTypeKey = finFileInsertTypeKey;
		this.finParentKey = finParentKey;
		this.finName = finName;
		this.finDescription = finDescription;
		this.finColor = finColor;
		this.finIsCoverSheet = finIsCoverSheet;
		this.finSortOrder = finSortOrder;
		this.finIsActive = finIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsFileInsertTypes#getFinFileInsertTypeKey()
	 */
	@Override
	@Id
	@Column(name = "fin_fileInsertTypeKey", unique = true, nullable = false)
	public int getFinFileInsertTypeKey() {
		return this.finFileInsertTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsFileInsertTypes#setFinFileInsertTypeKey(int)
	 */
	@Override
	public void setFinFileInsertTypeKey(final int finFileInsertTypeKey) {
		this.finFileInsertTypeKey = finFileInsertTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsFileInsertTypes#getFinParentKey()
	 */
	@Override
	@Column(name = "fin_parentKey")
	public Integer getFinParentKey() {
		return this.finParentKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsFileInsertTypes#setFinParentKey(java.lang.Integer)
	 */
	@Override
	public void setFinParentKey(final Integer finParentKey) {
		this.finParentKey = finParentKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsFileInsertTypes#getFinName()
	 */
	@Override
	@Column(name = "fin_name", nullable = false, length = 100)
	public String getFinName() {
		return this.finName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsFileInsertTypes#setFinName(java.lang.String)
	 */
	@Override
	public void setFinName(final String finName) {
		this.finName = finName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsFileInsertTypes#getFinDescription()
	 */
	@Override
	@Column(name = "fin_description")
	public String getFinDescription() {
		return this.finDescription;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsFileInsertTypes#setFinDescription(java.lang.String)
	 */
	@Override
	public void setFinDescription(final String finDescription) {
		this.finDescription = finDescription;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsFileInsertTypes#getFinColor()
	 */
	@Override
	@Column(name = "fin_color", nullable = false, length = 7)
	public String getFinColor() {
		return this.finColor;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsFileInsertTypes#setFinColor(java.lang.String)
	 */
	@Override
	public void setFinColor(final String finColor) {
		this.finColor = finColor;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsFileInsertTypes#isFinIsCoverSheet()
	 */
	@Override
	@Column(name = "fin_isCoverSheet", nullable = false)
	public boolean isFinIsCoverSheet() {
		return this.finIsCoverSheet;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsFileInsertTypes#setFinIsCoverSheet(boolean)
	 */
	@Override
	public void setFinIsCoverSheet(final boolean finIsCoverSheet) {
		this.finIsCoverSheet = finIsCoverSheet;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsFileInsertTypes#getFinSortOrder()
	 */
	@Override
	@Column(name = "fin_sortOrder", nullable = false)
	public int getFinSortOrder() {
		return this.finSortOrder;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsFileInsertTypes#setFinSortOrder(int)
	 */
	@Override
	public void setFinSortOrder(final int finSortOrder) {
		this.finSortOrder = finSortOrder;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsFileInsertTypes#isFinIsActive()
	 */
	@Override
	@Column(name = "fin_isActive", nullable = false)
	public boolean isFinIsActive() {
		return this.finIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsFileInsertTypes#setFinIsActive(boolean)
	 */
	@Override
	public void setFinIsActive(final boolean finIsActive) {
		this.finIsActive = finIsActive;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((finColor == null) ? 0 : finColor.hashCode());
		result = prime * result + ((finDescription == null) ? 0 : finDescription.hashCode());
		result = prime * result + finFileInsertTypeKey;
		result = prime * result + (finIsActive ? 1231 : 1237);
		result = prime * result + (finIsCoverSheet ? 1231 : 1237);
		result = prime * result + ((finName == null) ? 0 : finName.hashCode());
		result = prime * result + ((finParentKey == null) ? 0 : finParentKey.hashCode());
		result = prime * result + finSortOrder;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof CgtEDocsFileInsertTypesImpl)) {
			return false;
		}
		CgtEDocsFileInsertTypesImpl other = (CgtEDocsFileInsertTypesImpl) obj;
		if (finColor == null) {
			if (other.finColor != null) {
				return false;
			}
		} else if (!finColor.equals(other.finColor)) {
			return false;
		}
		if (finDescription == null) {
			if (other.finDescription != null) {
				return false;
			}
		} else if (!finDescription.equals(other.finDescription)) {
			return false;
		}
		if (finFileInsertTypeKey != other.finFileInsertTypeKey) {
			return false;
		}
		if (finIsActive != other.finIsActive) {
			return false;
		}
		if (finIsCoverSheet != other.finIsCoverSheet) {
			return false;
		}
		if (finName == null) {
			if (other.finName != null) {
				return false;
			}
		} else if (!finName.equals(other.finName)) {
			return false;
		}
		if (finParentKey == null) {
			if (other.finParentKey != null) {
				return false;
			}
		} else if (!finParentKey.equals(other.finParentKey)) {
			return false;
		}
		if (finSortOrder != other.finSortOrder) {
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"CgtEDocsFileInsertTypesImpl [finFileInsertTypeKey=%s, finParentKey=%s, finName=%s, finDescription=%s, finColor=%s, finIsCoverSheet=%s, finSortOrder=%s, finIsActive=%s]",
				finFileInsertTypeKey, finParentKey, finName, finDescription, finColor, finIsCoverSheet, finSortOrder,
				finIsActive);
	}

	public CgtEDocsFileInsertTypesDTO toDTO() {
		return new CgtEDocsFileInsertTypesDTO(this);
	}
}
