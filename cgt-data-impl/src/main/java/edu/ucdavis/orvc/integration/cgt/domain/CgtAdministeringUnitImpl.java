package edu.ucdavis.orvc.integration.cgt.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtAdministeringUnit;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtAdministeringUnitRole;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtProject;
import edu.ucdavis.orvc.integration.cgt.api.domain.MasDepartment;
import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtAdministeringUnitDTO;

/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/
@Entity
@Table(name = "CGT_AdministeringUnits")
public class CgtAdministeringUnitImpl  
	implements java.io.Serializable, CgtAdministeringUnit, CgtImplDtoConvertable<CgtAdministeringUnitImpl, CgtAdministeringUnitDTO> {

	private static final long serialVersionUID = 8115581249771036328L;

	private int aduAdministeringUnitKey;
	private String aduDepartmentKey;
	private Integer aduUnitRoleKey;
	private String aduProjectKey;
	private String aduOpaccount;
	private boolean aduIsActive;
	private String aduUpdatedBy;
	private LocalDateTime aduDateUpdated;
	private LocalDateTime aduDateCreated;

	private MasDepartmentImpl department;
	private CgtAdministeringUnitRoleImpl administeringUnitRole;
	private CgtProjectImpl project;
	
	

	public CgtAdministeringUnitImpl() {
	}

	public CgtAdministeringUnitImpl(final int aduAdministeringUnitKey, final String aduProjectKey, final boolean aduIsActive,
			final String aduUpdatedBy, final LocalDateTime aduDateUpdated, final LocalDateTime aduDateCreated) {
		this.aduAdministeringUnitKey = aduAdministeringUnitKey;
		this.aduProjectKey = aduProjectKey;
		this.aduIsActive = aduIsActive;
		this.aduUpdatedBy = aduUpdatedBy;
		this.aduDateUpdated = aduDateUpdated;
		this.aduDateCreated = aduDateCreated;
	}

	public CgtAdministeringUnitImpl(final int aduAdministeringUnitKey, final String aduDepartmentKey, final Integer aduUnitRoleKey,
			final String aduProjectKey, final String aduOpaccount, final boolean aduIsActive, final String aduUpdatedBy, final LocalDateTime aduDateUpdated,
			final LocalDateTime aduDateCreated) {
		this.aduAdministeringUnitKey = aduAdministeringUnitKey;
		this.aduDepartmentKey = aduDepartmentKey;
		this.aduUnitRoleKey = aduUnitRoleKey;
		this.aduProjectKey = aduProjectKey;
		this.aduOpaccount = aduOpaccount;
		this.aduIsActive = aduIsActive;
		this.aduUpdatedBy = aduUpdatedBy;
		this.aduDateUpdated = aduDateUpdated;
		this.aduDateCreated = aduDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtAdministeringUnit#getAduAdministeringUnitKey()
	 */
	@Override
	@Id
	@Column(name = "adu_administeringUnitKey", unique = true, nullable = false)
	public int getAduAdministeringUnitKey() {
		return this.aduAdministeringUnitKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtAdministeringUnit#setAduAdministeringUnitKey(int)
	 */
	@Override
	public void setAduAdministeringUnitKey(final int aduAdministeringUnitKey) {
		this.aduAdministeringUnitKey = aduAdministeringUnitKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtAdministeringUnit#getAduDepartmentKey()
	 */
	@Override
	@Column(name = "adu_departmentKey", length = 8, columnDefinition="char")
	public String getAduDepartmentKey() {
		return this.aduDepartmentKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtAdministeringUnit#setAduDepartmentKey(java.lang.String)
	 */
	@Override
	public void setAduDepartmentKey(final String aduDepartmentKey) {
		this.aduDepartmentKey = aduDepartmentKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtAdministeringUnit#getAduUnitRoleKey()
	 */
	@Override
	@Column(name = "adu_unitRoleKey")
	public Integer getAduUnitRoleKey() {
		return this.aduUnitRoleKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtAdministeringUnit#setAduUnitRoleKey(java.lang.Integer)
	 */
	@Override
	public void setAduUnitRoleKey(final Integer aduUnitRoleKey) {
		this.aduUnitRoleKey = aduUnitRoleKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtAdministeringUnit#getAduProjectKey()
	 */
	@Override
	@Column(name = "adu_projectKey", nullable = false, length = 10)
	public String getAduProjectKey() {
		return this.aduProjectKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtAdministeringUnit#setAduProjectKey(java.lang.String)
	 */
	@Override
	public void setAduProjectKey(final String aduProjectKey) {
		this.aduProjectKey = aduProjectKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtAdministeringUnit#getAduOpaccount()
	 */
	@Override
	@Column(name = "adu_OPAccount", length = 50)
	public String getAduOpaccount() {
		return this.aduOpaccount;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtAdministeringUnit#setAduOpaccount(java.lang.String)
	 */
	@Override
	public void setAduOpaccount(final String aduOpaccount) {
		this.aduOpaccount = aduOpaccount;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtAdministeringUnit#isAduIsActive()
	 */
	@Override
	@Column(name = "adu_isActive", nullable = false)
	public boolean isAduIsActive() {
		return this.aduIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtAdministeringUnit#setAduIsActive(boolean)
	 */
	@Override
	public void setAduIsActive(final boolean aduIsActive) {
		this.aduIsActive = aduIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtAdministeringUnit#getAduUpdatedBy()
	 */
	@Override
	@Column(name = "adu_updatedBy", nullable = false, length = 35, columnDefinition="char")
	public String getAduUpdatedBy() {
		return this.aduUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtAdministeringUnit#setAduUpdatedBy(java.lang.String)
	 */
	@Override
	public void setAduUpdatedBy(final String aduUpdatedBy) {
		this.aduUpdatedBy = aduUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtAdministeringUnit#getAduDateUpdated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "adu_dateUpdated", nullable = false, length = 23)
	public LocalDateTime getAduDateUpdated() {
		return this.aduDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtAdministeringUnit#setAduDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setAduDateUpdated(final LocalDateTime aduDateUpdated) {
		this.aduDateUpdated = aduDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtAdministeringUnit#getAduDateCreated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "adu_dateCreated", nullable = false, length = 23)
	public LocalDateTime getAduDateCreated() {
		return this.aduDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtAdministeringUnit#setAduDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setAduDateCreated(final LocalDateTime aduDateCreated) {
		this.aduDateCreated = aduDateCreated;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + aduAdministeringUnitKey;
		result = prime * result + ((aduDateCreated == null) ? 0 : aduDateCreated.hashCode());
		result = prime * result + ((aduDateUpdated == null) ? 0 : aduDateUpdated.hashCode());
		result = prime * result + ((aduDepartmentKey == null) ? 0 : aduDepartmentKey.hashCode());
		result = prime * result + (aduIsActive ? 1231 : 1237);
		result = prime * result + ((aduOpaccount == null) ? 0 : aduOpaccount.hashCode());
		result = prime * result + ((aduProjectKey == null) ? 0 : aduProjectKey.hashCode());
		result = prime * result + ((aduUnitRoleKey == null) ? 0 : aduUnitRoleKey.hashCode());
		result = prime * result + ((aduUpdatedBy == null) ? 0 : aduUpdatedBy.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof CgtAdministeringUnitImpl))
			return false;
		final CgtAdministeringUnitImpl other = (CgtAdministeringUnitImpl) obj;
		if (aduAdministeringUnitKey != other.aduAdministeringUnitKey)
			return false;
		if (aduDateCreated == null) {
			if (other.aduDateCreated != null)
				return false;
		} else if (!aduDateCreated.equals(other.aduDateCreated))
			return false;
		if (aduDateUpdated == null) {
			if (other.aduDateUpdated != null)
				return false;
		} else if (!aduDateUpdated.equals(other.aduDateUpdated))
			return false;
		if (aduDepartmentKey == null) {
			if (other.aduDepartmentKey != null)
				return false;
		} else if (!aduDepartmentKey.equals(other.aduDepartmentKey))
			return false;
		if (aduIsActive != other.aduIsActive)
			return false;
		if (aduOpaccount == null) {
			if (other.aduOpaccount != null)
				return false;
		} else if (!aduOpaccount.equals(other.aduOpaccount))
			return false;
		if (aduProjectKey == null) {
			if (other.aduProjectKey != null)
				return false;
		} else if (!aduProjectKey.equals(other.aduProjectKey))
			return false;
		if (aduUnitRoleKey == null) {
			if (other.aduUnitRoleKey != null)
				return false;
		} else if (!aduUnitRoleKey.equals(other.aduUnitRoleKey))
			return false;
		if (aduUpdatedBy == null) {
			if (other.aduUpdatedBy != null)
				return false;
		} else if (!aduUpdatedBy.equals(other.aduUpdatedBy))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"CgtAdministeringUnits [aduAdministeringUnitKey=%s, aduDepartmentKey=%s, aduUnitRoleKey=%s, aduProjectKey=%s, aduOpaccount=%s, aduIsActive=%s, aduUpdatedBy=%s, aduDateUpdated=%s, aduDateCreated=%s]",
				aduAdministeringUnitKey, aduDepartmentKey, aduUnitRoleKey, aduProjectKey, aduOpaccount, aduIsActive,
				aduUpdatedBy, aduDateUpdated, aduDateCreated);
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="adu_departmentkey")
	public MasDepartmentImpl getDepartmentImpl() {
		return department;
	}

	public void setDepartmentImpl(MasDepartmentImpl department) {
		this.department = department;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="adu_unitrolekey")
	public CgtAdministeringUnitRoleImpl getAdministeringUnitRoleImpl() {
		return administeringUnitRole;
	}

	public void setAdministeringUnitRoleImpl(CgtAdministeringUnitRoleImpl administeringUnitRole) {
		this.administeringUnitRole = administeringUnitRole;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="adu_projectkey",insertable=false,updatable=false)
	public CgtProjectImpl getProjectImpl() {
		return project;
	}

	public void setProjectImpl(CgtProjectImpl project) {
		this.project = project;
	}

	
	
	
	@Override
	@Transient
	public MasDepartment getDepartment() {
		return this.department;
	}

	@Override
	public void setDepartment(MasDepartment department) {
		this.department = (MasDepartmentImpl) department;
	}

	@Override
	@Transient
	public CgtAdministeringUnitRole getAdministeringUnitRole() {
		return this.administeringUnitRole;
	}

	@Override
	public void setAdministeringUnitRole(CgtAdministeringUnitRole administeringUnitRole) {
		this.administeringUnitRole = (CgtAdministeringUnitRoleImpl) administeringUnitRole;
	}

	@Override
	@Transient
	public CgtProject getProject() {
		return this.project;
	}

	@Override
	public void setProject(CgtProject project) {
		this.project = (CgtProjectImpl) project;
	}

	@Override
	public CgtAdministeringUnitImpl toImpl() {
		return this;
	}

	@Override
	public CgtAdministeringUnitDTO toDTO() {
		return new CgtAdministeringUnitDTO(this);
	}

}
