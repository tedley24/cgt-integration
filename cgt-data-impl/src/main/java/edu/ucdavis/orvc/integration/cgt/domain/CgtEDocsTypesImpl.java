package edu.ucdavis.orvc.integration.cgt.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtEDocsTypes;
import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtEDocsTypesDTO;



/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/
@Entity
@Table(name = "CGT_eDocsTypes")
public class CgtEDocsTypesImpl implements CgtEDocsTypes {

	private static final long serialVersionUID = -583751110313581968L;
	private int edtEDocsTypeKey;
	private String edtName;
	private boolean edtIsPhaseSpecific;
	private boolean edtCaptureText;
	private int edtSortOrder;
	private boolean edtIsActive;
	private LocalDateTime edtDateCreated;
	private LocalDateTime edtDateUpdated;
	private String edtUpdatedBy;

	public CgtEDocsTypesImpl() {
	}

	public CgtEDocsTypesImpl(final int edtEDocsTypeKey, final String edtName, final boolean edtIsPhaseSpecific, final boolean edtCaptureText,
			final int edtSortOrder, final boolean edtIsActive, final LocalDateTime edtDateCreated, final LocalDateTime edtDateUpdated, final String edtUpdatedBy) {
		this.edtEDocsTypeKey = edtEDocsTypeKey;
		this.edtName = edtName;
		this.edtIsPhaseSpecific = edtIsPhaseSpecific;
		this.edtCaptureText = edtCaptureText;
		this.edtSortOrder = edtSortOrder;
		this.edtIsActive = edtIsActive;
		this.edtDateCreated = edtDateCreated;
		this.edtDateUpdated = edtDateUpdated;
		this.edtUpdatedBy = edtUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsTypes#getEdtEDocsTypeKey()
	 */
	@Override
	@Id
	@Column(name = "edt_eDocsTypeKey", unique = true, nullable = false)
	public int getEdtEDocsTypeKey() {
		return this.edtEDocsTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsTypes#setEdtEDocsTypeKey(int)
	 */
	@Override
	public void setEdtEDocsTypeKey(final int edtEDocsTypeKey) {
		this.edtEDocsTypeKey = edtEDocsTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsTypes#getEdtName()
	 */
	@Override
	@Column(name = "edt_name", nullable = false, length = 60)
	public String getEdtName() {
		return this.edtName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsTypes#setEdtName(java.lang.String)
	 */
	@Override
	public void setEdtName(final String edtName) {
		this.edtName = edtName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsTypes#isEdtIsPhaseSpecific()
	 */
	@Override
	@Column(name = "edt_isPhaseSpecific", nullable = false)
	public boolean isEdtIsPhaseSpecific() {
		return this.edtIsPhaseSpecific;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsTypes#setEdtIsPhaseSpecific(boolean)
	 */
	@Override
	public void setEdtIsPhaseSpecific(final boolean edtIsPhaseSpecific) {
		this.edtIsPhaseSpecific = edtIsPhaseSpecific;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsTypes#isEdtCaptureText()
	 */
	@Override
	@Column(name = "edt_captureText", nullable = false)
	public boolean isEdtCaptureText() {
		return this.edtCaptureText;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsTypes#setEdtCaptureText(boolean)
	 */
	@Override
	public void setEdtCaptureText(final boolean edtCaptureText) {
		this.edtCaptureText = edtCaptureText;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsTypes#getEdtSortOrder()
	 */
	@Override
	@Column(name = "edt_sortOrder", nullable = false)
	public int getEdtSortOrder() {
		return this.edtSortOrder;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsTypes#setEdtSortOrder(int)
	 */
	@Override
	public void setEdtSortOrder(final int edtSortOrder) {
		this.edtSortOrder = edtSortOrder;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsTypes#isEdtIsActive()
	 */
	@Override
	@Column(name = "edt_isActive", nullable = false)
	public boolean isEdtIsActive() {
		return this.edtIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsTypes#setEdtIsActive(boolean)
	 */
	@Override
	public void setEdtIsActive(final boolean edtIsActive) {
		this.edtIsActive = edtIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsTypes#getEdtDateCreated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "edt_dateCreated", nullable = false, length = 23)
	public LocalDateTime getEdtDateCreated() {
		return this.edtDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsTypes#setEdtDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setEdtDateCreated(final LocalDateTime edtDateCreated) {
		this.edtDateCreated = edtDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsTypes#getEdtDateUpdated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "edt_dateUpdated", nullable = false, length = 23)
	public LocalDateTime getEdtDateUpdated() {
		return this.edtDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsTypes#setEdtDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setEdtDateUpdated(final LocalDateTime edtDateUpdated) {
		this.edtDateUpdated = edtDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsTypes#getEdtUpdatedBy()
	 */
	@Override
	@Column(name = "edt_updatedBy", nullable = false, length = 35, columnDefinition="char")
	public String getEdtUpdatedBy() {
		return this.edtUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsTypes#setEdtUpdatedBy(java.lang.String)
	 */
	@Override
	public void setEdtUpdatedBy(final String edtUpdatedBy) {
		this.edtUpdatedBy = edtUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + (edtCaptureText ? 1231 : 1237);
		result = prime * result + ((edtDateCreated == null) ? 0 : edtDateCreated.hashCode());
		result = prime * result + ((edtDateUpdated == null) ? 0 : edtDateUpdated.hashCode());
		result = prime * result + edtEDocsTypeKey;
		result = prime * result + (edtIsActive ? 1231 : 1237);
		result = prime * result + (edtIsPhaseSpecific ? 1231 : 1237);
		result = prime * result + ((edtName == null) ? 0 : edtName.hashCode());
		result = prime * result + edtSortOrder;
		result = prime * result + ((edtUpdatedBy == null) ? 0 : edtUpdatedBy.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof CgtEDocsTypesImpl)) {
			return false;
		}
		CgtEDocsTypesImpl other = (CgtEDocsTypesImpl) obj;
		if (edtCaptureText != other.edtCaptureText) {
			return false;
		}
		if (edtDateCreated == null) {
			if (other.edtDateCreated != null) {
				return false;
			}
		} else if (!edtDateCreated.equals(other.edtDateCreated)) {
			return false;
		}
		if (edtDateUpdated == null) {
			if (other.edtDateUpdated != null) {
				return false;
			}
		} else if (!edtDateUpdated.equals(other.edtDateUpdated)) {
			return false;
		}
		if (edtEDocsTypeKey != other.edtEDocsTypeKey) {
			return false;
		}
		if (edtIsActive != other.edtIsActive) {
			return false;
		}
		if (edtIsPhaseSpecific != other.edtIsPhaseSpecific) {
			return false;
		}
		if (edtName == null) {
			if (other.edtName != null) {
				return false;
			}
		} else if (!edtName.equals(other.edtName)) {
			return false;
		}
		if (edtSortOrder != other.edtSortOrder) {
			return false;
		}
		if (edtUpdatedBy == null) {
			if (other.edtUpdatedBy != null) {
				return false;
			}
		} else if (!edtUpdatedBy.equals(other.edtUpdatedBy)) {
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"CgtEDocsTypesImpl [edtEDocsTypeKey=%s, edtName=%s, edtIsPhaseSpecific=%s, edtCaptureText=%s, edtSortOrder=%s, edtIsActive=%s, edtDateCreated=%s, edtDateUpdated=%s, edtUpdatedBy=%s]",
				edtEDocsTypeKey, edtName, edtIsPhaseSpecific, edtCaptureText, edtSortOrder, edtIsActive, edtDateCreated,
				edtDateUpdated, edtUpdatedBy);
	}

	public CgtEDocsTypesDTO toDTO() {
		return new CgtEDocsTypesDTO(this);
	}
}
