package edu.ucdavis.orvc.integration.cgt.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.MasCountries;
import edu.ucdavis.orvc.integration.cgt.api.domain.dto.MasCountriesDTO;



/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/
@Entity
@Table(name = "MAS_Countries")
public class MasCountriesImpl implements MasCountries {

	private static final long serialVersionUID = 4546149736442689385L;
	private int couCountryKey;
	private String couName;
	private String couA2;
	private String couA3;
	private Boolean couIsActive;
	private LocalDateTime couDateCreated;
	private LocalDateTime couDateUpdated;
	private String couUpdatedBy;

	public MasCountriesImpl() {
	}

	public MasCountriesImpl(final int couCountryKey, final String couName, final String couA2, final String couA3) {
		this.couCountryKey = couCountryKey;
		this.couName = couName;
		this.couA2 = couA2;
		this.couA3 = couA3;
	}

	public MasCountriesImpl(final int couCountryKey, final String couName, final String couA2, final String couA3, final Boolean couIsActive,
			final LocalDateTime couDateCreated, final LocalDateTime couDateUpdated, final String couUpdatedBy) {
		this.couCountryKey = couCountryKey;
		this.couName = couName;
		this.couA2 = couA2;
		this.couA3 = couA3;
		this.couIsActive = couIsActive;
		this.couDateCreated = couDateCreated;
		this.couDateUpdated = couDateUpdated;
		this.couUpdatedBy = couUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasCountries#getCouCountryKey()
	 */
	@Override
	@Id
	@Column(name = "cou_countryKey", unique = true, nullable = false)
	public int getCouCountryKey() {
		return this.couCountryKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasCountries#setCouCountryKey(int)
	 */
	@Override
	public void setCouCountryKey(final int couCountryKey) {
		this.couCountryKey = couCountryKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasCountries#getCouName()
	 */
	@Override
	@Column(name = "cou_name", nullable = false)
	public String getCouName() {
		return this.couName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasCountries#setCouName(java.lang.String)
	 */
	@Override
	public void setCouName(final String couName) {
		this.couName = couName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasCountries#getCouA2()
	 */
	@Override
	@Column(name = "cou_A2", nullable = false, length = 2,columnDefinition="char")
	public String getCouA2() {
		return this.couA2;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasCountries#setCouA2(java.lang.String)
	 */
	@Override
	public void setCouA2(final String couA2) {
		this.couA2 = couA2;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasCountries#getCouA3()
	 */
	@Override
	@Column(name = "cou_A3", nullable = false, length = 3,columnDefinition="char")
	public String getCouA3() {
		return this.couA3;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasCountries#setCouA3(java.lang.String)
	 */
	@Override
	public void setCouA3(final String couA3) {
		this.couA3 = couA3;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasCountries#getCouIsActive()
	 */
	@Override
	@Column(name = "cou_isActive")
	public Boolean getCouIsActive() {
		return this.couIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasCountries#setCouIsActive(java.lang.Boolean)
	 */
	@Override
	public void setCouIsActive(final Boolean couIsActive) {
		this.couIsActive = couIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasCountries#getCouDateCreated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "cou_dateCreated", length = 23)
	public LocalDateTime getCouDateCreated() {
		return this.couDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasCountries#setCouDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setCouDateCreated(final LocalDateTime couDateCreated) {
		this.couDateCreated = couDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasCountries#getCouDateUpdated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "cou_dateUpdated", length = 23)
	public LocalDateTime getCouDateUpdated() {
		return this.couDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasCountries#setCouDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setCouDateUpdated(final LocalDateTime couDateUpdated) {
		this.couDateUpdated = couDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasCountries#getCouUpdatedBy()
	 */
	@Override
	@Column(name = "cou_updatedBy", length = 35, columnDefinition="char")
	public String getCouUpdatedBy() {
		return this.couUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasCountries#setCouUpdatedBy(java.lang.String)
	 */
	@Override
	public void setCouUpdatedBy(final String couUpdatedBy) {
		this.couUpdatedBy = couUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((couA2 == null) ? 0 : couA2.hashCode());
		result = prime * result + ((couA3 == null) ? 0 : couA3.hashCode());
		result = prime * result + couCountryKey;
		result = prime * result + ((couDateCreated == null) ? 0 : couDateCreated.hashCode());
		result = prime * result + ((couDateUpdated == null) ? 0 : couDateUpdated.hashCode());
		result = prime * result + ((couIsActive == null) ? 0 : couIsActive.hashCode());
		result = prime * result + ((couName == null) ? 0 : couName.hashCode());
		result = prime * result + ((couUpdatedBy == null) ? 0 : couUpdatedBy.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof MasCountriesImpl))
			return false;
		final MasCountriesImpl other = (MasCountriesImpl) obj;
		if (couA2 == null) {
			if (other.couA2 != null)
				return false;
		} else if (!couA2.equals(other.couA2))
			return false;
		if (couA3 == null) {
			if (other.couA3 != null)
				return false;
		} else if (!couA3.equals(other.couA3))
			return false;
		if (couCountryKey != other.couCountryKey)
			return false;
		if (couDateCreated == null) {
			if (other.couDateCreated != null)
				return false;
		} else if (!couDateCreated.equals(other.couDateCreated))
			return false;
		if (couDateUpdated == null) {
			if (other.couDateUpdated != null)
				return false;
		} else if (!couDateUpdated.equals(other.couDateUpdated))
			return false;
		if (couIsActive == null) {
			if (other.couIsActive != null)
				return false;
		} else if (!couIsActive.equals(other.couIsActive))
			return false;
		if (couName == null) {
			if (other.couName != null)
				return false;
		} else if (!couName.equals(other.couName))
			return false;
		if (couUpdatedBy == null) {
			if (other.couUpdatedBy != null)
				return false;
		} else if (!couUpdatedBy.equals(other.couUpdatedBy))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"MasCountries [couCountryKey=%s, couName=%s, couA2=%s, couA3=%s, couIsActive=%s, couDateCreated=%s, couDateUpdated=%s, couUpdatedBy=%s]",
				couCountryKey, couName, couA2, couA3, couIsActive, couDateCreated, couDateUpdated, couUpdatedBy);
	}



	public MasCountriesDTO toDTO() {
		return new MasCountriesDTO(this);
	}

}
