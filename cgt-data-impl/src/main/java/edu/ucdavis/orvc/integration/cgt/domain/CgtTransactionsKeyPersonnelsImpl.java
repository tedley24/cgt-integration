package edu.ucdavis.orvc.integration.cgt.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtTransactionsKeyPersonnels;
import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtTransactionsKeyPersonnelsDTO;



/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/
@Entity
@Table(name = "CGT_TransactionsKeyPersonnels")
public class CgtTransactionsKeyPersonnelsImpl implements CgtTransactionsKeyPersonnels {

	private static final long serialVersionUID = 7245288071463677952L;
	private int tkpTransactionKeyPersonnelKey;
	private int tpkTransactionKey;
	private int tpkKeyPersonnelKey;
	private long tpkAllocation;
	private boolean tpkIsActive;
	private String tpkUpdatedBy;
	private LocalDateTime tpkDateCreated;
	private LocalDateTime tpkDateUpdated;

	public CgtTransactionsKeyPersonnelsImpl() {
	}

	public CgtTransactionsKeyPersonnelsImpl(final int tkpTransactionKeyPersonnelKey, final int tpkTransactionKey,
			final int tpkKeyPersonnelKey, final long tpkAllocation, final boolean tpkIsActive, final String tpkUpdatedBy, final LocalDateTime tpkDateCreated,
			final LocalDateTime tpkDateUpdated) {
		this.tkpTransactionKeyPersonnelKey = tkpTransactionKeyPersonnelKey;
		this.tpkTransactionKey = tpkTransactionKey;
		this.tpkKeyPersonnelKey = tpkKeyPersonnelKey;
		this.tpkAllocation = tpkAllocation;
		this.tpkIsActive = tpkIsActive;
		this.tpkUpdatedBy = tpkUpdatedBy;
		this.tpkDateCreated = tpkDateCreated;
		this.tpkDateUpdated = tpkDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionsKeyPersonnels#getTkpTransactionKeyPersonnelKey()
	 */
	@Override
	@Id
	@Column(name = "tkp_transactionKeyPersonnelKey", unique = true, nullable = false)
	public int getTkpTransactionKeyPersonnelKey() {
		return this.tkpTransactionKeyPersonnelKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionsKeyPersonnels#setTkpTransactionKeyPersonnelKey(int)
	 */
	@Override
	public void setTkpTransactionKeyPersonnelKey(final int tkpTransactionKeyPersonnelKey) {
		this.tkpTransactionKeyPersonnelKey = tkpTransactionKeyPersonnelKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionsKeyPersonnels#getTpkTransactionKey()
	 */
	@Override
	@Column(name = "tpk_transactionKey", nullable = false)
	public int getTpkTransactionKey() {
		return this.tpkTransactionKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionsKeyPersonnels#setTpkTransactionKey(int)
	 */
	@Override
	public void setTpkTransactionKey(final int tpkTransactionKey) {
		this.tpkTransactionKey = tpkTransactionKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionsKeyPersonnels#getTpkKeyPersonnelKey()
	 */
	@Override
	@Column(name = "tpk_keyPersonnelKey", nullable = false)
	public int getTpkKeyPersonnelKey() {
		return this.tpkKeyPersonnelKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionsKeyPersonnels#setTpkKeyPersonnelKey(int)
	 */
	@Override
	public void setTpkKeyPersonnelKey(final int tpkKeyPersonnelKey) {
		this.tpkKeyPersonnelKey = tpkKeyPersonnelKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionsKeyPersonnels#getTpkAllocation()
	 */
	@Override
	@Column(name = "tpk_allocation", nullable = false, precision = 18, scale = 0, columnDefinition="decimal(18,0)")
	public long getTpkAllocation() {
		return this.tpkAllocation;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionsKeyPersonnels#setTpkAllocation(long)
	 */
	@Override
	public void setTpkAllocation(final long tpkAllocation) {
		this.tpkAllocation = tpkAllocation;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionsKeyPersonnels#isTpkIsActive()
	 */
	@Override
	@Column(name = "tpk_isActive", nullable = false)
	public boolean isTpkIsActive() {
		return this.tpkIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionsKeyPersonnels#setTpkIsActive(boolean)
	 */
	@Override
	public void setTpkIsActive(final boolean tpkIsActive) {
		this.tpkIsActive = tpkIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionsKeyPersonnels#getTpkUpdatedBy()
	 */
	@Override
	@Column(name = "tpk_updatedBy", nullable = false, length = 35, columnDefinition="char")
	public String getTpkUpdatedBy() {
		return this.tpkUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionsKeyPersonnels#setTpkUpdatedBy(java.lang.String)
	 */
	@Override
	public void setTpkUpdatedBy(final String tpkUpdatedBy) {
		this.tpkUpdatedBy = tpkUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionsKeyPersonnels#getTpkDateCreated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "tpk_dateCreated", nullable = false, length = 23)
	public LocalDateTime getTpkDateCreated() {
		return this.tpkDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionsKeyPersonnels#setTpkDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setTpkDateCreated(final LocalDateTime tpkDateCreated) {
		this.tpkDateCreated = tpkDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionsKeyPersonnels#getTpkDateUpdated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "tpk_dateUpdated", nullable = false, length = 23)
	public LocalDateTime getTpkDateUpdated() {
		return this.tpkDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionsKeyPersonnels#setTpkDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setTpkDateUpdated(final LocalDateTime tpkDateUpdated) {
		this.tpkDateUpdated = tpkDateUpdated;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + tkpTransactionKeyPersonnelKey;
		result = prime * result + (int) (tpkAllocation ^ (tpkAllocation >>> 32));
		result = prime * result + ((tpkDateCreated == null) ? 0 : tpkDateCreated.hashCode());
		result = prime * result + ((tpkDateUpdated == null) ? 0 : tpkDateUpdated.hashCode());
		result = prime * result + (tpkIsActive ? 1231 : 1237);
		result = prime * result + tpkKeyPersonnelKey;
		result = prime * result + tpkTransactionKey;
		result = prime * result + ((tpkUpdatedBy == null) ? 0 : tpkUpdatedBy.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof CgtTransactionsKeyPersonnelsImpl))
			return false;
		final CgtTransactionsKeyPersonnelsImpl other = (CgtTransactionsKeyPersonnelsImpl) obj;
		if (tkpTransactionKeyPersonnelKey != other.tkpTransactionKeyPersonnelKey)
			return false;
		if (tpkAllocation != other.tpkAllocation)
			return false;
		if (tpkDateCreated == null) {
			if (other.tpkDateCreated != null)
				return false;
		} else if (!tpkDateCreated.equals(other.tpkDateCreated))
			return false;
		if (tpkDateUpdated == null) {
			if (other.tpkDateUpdated != null)
				return false;
		} else if (!tpkDateUpdated.equals(other.tpkDateUpdated))
			return false;
		if (tpkIsActive != other.tpkIsActive)
			return false;
		if (tpkKeyPersonnelKey != other.tpkKeyPersonnelKey)
			return false;
		if (tpkTransactionKey != other.tpkTransactionKey)
			return false;
		if (tpkUpdatedBy == null) {
			if (other.tpkUpdatedBy != null)
				return false;
		} else if (!tpkUpdatedBy.equals(other.tpkUpdatedBy))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"CgtTransactionsKeyPersonnels [tkpTransactionKeyPersonnelKey=%s, tpkTransactionKey=%s, tpkKeyPersonnelKey=%s, tpkAllocation=%s, tpkIsActive=%s, tpkUpdatedBy=%s, tpkDateCreated=%s, tpkDateUpdated=%s]",
				tkpTransactionKeyPersonnelKey, tpkTransactionKey, tpkKeyPersonnelKey, tpkAllocation, tpkIsActive,
				tpkUpdatedBy, tpkDateCreated, tpkDateUpdated);
	}



	public CgtTransactionsKeyPersonnelsDTO toDTO() {
		return new CgtTransactionsKeyPersonnelsDTO(this);
	}

}
