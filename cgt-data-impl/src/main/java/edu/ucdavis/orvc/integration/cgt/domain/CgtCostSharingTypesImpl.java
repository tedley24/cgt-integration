package edu.ucdavis.orvc.integration.cgt.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtCostSharingTypes;
import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtCostSharingTypesDTO;

/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/
@Entity
@Table(name = "CGT_CostSharingTypes")
public class CgtCostSharingTypesImpl implements CgtCostSharingTypes {

	private static final long serialVersionUID = -7047472432815218092L;

	private int cstCostSharingTypeKey;
	private String cstName;
	private String cstShortName;
	private Integer cstSortOrder;
	private boolean cstIsActive;
	private String cstUpdatedBy;
	private LocalDateTime cstDateCreated;
	private LocalDateTime cstDateUpdated;

	public CgtCostSharingTypesImpl() {
	}

	public CgtCostSharingTypesImpl(final int cstCostSharingTypeKey, final String cstName, final boolean cstIsActive, final String cstUpdatedBy,
			final LocalDateTime cstDateCreated, final LocalDateTime cstDateUpdated) {
		this.cstCostSharingTypeKey = cstCostSharingTypeKey;
		this.cstName = cstName;
		this.cstIsActive = cstIsActive;
		this.cstUpdatedBy = cstUpdatedBy;
		this.cstDateCreated = cstDateCreated;
		this.cstDateUpdated = cstDateUpdated;
	}

	public CgtCostSharingTypesImpl(final int cstCostSharingTypeKey, final String cstName, final String cstShortName, final Integer cstSortOrder,
			final boolean cstIsActive, final String cstUpdatedBy, final LocalDateTime cstDateCreated, final LocalDateTime cstDateUpdated) {
		this.cstCostSharingTypeKey = cstCostSharingTypeKey;
		this.cstName = cstName;
		this.cstShortName = cstShortName;
		this.cstSortOrder = cstSortOrder;
		this.cstIsActive = cstIsActive;
		this.cstUpdatedBy = cstUpdatedBy;
		this.cstDateCreated = cstDateCreated;
		this.cstDateUpdated = cstDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtCostSharingTypes#getCstCostSharingTypeKey()
	 */
	@Override
	@Id
	@Column(name = "cst_costSharingTypeKey", unique = true, nullable = false)
	public int getCstCostSharingTypeKey() {
		return this.cstCostSharingTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtCostSharingTypes#setCstCostSharingTypeKey(int)
	 */
	@Override
	public void setCstCostSharingTypeKey(final int cstCostSharingTypeKey) {
		this.cstCostSharingTypeKey = cstCostSharingTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtCostSharingTypes#getCstName()
	 */
	@Override
	@Column(name = "cst_name", nullable = false)
	public String getCstName() {
		return this.cstName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtCostSharingTypes#setCstName(java.lang.String)
	 */
	@Override
	public void setCstName(final String cstName) {
		this.cstName = cstName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtCostSharingTypes#getCstShortName()
	 */
	@Override
	@Column(name = "cst_shortName")
	public String getCstShortName() {
		return this.cstShortName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtCostSharingTypes#setCstShortName(java.lang.String)
	 */
	@Override
	public void setCstShortName(final String cstShortName) {
		this.cstShortName = cstShortName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtCostSharingTypes#getCstSortOrder()
	 */
	@Override
	@Column(name = "cst_sortOrder")
	public Integer getCstSortOrder() {
		return this.cstSortOrder;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtCostSharingTypes#setCstSortOrder(java.lang.Integer)
	 */
	@Override
	public void setCstSortOrder(final Integer cstSortOrder) {
		this.cstSortOrder = cstSortOrder;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtCostSharingTypes#isCstIsActive()
	 */
	@Override
	@Column(name = "cst_isActive", nullable = false)
	public boolean isCstIsActive() {
		return this.cstIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtCostSharingTypes#setCstIsActive(boolean)
	 */
	@Override
	public void setCstIsActive(final boolean cstIsActive) {
		this.cstIsActive = cstIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtCostSharingTypes#getCstUpdatedBy()
	 */
	@Override
	@Column(name = "cst_updatedBy", nullable = false, length = 35, columnDefinition="char")
	public String getCstUpdatedBy() {
		return this.cstUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtCostSharingTypes#setCstUpdatedBy(java.lang.String)
	 */
	@Override
	public void setCstUpdatedBy(final String cstUpdatedBy) {
		this.cstUpdatedBy = cstUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtCostSharingTypes#getCstDateCreated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "cst_dateCreated", nullable = false, length = 23)
	public LocalDateTime getCstDateCreated() {
		return this.cstDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtCostSharingTypes#setCstDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setCstDateCreated(final LocalDateTime cstDateCreated) {
		this.cstDateCreated = cstDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtCostSharingTypes#getCstDateUpdated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "cst_dateUpdated", nullable = false, length = 23)
	public LocalDateTime getCstDateUpdated() {
		return this.cstDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtCostSharingTypes#setCstDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setCstDateUpdated(final LocalDateTime cstDateUpdated) {
		this.cstDateUpdated = cstDateUpdated;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + cstCostSharingTypeKey;
		result = prime * result + ((cstDateCreated == null) ? 0 : cstDateCreated.hashCode());
		result = prime * result + ((cstDateUpdated == null) ? 0 : cstDateUpdated.hashCode());
		result = prime * result + (cstIsActive ? 1231 : 1237);
		result = prime * result + ((cstName == null) ? 0 : cstName.hashCode());
		result = prime * result + ((cstShortName == null) ? 0 : cstShortName.hashCode());
		result = prime * result + ((cstSortOrder == null) ? 0 : cstSortOrder.hashCode());
		result = prime * result + ((cstUpdatedBy == null) ? 0 : cstUpdatedBy.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof CgtCostSharingTypesImpl))
			return false;
		final CgtCostSharingTypesImpl other = (CgtCostSharingTypesImpl) obj;
		if (cstCostSharingTypeKey != other.cstCostSharingTypeKey)
			return false;
		if (cstDateCreated == null) {
			if (other.cstDateCreated != null)
				return false;
		} else if (!cstDateCreated.equals(other.cstDateCreated))
			return false;
		if (cstDateUpdated == null) {
			if (other.cstDateUpdated != null)
				return false;
		} else if (!cstDateUpdated.equals(other.cstDateUpdated))
			return false;
		if (cstIsActive != other.cstIsActive)
			return false;
		if (cstName == null) {
			if (other.cstName != null)
				return false;
		} else if (!cstName.equals(other.cstName))
			return false;
		if (cstShortName == null) {
			if (other.cstShortName != null)
				return false;
		} else if (!cstShortName.equals(other.cstShortName))
			return false;
		if (cstSortOrder == null) {
			if (other.cstSortOrder != null)
				return false;
		} else if (!cstSortOrder.equals(other.cstSortOrder))
			return false;
		if (cstUpdatedBy == null) {
			if (other.cstUpdatedBy != null)
				return false;
		} else if (!cstUpdatedBy.equals(other.cstUpdatedBy))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"CgtCostSharingTypes [cstCostSharingTypeKey=%s, cstName=%s, cstShortName=%s, cstSortOrder=%s, cstIsActive=%s, cstUpdatedBy=%s, cstDateCreated=%s, cstDateUpdated=%s]",
				cstCostSharingTypeKey, cstName, cstShortName, cstSortOrder, cstIsActive, cstUpdatedBy, cstDateCreated,
				cstDateUpdated);
	}

	public CgtCostSharingTypesDTO toDTO() {
		return new CgtCostSharingTypesDTO(this);
	}
}
