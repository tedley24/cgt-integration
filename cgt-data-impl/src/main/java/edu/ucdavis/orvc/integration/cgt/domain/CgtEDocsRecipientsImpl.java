package edu.ucdavis.orvc.integration.cgt.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtEDocsRecipients;
import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtEDocsRecipientsDTO;



/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/
@Entity
@Table(name = "CGT_eDocsRecipients")
public class CgtEDocsRecipientsImpl implements CgtEDocsRecipients {

	private static final long serialVersionUID = -7869776192598517834L;
	private int recRecipientKey;
	private String recEdocKey;
	private String recEmail;
	private String recEmailType;
	private boolean recIsActive;
	private LocalDateTime recDateCreated;
	private LocalDateTime recDateUpdated;
	private String recUpdatedBy;

	public CgtEDocsRecipientsImpl() {
	}

	public CgtEDocsRecipientsImpl(final int recRecipientKey, final String recEdocKey, final String recEmail, final String recEmailType,
			final boolean recIsActive, final LocalDateTime recDateCreated, final LocalDateTime recDateUpdated, final String recUpdatedBy) {
		this.recRecipientKey = recRecipientKey;
		this.recEdocKey = recEdocKey;
		this.recEmail = recEmail;
		this.recEmailType = recEmailType;
		this.recIsActive = recIsActive;
		this.recDateCreated = recDateCreated;
		this.recDateUpdated = recDateUpdated;
		this.recUpdatedBy = recUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsRecipients#getRecRecipientKey()
	 */
	@Override
	@Id
	@Column(name = "rec_recipientKey", unique = true, nullable = false)
	public int getRecRecipientKey() {
		return this.recRecipientKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsRecipients#setRecRecipientKey(int)
	 */
	@Override
	public void setRecRecipientKey(final int recRecipientKey) {
		this.recRecipientKey = recRecipientKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsRecipients#getRecEdocKey()
	 */
	@Override
	@Column(name = "rec_edocKey", nullable = false, length = 35, columnDefinition="char")
	public String getRecEdocKey() {
		return this.recEdocKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsRecipients#setRecEdocKey(java.lang.String)
	 */
	@Override
	public void setRecEdocKey(final String recEdocKey) {
		this.recEdocKey = recEdocKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsRecipients#getRecEmail()
	 */
	@Override
	@Column(name = "rec_email", nullable = false, length = 500)
	public String getRecEmail() {
		return this.recEmail;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsRecipients#setRecEmail(java.lang.String)
	 */
	@Override
	public void setRecEmail(final String recEmail) {
		this.recEmail = recEmail;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsRecipients#getRecEmailType()
	 */
	@Override
	@Column(name = "rec_emailType", nullable = false, length = 3)
	public String getRecEmailType() {
		return this.recEmailType;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsRecipients#setRecEmailType(java.lang.String)
	 */
	@Override
	public void setRecEmailType(final String recEmailType) {
		this.recEmailType = recEmailType;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsRecipients#isRecIsActive()
	 */
	@Override
	@Column(name = "rec_isActive", nullable = false)
	public boolean isRecIsActive() {
		return this.recIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsRecipients#setRecIsActive(boolean)
	 */
	@Override
	public void setRecIsActive(final boolean recIsActive) {
		this.recIsActive = recIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsRecipients#getRecDateCreated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "rec_dateCreated", nullable = false, length = 23)
	public LocalDateTime getRecDateCreated() {
		return this.recDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsRecipients#setRecDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setRecDateCreated(final LocalDateTime recDateCreated) {
		this.recDateCreated = recDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsRecipients#getRecDateUpdated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "rec_dateUpdated", nullable = false, length = 23)
	public LocalDateTime getRecDateUpdated() {
		return this.recDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsRecipients#setRecDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setRecDateUpdated(final LocalDateTime recDateUpdated) {
		this.recDateUpdated = recDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsRecipients#getRecUpdatedBy()
	 */
	@Override
	@Column(name = "rec_updatedBy", nullable = false, length = 35, columnDefinition="char")
	public String getRecUpdatedBy() {
		return this.recUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsRecipients#setRecUpdatedBy(java.lang.String)
	 */
	@Override
	public void setRecUpdatedBy(final String recUpdatedBy) {
		this.recUpdatedBy = recUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((recDateCreated == null) ? 0 : recDateCreated.hashCode());
		result = prime * result + ((recDateUpdated == null) ? 0 : recDateUpdated.hashCode());
		result = prime * result + ((recEdocKey == null) ? 0 : recEdocKey.hashCode());
		result = prime * result + ((recEmail == null) ? 0 : recEmail.hashCode());
		result = prime * result + ((recEmailType == null) ? 0 : recEmailType.hashCode());
		result = prime * result + (recIsActive ? 1231 : 1237);
		result = prime * result + recRecipientKey;
		result = prime * result + ((recUpdatedBy == null) ? 0 : recUpdatedBy.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof CgtEDocsRecipientsImpl)) {
			return false;
		}
		CgtEDocsRecipientsImpl other = (CgtEDocsRecipientsImpl) obj;
		if (recDateCreated == null) {
			if (other.recDateCreated != null) {
				return false;
			}
		} else if (!recDateCreated.equals(other.recDateCreated)) {
			return false;
		}
		if (recDateUpdated == null) {
			if (other.recDateUpdated != null) {
				return false;
			}
		} else if (!recDateUpdated.equals(other.recDateUpdated)) {
			return false;
		}
		if (recEdocKey == null) {
			if (other.recEdocKey != null) {
				return false;
			}
		} else if (!recEdocKey.equals(other.recEdocKey)) {
			return false;
		}
		if (recEmail == null) {
			if (other.recEmail != null) {
				return false;
			}
		} else if (!recEmail.equals(other.recEmail)) {
			return false;
		}
		if (recEmailType == null) {
			if (other.recEmailType != null) {
				return false;
			}
		} else if (!recEmailType.equals(other.recEmailType)) {
			return false;
		}
		if (recIsActive != other.recIsActive) {
			return false;
		}
		if (recRecipientKey != other.recRecipientKey) {
			return false;
		}
		if (recUpdatedBy == null) {
			if (other.recUpdatedBy != null) {
				return false;
			}
		} else if (!recUpdatedBy.equals(other.recUpdatedBy)) {
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"CgtEDocsRecipientsImpl [recRecipientKey=%s, recEdocKey=%s, recEmail=%s, recEmailType=%s, recIsActive=%s, recDateCreated=%s, recDateUpdated=%s, recUpdatedBy=%s]",
				recRecipientKey, recEdocKey, recEmail, recEmailType, recIsActive, recDateCreated, recDateUpdated,
				recUpdatedBy);
	}

	public CgtEDocsRecipientsDTO toDTO() {
		return new CgtEDocsRecipientsDTO(this);
	}
}
