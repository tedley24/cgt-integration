package edu.ucdavis.orvc.integration.cgt.domain;


import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtEDocsTypesToTransactionTypes;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtEDocsTypesToTransactionTypesId;
import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtEDocsTypesToTransactionTypesDTO;

/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/
@Entity
@Table(name = "CGT_eDocsTypesToTransactionTypes")
public class CgtEDocsTypesToTransactionTypesImpl implements CgtEDocsTypesToTransactionTypes {

	private static final long serialVersionUID = -3417942255173834675L;
	private CgtEDocsTypesToTransactionTypesIdImpl id;

	public CgtEDocsTypesToTransactionTypesImpl() {
	}

	public CgtEDocsTypesToTransactionTypesImpl(final CgtEDocsTypesToTransactionTypesIdImpl id) {
		this.id = id;
	}


	@EmbeddedId
	@AttributeOverrides({
		@AttributeOverride(name = "e2tEDocsTypeKey", column = @Column(name = "e2t_eDocsTypeKey", nullable = false) ),
		@AttributeOverride(name = "e2tTransactionSubmissionTypeKey", column = @Column(name = "e2t_transactionSubmissionTypeKey", nullable = false) ) })
	public CgtEDocsTypesToTransactionTypesId getIdImpl() {
		return this.id;
	}

	public void setIdImpl(final CgtEDocsTypesToTransactionTypesIdImpl id) {
		this.id = id;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof CgtEDocsTypesToTransactionTypesImpl)) {
			return false;
		}
		CgtEDocsTypesToTransactionTypesImpl other = (CgtEDocsTypesToTransactionTypesImpl) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format("CgtEDocsTypesToTransactionTypesImpl [id=%s]", id);
	}

	@Override
	@Transient
	public CgtEDocsTypesToTransactionTypesId getId() {
		return id;
	}

	@Override
	public void setId(CgtEDocsTypesToTransactionTypesId id) {
		this.id = (CgtEDocsTypesToTransactionTypesIdImpl) id;
	}

	public CgtEDocsTypesToTransactionTypesDTO toDTO() {
		return new CgtEDocsTypesToTransactionTypesDTO(this);
	}
}
