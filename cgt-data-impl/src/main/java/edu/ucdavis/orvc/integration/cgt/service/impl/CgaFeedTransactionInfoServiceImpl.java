package edu.ucdavis.orvc.integration.cgt.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtTransaction;
import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtTransactionDTO;
import edu.ucdavis.orvc.integration.cgt.api.domain.service.CgtFeedTransactionInfoService;
import edu.ucdavis.orvc.integration.cgt.data.dao.CgtProjectDao;
import edu.ucdavis.orvc.integration.cgt.data.dao.CgtTransactionDao;
import edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionImpl;

@Service("cgtFeedTransactionInfoService")
@Transactional
public class CgaFeedTransactionInfoServiceImpl implements CgtFeedTransactionInfoService {

	@Autowired
	CgtProjectDao cgtProjectDao;
	
	@Autowired
	CgtTransactionDao cgtTransactionDao;
	
	protected List<CgtTransaction> convertListToInterface(List<CgtTransactionImpl> inList) {
		final List<CgtTransaction> results = new ArrayList<CgtTransaction>();
		for (CgtTransactionImpl elt : inList ) {
			results.add(new CgtTransactionDTO(elt));
		}
		return results;
	}
	
	@Override
	public List<CgtTransaction> getUnprocessedTransactions(final int maxDaysOld) {
		return convertListToInterface(cgtTransactionDao.findUnprocessedFeedEligibleTransactions(maxDaysOld) );
	}
	
	
}
