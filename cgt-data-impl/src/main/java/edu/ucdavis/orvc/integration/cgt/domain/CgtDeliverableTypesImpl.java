package edu.ucdavis.orvc.integration.cgt.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtDeliverableTypes;
import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtDeliverableTypesDTO;

/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/
@Entity
@Table(name = "CGT_DeliverableTypes")
public class CgtDeliverableTypesImpl implements CgtDeliverableTypes {

	private static final long serialVersionUID = -8134536435584774041L;

	private int detDeliverableTypeKey;
	private String detName;
	private String detShortName;
	private Integer detSortOrder;
	private Boolean detIsActive;
	private LocalDateTime detDateCreated;
	private LocalDateTime detDateUpdated;
	private String detUpdatedBy;

	public CgtDeliverableTypesImpl() {
	}

	public CgtDeliverableTypesImpl(final int detDeliverableTypeKey, final String detName) {
		this.detDeliverableTypeKey = detDeliverableTypeKey;
		this.detName = detName;
	}

	public CgtDeliverableTypesImpl(final int detDeliverableTypeKey, final String detName, final String detShortName, final Integer detSortOrder,
			final Boolean detIsActive, final LocalDateTime detDateCreated, final LocalDateTime detDateUpdated, final String detUpdatedBy) {
		this.detDeliverableTypeKey = detDeliverableTypeKey;
		this.detName = detName;
		this.detShortName = detShortName;
		this.detSortOrder = detSortOrder;
		this.detIsActive = detIsActive;
		this.detDateCreated = detDateCreated;
		this.detDateUpdated = detDateUpdated;
		this.detUpdatedBy = detUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDeliverableTypes#getDetDeliverableTypeKey()
	 */
	@Override
	@Id
	@Column(name = "det_deliverableTypeKey", unique = true, nullable = false)
	public int getDetDeliverableTypeKey() {
		return this.detDeliverableTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDeliverableTypes#setDetDeliverableTypeKey(int)
	 */
	@Override
	public void setDetDeliverableTypeKey(final int detDeliverableTypeKey) {
		this.detDeliverableTypeKey = detDeliverableTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDeliverableTypes#getDetName()
	 */
	@Override
	@Column(name = "det_name", nullable = false)
	public String getDetName() {
		return this.detName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDeliverableTypes#setDetName(java.lang.String)
	 */
	@Override
	public void setDetName(final String detName) {
		this.detName = detName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDeliverableTypes#getDetShortName()
	 */
	@Override
	@Column(name = "det_shortName")
	public String getDetShortName() {
		return this.detShortName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDeliverableTypes#setDetShortName(java.lang.String)
	 */
	@Override
	public void setDetShortName(final String detShortName) {
		this.detShortName = detShortName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDeliverableTypes#getDetSortOrder()
	 */
	@Override
	@Column(name = "det_sortOrder")
	public Integer getDetSortOrder() {
		return this.detSortOrder;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDeliverableTypes#setDetSortOrder(java.lang.Integer)
	 */
	@Override
	public void setDetSortOrder(final Integer detSortOrder) {
		this.detSortOrder = detSortOrder;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDeliverableTypes#getDetIsActive()
	 */
	@Override
	@Column(name = "det_isActive")
	public Boolean getDetIsActive() {
		return this.detIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDeliverableTypes#setDetIsActive(java.lang.Boolean)
	 */
	@Override
	public void setDetIsActive(final Boolean detIsActive) {
		this.detIsActive = detIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDeliverableTypes#getDetDateCreated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "det_dateCreated", length = 23)
	public LocalDateTime getDetDateCreated() {
		return this.detDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDeliverableTypes#setDetDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setDetDateCreated(final LocalDateTime detDateCreated) {
		this.detDateCreated = detDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDeliverableTypes#getDetDateUpdated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "det_dateUpdated", length = 23)
	public LocalDateTime getDetDateUpdated() {
		return this.detDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDeliverableTypes#setDetDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setDetDateUpdated(final LocalDateTime detDateUpdated) {
		this.detDateUpdated = detDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDeliverableTypes#getDetUpdatedBy()
	 */
	@Override
	@Column(name = "det_updatedBy", length = 35, columnDefinition="char")
	public String getDetUpdatedBy() {
		return this.detUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDeliverableTypes#setDetUpdatedBy(java.lang.String)
	 */
	@Override
	public void setDetUpdatedBy(final String detUpdatedBy) {
		this.detUpdatedBy = detUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((detDateCreated == null) ? 0 : detDateCreated.hashCode());
		result = prime * result + ((detDateUpdated == null) ? 0 : detDateUpdated.hashCode());
		result = prime * result + detDeliverableTypeKey;
		result = prime * result + ((detIsActive == null) ? 0 : detIsActive.hashCode());
		result = prime * result + ((detName == null) ? 0 : detName.hashCode());
		result = prime * result + ((detShortName == null) ? 0 : detShortName.hashCode());
		result = prime * result + ((detSortOrder == null) ? 0 : detSortOrder.hashCode());
		result = prime * result + ((detUpdatedBy == null) ? 0 : detUpdatedBy.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof CgtDeliverableTypesImpl)) {
			return false;
		}
		CgtDeliverableTypesImpl other = (CgtDeliverableTypesImpl) obj;
		if (detDateCreated == null) {
			if (other.detDateCreated != null) {
				return false;
			}
		} else if (!detDateCreated.equals(other.detDateCreated)) {
			return false;
		}
		if (detDateUpdated == null) {
			if (other.detDateUpdated != null) {
				return false;
			}
		} else if (!detDateUpdated.equals(other.detDateUpdated)) {
			return false;
		}
		if (detDeliverableTypeKey != other.detDeliverableTypeKey) {
			return false;
		}
		if (detIsActive == null) {
			if (other.detIsActive != null) {
				return false;
			}
		} else if (!detIsActive.equals(other.detIsActive)) {
			return false;
		}
		if (detName == null) {
			if (other.detName != null) {
				return false;
			}
		} else if (!detName.equals(other.detName)) {
			return false;
		}
		if (detShortName == null) {
			if (other.detShortName != null) {
				return false;
			}
		} else if (!detShortName.equals(other.detShortName)) {
			return false;
		}
		if (detSortOrder == null) {
			if (other.detSortOrder != null) {
				return false;
			}
		} else if (!detSortOrder.equals(other.detSortOrder)) {
			return false;
		}
		if (detUpdatedBy == null) {
			if (other.detUpdatedBy != null) {
				return false;
			}
		} else if (!detUpdatedBy.equals(other.detUpdatedBy)) {
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"CgtDeliverableTypesImpl [detDeliverableTypeKey=%s, detName=%s, detShortName=%s, detSortOrder=%s, detIsActive=%s, detDateCreated=%s, detDateUpdated=%s, detUpdatedBy=%s]",
				detDeliverableTypeKey, detName, detShortName, detSortOrder, detIsActive, detDateCreated, detDateUpdated,
				detUpdatedBy);
	}

	public CgtDeliverableTypesDTO toDTO() {
		return new CgtDeliverableTypesDTO(this);
	}
}
