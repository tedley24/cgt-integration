package edu.ucdavis.orvc.integration.cgt.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.MasUcdbouccodes;
import edu.ucdavis.orvc.integration.cgt.api.domain.dto.MasUcdbouccodesDTO;



/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/
@Entity
@Table(name = "MAS_UCDBOUCCodes")
public class MasUcdbouccodesImpl implements MasUcdbouccodes {

	private static final long serialVersionUID = 6594521415450811313L;
	private String bouCode;
	private String bouShortName;
	private String bouLongName;
	private LocalDateTime bouDateCreated;
	private LocalDateTime bouDateUpdated;
	private String bouUpdatedBy;

	public MasUcdbouccodesImpl() {
	}

	public MasUcdbouccodesImpl(final String bouCode, final String bouShortName, final String bouLongName, final LocalDateTime bouDateCreated,
			final LocalDateTime bouDateUpdated, final String bouUpdatedBy) {
		this.bouCode = bouCode;
		this.bouShortName = bouShortName;
		this.bouLongName = bouLongName;
		this.bouDateCreated = bouDateCreated;
		this.bouDateUpdated = bouDateUpdated;
		this.bouUpdatedBy = bouUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUcdbouccodes#getBouCode()
	 */
	@Override
	@Id
	@Column(name = "bou_code", unique = true, nullable = false, length = 2, columnDefinition="char")
	public String getBouCode() {
		return this.bouCode;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUcdbouccodes#setBouCode(java.lang.String)
	 */
	@Override
	public void setBouCode(final String bouCode) {
		this.bouCode = bouCode;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUcdbouccodes#getBouShortName()
	 */
	@Override
	@Column(name = "bou_shortName", nullable = false, length = 50)
	public String getBouShortName() {
		return this.bouShortName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUcdbouccodes#setBouShortName(java.lang.String)
	 */
	@Override
	public void setBouShortName(final String bouShortName) {
		this.bouShortName = bouShortName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUcdbouccodes#getBouLongName()
	 */
	@Override
	@Column(name = "bou_longName", nullable = false)
	public String getBouLongName() {
		return this.bouLongName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUcdbouccodes#setBouLongName(java.lang.String)
	 */
	@Override
	public void setBouLongName(final String bouLongName) {
		this.bouLongName = bouLongName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUcdbouccodes#getBouDateCreated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "bou_dateCreated", nullable = false, length = 23)
	public LocalDateTime getBouDateCreated() {
		return this.bouDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUcdbouccodes#setBouDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setBouDateCreated(final LocalDateTime bouDateCreated) {
		this.bouDateCreated = bouDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUcdbouccodes#getBouDateUpdated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "bou_dateUpdated", nullable = false, length = 23)
	public LocalDateTime getBouDateUpdated() {
		return this.bouDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUcdbouccodes#setBouDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setBouDateUpdated(final LocalDateTime bouDateUpdated) {
		this.bouDateUpdated = bouDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUcdbouccodes#getBouUpdatedBy()
	 */
	@Override
	@Column(name = "bou_updatedBy", nullable = false, length = 35, columnDefinition="char")
	public String getBouUpdatedBy() {
		return this.bouUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUcdbouccodes#setBouUpdatedBy(java.lang.String)
	 */
	@Override
	public void setBouUpdatedBy(final String bouUpdatedBy) {
		this.bouUpdatedBy = bouUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((bouCode == null) ? 0 : bouCode.hashCode());
		result = prime * result + ((bouDateCreated == null) ? 0 : bouDateCreated.hashCode());
		result = prime * result + ((bouDateUpdated == null) ? 0 : bouDateUpdated.hashCode());
		result = prime * result + ((bouLongName == null) ? 0 : bouLongName.hashCode());
		result = prime * result + ((bouShortName == null) ? 0 : bouShortName.hashCode());
		result = prime * result + ((bouUpdatedBy == null) ? 0 : bouUpdatedBy.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof MasUcdbouccodesImpl))
			return false;
		final MasUcdbouccodesImpl other = (MasUcdbouccodesImpl) obj;
		if (bouCode == null) {
			if (other.bouCode != null)
				return false;
		} else if (!bouCode.equals(other.bouCode))
			return false;
		if (bouDateCreated == null) {
			if (other.bouDateCreated != null)
				return false;
		} else if (!bouDateCreated.equals(other.bouDateCreated))
			return false;
		if (bouDateUpdated == null) {
			if (other.bouDateUpdated != null)
				return false;
		} else if (!bouDateUpdated.equals(other.bouDateUpdated))
			return false;
		if (bouLongName == null) {
			if (other.bouLongName != null)
				return false;
		} else if (!bouLongName.equals(other.bouLongName))
			return false;
		if (bouShortName == null) {
			if (other.bouShortName != null)
				return false;
		} else if (!bouShortName.equals(other.bouShortName))
			return false;
		if (bouUpdatedBy == null) {
			if (other.bouUpdatedBy != null)
				return false;
		} else if (!bouUpdatedBy.equals(other.bouUpdatedBy))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"MasUcdbouccodes [bouCode=%s, bouShortName=%s, bouLongName=%s, bouDateCreated=%s, bouDateUpdated=%s, bouUpdatedBy=%s]",
				bouCode, bouShortName, bouLongName, bouDateCreated, bouDateUpdated, bouUpdatedBy);
	}



	public MasUcdbouccodesDTO toDTO() {
		return new MasUcdbouccodesDTO(this);
	}

}
