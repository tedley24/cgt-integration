package edu.ucdavis.orvc.integration.cgt.domain;


import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtCloseoutContacts;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtCloseoutContactsId;
import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtCloseoutContactsDTO;

/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/
@Entity
@Table(name = "CGT_Closeout_Contacts")
public class CgtCloseoutContactsImpl implements CgtCloseoutContacts {

	private static final long serialVersionUID = 1792447582136987245L;
	private CgtCloseoutContactsIdImpl id;

	public CgtCloseoutContactsImpl() {
	}

	public CgtCloseoutContactsImpl(final CgtCloseoutContactsIdImpl id) {
		this.id = id;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtCloseoutContacts#getId()
	 */
	@EmbeddedId
	@AttributeOverrides({
		@AttributeOverride(name = "ccUserKey", column = @Column(name = "cc_userKey", nullable = false, length = 35,columnDefinition="char") ),
		@AttributeOverride(name = "ccMothraId", column = @Column(name = "cc_mothraID", length = 8,columnDefinition="char") ),
		@AttributeOverride(name = "ccAdminDept", column = @Column(name = "cc_adminDept", length = 10) ),
		@AttributeOverride(name = "ccRole", column = @Column(name = "cc_role") ),
		@AttributeOverride(name = "ccRoleName", column = @Column(name = "cc_role_name", length = 20) ) })
	public CgtCloseoutContactsIdImpl getIdImpl() {
		return this.id;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtCloseoutContacts#setId(edu.ucdavis.orvc.integration.cgt.domain.CgtCloseoutContactsId)
	 */

	public void setIdImpl(final CgtCloseoutContactsIdImpl id) {
		this.id = id;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof CgtCloseoutContactsImpl))
			return false;
		final CgtCloseoutContactsImpl other = (CgtCloseoutContactsImpl) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	@Transient
	public CgtCloseoutContactsId getId() {
		return id;
	}

	@Override
	public void setId(CgtCloseoutContactsId id) {
		this.id = (CgtCloseoutContactsIdImpl) id;
	}

	public CgtCloseoutContactsDTO toDTO() {
		return new CgtCloseoutContactsDTO(this);
	}
}
