package edu.ucdavis.orvc.integration.cgt.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtWrapUpCloseRulesToQaaAnswers;
import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtWrapUpCloseRulesToQaaAnswersDTO;

/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/
@Entity
@Table(name = "CGT_WrapUpCloseRulesToQaaAnswers")
public class CgtWrapUpCloseRulesToQaaAnswersImpl implements CgtWrapUpCloseRulesToQaaAnswers {

	private static final long serialVersionUID = 57056328359960440L;
	private int w2qRuleKey;
	private int w2qCloseRuleKey;
	private int w2qQuestionKey;
	private String w2qAnswer;
	private String w2qBouCode;
	private String w2qNotBouCode;
	private Integer w2qResultSubmissionTypeKey;

	public CgtWrapUpCloseRulesToQaaAnswersImpl() {
	}

	public CgtWrapUpCloseRulesToQaaAnswersImpl(final int w2qRuleKey, final int w2qCloseRuleKey, final int w2qQuestionKey, final String w2qAnswer) {
		this.w2qRuleKey = w2qRuleKey;
		this.w2qCloseRuleKey = w2qCloseRuleKey;
		this.w2qQuestionKey = w2qQuestionKey;
		this.w2qAnswer = w2qAnswer;
	}

	public CgtWrapUpCloseRulesToQaaAnswersImpl(final int w2qRuleKey, final int w2qCloseRuleKey, final int w2qQuestionKey, final String w2qAnswer,
			final String w2qBouCode, final String w2qNotBouCode, final Integer w2qResultSubmissionTypeKey) {
		this.w2qRuleKey = w2qRuleKey;
		this.w2qCloseRuleKey = w2qCloseRuleKey;
		this.w2qQuestionKey = w2qQuestionKey;
		this.w2qAnswer = w2qAnswer;
		this.w2qBouCode = w2qBouCode;
		this.w2qNotBouCode = w2qNotBouCode;
		this.w2qResultSubmissionTypeKey = w2qResultSubmissionTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpCloseRulesToQaaAnswers#getW2qRuleKey()
	 */
	@Override
	@Id
	@Column(name = "w2q_ruleKey", unique = true, nullable = false)
	public int getW2qRuleKey() {
		return this.w2qRuleKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpCloseRulesToQaaAnswers#setW2qRuleKey(int)
	 */
	@Override
	public void setW2qRuleKey(final int w2qRuleKey) {
		this.w2qRuleKey = w2qRuleKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpCloseRulesToQaaAnswers#getW2qCloseRuleKey()
	 */
	@Override
	@Column(name = "w2q_closeRuleKey", nullable = false)
	public int getW2qCloseRuleKey() {
		return this.w2qCloseRuleKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpCloseRulesToQaaAnswers#setW2qCloseRuleKey(int)
	 */
	@Override
	public void setW2qCloseRuleKey(final int w2qCloseRuleKey) {
		this.w2qCloseRuleKey = w2qCloseRuleKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpCloseRulesToQaaAnswers#getW2qQuestionKey()
	 */
	@Override
	@Column(name = "w2q_questionKey", nullable = false)
	public int getW2qQuestionKey() {
		return this.w2qQuestionKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpCloseRulesToQaaAnswers#setW2qQuestionKey(int)
	 */
	@Override
	public void setW2qQuestionKey(final int w2qQuestionKey) {
		this.w2qQuestionKey = w2qQuestionKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpCloseRulesToQaaAnswers#getW2qAnswer()
	 */
	@Override
	@Column(name = "w2q_answer", nullable = false, length = 50)
	public String getW2qAnswer() {
		return this.w2qAnswer;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpCloseRulesToQaaAnswers#setW2qAnswer(java.lang.String)
	 */
	@Override
	public void setW2qAnswer(final String w2qAnswer) {
		this.w2qAnswer = w2qAnswer;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpCloseRulesToQaaAnswers#getW2qBouCode()
	 */
	@Override
	@Column(name = "w2q_bouCode", length = 2,columnDefinition="char")
	public String getW2qBouCode() {
		return this.w2qBouCode;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpCloseRulesToQaaAnswers#setW2qBouCode(java.lang.String)
	 */
	@Override
	public void setW2qBouCode(final String w2qBouCode) {
		this.w2qBouCode = w2qBouCode;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpCloseRulesToQaaAnswers#getW2qNotBouCode()
	 */
	@Override
	@Column(name = "w2q_NotBouCode", length = 2,columnDefinition="char")
	public String getW2qNotBouCode() {
		return this.w2qNotBouCode;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpCloseRulesToQaaAnswers#setW2qNotBouCode(java.lang.String)
	 */
	@Override
	public void setW2qNotBouCode(final String w2qNotBouCode) {
		this.w2qNotBouCode = w2qNotBouCode;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpCloseRulesToQaaAnswers#getW2qResultSubmissionTypeKey()
	 */
	@Override
	@Column(name = "w2q_resultSubmissionTypeKey")
	public Integer getW2qResultSubmissionTypeKey() {
		return this.w2qResultSubmissionTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpCloseRulesToQaaAnswers#setW2qResultSubmissionTypeKey(java.lang.Integer)
	 */
	@Override
	public void setW2qResultSubmissionTypeKey(final Integer w2qResultSubmissionTypeKey) {
		this.w2qResultSubmissionTypeKey = w2qResultSubmissionTypeKey;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((w2qAnswer == null) ? 0 : w2qAnswer.hashCode());
		result = prime * result + ((w2qBouCode == null) ? 0 : w2qBouCode.hashCode());
		result = prime * result + w2qCloseRuleKey;
		result = prime * result + ((w2qNotBouCode == null) ? 0 : w2qNotBouCode.hashCode());
		result = prime * result + w2qQuestionKey;
		result = prime * result + ((w2qResultSubmissionTypeKey == null) ? 0 : w2qResultSubmissionTypeKey.hashCode());
		result = prime * result + w2qRuleKey;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof CgtWrapUpCloseRulesToQaaAnswersImpl))
			return false;
		final CgtWrapUpCloseRulesToQaaAnswersImpl other = (CgtWrapUpCloseRulesToQaaAnswersImpl) obj;
		if (w2qAnswer == null) {
			if (other.w2qAnswer != null)
				return false;
		} else if (!w2qAnswer.equals(other.w2qAnswer))
			return false;
		if (w2qBouCode == null) {
			if (other.w2qBouCode != null)
				return false;
		} else if (!w2qBouCode.equals(other.w2qBouCode))
			return false;
		if (w2qCloseRuleKey != other.w2qCloseRuleKey)
			return false;
		if (w2qNotBouCode == null) {
			if (other.w2qNotBouCode != null)
				return false;
		} else if (!w2qNotBouCode.equals(other.w2qNotBouCode))
			return false;
		if (w2qQuestionKey != other.w2qQuestionKey)
			return false;
		if (w2qResultSubmissionTypeKey == null) {
			if (other.w2qResultSubmissionTypeKey != null)
				return false;
		} else if (!w2qResultSubmissionTypeKey.equals(other.w2qResultSubmissionTypeKey))
			return false;
		if (w2qRuleKey != other.w2qRuleKey)
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"CgtWrapUpCloseRulesToQaaAnswers [w2qRuleKey=%s, w2qCloseRuleKey=%s, w2qQuestionKey=%s, w2qAnswer=%s, w2qBouCode=%s, w2qNotBouCode=%s, w2qResultSubmissionTypeKey=%s]",
				w2qRuleKey, w2qCloseRuleKey, w2qQuestionKey, w2qAnswer, w2qBouCode, w2qNotBouCode,
				w2qResultSubmissionTypeKey);
	}



	public CgtWrapUpCloseRulesToQaaAnswersDTO toDTO() {
		return new CgtWrapUpCloseRulesToQaaAnswersDTO(this);
	}

}
