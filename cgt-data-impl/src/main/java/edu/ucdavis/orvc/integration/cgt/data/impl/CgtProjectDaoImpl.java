package edu.ucdavis.orvc.integration.cgt.data.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.codehaus.plexus.util.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtAdministeringUnit;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtInstrumentTypesEnum;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtKeyPersonnel;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtProject;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtSponsor;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtTransaction;
import edu.ucdavis.orvc.integration.cgt.data.dao.CgtProjectDao;
import edu.ucdavis.orvc.integration.cgt.domain.CgtProjectImpl;

@Transactional
@Repository("cgtProjectDao")
public class CgtProjectDaoImpl implements CgtProjectDao {
	
	public static final String FIELD_PROJECT_KEY = "proProjectKey";
	public static final String FIELD_ISACTIVE = "proIsActive";
	public static final String FIELD_DATE_CREATED = "proDateCreated";
	public static final String FIELD_INSTRUMENT_TYPE_KEY = "proInstrumentTypeKey";
	
	public static final CgtInstrumentTypesEnum[] CGA_INSTRUMENT_TYPES = 
			new CgtInstrumentTypesEnum[] { CgtInstrumentTypesEnum.CONTRACT
					,CgtInstrumentTypesEnum.CONTRACT
					,CgtInstrumentTypesEnum.COOP_AGREEMENT
			};
	
	private static final List<Integer> CGA_INSTRUMENT_TYPE_KEYS;
	
	static {
		Builder<Integer> build = new ImmutableList.Builder<Integer>();
		for (CgtInstrumentTypesEnum type : CGA_INSTRUMENT_TYPES) {
			build.add(type.typeKey);
		}
		CGA_INSTRUMENT_TYPE_KEYS = build.build();
	}

	
	
	
	@Autowired
	@Qualifier("cgtDataSessionFactory")
	private SessionFactory cgtSessionFactory;
	
	private final Logger logger = LoggerFactory.getLogger(CgtProjectDaoImpl.class);
	
	
	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.data.CgtProjectDao#findAllByProjectNumber(java.lang.String)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public List<CgtProjectImpl> findAllByProjectNumber(final String projectNumber) {
		return getCurrentSession()
				.createCriteria(CgtProjectImpl.class)
				.add(Restrictions.eq("proProjectKey", projectNumber))
				.list();
	}
	
	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.data.CgtProjectDao#findByProjectNumber(java.lang.String)
	 */
	@Override
	public CgtProject findByProjectNumber(final String projectNumber) {
		return (CgtProject) getCurrentSession().byId(CgtProjectImpl.class).load(projectNumber);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public CgtProject findActiveByProjectNumber(final String projectNumber) {
		final CgtProject result;
		List<CgtProjectImpl> projects = getCurrentSession().createCriteria(CgtProjectImpl.class)
										.add(Restrictions.eq(FIELD_PROJECT_KEY, projectNumber))
										.add(Restrictions.eq(FIELD_ISACTIVE, true))
										.list();
		
		switch (projects.size()) {
			case 1: result = projects.get(0);
					break;
			case 0: result = null;
					break;
			default: logger.warn("project {} has {} active records in CGT_PROJECTS", projectNumber,projects.size());
					 throw new IllegalStateException(
							 String.format("Project %s has %s active records in the project table.",projectNumber,projects.size())
							 ); 
		}
		
			
		return result;
	}					
	
	
	@Override
	public List<CgtProjectImpl> findCgaFeedEligibleProjects(final String projectNumber
			, final boolean areActive) {
		
		logger.debug("finding projects eligible for cga feed. (projectKey={},areActive={})",projectNumber,areActive);
		Criteria criteria = getCurrentSession().createCriteria(CgtProjectImpl.class);
		//AND	CGT_Projects.pro_instrumentTypeKey IN (1,2,3)
		criteria.add(Restrictions.in(FIELD_INSTRUMENT_TYPE_KEY, CGA_INSTRUMENT_TYPE_KEYS));
		
		//AND	CGT_Projects.pro_isActive = 1
		if (areActive) {
			criteria.add(Restrictions.eq(FIELD_ISACTIVE, true));
		}
		
		if (StringUtils.isNotEmpty(projectNumber)) {
			criteria.add(Restrictions.eq(FIELD_PROJECT_KEY,projectNumber));
		}
		
		List<CgtProjectImpl> results = criteria.list();
		
		if (logger.isDebugEnabled()) {
			logger.debug("Found {} results for (cga eligible, projectKey={}, areActive={}). Project Keys:",projectNumber,areActive);
			for (CgtProject p:results) {
				logger.debug(p.getProProjectKey());
			}
		}
		return results;
	}
	
	
	/**
	 * @param projectNumber
	 * @param isActive
	 * @return
	 * @see 
	 */
	@Override
	public CgtProject getCgaEligibleProject(final String projectNumber,boolean isActive) {
		
		List<CgtProjectImpl> projects = findCgaFeedEligibleProjects(projectNumber, isActive);
		final CgtProject result;
		
		switch (projects.size()) {
		case 1: result = projects.get(0);
				break;
		case 0: result = null;
				break;
		default: logger.warn("project {} has {} active and eligible records in CGT_PROJECTS", projectNumber,projects.size());
				 throw new IllegalStateException(
						 String.format("Project %s has %s active records in the project table.",projectNumber,projects.size())
						 ); 
		}
	
		
		return result;

		
		
	}
	
	public SessionFactory getCgtSessionFactory() { return cgtSessionFactory; }
	public Session getCurrentSession() { return getCgtSessionFactory().getCurrentSession(); }

	@Override
	public void logProjectInfo(String projectNumber) {
		CgtProject project = findByProjectNumber(projectNumber);
		
		if (project != null) {
			logger.info(project.toString());
			for(CgtTransaction tx: project.getTransactions()) {
				logger.info(tx.toString());
			}
			
			for (CgtSponsor projectSponsor : project.getProjectSponsors()) {
				logger.info(projectSponsor.toString());
			}
			
			for (CgtAdministeringUnit adminUnit:project.getAdministeringUnits()) {
				logger.info(adminUnit.toString());
				logger.info(adminUnit.getDepartment().toString());
			}
			
			for (CgtKeyPersonnel personnel : project.getKeyPersonnel()) {
				logger.info("KP TYPE = {}, {}",personnel.getPersonnelRole().getRoleTypeEnum().name,personnel.getUser());
			}
		
			logger.info("Primary Sponsor: {}", project.getPrimaryProjectSponsor());
			logger.info("Originating Sponsor: {}", project.getOriginatingProjectSponsor());
			
			
		} else {
			logger.info("Could not load {}, does not exist.", projectNumber);
		}
		
		
	}
}
