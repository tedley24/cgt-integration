package edu.ucdavis.orvc.integration.cgt.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtWrapUpCloseRulesToDepartments;
import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtWrapUpCloseRulesToDepartmentsDTO;

/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/
@Entity
@Table(name = "CGT_WrapUpCloseRulesToDepartments")
public class CgtWrapUpCloseRulesToDepartmentsImpl implements CgtWrapUpCloseRulesToDepartments {

	private static final long serialVersionUID = -6384755074245965671L;
	private int w2sRuleKey;
	private int w2sCloseRuleKey;
	private String w2sNameBeginsWith;
	private String w2sBoucode;
	private String w2sSponsorSubAgencyKey;
	private int w2sResultSubmissionTypeKey;

	public CgtWrapUpCloseRulesToDepartmentsImpl() {
	}

	public CgtWrapUpCloseRulesToDepartmentsImpl(final int w2sRuleKey, final int w2sCloseRuleKey, final String w2sBoucode,
			final int w2sResultSubmissionTypeKey) {
		this.w2sRuleKey = w2sRuleKey;
		this.w2sCloseRuleKey = w2sCloseRuleKey;
		this.w2sBoucode = w2sBoucode;
		this.w2sResultSubmissionTypeKey = w2sResultSubmissionTypeKey;
	}

	public CgtWrapUpCloseRulesToDepartmentsImpl(final int w2sRuleKey, final int w2sCloseRuleKey, final String w2sNameBeginsWith,
			final String w2sBoucode, final String w2sSponsorSubAgencyKey, final int w2sResultSubmissionTypeKey) {
		this.w2sRuleKey = w2sRuleKey;
		this.w2sCloseRuleKey = w2sCloseRuleKey;
		this.w2sNameBeginsWith = w2sNameBeginsWith;
		this.w2sBoucode = w2sBoucode;
		this.w2sSponsorSubAgencyKey = w2sSponsorSubAgencyKey;
		this.w2sResultSubmissionTypeKey = w2sResultSubmissionTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpCloseRulesToDepartments#getW2sRuleKey()
	 */
	@Override
	@Id
	@Column(name = "w2s_ruleKey", unique = true, nullable = false)
	public int getW2sRuleKey() {
		return this.w2sRuleKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpCloseRulesToDepartments#setW2sRuleKey(int)
	 */
	@Override
	public void setW2sRuleKey(final int w2sRuleKey) {
		this.w2sRuleKey = w2sRuleKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpCloseRulesToDepartments#getW2sCloseRuleKey()
	 */
	@Override
	@Column(name = "w2s_closeRuleKey", nullable = false)
	public int getW2sCloseRuleKey() {
		return this.w2sCloseRuleKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpCloseRulesToDepartments#setW2sCloseRuleKey(int)
	 */
	@Override
	public void setW2sCloseRuleKey(final int w2sCloseRuleKey) {
		this.w2sCloseRuleKey = w2sCloseRuleKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpCloseRulesToDepartments#getW2sNameBeginsWith()
	 */
	@Override
	@Column(name = "w2s_nameBeginsWith", length = 50)
	public String getW2sNameBeginsWith() {
		return this.w2sNameBeginsWith;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpCloseRulesToDepartments#setW2sNameBeginsWith(java.lang.String)
	 */
	@Override
	public void setW2sNameBeginsWith(final String w2sNameBeginsWith) {
		this.w2sNameBeginsWith = w2sNameBeginsWith;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpCloseRulesToDepartments#getW2sBoucode()
	 */
	@Override
	@Column(name = "w2s_boucode", nullable = false, length = 2,columnDefinition="char")
	public String getW2sBoucode() {
		return this.w2sBoucode;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpCloseRulesToDepartments#setW2sBoucode(java.lang.String)
	 */
	@Override
	public void setW2sBoucode(final String w2sBoucode) {
		this.w2sBoucode = w2sBoucode;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpCloseRulesToDepartments#getW2sSponsorSubAgencyKey()
	 */
	@Override
	@Column(name = "w2s_sponsorSubAgencyKey", length = 2,columnDefinition="char")
	public String getW2sSponsorSubAgencyKey() {
		return this.w2sSponsorSubAgencyKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpCloseRulesToDepartments#setW2sSponsorSubAgencyKey(java.lang.String)
	 */
	@Override
	public void setW2sSponsorSubAgencyKey(final String w2sSponsorSubAgencyKey) {
		this.w2sSponsorSubAgencyKey = w2sSponsorSubAgencyKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpCloseRulesToDepartments#getW2sResultSubmissionTypeKey()
	 */
	@Override
	@Column(name = "w2s_resultSubmissionTypeKey", nullable = false)
	public int getW2sResultSubmissionTypeKey() {
		return this.w2sResultSubmissionTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpCloseRulesToDepartments#setW2sResultSubmissionTypeKey(int)
	 */
	@Override
	public void setW2sResultSubmissionTypeKey(final int w2sResultSubmissionTypeKey) {
		this.w2sResultSubmissionTypeKey = w2sResultSubmissionTypeKey;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((w2sBoucode == null) ? 0 : w2sBoucode.hashCode());
		result = prime * result + w2sCloseRuleKey;
		result = prime * result + ((w2sNameBeginsWith == null) ? 0 : w2sNameBeginsWith.hashCode());
		result = prime * result + w2sResultSubmissionTypeKey;
		result = prime * result + w2sRuleKey;
		result = prime * result + ((w2sSponsorSubAgencyKey == null) ? 0 : w2sSponsorSubAgencyKey.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof CgtWrapUpCloseRulesToDepartmentsImpl))
			return false;
		final CgtWrapUpCloseRulesToDepartmentsImpl other = (CgtWrapUpCloseRulesToDepartmentsImpl) obj;
		if (w2sBoucode == null) {
			if (other.w2sBoucode != null)
				return false;
		} else if (!w2sBoucode.equals(other.w2sBoucode))
			return false;
		if (w2sCloseRuleKey != other.w2sCloseRuleKey)
			return false;
		if (w2sNameBeginsWith == null) {
			if (other.w2sNameBeginsWith != null)
				return false;
		} else if (!w2sNameBeginsWith.equals(other.w2sNameBeginsWith))
			return false;
		if (w2sResultSubmissionTypeKey != other.w2sResultSubmissionTypeKey)
			return false;
		if (w2sRuleKey != other.w2sRuleKey)
			return false;
		if (w2sSponsorSubAgencyKey == null) {
			if (other.w2sSponsorSubAgencyKey != null)
				return false;
		} else if (!w2sSponsorSubAgencyKey.equals(other.w2sSponsorSubAgencyKey))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"CgtWrapUpCloseRulesToDepartments [w2sRuleKey=%s, w2sCloseRuleKey=%s, w2sNameBeginsWith=%s, w2sBoucode=%s, w2sSponsorSubAgencyKey=%s, w2sResultSubmissionTypeKey=%s]",
				w2sRuleKey, w2sCloseRuleKey, w2sNameBeginsWith, w2sBoucode, w2sSponsorSubAgencyKey,
				w2sResultSubmissionTypeKey);
	}



	public CgtWrapUpCloseRulesToDepartmentsDTO toDTO() {
		return new CgtWrapUpCloseRulesToDepartmentsDTO(this);
	}

}
