package edu.ucdavis.orvc.integration.cgt.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtEmailTemplates;
import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtEmailTemplatesDTO;



/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/
@Entity
@Table(name = "CGT_EmailTemplates")
public class CgtEmailTemplatesImpl implements CgtEmailTemplates {

	private static final long serialVersionUID = -4233290267361894291L;
	private int emtEmailTemplateKey;
	private int emtTransactionSubmissionTypeKey;
	private int emtTransactionTypeKey;
	private String emtName;
	private String emtTo;
	private String emtFrom;
	private String emtCc;
	private String emtOptionalCc;
	private String emtBcc;
	private String emtSubject;
	private String emtBody;
	private boolean emtAllowAttachments;
	private boolean emtIsActive;
	private String emtUpdatedBy;
	private LocalDateTime emtDateUpdated;
	private LocalDateTime emtDateCreated;

	public CgtEmailTemplatesImpl() {
	}

	public CgtEmailTemplatesImpl(final int emtEmailTemplateKey, final int emtTransactionSubmissionTypeKey, final int emtTransactionTypeKey,
			final String emtName, final String emtSubject, final String emtBody, final boolean emtAllowAttachments, final boolean emtIsActive,
			final String emtUpdatedBy, final LocalDateTime emtDateUpdated, final LocalDateTime emtDateCreated) {
		this.emtEmailTemplateKey = emtEmailTemplateKey;
		this.emtTransactionSubmissionTypeKey = emtTransactionSubmissionTypeKey;
		this.emtTransactionTypeKey = emtTransactionTypeKey;
		this.emtName = emtName;
		this.emtSubject = emtSubject;
		this.emtBody = emtBody;
		this.emtAllowAttachments = emtAllowAttachments;
		this.emtIsActive = emtIsActive;
		this.emtUpdatedBy = emtUpdatedBy;
		this.emtDateUpdated = emtDateUpdated;
		this.emtDateCreated = emtDateCreated;
	}

	public CgtEmailTemplatesImpl(final int emtEmailTemplateKey, final int emtTransactionSubmissionTypeKey, final int emtTransactionTypeKey,
			final String emtName, final String emtTo, final String emtFrom, final String emtCc, final String emtOptionalCc, final String emtBcc,
			final String emtSubject, final String emtBody, final boolean emtAllowAttachments, final boolean emtIsActive, final String emtUpdatedBy,
			final LocalDateTime emtDateUpdated, final LocalDateTime emtDateCreated) {
		this.emtEmailTemplateKey = emtEmailTemplateKey;
		this.emtTransactionSubmissionTypeKey = emtTransactionSubmissionTypeKey;
		this.emtTransactionTypeKey = emtTransactionTypeKey;
		this.emtName = emtName;
		this.emtTo = emtTo;
		this.emtFrom = emtFrom;
		this.emtCc = emtCc;
		this.emtOptionalCc = emtOptionalCc;
		this.emtBcc = emtBcc;
		this.emtSubject = emtSubject;
		this.emtBody = emtBody;
		this.emtAllowAttachments = emtAllowAttachments;
		this.emtIsActive = emtIsActive;
		this.emtUpdatedBy = emtUpdatedBy;
		this.emtDateUpdated = emtDateUpdated;
		this.emtDateCreated = emtDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEmailTemplates#getEmtEmailTemplateKey()
	 */
	@Override
	@Id
	@Column(name = "emt_emailTemplateKey", unique = true, nullable = false)
	public int getEmtEmailTemplateKey() {
		return this.emtEmailTemplateKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEmailTemplates#setEmtEmailTemplateKey(int)
	 */
	@Override
	public void setEmtEmailTemplateKey(final int emtEmailTemplateKey) {
		this.emtEmailTemplateKey = emtEmailTemplateKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEmailTemplates#getEmtTransactionSubmissionTypeKey()
	 */
	@Override
	@Column(name = "emt_transactionSubmissionTypeKey", nullable = false)
	public int getEmtTransactionSubmissionTypeKey() {
		return this.emtTransactionSubmissionTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEmailTemplates#setEmtTransactionSubmissionTypeKey(int)
	 */
	@Override
	public void setEmtTransactionSubmissionTypeKey(final int emtTransactionSubmissionTypeKey) {
		this.emtTransactionSubmissionTypeKey = emtTransactionSubmissionTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEmailTemplates#getEmtTransactionTypeKey()
	 */
	@Override
	@Column(name = "emt_transactionTypeKey", nullable = false)
	public int getEmtTransactionTypeKey() {
		return this.emtTransactionTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEmailTemplates#setEmtTransactionTypeKey(int)
	 */
	@Override
	public void setEmtTransactionTypeKey(final int emtTransactionTypeKey) {
		this.emtTransactionTypeKey = emtTransactionTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEmailTemplates#getEmtName()
	 */
	@Override
	@Column(name = "emt_name", nullable = false)
	public String getEmtName() {
		return this.emtName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEmailTemplates#setEmtName(java.lang.String)
	 */
	@Override
	public void setEmtName(final String emtName) {
		this.emtName = emtName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEmailTemplates#getEmtTo()
	 */
	@Override
	@Column(name = "emt_to")
	public String getEmtTo() {
		return this.emtTo;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEmailTemplates#setEmtTo(java.lang.String)
	 */
	@Override
	public void setEmtTo(final String emtTo) {
		this.emtTo = emtTo;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEmailTemplates#getEmtFrom()
	 */
	@Override
	@Column(name = "emt_from")
	public String getEmtFrom() {
		return this.emtFrom;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEmailTemplates#setEmtFrom(java.lang.String)
	 */
	@Override
	public void setEmtFrom(final String emtFrom) {
		this.emtFrom = emtFrom;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEmailTemplates#getEmtCc()
	 */
	@Override
	@Column(name = "emt_cc")
	public String getEmtCc() {
		return this.emtCc;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEmailTemplates#setEmtCc(java.lang.String)
	 */
	@Override
	public void setEmtCc(final String emtCc) {
		this.emtCc = emtCc;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEmailTemplates#getEmtOptionalCc()
	 */
	@Override
	@Column(name = "emt_optionalCc")
	public String getEmtOptionalCc() {
		return this.emtOptionalCc;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEmailTemplates#setEmtOptionalCc(java.lang.String)
	 */
	@Override
	public void setEmtOptionalCc(final String emtOptionalCc) {
		this.emtOptionalCc = emtOptionalCc;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEmailTemplates#getEmtBcc()
	 */
	@Override
	@Column(name = "emt_bcc")
	public String getEmtBcc() {
		return this.emtBcc;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEmailTemplates#setEmtBcc(java.lang.String)
	 */
	@Override
	public void setEmtBcc(final String emtBcc) {
		this.emtBcc = emtBcc;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEmailTemplates#getEmtSubject()
	 */
	@Override
	@Column(name = "emt_subject", nullable = false)
	public String getEmtSubject() {
		return this.emtSubject;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEmailTemplates#setEmtSubject(java.lang.String)
	 */
	@Override
	public void setEmtSubject(final String emtSubject) {
		this.emtSubject = emtSubject;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEmailTemplates#getEmtBody()
	 */
	@Override
	@Column(name = "emt_body", nullable = false)
	public String getEmtBody() {
		return this.emtBody;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEmailTemplates#setEmtBody(java.lang.String)
	 */
	@Override
	public void setEmtBody(final String emtBody) {
		this.emtBody = emtBody;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEmailTemplates#isEmtAllowAttachments()
	 */
	@Override
	@Column(name = "emt_allowAttachments", nullable = false)
	public boolean isEmtAllowAttachments() {
		return this.emtAllowAttachments;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEmailTemplates#setEmtAllowAttachments(boolean)
	 */
	@Override
	public void setEmtAllowAttachments(final boolean emtAllowAttachments) {
		this.emtAllowAttachments = emtAllowAttachments;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEmailTemplates#isEmtIsActive()
	 */
	@Override
	@Column(name = "emt_isActive", nullable = false)
	public boolean isEmtIsActive() {
		return this.emtIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEmailTemplates#setEmtIsActive(boolean)
	 */
	@Override
	public void setEmtIsActive(final boolean emtIsActive) {
		this.emtIsActive = emtIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEmailTemplates#getEmtUpdatedBy()
	 */
	@Override
	@Column(name = "emt_updatedBy", nullable = false, length = 35, columnDefinition="char")
	public String getEmtUpdatedBy() {
		return this.emtUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEmailTemplates#setEmtUpdatedBy(java.lang.String)
	 */
	@Override
	public void setEmtUpdatedBy(final String emtUpdatedBy) {
		this.emtUpdatedBy = emtUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEmailTemplates#getEmtDateUpdated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "emt_dateUpdated", nullable = false, length = 23)
	public LocalDateTime getEmtDateUpdated() {
		return this.emtDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEmailTemplates#setEmtDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setEmtDateUpdated(final LocalDateTime emtDateUpdated) {
		this.emtDateUpdated = emtDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEmailTemplates#getEmtDateCreated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "emt_dateCreated", nullable = false, length = 23)
	public LocalDateTime getEmtDateCreated() {
		return this.emtDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEmailTemplates#setEmtDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setEmtDateCreated(final LocalDateTime emtDateCreated) {
		this.emtDateCreated = emtDateCreated;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + (emtAllowAttachments ? 1231 : 1237);
		result = prime * result + ((emtBcc == null) ? 0 : emtBcc.hashCode());
		result = prime * result + ((emtBody == null) ? 0 : emtBody.hashCode());
		result = prime * result + ((emtCc == null) ? 0 : emtCc.hashCode());
		result = prime * result + ((emtDateCreated == null) ? 0 : emtDateCreated.hashCode());
		result = prime * result + ((emtDateUpdated == null) ? 0 : emtDateUpdated.hashCode());
		result = prime * result + emtEmailTemplateKey;
		result = prime * result + ((emtFrom == null) ? 0 : emtFrom.hashCode());
		result = prime * result + (emtIsActive ? 1231 : 1237);
		result = prime * result + ((emtName == null) ? 0 : emtName.hashCode());
		result = prime * result + ((emtOptionalCc == null) ? 0 : emtOptionalCc.hashCode());
		result = prime * result + ((emtSubject == null) ? 0 : emtSubject.hashCode());
		result = prime * result + ((emtTo == null) ? 0 : emtTo.hashCode());
		result = prime * result + emtTransactionSubmissionTypeKey;
		result = prime * result + emtTransactionTypeKey;
		result = prime * result + ((emtUpdatedBy == null) ? 0 : emtUpdatedBy.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof CgtEmailTemplatesImpl)) {
			return false;
		}
		CgtEmailTemplatesImpl other = (CgtEmailTemplatesImpl) obj;
		if (emtAllowAttachments != other.emtAllowAttachments) {
			return false;
		}
		if (emtBcc == null) {
			if (other.emtBcc != null) {
				return false;
			}
		} else if (!emtBcc.equals(other.emtBcc)) {
			return false;
		}
		if (emtBody == null) {
			if (other.emtBody != null) {
				return false;
			}
		} else if (!emtBody.equals(other.emtBody)) {
			return false;
		}
		if (emtCc == null) {
			if (other.emtCc != null) {
				return false;
			}
		} else if (!emtCc.equals(other.emtCc)) {
			return false;
		}
		if (emtDateCreated == null) {
			if (other.emtDateCreated != null) {
				return false;
			}
		} else if (!emtDateCreated.equals(other.emtDateCreated)) {
			return false;
		}
		if (emtDateUpdated == null) {
			if (other.emtDateUpdated != null) {
				return false;
			}
		} else if (!emtDateUpdated.equals(other.emtDateUpdated)) {
			return false;
		}
		if (emtEmailTemplateKey != other.emtEmailTemplateKey) {
			return false;
		}
		if (emtFrom == null) {
			if (other.emtFrom != null) {
				return false;
			}
		} else if (!emtFrom.equals(other.emtFrom)) {
			return false;
		}
		if (emtIsActive != other.emtIsActive) {
			return false;
		}
		if (emtName == null) {
			if (other.emtName != null) {
				return false;
			}
		} else if (!emtName.equals(other.emtName)) {
			return false;
		}
		if (emtOptionalCc == null) {
			if (other.emtOptionalCc != null) {
				return false;
			}
		} else if (!emtOptionalCc.equals(other.emtOptionalCc)) {
			return false;
		}
		if (emtSubject == null) {
			if (other.emtSubject != null) {
				return false;
			}
		} else if (!emtSubject.equals(other.emtSubject)) {
			return false;
		}
		if (emtTo == null) {
			if (other.emtTo != null) {
				return false;
			}
		} else if (!emtTo.equals(other.emtTo)) {
			return false;
		}
		if (emtTransactionSubmissionTypeKey != other.emtTransactionSubmissionTypeKey) {
			return false;
		}
		if (emtTransactionTypeKey != other.emtTransactionTypeKey) {
			return false;
		}
		if (emtUpdatedBy == null) {
			if (other.emtUpdatedBy != null) {
				return false;
			}
		} else if (!emtUpdatedBy.equals(other.emtUpdatedBy)) {
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"CgtEmailTemplatesImpl [emtEmailTemplateKey=%s, emtTransactionSubmissionTypeKey=%s, emtTransactionTypeKey=%s, emtName=%s, emtTo=%s, emtFrom=%s, emtCc=%s, emtOptionalCc=%s, emtBcc=%s, emtSubject=%s, emtBody=%s, emtAllowAttachments=%s, emtIsActive=%s, emtUpdatedBy=%s, emtDateUpdated=%s, emtDateCreated=%s]",
				emtEmailTemplateKey, emtTransactionSubmissionTypeKey, emtTransactionTypeKey, emtName, emtTo, emtFrom,
				emtCc, emtOptionalCc, emtBcc, emtSubject, emtBody, emtAllowAttachments, emtIsActive, emtUpdatedBy,
				emtDateUpdated, emtDateCreated);
	}

	public CgtEmailTemplatesDTO toDTO() {
		return new CgtEmailTemplatesDTO(this);
	}
}
