package edu.ucdavis.orvc.integration.cgt.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtEfaTransactionProcessInfo;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtProject;
import edu.ucdavis.orvc.integration.cgt.api.domain.service.CgtProjectInfoService;
import edu.ucdavis.orvc.integration.cgt.data.dao.CgtProjectDao;
import edu.ucdavis.orvc.integration.cgt.data.dao.CgtTransactionDao;

@Service("cgtProjectInfoService")
@Transactional
public class CgtProjectInfoServiceImpl implements CgtProjectInfoService {
	
	@Autowired
	CgtProjectDao cgtProjectDao;
	
	@Autowired
	CgtTransactionDao cgtTransactionDao;

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.service.impl.CgtProjectInfoService#getEfaProcessInfo(java.lang.String)
	 */
	@Override
	public List<CgtEfaTransactionProcessInfo> getEfaProcessInfo(String projectNumber) {
		return cgtTransactionDao.findAllEfaProcessInfo(projectNumber);
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.service.impl.CgtProjectInfoService#getProject(java.lang.String)
	 */
	@Override
	public CgtProject getProject(String projectNumber) {
		return cgtProjectDao.findByProjectNumber(projectNumber);
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.service.impl.CgtProjectInfoService#getProject(java.lang.String, boolean)
	 */
	@Override
	public CgtProject getProject(String projectNumber, boolean active) {
		final CgtProject result;
		if (active) {
			result = cgtProjectDao.findActiveByProjectNumber(projectNumber);
		} else {
			result = cgtProjectDao.findByProjectNumber(projectNumber);
		}
		return result;
	}

}
