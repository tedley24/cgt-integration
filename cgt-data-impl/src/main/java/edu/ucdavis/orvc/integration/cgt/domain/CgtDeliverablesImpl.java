package edu.ucdavis.orvc.integration.cgt.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtDeliverables;
import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtDeliverablesDTO;

/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/
@Entity
@Table(name = "CGT_Deliverables")
public class CgtDeliverablesImpl implements CgtDeliverables {

	private static final long serialVersionUID = 3437989733442810336L;

	private int delDeliverableKey;
	private int delDeliverableTypeKey;
	private String delProjectKey;
	private int delDeliverableFormKey;
	private int delDaysDue;
	private LocalDateTime delDateReceived;
	private String delReceivedBy;
	private boolean delIsActive;
	private String delUpdatedBy;
	private LocalDateTime delDateUpdated;
	private LocalDateTime delDateCreated;

	public CgtDeliverablesImpl() {
	}

	public CgtDeliverablesImpl(final int delDeliverableKey, final int delDeliverableTypeKey, final String delProjectKey,
			final int delDeliverableFormKey, final int delDaysDue, final boolean delIsActive, final String delUpdatedBy, final LocalDateTime delDateUpdated,
			final LocalDateTime delDateCreated) {
		this.delDeliverableKey = delDeliverableKey;
		this.delDeliverableTypeKey = delDeliverableTypeKey;
		this.delProjectKey = delProjectKey;
		this.delDeliverableFormKey = delDeliverableFormKey;
		this.delDaysDue = delDaysDue;
		this.delIsActive = delIsActive;
		this.delUpdatedBy = delUpdatedBy;
		this.delDateUpdated = delDateUpdated;
		this.delDateCreated = delDateCreated;
	}

	public CgtDeliverablesImpl(final int delDeliverableKey, final int delDeliverableTypeKey, final String delProjectKey,
			final int delDeliverableFormKey, final int delDaysDue, final LocalDateTime delDateReceived, final String delReceivedBy, final boolean delIsActive,
			final String delUpdatedBy, final LocalDateTime delDateUpdated, final LocalDateTime delDateCreated) {
		this.delDeliverableKey = delDeliverableKey;
		this.delDeliverableTypeKey = delDeliverableTypeKey;
		this.delProjectKey = delProjectKey;
		this.delDeliverableFormKey = delDeliverableFormKey;
		this.delDaysDue = delDaysDue;
		this.delDateReceived = delDateReceived;
		this.delReceivedBy = delReceivedBy;
		this.delIsActive = delIsActive;
		this.delUpdatedBy = delUpdatedBy;
		this.delDateUpdated = delDateUpdated;
		this.delDateCreated = delDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDeliverables#getDelDeliverableKey()
	 */
	@Override
	@Id
	@Column(name = "del_deliverableKey", unique = true, nullable = false)
	public int getDelDeliverableKey() {
		return this.delDeliverableKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDeliverables#setDelDeliverableKey(int)
	 */
	@Override
	public void setDelDeliverableKey(final int delDeliverableKey) {
		this.delDeliverableKey = delDeliverableKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDeliverables#getDelDeliverableTypeKey()
	 */
	@Override
	@Column(name = "del_deliverableTypeKey", nullable = false)
	public int getDelDeliverableTypeKey() {
		return this.delDeliverableTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDeliverables#setDelDeliverableTypeKey(int)
	 */
	@Override
	public void setDelDeliverableTypeKey(final int delDeliverableTypeKey) {
		this.delDeliverableTypeKey = delDeliverableTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDeliverables#getDelProjectKey()
	 */
	@Override
	@Column(name = "del_projectKey", nullable = false, length = 10)
	public String getDelProjectKey() {
		return this.delProjectKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDeliverables#setDelProjectKey(java.lang.String)
	 */
	@Override
	public void setDelProjectKey(final String delProjectKey) {
		this.delProjectKey = delProjectKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDeliverables#getDelDeliverableFormKey()
	 */
	@Override
	@Column(name = "del_deliverableFormKey", nullable = false)
	public int getDelDeliverableFormKey() {
		return this.delDeliverableFormKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDeliverables#setDelDeliverableFormKey(int)
	 */
	@Override
	public void setDelDeliverableFormKey(final int delDeliverableFormKey) {
		this.delDeliverableFormKey = delDeliverableFormKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDeliverables#getDelDaysDue()
	 */
	@Override
	@Column(name = "del_daysDue", nullable = false)
	public int getDelDaysDue() {
		return this.delDaysDue;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDeliverables#setDelDaysDue(int)
	 */
	@Override
	public void setDelDaysDue(final int delDaysDue) {
		this.delDaysDue = delDaysDue;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDeliverables#getDelDateReceived()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "del_dateReceived", length = 23)
	public LocalDateTime getDelDateReceived() {
		return this.delDateReceived;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDeliverables#setDelDateReceived(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setDelDateReceived(final LocalDateTime delDateReceived) {
		this.delDateReceived = delDateReceived;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDeliverables#getDelReceivedBy()
	 */
	@Override
	@Column(name = "del_receivedBy", length = 35, columnDefinition="char")
	public String getDelReceivedBy() {
		return this.delReceivedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDeliverables#setDelReceivedBy(java.lang.String)
	 */
	@Override
	public void setDelReceivedBy(final String delReceivedBy) {
		this.delReceivedBy = delReceivedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDeliverables#isDelIsActive()
	 */
	@Override
	@Column(name = "del_isActive", nullable = false)
	public boolean isDelIsActive() {
		return this.delIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDeliverables#setDelIsActive(boolean)
	 */
	@Override
	public void setDelIsActive(final boolean delIsActive) {
		this.delIsActive = delIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDeliverables#getDelUpdatedBy()
	 */
	@Override
	@Column(name = "del_updatedBy", nullable = false, length = 35, columnDefinition="char")
	public String getDelUpdatedBy() {
		return this.delUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDeliverables#setDelUpdatedBy(java.lang.String)
	 */
	@Override
	public void setDelUpdatedBy(final String delUpdatedBy) {
		this.delUpdatedBy = delUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDeliverables#getDelDateUpdated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "del_dateUpdated", nullable = false, length = 23)
	public LocalDateTime getDelDateUpdated() {
		return this.delDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDeliverables#setDelDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setDelDateUpdated(final LocalDateTime delDateUpdated) {
		this.delDateUpdated = delDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDeliverables#getDelDateCreated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "del_dateCreated", nullable = false, length = 23)
	public LocalDateTime getDelDateCreated() {
		return this.delDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDeliverables#setDelDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setDelDateCreated(final LocalDateTime delDateCreated) {
		this.delDateCreated = delDateCreated;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((delDateCreated == null) ? 0 : delDateCreated.hashCode());
		result = prime * result + ((delDateReceived == null) ? 0 : delDateReceived.hashCode());
		result = prime * result + ((delDateUpdated == null) ? 0 : delDateUpdated.hashCode());
		result = prime * result + delDaysDue;
		result = prime * result + delDeliverableFormKey;
		result = prime * result + delDeliverableKey;
		result = prime * result + delDeliverableTypeKey;
		result = prime * result + (delIsActive ? 1231 : 1237);
		result = prime * result + ((delProjectKey == null) ? 0 : delProjectKey.hashCode());
		result = prime * result + ((delReceivedBy == null) ? 0 : delReceivedBy.hashCode());
		result = prime * result + ((delUpdatedBy == null) ? 0 : delUpdatedBy.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof CgtDeliverablesImpl))
			return false;
		final CgtDeliverablesImpl other = (CgtDeliverablesImpl) obj;
		if (delDateCreated == null) {
			if (other.delDateCreated != null)
				return false;
		} else if (!delDateCreated.equals(other.delDateCreated))
			return false;
		if (delDateReceived == null) {
			if (other.delDateReceived != null)
				return false;
		} else if (!delDateReceived.equals(other.delDateReceived))
			return false;
		if (delDateUpdated == null) {
			if (other.delDateUpdated != null)
				return false;
		} else if (!delDateUpdated.equals(other.delDateUpdated))
			return false;
		if (delDaysDue != other.delDaysDue)
			return false;
		if (delDeliverableFormKey != other.delDeliverableFormKey)
			return false;
		if (delDeliverableKey != other.delDeliverableKey)
			return false;
		if (delDeliverableTypeKey != other.delDeliverableTypeKey)
			return false;
		if (delIsActive != other.delIsActive)
			return false;
		if (delProjectKey == null) {
			if (other.delProjectKey != null)
				return false;
		} else if (!delProjectKey.equals(other.delProjectKey))
			return false;
		if (delReceivedBy == null) {
			if (other.delReceivedBy != null)
				return false;
		} else if (!delReceivedBy.equals(other.delReceivedBy))
			return false;
		if (delUpdatedBy == null) {
			if (other.delUpdatedBy != null)
				return false;
		} else if (!delUpdatedBy.equals(other.delUpdatedBy))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"CgtDeliverables [delDeliverableKey=%s, delDeliverableTypeKey=%s, delProjectKey=%s, delDeliverableFormKey=%s, delDaysDue=%s, delDateReceived=%s, delReceivedBy=%s, delIsActive=%s, delUpdatedBy=%s, delDateUpdated=%s, delDateCreated=%s]",
				delDeliverableKey, delDeliverableTypeKey, delProjectKey, delDeliverableFormKey, delDaysDue,
				delDateReceived, delReceivedBy, delIsActive, delUpdatedBy, delDateUpdated, delDateCreated);
	}

	public CgtDeliverablesDTO toDTO() {
		return new CgtDeliverablesDTO(this);
	}
}
