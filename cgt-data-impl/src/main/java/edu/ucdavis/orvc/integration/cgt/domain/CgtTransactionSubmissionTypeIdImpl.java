package edu.ucdavis.orvc.integration.cgt.domain;


import javax.persistence.Column;
import javax.persistence.Embeddable;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtTransactionSubmissionTypeId;
import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtTransactionSubmissionTypeIdDTO;

/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/
@Embeddable
public class CgtTransactionSubmissionTypeIdImpl implements CgtTransactionSubmissionTypeId {

	private static final long serialVersionUID = -4603963289153616800L;
	private int tstTransactionSubmissionTypeKey;
	private int tstTransactionTypeKey;

	public CgtTransactionSubmissionTypeIdImpl() {
	}

	public CgtTransactionSubmissionTypeIdImpl(final int tstTransactionSubmissionTypeKey, final int tstTransactionTypeKey) {
		this.tstTransactionSubmissionTypeKey = tstTransactionSubmissionTypeKey;
		this.tstTransactionTypeKey = tstTransactionTypeKey;
	}

	public CgtTransactionSubmissionTypeIdImpl(CgtTransactionSubmissionTypeId fromObj) {
		this.tstTransactionSubmissionTypeKey = fromObj.getTstTransactionSubmissionTypeKey();
		this.tstTransactionTypeKey = fromObj.getTstTransactionTypeKey();
	}

	@Override
	public CgtTransactionSubmissionTypeIdDTO toDTO() {
		return new CgtTransactionSubmissionTypeIdDTO(this);
	}
	
	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionSubmissionTypeId#getTstTransactionSubmissionTypeKey()
	 */
	@Override
	@Column(name = "tst_transactionSubmissionTypeKey", nullable = false)
	public int getTstTransactionSubmissionTypeKey() {
		return this.tstTransactionSubmissionTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionSubmissionTypeId#setTstTransactionSubmissionTypeKey(int)
	 */
	@Override
	public void setTstTransactionSubmissionTypeKey(final int tstTransactionSubmissionTypeKey) {
		this.tstTransactionSubmissionTypeKey = tstTransactionSubmissionTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionSubmissionTypeId#getTstTransactionTypeKey()
	 */
	@Override
	@Column(name = "tst_transactionTypeKey", nullable = false)
	public int getTstTransactionTypeKey() {
		return this.tstTransactionTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionSubmissionTypeId#setTstTransactionTypeKey(int)
	 */
	@Override
	public void setTstTransactionTypeKey(final int tstTransactionTypeKey) {
		this.tstTransactionTypeKey = tstTransactionTypeKey;
	}

	@Override
	public boolean equals(final Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof CgtTransactionSubmissionTypeIdImpl))
			return false;
		final CgtTransactionSubmissionTypeId castOther = (CgtTransactionSubmissionTypeId) other;

		return (this.getTstTransactionSubmissionTypeKey() == castOther.getTstTransactionSubmissionTypeKey())
				&& (this.getTstTransactionTypeKey() == castOther.getTstTransactionTypeKey());
	}

	@Override
	public int hashCode() {
		int result = 17;

		result = 37 * result + this.getTstTransactionSubmissionTypeKey();
		result = 37 * result + this.getTstTransactionTypeKey();
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"CgtTransactionSubmissionTypesId [tstTransactionSubmissionTypeKey=%s, tstTransactionTypeKey=%s]",
				tstTransactionSubmissionTypeKey, tstTransactionTypeKey);
	}

}
