package edu.ucdavis.orvc.integration.cgt.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtSubcontractTypes;
import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtSubcontractTypesDTO;



/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/
@Entity
@Table(name = "CGT_SubcontractTypes")
public class CgtSubcontractTypesImpl implements CgtSubcontractTypes {

	private static final long serialVersionUID = -2428635033431219728L;
	private int sctSubcontractTypeKey;
	private String sctName;
	private String sctShortName;
	private Integer sctSortOrder;
	private boolean sctIsActive;
	private String sctUpdatedBy;
	private LocalDateTime sctDateCreated;
	private LocalDateTime sctDateUpdated;

	public CgtSubcontractTypesImpl() {
	}

	public CgtSubcontractTypesImpl(final int sctSubcontractTypeKey, final String sctName, final boolean sctIsActive, final String sctUpdatedBy,
			final LocalDateTime sctDateCreated, final LocalDateTime sctDateUpdated) {
		this.sctSubcontractTypeKey = sctSubcontractTypeKey;
		this.sctName = sctName;
		this.sctIsActive = sctIsActive;
		this.sctUpdatedBy = sctUpdatedBy;
		this.sctDateCreated = sctDateCreated;
		this.sctDateUpdated = sctDateUpdated;
	}

	public CgtSubcontractTypesImpl(final int sctSubcontractTypeKey, final String sctName, final String sctShortName, final Integer sctSortOrder,
			final boolean sctIsActive, final String sctUpdatedBy, final LocalDateTime sctDateCreated, final LocalDateTime sctDateUpdated) {
		this.sctSubcontractTypeKey = sctSubcontractTypeKey;
		this.sctName = sctName;
		this.sctShortName = sctShortName;
		this.sctSortOrder = sctSortOrder;
		this.sctIsActive = sctIsActive;
		this.sctUpdatedBy = sctUpdatedBy;
		this.sctDateCreated = sctDateCreated;
		this.sctDateUpdated = sctDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubcontractTypes#getSctSubcontractTypeKey()
	 */
	@Override
	@Id
	@Column(name = "sct_subcontractTypeKey", unique = true, nullable = false)
	public int getSctSubcontractTypeKey() {
		return this.sctSubcontractTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubcontractTypes#setSctSubcontractTypeKey(int)
	 */
	@Override
	public void setSctSubcontractTypeKey(final int sctSubcontractTypeKey) {
		this.sctSubcontractTypeKey = sctSubcontractTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubcontractTypes#getSctName()
	 */
	@Override
	@Column(name = "sct_name", nullable = false)
	public String getSctName() {
		return this.sctName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubcontractTypes#setSctName(java.lang.String)
	 */
	@Override
	public void setSctName(final String sctName) {
		this.sctName = sctName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubcontractTypes#getSctShortName()
	 */
	@Override
	@Column(name = "sct_shortName")
	public String getSctShortName() {
		return this.sctShortName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubcontractTypes#setSctShortName(java.lang.String)
	 */
	@Override
	public void setSctShortName(final String sctShortName) {
		this.sctShortName = sctShortName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubcontractTypes#getSctSortOrder()
	 */
	@Override
	@Column(name = "sct_sortOrder")
	public Integer getSctSortOrder() {
		return this.sctSortOrder;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubcontractTypes#setSctSortOrder(java.lang.Integer)
	 */
	@Override
	public void setSctSortOrder(final Integer sctSortOrder) {
		this.sctSortOrder = sctSortOrder;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubcontractTypes#isSctIsActive()
	 */
	@Override
	@Column(name = "sct_isActive", nullable = false)
	public boolean isSctIsActive() {
		return this.sctIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubcontractTypes#setSctIsActive(boolean)
	 */
	@Override
	public void setSctIsActive(final boolean sctIsActive) {
		this.sctIsActive = sctIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubcontractTypes#getSctUpdatedBy()
	 */
	@Override
	@Column(name = "sct_updatedBy", nullable = false, length = 35, columnDefinition="char")
	public String getSctUpdatedBy() {
		return this.sctUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubcontractTypes#setSctUpdatedBy(java.lang.String)
	 */
	@Override
	public void setSctUpdatedBy(final String sctUpdatedBy) {
		this.sctUpdatedBy = sctUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubcontractTypes#getSctDateCreated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "sct_dateCreated", nullable = false, length = 23)
	public LocalDateTime getSctDateCreated() {
		return this.sctDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubcontractTypes#setSctDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setSctDateCreated(final LocalDateTime sctDateCreated) {
		this.sctDateCreated = sctDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubcontractTypes#getSctDateUpdated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "sct_dateUpdated", nullable = false, length = 23)
	public LocalDateTime getSctDateUpdated() {
		return this.sctDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubcontractTypes#setSctDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setSctDateUpdated(final LocalDateTime sctDateUpdated) {
		this.sctDateUpdated = sctDateUpdated;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((sctDateCreated == null) ? 0 : sctDateCreated.hashCode());
		result = prime * result + ((sctDateUpdated == null) ? 0 : sctDateUpdated.hashCode());
		result = prime * result + (sctIsActive ? 1231 : 1237);
		result = prime * result + ((sctName == null) ? 0 : sctName.hashCode());
		result = prime * result + ((sctShortName == null) ? 0 : sctShortName.hashCode());
		result = prime * result + ((sctSortOrder == null) ? 0 : sctSortOrder.hashCode());
		result = prime * result + sctSubcontractTypeKey;
		result = prime * result + ((sctUpdatedBy == null) ? 0 : sctUpdatedBy.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof CgtSubcontractTypesImpl))
			return false;
		final CgtSubcontractTypesImpl other = (CgtSubcontractTypesImpl) obj;
		if (sctDateCreated == null) {
			if (other.sctDateCreated != null)
				return false;
		} else if (!sctDateCreated.equals(other.sctDateCreated))
			return false;
		if (sctDateUpdated == null) {
			if (other.sctDateUpdated != null)
				return false;
		} else if (!sctDateUpdated.equals(other.sctDateUpdated))
			return false;
		if (sctIsActive != other.sctIsActive)
			return false;
		if (sctName == null) {
			if (other.sctName != null)
				return false;
		} else if (!sctName.equals(other.sctName))
			return false;
		if (sctShortName == null) {
			if (other.sctShortName != null)
				return false;
		} else if (!sctShortName.equals(other.sctShortName))
			return false;
		if (sctSortOrder == null) {
			if (other.sctSortOrder != null)
				return false;
		} else if (!sctSortOrder.equals(other.sctSortOrder))
			return false;
		if (sctSubcontractTypeKey != other.sctSubcontractTypeKey)
			return false;
		if (sctUpdatedBy == null) {
			if (other.sctUpdatedBy != null)
				return false;
		} else if (!sctUpdatedBy.equals(other.sctUpdatedBy))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"CgtSubcontractTypes [sctSubcontractTypeKey=%s, sctName=%s, sctShortName=%s, sctSortOrder=%s, sctIsActive=%s, sctUpdatedBy=%s, sctDateCreated=%s, sctDateUpdated=%s]",
				sctSubcontractTypeKey, sctName, sctShortName, sctSortOrder, sctIsActive, sctUpdatedBy, sctDateCreated,
				sctDateUpdated);
	}



	public CgtSubcontractTypesDTO toDTO() {
		return new CgtSubcontractTypesDTO(this);
	}

}
