package edu.ucdavis.orvc.integration.cgt.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtQaaAnswers;
import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtQaaAnswersDTO;

/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/
@Entity
@Table(name = "CGT_QaaAnswers", uniqueConstraints = @UniqueConstraint(columnNames = {
		"ans_questionKey", "ans_recordKey" }) )
public class CgtQaaAnswersImpl implements CgtQaaAnswers {

	private static final long serialVersionUID = -7856255714628381944L;
	private int ansAnswerKey;
	private int ansQuestionKey;
	private String ansRecordKey;
	private String ansAnswer;
	private String ansAdditionalAnswer;
	private boolean ansIsActive;
	private LocalDateTime ansDateCreated;
	private LocalDateTime ansDateUpdated;
	private String ansUpdatedBy;

	public CgtQaaAnswersImpl() {
	}

	public CgtQaaAnswersImpl(final int ansAnswerKey, final int ansQuestionKey, final String ansRecordKey, final String ansAnswer,
			final boolean ansIsActive, final LocalDateTime ansDateCreated, final LocalDateTime ansDateUpdated, final String ansUpdatedBy) {
		this.ansAnswerKey = ansAnswerKey;
		this.ansQuestionKey = ansQuestionKey;
		this.ansRecordKey = ansRecordKey;
		this.ansAnswer = ansAnswer;
		this.ansIsActive = ansIsActive;
		this.ansDateCreated = ansDateCreated;
		this.ansDateUpdated = ansDateUpdated;
		this.ansUpdatedBy = ansUpdatedBy;
	}

	public CgtQaaAnswersImpl(final int ansAnswerKey, final int ansQuestionKey, final String ansRecordKey, final String ansAnswer,
			final String ansAdditionalAnswer, final boolean ansIsActive, final LocalDateTime ansDateCreated, final LocalDateTime ansDateUpdated,
			final String ansUpdatedBy) {
		this.ansAnswerKey = ansAnswerKey;
		this.ansQuestionKey = ansQuestionKey;
		this.ansRecordKey = ansRecordKey;
		this.ansAnswer = ansAnswer;
		this.ansAdditionalAnswer = ansAdditionalAnswer;
		this.ansIsActive = ansIsActive;
		this.ansDateCreated = ansDateCreated;
		this.ansDateUpdated = ansDateUpdated;
		this.ansUpdatedBy = ansUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaAnswers#getAnsAnswerKey()
	 */
	@Override
	@Id
	@Column(name = "ans_answerKey", unique = true, nullable = false)
	public int getAnsAnswerKey() {
		return this.ansAnswerKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaAnswers#setAnsAnswerKey(int)
	 */
	@Override
	public void setAnsAnswerKey(final int ansAnswerKey) {
		this.ansAnswerKey = ansAnswerKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaAnswers#getAnsQuestionKey()
	 */
	@Override
	@Column(name = "ans_questionKey", nullable = false)
	public int getAnsQuestionKey() {
		return this.ansQuestionKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaAnswers#setAnsQuestionKey(int)
	 */
	@Override
	public void setAnsQuestionKey(final int ansQuestionKey) {
		this.ansQuestionKey = ansQuestionKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaAnswers#getAnsRecordKey()
	 */
	@Override
	@Column(name = "ans_recordKey", nullable = false, length = 50)
	public String getAnsRecordKey() {
		return this.ansRecordKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaAnswers#setAnsRecordKey(java.lang.String)
	 */
	@Override
	public void setAnsRecordKey(final String ansRecordKey) {
		this.ansRecordKey = ansRecordKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaAnswers#getAnsAnswer()
	 */
	@Override
	@Column(name = "ans_answer", nullable = false, length = 50)
	public String getAnsAnswer() {
		return this.ansAnswer;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaAnswers#setAnsAnswer(java.lang.String)
	 */
	@Override
	public void setAnsAnswer(final String ansAnswer) {
		this.ansAnswer = ansAnswer;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaAnswers#getAnsAdditionalAnswer()
	 */
	@Override
	@Column(name = "ans_additionalAnswer", length = 50)
	public String getAnsAdditionalAnswer() {
		return this.ansAdditionalAnswer;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaAnswers#setAnsAdditionalAnswer(java.lang.String)
	 */
	@Override
	public void setAnsAdditionalAnswer(final String ansAdditionalAnswer) {
		this.ansAdditionalAnswer = ansAdditionalAnswer;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaAnswers#isAnsIsActive()
	 */
	@Override
	@Column(name = "ans_isActive", nullable = false)
	public boolean isAnsIsActive() {
		return this.ansIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaAnswers#setAnsIsActive(boolean)
	 */
	@Override
	public void setAnsIsActive(final boolean ansIsActive) {
		this.ansIsActive = ansIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaAnswers#getAnsDateCreated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "ans_dateCreated", nullable = false, length = 23)
	public LocalDateTime getAnsDateCreated() {
		return this.ansDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaAnswers#setAnsDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setAnsDateCreated(final LocalDateTime ansDateCreated) {
		this.ansDateCreated = ansDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaAnswers#getAnsDateUpdated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "ans_dateUpdated", nullable = false, length = 23)
	public LocalDateTime getAnsDateUpdated() {
		return this.ansDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaAnswers#setAnsDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setAnsDateUpdated(final LocalDateTime ansDateUpdated) {
		this.ansDateUpdated = ansDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaAnswers#getAnsUpdatedBy()
	 */
	@Override
	@Column(name = "ans_updatedBy", nullable = false, length = 35, columnDefinition="char")
	public String getAnsUpdatedBy() {
		return this.ansUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtQaaAnswers#setAnsUpdatedBy(java.lang.String)
	 */
	@Override
	public void setAnsUpdatedBy(final String ansUpdatedBy) {
		this.ansUpdatedBy = ansUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((ansAdditionalAnswer == null) ? 0 : ansAdditionalAnswer.hashCode());
		result = prime * result + ((ansAnswer == null) ? 0 : ansAnswer.hashCode());
		result = prime * result + ansAnswerKey;
		result = prime * result + ((ansDateCreated == null) ? 0 : ansDateCreated.hashCode());
		result = prime * result + ((ansDateUpdated == null) ? 0 : ansDateUpdated.hashCode());
		result = prime * result + (ansIsActive ? 1231 : 1237);
		result = prime * result + ansQuestionKey;
		result = prime * result + ((ansRecordKey == null) ? 0 : ansRecordKey.hashCode());
		result = prime * result + ((ansUpdatedBy == null) ? 0 : ansUpdatedBy.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof CgtQaaAnswersImpl)) {
			return false;
		}
		CgtQaaAnswersImpl other = (CgtQaaAnswersImpl) obj;
		if (ansAdditionalAnswer == null) {
			if (other.ansAdditionalAnswer != null) {
				return false;
			}
		} else if (!ansAdditionalAnswer.equals(other.ansAdditionalAnswer)) {
			return false;
		}
		if (ansAnswer == null) {
			if (other.ansAnswer != null) {
				return false;
			}
		} else if (!ansAnswer.equals(other.ansAnswer)) {
			return false;
		}
		if (ansAnswerKey != other.ansAnswerKey) {
			return false;
		}
		if (ansDateCreated == null) {
			if (other.ansDateCreated != null) {
				return false;
			}
		} else if (!ansDateCreated.equals(other.ansDateCreated)) {
			return false;
		}
		if (ansDateUpdated == null) {
			if (other.ansDateUpdated != null) {
				return false;
			}
		} else if (!ansDateUpdated.equals(other.ansDateUpdated)) {
			return false;
		}
		if (ansIsActive != other.ansIsActive) {
			return false;
		}
		if (ansQuestionKey != other.ansQuestionKey) {
			return false;
		}
		if (ansRecordKey == null) {
			if (other.ansRecordKey != null) {
				return false;
			}
		} else if (!ansRecordKey.equals(other.ansRecordKey)) {
			return false;
		}
		if (ansUpdatedBy == null) {
			if (other.ansUpdatedBy != null) {
				return false;
			}
		} else if (!ansUpdatedBy.equals(other.ansUpdatedBy)) {
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"CgtQaaAnswersImpl [ansAnswerKey=%s, ansQuestionKey=%s, ansRecordKey=%s, ansAnswer=%s, ansAdditionalAnswer=%s, ansIsActive=%s, ansDateCreated=%s, ansDateUpdated=%s, ansUpdatedBy=%s]",
				ansAnswerKey, ansQuestionKey, ansRecordKey, ansAnswer, ansAdditionalAnswer, ansIsActive, ansDateCreated,
				ansDateUpdated, ansUpdatedBy);
	}



	public CgtQaaAnswersDTO toDTO() {
		return new CgtQaaAnswersDTO(this);
	}

}
